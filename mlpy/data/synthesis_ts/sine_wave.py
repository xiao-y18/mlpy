import numpy as np
from six.moves import xrange
import matplotlib.pyplot as plt


def sine_wave(seq_length=30, num_samples=28*5*100, num_signals=1, freq_low=1,
              freq_high=5, amplitude_low=0.1, amplitude_high=0.9, **kwargs):
    ix = np.arange(seq_length) + 1
    samples = []
    for i in xrange(num_samples):
        signals = []
        for ii in xrange(num_signals):
            freq = np.random.uniform(low=freq_high, high=freq_low)  # frequency
            A = np.random.uniform(low=amplitude_low, high=amplitude_high)  # amplitude
            offset = np.random.uniform(low=-np.pi, high=np.pi)
            signals.append(A * np.sin(2 * np.pi * freq * ix / float(seq_length) + offset))
        samples.append(np.array(signals).T)
    # the shape of the samples is num_sample x seq_length x num_signals
    samples = np.array(samples)
    return samples


def sample_plot(out_path='sine_wave.png'):
    nsample = 6
    samples = sine_wave(num_samples=nsample)
    f, axes = plt.subplots(nsample//2, 2)
    axes = axes.flat[:]

    plt.suptitle('samples of sine wave')
    for i, ax in enumerate(axes):
        ax.plot(samples[i])

    plt.savefig(out_path)
    plt.close(f)

if __name__ == '__main__':
    sample_plot()


