import numpy as np
import matplotlib.pyplot as plt
import os
import shutil

from six.moves import xrange


def load_cmapss(dataset):
    """ 
     - Turbofan Engine Degradation Simulation Data Set, running to failure. 
     - Link: https://ti.arc.nasa.gov/tech/dash/groups/pcoe/prognostic-data-repository/
     - There are several constant variables, which are cols = c(5,6,10,11,15,21,23,24).
     - V1 is the unit number, V2 represent the time-stamp in current experiment unit.
     - There are 26 parameters, the variable no. 3,4,5 are the operational setting, 
       the variable from no. 6 to 26 are the sensor measurement. 

    :return: 
    """

    filename = '/tmp/dataset_fl/CMAPSSData/{}'.format(dataset)

    data = np.genfromtxt(filename, delimiter=' ', dtype='float')

    ret = []
    min_seq_len = np.inf
    for i in xrange(int(max(data[:, 0]))):
        unit = data[data[:, 0] == i + 1, :]
        if unit.shape[0] < min_seq_len:
            min_seq_len = unit.shape[0]
        ret.append(unit)

    ret = np.array(ret)
    return ret, min_seq_len


def load_campss_col(dataset, v_id):
    data_all, min_seq_len = load_cmapss(dataset)
    print(data_all.shape, min_seq_len)
    data = []
    for i in xrange(data_all.shape[0]):
        data.append(data_all[i][:min_seq_len, v_id])

    data = np.array(data)

    return data


def plot_samples(samples):
    nrows = samples.shape[0] // 2
    f, axes = plt.subplots(nrows, 2)
    axes = axes.flat[:]
    for i, ax in enumerate(axes):
        ax.plot(samples[i])

    return f


def plot_data(infile):
    data, min_seq_len = load_cmapss(infile)
    out_root = 'plot/{}'.format(os.path.split(infile)[-1].split('.')[-2])
    if os.path.exists(out_root):
        shutil.rmtree(out_root)
    os.makedirs(out_root)
    for ncol in range(data[0].shape[1]):
        col_data = load_campss_col(infile, ncol)
        filename = 'samples-{}_length-{}_no-col-{}.png'\
            .format(col_data.shape[0], col_data.shape[1], ncol)
        nsamples = min(col_data.shape[0], 4)
        f = plot_samples(col_data[:nsamples])
        f.suptitle(filename.split('.')[-2])
        plt.savefig(os.path.join(out_root, filename))
        plt.close(f)

if __name__ == '__main__':
    plot_data('train_FD004.txt')
