"""
    https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix/tree/master/datasets/download_cyclegan_dataset.sh
    https://github.com/tensorflow/docs/blob/master/site/en/tutorials/generative/cyclegan.ipynb
"""

import os
import tensorflow as tf

AUTOTUNE = tf.data.experimental.AUTOTUNE

BUFFER_SIZE = 1000
IMG_WIDTH = 256
IMG_HEIGHT = 256


def load_image(image_file):
    image = tf.io.read_file(image_file)
    image = tf.image.decode_jpeg(image)
    image = tf.cast(image, tf.float32)
    return image


def random_crop(image):
    cropped_image = tf.image.random_crop(
        image, size=[IMG_HEIGHT, IMG_WIDTH, 3])

    return cropped_image


# normalizing the images to [-1, 1]
def normalize(image):
    image = tf.cast(image, tf.float32)
    image = (image / 127.5) - 1
    return image


def random_jitter(image):
    # resizing to 286 x 286 x 3
    image = tf.image.resize(image, [286, 286], method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)

    # randomly cropping to 256 x 256 x 3
    image = random_crop(image)

    # random mirroring
    image = tf.image.random_flip_left_right(image)

    return image


def load_image_train_horses(image_file):
    image = load_image(image_file)
    image = random_jitter(image)
    image = normalize(image)
    return image


def load_image_train_zebras(image_file):
    # Note: random_crop() can not be applied. otherwise, it will raise an error:
    # [Need value.shape >= size, got ] [286 286 1] [256 256 3].
    image = load_image(image_file)
    image = normalize(image)
    return image


def load_image_test(image_file):
    image = load_image(image_file)
    image = normalize(image)
    return image


def load_data(batch_size, cache_dir, cache_subdir='image'):
    origin_url = "https://people.eecs.berkeley.edu/~taesung_park/CycleGAN/datasets/horse2zebra.zip"
    path_to_zip = tf.keras.utils.get_file('horse2zebra.zip',
                                          origin=origin_url,
                                          extract=True, cache_subdir=cache_subdir, cache_dir=cache_dir)
    path = os.path.join(os.path.dirname(path_to_zip), 'horse2zebra/')

    train_horses = tf.data.Dataset.list_files(os.path.join(path, 'trainA/*.jpg'))
    train_zebras = tf.data.Dataset.list_files(os.path.join(path, 'trainB/*.jpg'))
    test_horses = tf.data.Dataset.list_files(os.path.join(path, 'testA/*.jpg'))
    test_zebras = tf.data.Dataset.list_files(os.path.join(path, 'testB/*.jpg'))

    train_horses = train_horses.map(
        load_image_train_horses, num_parallel_calls=AUTOTUNE).shuffle(
        BUFFER_SIZE).batch(batch_size)

    train_zebras = train_zebras.map(
        load_image_train_zebras, num_parallel_calls=AUTOTUNE).shuffle(
        BUFFER_SIZE).batch(batch_size)

    test_horses = test_horses.map(
        load_image_test, num_parallel_calls=AUTOTUNE).shuffle(
        BUFFER_SIZE).batch(batch_size)

    test_zebras = test_zebras.map(
        load_image_test, num_parallel_calls=AUTOTUNE).shuffle(
        BUFFER_SIZE).batch(batch_size)

    return train_horses, train_zebras, test_horses, test_zebras, path




