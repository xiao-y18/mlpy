"""
    reference: https://www.tensorflow.org/tutorials/generative/pix2pix
"""

import os
import tensorflow as tf

AUTOTUNE = tf.data.experimental.AUTOTUNE

IMG_WIDTH = 256
IMG_HEIGHT = 256

"""
    operations for image processing: 
        1. Resize an image to bigger height and width;
        2. Randomly crop to the target size;
        3. Randomly flip the image horizontally.
"""


def resize_image(input_image, real_image, height, width):
    input_image = tf.image.resize(input_image, [height, width],
                                  method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)
    real_image = tf.image.resize(real_image, [height, width],
                                 method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)

    return input_image, real_image


def random_crop_image(input_image, real_image):
    stacked_image = tf.stack([input_image, real_image], axis=0)
    cropped_image = tf.image.random_crop(
        stacked_image, size=[2, IMG_HEIGHT, IMG_WIDTH, 3])

    return cropped_image[0], cropped_image[1]


def normalize_image(input_image, real_image):
    """
        normalizing the images to [-1, 1]
    """
    input_image = (input_image / 127.5) - 1
    real_image = (real_image / 127.5) - 1

    return input_image, real_image


@tf.function()
def random_jitter_image(input_image, real_image):
    # resizing to 286 x 286 x 3
    input_image, real_image = resize_image(input_image, real_image, 286, 286)

    # randomly cropping to 256 x 256 x 3
    input_image, real_image = random_crop_image(input_image, real_image)

    if tf.random.uniform(()) > 0.5:
        # random mirroring
        input_image = tf.image.flip_left_right(input_image)
        real_image = tf.image.flip_left_right(real_image)

    return input_image, real_image


"""
    load data
"""


def load_image(image_file):
    image = tf.io.read_file(image_file)
    image = tf.image.decode_jpeg(image)

    w = tf.shape(image)[1]

    w = w // 2
    real_image = image[:, :w, :]
    input_image = image[:, w:, :]

    input_image = tf.cast(input_image, tf.float32)
    real_image = tf.cast(real_image, tf.float32)

    return input_image, real_image


def load_image_train(image_file):
    input_image, real_image = load_image(image_file)
    input_image, real_image = random_jitter_image(input_image, real_image)
    input_image, real_image = normalize_image(input_image, real_image)

    return input_image, real_image


def load_image_test(image_file):
    input_image, real_image = load_image(image_file)
    input_image, real_image = resize_image(input_image, real_image, IMG_HEIGHT, IMG_WIDTH)
    input_image, real_image = normalize_image(input_image, real_image)

    return input_image, real_image


def load_data(batch_size, cache_dir, buffer_size=400, cache_subdir='image'):
    origin_url = 'https://people.eecs.berkeley.edu/~tinghuiz/projects/pix2pix/datasets/facades.tar.gz'
    path_to_zip = tf.keras.utils.get_file('facades.tar.gz',
                                          origin=origin_url,
                                          extract=True, cache_subdir=cache_subdir, cache_dir=cache_dir)
    path = os.path.join(os.path.dirname(path_to_zip), 'facades/')

    train_dataset = tf.data.Dataset.list_files(path+'train/*.jpg')
    train_dataset = train_dataset.map(load_image_train, num_parallel_calls=AUTOTUNE)

    train_dataset = train_dataset.shuffle(buffer_size)
    train_dataset = train_dataset.batch(batch_size)

    test_dataset = tf.data.Dataset.list_files(path+'test/*.jpg')
    test_dataset = test_dataset.map(load_image_test, num_parallel_calls=AUTOTUNE)
    test_dataset = test_dataset.batch(batch_size)

    return train_dataset, test_dataset, path
