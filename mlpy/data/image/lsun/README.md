Initialization: I download the code from [lsun official website](http://www.yf.io/p/lsun). The was realized by python2. I adapt it to my python3.6 environment. Details are as follows. 

The [original README file](README.origin.md).

Some changes of `download.py`: 
Following [reference](https://www.jianshu.com/p/70ec0f807fb8), I convert `urllib2` in python2 to `urllib.request`. Further, this [reference](https://stackoverflow.com/questions/37042152/python-3-5-1-urllib-has-no-attribute-request) explained the reason is "With packages, like this, you sometimes need to explicitly import the piece you want. That way, the urllib module doesn't have to load everything up just because you wanted one small part."