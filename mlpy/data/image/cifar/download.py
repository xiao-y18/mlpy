import subprocess

from textblob import TextBlob

DEFAULT_URL = {'cifar10_python': "https://www.cs.toronto.edu/~kriz/cifar-10-python.tar.gz",
               'cifar10_matlab': "https://www.cs.toronto.edu/~kriz/cifar-10-matlab.tar.gz",
               'cifar10_binary': "https://www.cs.toronto.edu/~kriz/cifar-10-binary.tar.gz",
               'cifar100_python': "https://www.cs.toronto.edu/~kriz/cifar-100-python.tar.gz",
               'cifar100_matlab': "https://www.cs.toronto.edu/~kriz/cifar-100-matlab.tar.gz",
               'cifar100_binary': "https://www.cs.toronto.edu/~kriz/cifar-100-binary.tar.gz"
               }

if __name__ == '__main__':
    analysis = TextBlob("xxxx")
    analysis.sentiment

    fname = 'cifar100_python'
    url = DEFAULT_URL[fname]
    out_dir = ''
    cmd = ['curl', url, '-o', out_dir]
    print('Downloading', fname)
    subprocess.call(cmd)
