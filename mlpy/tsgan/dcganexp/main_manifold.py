"""
    Learning a low-dimensional embedding.
"""

from mlpy.lib.utils.base import tag_path
from mlpy.data import ucr
from mlpy.tsgan.dcganexp import base
from mlpy.tsgan.dcganexp.main_encoder import encode

import numpy as np
import os
import pandas as pd
from sklearn.decomposition import PCA
from sklearn.manifold import MDS
from sklearn.manifold import TSNE
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

SEED = 86
np.random.seed(SEED)


def run_manifold(x_tr, y_tr, x_te, y_te, dir_out, mode='mds'):
    print("****** Processing {}".format(mode))

    n_components = 2
    if mode == 'pca':
        model = PCA(n_components)
    elif mode == 'mds':
        model = MDS(n_components)
    elif mode == 'tsne':
        model = TSNE(n_components)
    else:
        raise ValueError("The mode = {} can not be found!".format(mode))

    manifold_tr = model.fit_transform(x_tr)
    plt.scatter(manifold_tr[:, 0], manifold_tr[:, 1], c=y_tr)
    plt.savefig(os.path.join(dir_out, 'tr_{}.png'.format(mode)))
    plt.clf()

    manifold_te = model.fit_transform(x_te)
    plt.scatter(manifold_te[:, 0], manifold_te[:, 1], c=y_te)
    plt.savefig(os.path.join(dir_out, 'te_{}.png'.format(mode)))
    plt.clf()

    x = np.vstack([x_tr, x_te])
    y = np.hstack([y_tr, y_te])
    manifold = model.fit_transform(x)
    plt.scatter(manifold[:, 0], manifold[:, 1], c=y)
    plt.savefig(os.path.join(dir_out, 'all_{}.png'.format(mode)))
    plt.clf()

    np.savetxt(os.path.join(dir_out, 'tr{}'.format(mode)), manifold_tr, delimiter=',')
    np.savetxt(os.path.join(dir_out, 'te{}'.format(mode)), manifold_te, delimiter=',')

def run_tsgan(data_name, dir_data, dir_out):
    path_out = os.path.join(dir_out, data_name, 'tsgan')
    if os.path.exists(path_out) is False:
        os.makedirs(path_out)

    feature_type = 'local-max'
    norm_type = 'znorm'
    # dir_gan = 'cache/tsgan/bn.tf.keras_batch16_kernel10/run0'
    dir_gan = 'cache/tsgan/bn.tf.keras_batch16_kernel10_adapt75/run0'
    features_tr, y_tr, features_te, y_te, n_classes = encode(
        data_name, dir_data, feature_type, norm_type, dir_gan)

    np.savetxt(os.path.join(path_out, 'trx'), features_tr, delimiter=',')
    np.savetxt(os.path.join(path_out, 'try'), y_tr, delimiter=',')
    np.savetxt(os.path.join(path_out, 'tex'), features_te, delimiter=',')
    np.savetxt(os.path.join(path_out, 'tey'), y_te, delimiter=',')

    run_manifold(features_tr, y_tr, features_te, y_te, path_out, mode='pca')
    run_manifold(features_tr, y_tr, features_te, y_te, path_out, mode='mds')
    run_manifold(features_tr, y_tr, features_te, y_te, path_out, mode='tsne')

def run_raw(data_name, dir_data, dir_out):
    path_out = os.path.join(dir_out, data_name, 'raw')
    if os.path.exists(path_out) is False:
        os.makedirs(path_out)

    x_tr, y_tr, x_te, y_te, n_classes = ucr.load_ucr_flat(data_name, dir_data)

    np.savetxt(os.path.join(path_out, 'try'), y_tr, delimiter=',')
    np.savetxt(os.path.join(path_out, 'tey'), y_te, delimiter=',')

    run_manifold(x_tr, y_tr, x_te, y_te, path_out, mode='pca')
    run_manifold(x_tr, y_tr, x_te, y_te, path_out, mode='mds')
    run_manifold(x_tr, y_tr, x_te, y_te, path_out, mode='tsne')


if __name__ == '__main__':
    dir_data = base.UCR_DIR
    tag = tag_path(os.path.abspath(__file__), 1)
    dir_out = 'cache/{}'.format(tag)
    if os.path.exists(dir_out) is False:
        os.makedirs(dir_out)

    # data_name_list = ['FordA', 'Two_Patterns', 'Trace', 'synthetic_control']
    data_name_list = ucr.get_data_name_list(dir_data)

    for data_name in data_name_list:
        run_tsgan(data_name, dir_data, dir_out)

    # for data_name in data_name_list:
    #     run_raw(data_name, dir_data, dir_out)


