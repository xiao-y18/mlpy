from mlpy.lib.utils.base import tag_path
from mlpy.tsgan.dcganexp import base
from mlpy.data import ucr
from mlpy.data import ts
from mlpy.tsgan.dcganexp.main_encoder import encode

import pandas as pd
import os
import warnings

# def make_dir(path):
#     if os.path.exists(path):
#         raise ValueError("The path has existed!")
#     else:
#         os.makedirs(path)

def make_dir(path):
    if os.path.exists(path) is False:
        os.makedirs(path)

"""
    Standard Baselines on raw data
"""
def raw_data(dir_data, data_name_list, dir_out_root):
    dir_out = os.path.join(dir_out_root, 'raw')
    n_runs = 3
    for ir in range(n_runs):
        dir_out_cur = os.path.join(dir_out, 'run{}'.format(ir))
        make_dir(dir_out_cur)
        for model_name, model in base.StandardClassifierDic.items():
            print("******** processing model {}".format(model_name))
            res = {'dataset': [], 'acc': [], 'acc_te': [], 'time': [], 'time_te': []}
            for data_name in data_name_list:
                x_tr, y_tr, x_te, y_te, n_classes = ucr.load_ucr_flat(data_name, dir_data)
                acc, t = base.classify(model, x_tr, y_tr, x_te, y_te)
                res['dataset'].append(data_name)
                res['acc'].append(acc[0])
                res['acc_te'].append(acc[1])
                res['time'].append(t[0])
                res['time_te'].append(t[1])
                print(model_name, data_name, acc, t)
            df = pd.DataFrame(res)
            df.to_csv(os.path.join(dir_out_cur, '{}.csv'.format(model_name)), index=False)

"""
    Evaluate TSGAN feature.
"""
DIR_GAN_LIST = ['cache/tsgan/bn.tf.keras_batch16_kernel10/run{}'.format(i) for i in range(3)]
# DIR_GAN_LIST = ['cache/tsgan/bn.tf.keras_batch16_kernel10/run2']

def run(model, feature_type, norm_type, data_name_list, dir_gan, dir_data):
    res = {'dataset': [], 'acc': [], 'acc_te': [], 'time': [], 'time_te': []}
    n_datasets = len(data_name_list)
    for j, data_name in enumerate(data_name_list):
        print("******** [{}/{}] processing {}".format(j, n_datasets, data_name))
        ## load data
        features_tr, y_tr, features_te, y_te, n_classes = encode(
            data_name, dir_data, feature_type, norm_type, dir_gan)
        acc, t = base.classify(model, features_tr, y_tr, features_te, y_te)
        res['dataset'].append(data_name)
        res['acc'].append(acc[0])
        res['acc_te'].append(acc[1])
        res['time'].append(t[0])
        res['time_te'].append(t[1])
        print(acc, t)
    ## save result
    df = pd.DataFrame(res)
    return df

def cmp_norms(norm_list, dir_data, data_name_list, dir_out_root):
    feature_type = 'local-max'
    dir_out = os.path.join(dir_out_root, 'cmp_norms')
    for dir_gan in DIR_GAN_LIST:
        idir = int(dir_gan[-1])
        for model_name, model in base.StandardClassifierDicV2.items():
            dir_out_cur = os.path.join(dir_out, 'run{}'.format(idir), model_name)
            make_dir(dir_out_cur)
            for norm_type in norm_list:
                df = run(model, feature_type, norm_type, data_name_list, dir_gan, dir_data)
                df.to_csv(os.path.join(dir_out_cur, '{}.csv'.format(norm_type)), index=False)


def cmp_features(feature_type_list, dir_data, data_name_list, dir_out_root):
    norm_type = 'tanh'
    model_name = 'LR'
    model = base.StandardClassifierDic[model_name]
    dir_out = os.path.join(dir_out_root, 'cmp_features')
    for idir, dir_gan in enumerate(DIR_GAN_LIST):
        dir_out_cur = os.path.join(dir_out, 'run{}'.format(idir))
        make_dir(dir_out_cur)
        for feature_type in feature_type_list:
            df = run(model, feature_type, norm_type, data_name_list, dir_gan, dir_data)
            df.to_csv(os.path.join(dir_out_cur, '{}.csv'.format(feature_type)), index=False)


if __name__ == '__main__':
    dir_data = base.UCR_DIR
    tag = tag_path(os.path.abspath(__file__), 1)
    dir_out = 'cache/{}'.format(tag)

    data_name_list = ucr.get_data_name_list(dir_data)
    # data_name_list = ['ArrowHead', 'ECGFiveDays']  # for test
    # data_name_list = ['ArrowHead']

    raw_data(dir_data, data_name_list, dir_out)
    cmp_norms(ts.TS_NORMALIZE_TYPES, dir_data, data_name_list, dir_out)
    # cmp_features(Feature_Types_Layers+Feature_Types, dir_data, data_name_list, dir_out)
