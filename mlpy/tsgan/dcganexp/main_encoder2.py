"""
    Extract local sub-sequence, instead of points, from filters.
"""

from mlpy.lib.utils.base import tag_path
from mlpy.data import ucr
from mlpy.data import ts
from mlpy.tsgan.lib.ops import *
from mlpy.tsgan.dcgan.model import TSGAN
from mlpy.tsgan.dcgan.config import Config
from mlpy.tsgan.dcganexp import base

import numpy as np
import os
import pandas as pd
import tensorflow as tf
tf_conf = tf.ConfigProto()
tf_conf.gpu_options.allow_growth = True

Feature_Types = ['peak', 'peak-abs', 'max', 'max-keep-last', 'max-abs']
class EncoderGAN:
    def __init__(self, gan, input_shape, is_training=False, type=None):
        self.gan = gan
        self.input_shape = input_shape
        self.is_training = is_training
        self.type = type

        self.x = tf.placeholder(tf.float32, shape=[None] + self.input_shape, name='x')
        self.layers = self.gan.discriminator_layers(self.x, self.is_training)
        self.output = self.build()

    def build(self):
        h0, h1, h2, h3 = self.layers

        def reshape(h):
            ret = tf.reshape(
                h, [-1, h.get_shape()[1].value, h.get_shape()[2].value * h.get_shape()[3].value])
            ret = tf.transpose(ret, perm=(0, 2, 1))
            return ret

        h1_values = reshape(h1) # [sample, filter, length]
        h2_values = reshape(h2)
        h3_values = reshape(h3)
        return [h1_values, h2_values, h3_values]

    def encode(self, sess, X, batch_size, norm=None):
        values_layerwise = self.run_on_batch(sess, X, batch_size)
        features = self.embedding(values_layerwise, norm)
        return features

    def run_on_batch(self, sess, X, batch_size):
        ## extract values
        res_on_batch = [[] for _ in range(len(self.output))]
        n_samples = len(X)
        n_batches = n_samples // batch_size
        for i in range(n_batches):
            x_batch = X[i*batch_size:(i+1)*batch_size]
            output_batch = sess.run(self.output, feed_dict={self.x: x_batch})
            for r, o in zip(res_on_batch, output_batch):
                r.append(o)
        n_samples_left = n_samples - n_batches*batch_size
        if n_samples_left > 0:
            x_left = X[-n_samples_left:]
            output_left = sess.run(self.output, feed_dict={self.x: x_left})
            for r, o in zip(res_on_batch, output_left):
                r.append(o)
        res_layerwise = [np.vstack(out) for out in res_on_batch] # [[sample, filter, length],..]
        return res_layerwise

    def embedding(self, values, norm=None):
        if self.type == 'peak':
            return self.embedding_peak(values, norm)
        if self.type == 'peak-abs':
            values_abs = [abs(v) for v in values]
            return self.embedding_peak(values_abs, norm)
        elif self.type == 'max':
            return self.embedding_max(values, norm)
        elif self.type == 'max-keep-last':
            return self.embedding_max(values, norm, keep_last=True)
        elif self.type == 'max-abs': # results are same as max
            values_abs = [abs(v) for v in values]
            return self.embedding_max(values_abs, norm)
        else:
            raise ValueError('The type of features, {}, no found!'.format(self.type))

    def embedding_peak(self, values, norm=None):
        from scipy.signal import find_peaks
        n_layers = len(values)
        n_samples = values[0].shape[0]
        wins = [5, 5, 9]
        n_peaks_selected = [3, 3, 2]

        ## find peaks
        peaks = []
        peaks_distr = [] # (max, medium)
        n_empty_peaks = 0
        for l in range(n_layers):
            n_peaks_layer = []
            peaks_layer = []
            win = wins[l]
            for s in range(n_samples):
                n_filters = values[l][s].shape[0]
                peaks_sample = []
                for f in range(n_filters):
                    series = values[l][s][f]
                    pks, _ = find_peaks(series, distance=win)
                    peaks_sample.append(pks)
                    n_peaks_layer.append(len(pks))
                    if len(pks) == 0:
                        n_empty_peaks += 1
                peaks_layer.append(peaks_sample)
            peaks.append(peaks_layer)
            peaks_distr.append((
                np.max(n_peaks_layer), np.mean(n_peaks_layer),
                np.median(n_peaks_layer), np.std(n_peaks_layer)))
        # print("Finish to find peaks")
        # print("peaks_distr: ", peaks_distr)
        # print("n_empty_peaks: ", n_empty_peaks)

        ## construct features, there are the peak segments in each filters
        features = []
        for s in range(n_samples): # for each sample
            pk_series_sample = []
            for l in range(n_layers): # for each layer
                n_filters = values[l][s].shape[0]
                n_pks_selected = n_peaks_selected[l]
                win = wins[l]
                length = len(values[l][s][0])
                if length < n_pks_selected * win: # series too short
                    pk_series_layer = np.hstack(values[l][s])
                else:
                    pk_series_layer = []
                    for f in range(n_filters): # for each filter
                        series = values[l][s][f]
                        pks = peaks[l][s][f]
                        n_pks = len(pks)
                        pk_series = []
                        if n_pks == 0:  # no peaks
                            n_pad = n_pks_selected * win
                            left = length // 2 - n_pad // 2
                            pk_series = series[(left):(left + n_pad)]
                        else:
                            if n_pks > n_pks_selected:  # cutting the redundant peaks
                                ## keep the left. not good.
                                # pks = pks[:n_pks_real]
                                ## keep the middle. better than the left.
                                # n_cut = n_pks - n_pks_real
                                # n_cut_left = n_cut // 2
                                # n_cut_right = n_cut_left + n_pks_real
                                # pks = pks[n_cut_left:n_cut_right]
                                # pks = pks[:n_pks_real]
                                ## keep the tops
                                pk_ids_top = np.argsort(series[pks])[::-1] # descending
                                pk_ids_top = pk_ids_top[:n_pks_selected]
                                pks = np.sort(pks[pk_ids_top])
                            for p in pks:  # for each peaks
                                span = win // 2
                                left = p - span
                                right = p + span + 1
                                if left < 0:
                                    right += abs(left)
                                    left = 0
                                elif right > length:
                                    left -= (right - length)
                                    right = length
                                pk_series.extend(series[left:right])
                            if n_pks < n_pks_selected:  # padding for insufficient peaks
                                n_pad = (n_pks_selected - n_pks) * win
                                n_pad_left = n_pad // 2
                                n_pad_right = n_pad - n_pad_left
                                pk_series = [pk_series[0]]*n_pad_left \
                                            + pk_series + [pk_series[-1]]*n_pad_right
                        pk_series_layer.extend(pk_series)
                pk_series_sample.extend(pk_series_layer)
            features.append(pk_series_sample)
        features = np.array(features)

        ## normalization
        if norm is not None:
            features = self.normalize(features, norm)
        return features

    def embedding_max(self, values, norm=None, keep_last=False):
        maxids = [np.argmax(v, axis=-1) for v in values]
        n_layers = len(values)
        n_samples = values[0].shape[0]
        wins = [3, 5, 8]

        if keep_last:
            n_layers -= 1
        features = []
        for s in range(n_samples):
            max_series_sample = []
            # print("processing a sample: ")
            for l in range(n_layers):
                # for l in [2]:
                win = wins[l]
                idl = maxids[l][s]
                n_filters = values[l][s].shape[0]
                length = values[l][s].shape[1]
                max_series_layer = []
                if length < win * 2:
                    max_series_sample.extend(values[l][s].ravel())
                    continue
                for f in range(n_filters):
                    series = values[l][s][f]
                    i_center = idl[f]
                    i_low = i_center - win
                    i_up = i_center + win
                    if i_low < 0:
                        i_up += abs(i_low)
                        i_low = 0
                    elif i_up > length:
                        i_low -= (i_up - length)
                        i_up = length
                    max_series_layer.extend(series[i_low:i_up])
                max_series_sample.extend(max_series_layer)
            features.append(max_series_sample)
        if keep_last: # add all the values in last layer
            for s in range(n_samples):
                features[s].extend(values[-1][s].ravel())
        features = np.array(features)
        if norm is not None:
            features = self.normalize(features, norm)
        return features

    def normalize(self, features, norm):
        return ts.normalize(features, norm)


def encode(data_name, dir_data, feature_type, norm_type, dir_gan):
    tf.reset_default_graph()
    ## load data
    x_tr, y_tr, x_te, y_te, n_classes = ucr.load_ucr_flat(data_name, dir_data)
    x_tr = np.reshape(x_tr, x_tr.shape + (1, 1))
    x_te = np.reshape(x_te, x_te.shape + (1, 1))
    ## set up GAN
    dir_checkpoint = os.path.join(dir_gan, data_name, 'checkpoint')
    conf = Config(x_tr, '', '', x_tr.shape[1], x_tr.shape[2], x_tr.shape[3], state='test')
    gan = TSGAN(conf.dim_z, conf.dim_h, conf.dim_w, conf.dim_c, conf.random_seed,
                conf.g_lr, conf.d_lr, conf.g_beta1, conf.d_beta1, conf.gf_dim, conf.df_dim)
    ## start to run
    with tf.Session(config=tf_conf) as sess:
        isload, counter = gan.load(sess, dir_checkpoint)
        if not isload:
            raise Exception("[!] Train a model first, then run test mode")
        input_shape = [x_tr.shape[1], x_tr.shape[2], x_tr.shape[3]]
        encoder = EncoderGAN(gan, input_shape, type=feature_type)
        features_tr = encoder.encode(sess, x_tr, conf.batch_size, norm=norm_type)
        features_te = encoder.encode(sess, x_te, conf.batch_size, norm=norm_type)
        return features_tr, y_tr, features_te, y_te, n_classes


def run_classifier(model, model_name, feature_type, norm_type, data_name_list, dir_gan, dir_data, dir_out):
    print("******** processing {}, {}".format(model_name, feature_type, norm_type))
    res = {'dataset': [], 'acc': [], 'acc_te': [], 'time': [], 'time_te': []}
    n_datasets = len(data_name_list)
    for j, data_name in enumerate(data_name_list):
        print("******** [{}/{}] processing {}".format(j, n_datasets, data_name))
        ## load data
        features_tr, y_tr, features_te, y_te, n_classes = encode(
            data_name, dir_data, feature_type, norm_type, dir_gan)
        acc, t = base.classify(model, features_tr, y_tr, features_te, y_te)
        res['dataset'].append(data_name)
        res['acc'].append(acc[0])
        res['acc_te'].append(acc[1])
        res['time'].append(t[0])
        res['time_te'].append(t[1])
        print(acc, t)
    ## save result
    df = pd.DataFrame(res)
    df.to_csv(
        os.path.join(dir_out, '{}_{}_{}.csv'.format(model_name, feature_type, norm_type)),
        index=False)
    return df

def run_standard_classifier(
        model_name, feature_type, norm_type, data_name_list, dir_gan, dir_data, dir_out):
    model = base.StandardClassifierDic[model_name]
    return run_classifier(model, model_name, feature_type, norm_type, data_name_list, dir_gan, dir_data, dir_out)


def cal_length_of_features(data_name_list, feature_type_list, dir_gan, dir_data, dir_out):
    print("********** start cal_length_of_features: ")
    ## initialize result
    res = {'dataset':[], 'ts':[]}
    for feature_type in feature_type_list:
        res[feature_type] = []
    ## start to run
    n_datasets = len(data_name_list)
    for i, data_name in enumerate(data_name_list):
        tf.reset_default_graph()
        print("[{}/{}] ***** {}".format(i, n_datasets, data_name))
        # load data and
        x_tr, y_tr, _, _, _ = ucr.load_ucr_flat(data_name, dir_data)
        x_tr = np.reshape(x_tr, x_tr.shape + (1, 1))
        dir_checkpoint = os.path.join(dir_gan, data_name, 'checkpoint')
        conf = Config(x_tr, '', '', x_tr.shape[1], x_tr.shape[2], x_tr.shape[3], state='test')
        gan = TSGAN(conf.dim_z, conf.dim_h, conf.dim_w, conf.dim_c, conf.random_seed,
                    conf.g_lr, conf.d_lr, conf.g_beta1, conf.d_beta1, conf.gf_dim, conf.df_dim)
        # set up session and extract features
        sess = tf.Session(config=tf_conf)
        isload, counter = gan.load(sess, dir_checkpoint)
        if not isload:
            raise Exception("[!] Train a model first, then run test mode")
        res['dataset'].append(data_name)
        res['ts'].append(x_tr.shape[1])
        for feature_type in feature_type_list:
            input_shape = [x_tr.shape[1], x_tr.shape[2], x_tr.shape[3]]
            encoder = EncoderGAN(gan, input_shape, type=feature_type)
            features_tr = encoder.encode(sess, x_tr, conf.batch_size)
            res[feature_type].append(features_tr.shape[1])
        sess.close()
    ## save results
    df = pd.DataFrame(res)
    df.to_csv(os.path.join(dir_out, 'length_of_features.csv'), index=False)


if __name__ == '__main__':
    dir_data = base.UCR_DIR
    dir_gan = 'cache/tsgan/bn.tf.keras_batch16_kernel10/run0'
    tag = tag_path(os.path.abspath(__file__), 1)
    dir_out = 'cache/{}/'.format(tag)
    if os.path.exists(dir_out) is False:
        os.makedirs(dir_out)

    # data_name_list = ucr.get_data_name_list(dir_data)
    data_name_list = ['ArrowHead'] # for test

    run_standard_classifier('LR', 'max', 'tanh', data_name_list, dir_gan, dir_data, dir_out)

    cal_length_of_features(data_name_list, Feature_Types, dir_gan, dir_data, dir_out)


