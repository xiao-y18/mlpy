"""
    Core idea:
        - Task: anomaly detection, which is a binary classification problem with a specialty that the number of normal
            samples is far less than the number of abnormal samples.
        - Approach:
            1. train gan with the normal samples.
            2. encode all samples, including both normal and abnormal samples, into representation vectors by the
                trained gan's encoder.
            3. anomaly detection, there are two strategies:
                a. calculate the distance from each testing sample to training samples (i.e. normal samples). the test
                    sample is categorized into anomaly if its distance exceed the given threshold that is selected based
                    on the distances between normal samples in training set.
                b. based on the score derived by the trained gan.
    Performance: not good.
"""

# import matplotlib
# matplotlib.use('Agg')
import os
import sklearn
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from IPython import embed

from sklearn import svm
from sklearn.decomposition import PCA

from mlpy.data import ucr
from mlpy.data import utils, ts
from mlpy.lib.utils import tag_path
from mlpy.tsgan.dcganexp import base
from mlpy.tsgan.dcgan import train
from mlpy.tsgan.dcgan.config import Config
from mlpy.tsgan.dcganexp.main_encoder_aaai19 import encode


def train_gan(data_name, dir_data, tag):
    """train gan with mode='half'
    """
    x_tr, y_tr, _, _, _ = ucr.load_ucr_flat(data_name, dir_data)
    distr = utils.distribute_dataset(x_tr, y_tr)
    x = distr[0] # train for class 0
    x = np.reshape(x, x.shape + (1, 1))
    conf = Config(x, data_name, tag, x.shape[1], x.shape[2], x.shape[3], state='train')
    train.train(conf)


def classify(data_name, dir_data, dir_gan):
    feature_type = 'local-max'
    norm_type = 'tanh'
    features_tr, y_tr, features_te, y_te, n_classes = encode(
        data_name, dir_data, feature_type, norm_type, dir_gan)
    # base classification model.
    model_name = 'LR'
    model = base.StandardClassifierDic[model_name]
    acc, t = base.classify(model, features_tr, y_tr, features_te, y_te)
    print(acc)


def cal_distance(data_name, dir_data, dir_gan, dir_out):
    feature_type = 'local-max'
    # norm_type = 'tanh'
    for norm_type in ts.TS_NORMALIZE_TYPES:
        dir_out_cur = os.path.join(dir_out, norm_type)
        if os.path.exists(dir_out_cur) is False:
            os.makedirs(dir_out_cur)
        features_tr, y_tr, features_te, y_te, n_classes = encode(
            data_name, dir_data, feature_type, norm_type, dir_gan)
        distr_tr = utils.distribute_dataset(features_tr, y_tr)
        feat_tr_0 = distr_tr[0]
        # train
        distances_tr = []
        for i, fei in enumerate(feat_tr_0):
            nnd = np.inf
            for j, fej in enumerate(feat_tr_0):
                if i == j:
                    continue
                ed = np.sum((fei - fej) ** 2)
                nnd = min(nnd, ed)
            distances_tr.append(nnd)
        distances_tr = pd.DataFrame({'dist': distances_tr})
        distances_tr.to_csv(os.path.join(dir_out_cur, 'distance_tr.csv'), index=False)
        # test
        distances_te = []
        for fe in features_te:
            nnd = np.inf
            for ft in feat_tr_0:
                ed = np.sum((fe - ft) ** 2)
                nnd = min(nnd, ed)
            distances_te.append(nnd)
        distances_te = np.array(distances_te)
        df = pd.DataFrame(
            {'dist': distances_te, 'y': y_te})
        df.to_csv(os.path.join(dir_out_cur, 'distance_te.csv'), index=False)

# def novelty_detection(data_name, dir_data, dir_dist, dir_out):
#     subd = 'tanh'
#     df_tr = pd.read_csv(os.path.join(dir_dist, subd, 'distance_tr.csv'))
#     disttr = df_tr['dist']
#
#     df_te = pd.read_csv(os.path.join(dir_dist, subd, 'distance_te.csv'))
#     dist0 = df_te['dist'][df_te['y'] == 0]
#     dist1 = df_te['dist'][df_te['y'] == 1]
#
#     clf = svm.OneClassSVM()
#     clf.fit(disttr.values.reshape((disttr.shape[0], 1)))
#     y_perd_0 = clf.predict(dist0.values.reshape((dist0.shape[0], 1)))
#     y_pred_1 = clf.predict(dist1.values.reshape((dist1.shape[0], 1)))
#
#     tp = y_perd_0[y_perd_0==1].size
#     fn = y_perd_0.shape[0] - tp
#     tn = y_pred_1[y_pred_1==-1].size
#     fp = y_pred_1.shape[0] - tn
#
#     recall = tn/(tn+fp)
#     precision = tn/(tn+fn)
#     acc = (tp+tn) / (tp+fn+tn+fp)
#     print('recall: {}'.format(recall), 'precision: {}'.format(precision), 'acc:{}'.format(acc))
#
#     embed()

def novelty_detection(data_name, dir_data, dir_gan, dir_out):
    feature_type = 'local-max'
    norm_type = 'tanh'
    features_tr, y_tr, features_te, y_te, n_classes = encode(
        data_name, dir_data, feature_type, norm_type, dir_gan)
    pca = PCA(n_components=3, whiten=False)
    pca.fit(features_tr)
    features_tr = pca.transform(features_tr)
    features_te = pca.transform(features_te)

    distr_tr = utils.distribute_dataset(features_tr, y_tr)
    feat_tr_0 = distr_tr[0]
    distr_te = utils.distribute_dataset(features_te, y_te)
    feat_te_0 = distr_te[0]
    feat_te_1 = distr_te[1]

    clf = svm.OneClassSVM(gamma=0.5)
    clf.fit(feat_tr_0)
    y_perd_0 = clf.predict(feat_te_0)
    y_pred_1 = clf.predict(feat_te_1)

    tp = y_perd_0[y_perd_0==1].size
    fn = y_perd_0.shape[0] - tp
    tn = y_pred_1[y_pred_1==-1].size
    fp = y_pred_1.shape[0] - tn

    recall = tn/(tn+fp)
    precision = tn/(tn+fn)
    acc = (tp+tn) / (tp+fn+tn+fp)
    f1 = (2*precision*recall) / (precision + recall)
    print('recall: {}'.format(recall), 'precision: {}'.format(precision),
          'acc:{}'.format(acc), 'f1:{}'.format(f1))

def eda(data_name, dir_data, dir_dist, dir_out):
    subdirs = [d for d in os.listdir(dir_dist) if os.path.isdir(os.path.join(dir_dist, d))]
    for subd in subdirs:
        df_tr = pd.read_csv(os.path.join(dir_dist, subd, 'distance_tr.csv'))
        disttr = df_tr['dist']

        df_te = pd.read_csv(os.path.join(dir_dist, subd, 'distance_te.csv'))
        dist0 = df_te['dist'][df_te['y'] == 0]
        dist1 = df_te['dist'][df_te['y'] == 1]

        describe = pd.DataFrame({
            'tr': disttr.describe(),
            'te0': dist0.describe(),
            'te1': dist1.describe()})
        describe.to_csv(os.path.join(dir_out, '{}_describe.csv'.format(subd)))

        xmin = min(min(disttr), min(dist0), min(dist1))
        xmax = max(max(disttr), max(dist0), max(dist1))
        bins = 20
        plt.clf()
        plt.hist(disttr, bins=bins)
        plt.xlim(xmin, xmax)
        plt.savefig(os.path.join(dir_out, '{}_tr.png'.format(subd)))
        plt.clf()
        plt.hist(dist0, bins=bins)
        plt.xlim(xmin, xmax)
        plt.savefig(os.path.join(dir_out, '{}_te0.png'.format(subd)))
        plt.clf()
        plt.hist(dist1, bins=bins)
        plt.xlim(xmin, xmax)
        plt.savefig(os.path.join(dir_out, '{}_te1.png'.format(subd)))
        plt.clf()
        plt.hist(disttr, bins=bins, label='tr')
        plt.hist(dist0, bins=bins, label='te0')
        plt.legend(loc='best')
        plt.xlim(xmin, xmax)
        plt.savefig(os.path.join(dir_out, '{}_tr_te0.png'.format(subd)))
        plt.clf()
        plt.hist(disttr, bins=bins, label='tr')
        plt.hist(dist1, bins=bins, label='te1')
        plt.legend(loc='best')
        plt.xlim(xmin, xmax)
        plt.savefig(os.path.join(dir_out, '{}_tr_te1.png'.format(subd)))
        plt.clf()
        plt.hist(dist0, bins=bins, label='te0')
        plt.hist(dist1, bins=bins, label='te1')
        plt.legend(loc='best')
        plt.xlim(xmin, xmax)
        plt.savefig(os.path.join(dir_out, '{}_te0_te1.png'.format(subd)))



if __name__ == '__main__':
    dir_data = base.UCR_DIR
    tag = '{}'.format(tag_path(os.path.abspath(__file__), 1))
    dir_out = 'cache/{}'.format(tag)
    data_name = 'FordA'

    ## train gan
    dir_gan = os.path.join(dir_out, 'tsgan')
    # train_gan(data_name, dir_data, tag)

    ## classification
    # classify(data_name, dir_data, dir_gan)

    ## distance based
    dir_dist = os.path.join(dir_out, 'distance')
    # cal_distance(data_name, dir_data, dir_gan, dir_dist)

    ## EDA
    # dir_eda = os.path.join(dir_out, 'eda')
    # if os.path.exists(dir_eda) is False:
    #     os.makedirs(dir_eda)
    # eda(data_name, dir_data, dir_dist, dir_eda)

    ## novelty detection
    dir_ad = os.path.join(dir_out, 'ad')
    # novelty_detection(data_name, dir_data, dir_dist, dir_ad)
    novelty_detection(data_name, dir_data, dir_gan, dir_ad)













