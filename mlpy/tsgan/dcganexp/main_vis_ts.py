"""
    Visualize presentation.
"""

from mlpy.lib.utils.base import tag_path
from mlpy.data import ucr, utils
from mlpy.tsgan.dcganexp import base

import json
import numpy as np
import os
import matplotlib
matplotlib.use('Agg')
matplotlib.rc('xtick', labelsize=15)
matplotlib.rc('ytick', labelsize=15)
import matplotlib.pyplot as plt


if __name__ == '__main__':
    dir_data = base.UCR_DIR
    tag = tag_path(os.path.abspath(__file__), 1)
    dir_out = 'cache/{}'.format(tag)
    if os.path.exists(dir_out) is False:
        os.makedirs(dir_out)

    data_name = 'Two_Patterns'
    _, _, x_te, y_te, n_classes = ucr.load_ucr_flat(data_name, dir_data)
    distr = utils.distribute_dataset(x_te, y_te)
    n_samples = 10
    for c in distr.keys():
        x_plot.extend(distr[c][:n_samples])
        y_plot.extend([c]*n_samples)







