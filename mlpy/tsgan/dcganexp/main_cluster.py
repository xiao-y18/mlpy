"""
    The baseline results, kmeans+ED, were validated according to reference: Zakaria, Jesin, Abdullah Mueen, and Eamonn Keogh. "Clustering time series using unsupervised-shapelets." 2012 IEEE 12th International Conference on Data Mining. IEEE, 2012.
"""

from mlpy.lib.utils.base import tag_path
from mlpy.data import ucr
from mlpy.tsgan.dcganexp import base
from mlpy.tsgan.dcganexp.main_encoder import encode

import numpy as np
import os
import pandas as pd
from time import time
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.manifold import MDS
from sklearn.manifold import TSNE


def rand_index(y_actual, y_pred):
    """
    Test results on k-means-ed are consistent to the literature: Zakaria, Jesin, Abdullah Mueen, and Eamonn Keogh. "Clustering time series using unsupervised-shapelets." 2012 IEEE 12th International Conference on Data Mining. IEEE, 2012.
    The definition can be referred to : Paparrizos, John, and Luis Gravano. "k-shape: Efficient and accurate clustering of time series." Proceedings of the 2015 ACM SIGMOD International Conference on Management of Data. ACM, 2015.
    :param y_actual: 
    :param y_pred: 
    :return: 
    """
    n = len(y_actual)
    tp, tn, fp, fn = 0, 0, 0, 0

    for i in range(n):
        for j in range(i, n):
            if y_actual[i] == y_actual[j] and y_pred[i] == y_pred[j]:
                tp += 1
            elif y_actual[i] == y_actual[j] and y_pred[i] != y_pred[j]:
                fn += 1
            elif y_actual[i] != y_actual[j] and y_pred[i] == y_pred[j]:
                fp += 1
            elif y_actual[i] != y_actual[j] and y_pred[i] != y_pred[j]:
                tn += 1

    return (tp + tn) / (tp + fp + fn + tn)

def kmeans(x, y, n_clusters):
    model = KMeans(n_clusters=n_clusters)
    t_start = time()
    model.fit(x)
    t = time() - t_start
    ri = rand_index(y, model.labels_)
    return ri, t


def run_feature_reduced(feature_type, norm_type, data_name_list, dir_gan, dir_data, dir_out,
                        n_reduced=3.0, mode='pca'):
    res = {'dataset': [], 'randIndex': [], 'time': [], 'length':[]}
    n_datasets = len(data_name_list)
    for i, data_name in enumerate(data_name_list):
        print("******** [{}/{}] processing {}".format(i, n_datasets, data_name))
        ## load data
        features_tr, y_tr, features_te, y_te, n_classes = encode(
            data_name, dir_data, feature_type, norm_type, dir_gan)
        features = np.vstack([features_tr, features_te])
        y = np.hstack([y_tr, y_te])
        ## set the estimated number of components.
        if n_reduced > 1:
            n_components = int(n_reduced)
        else:
            n_components = int(features.shape[1] * n_reduced)
            if n_components < 3:
                n_components = 3
            if n_components > features.shape[0]: # account the limitation of PCA
                n_components = features.shape[0]
        ## conduct dimensionality reduction.
        if mode == 'pca':
            pca = PCA(n_components=n_components, whiten=True)  #  y : Ignored
            features_reduced = pca.fit_transform(features)
        elif mode == 'mds':
            mds = MDS(n_components=n_components) #  y : Ignored
            features_reduced = mds.fit_transform(features)
        elif mode == 'tsne':
            tsne = TSNE(n_components=n_components) #  y : Ignored
            features_reduced = tsne.fit_transform(features)
        else:
            raise ValueError("The mode={} can not be found!".format(mode))
        ## conduct evaluation
        ri, t = kmeans(features_reduced, y, n_classes)
        print(ri, t)
        res['dataset'].append(data_name)
        res['randIndex'].append(ri)
        res['time'].append(t)
        res['length'].append(features_reduced.shape[1])

    ## save result
    df = pd.DataFrame(res)
    df.to_csv(os.path.join(dir_out, 'kmeans_{}{}_{}_{}.csv'.format(
        mode, n_reduced, feature_type, norm_type)), index=False)
    return df

def run_feature(feature_type, norm_type, data_name_list, dir_gan, dir_data, dir_out):
    print("******** kmeans over features")
    res = {'dataset': [], 'randIndex': [], 'time': []}
    n_datasets = len(data_name_list)
    for i, data_name in enumerate(data_name_list):
        print("******** [{}/{}] processing {}".format(i, n_datasets, data_name))
        ## load data
        features_tr, y_tr, features_te, y_te, n_classes = encode(
            data_name, dir_data, feature_type, norm_type, dir_gan)
        features = np.vstack([features_tr, features_te])
        y = np.hstack([y_tr, y_te])
        ri, t = kmeans(features, y, n_classes)
        print(data_name, ri, t)
        res['dataset'].append(data_name)
        res['randIndex'].append(ri)
        res['time'].append(t)
    ## save result
    df = pd.DataFrame(res)
    df.to_csv(os.path.join(dir_out,'kmeans_{}_{}.csv'.format(feature_type, norm_type)), index=False)
    return df


def run_raw(data_name_list, dir_data, dir_out):
    print("******** kmeans over raw data")
    res = {'dataset': [], 'randIndex':[], 'time':[]}
    n_datasets = len(data_name_list)
    for i, data_name in enumerate(data_name_list):
        print("******** [{}/{}] processing {}".format(i, n_datasets, data_name))
        ## load data
        x_tr, y_tr, x_te, y_te, n_classes = ucr.load_ucr_flat(data_name, dir_data)
        x = np.vstack([x_tr, x_te])
        y = np.hstack([y_tr, y_te])
        ## start to run
        ri, t = kmeans(x, y, n_classes)
        res['dataset'].append(data_name)
        res['randIndex'].append(ri)
        res['time'].append(t)
    ## save result
    df = pd.DataFrame(res)
    df.to_csv(os.path.join(dir_out, 'kmeans_raw.csv'), index=False)
    return df

def run_raw_reduced(data_name_list, dir_data, dir_out, n_reduced=3.0):
    print("******** kmeans over reduced raw data")
    res = {'dataset': [], 'randIndex':[], 'time':[]}
    n_datasets = len(data_name_list)
    for i, data_name in enumerate(data_name_list):
        print("******** [{}/{}] processing {}".format(i, n_datasets, data_name))
        ## load data
        x_tr, y_tr, x_te, y_te, n_classes = ucr.load_ucr_flat(data_name, dir_data)
        x = np.vstack([x_tr, x_te])
        y = np.hstack([y_tr, y_te])

        if n_reduced > 1:
            n_components = int(n_reduced)
        else:
            n_components = int(x_tr.shape[1] * n_reduced)

        pca = PCA(n_components=n_components, whiten=False)
        pca.fit(x_tr)
        reduced_tr = pca.transform(x_tr)
        reduced_te = pca.transform(x_te)
        reduced_x = np.vstack([reduced_tr, reduced_te])
        ## start to run
        ri, t = kmeans(reduced_x, y, n_classes)
        res['dataset'].append(data_name)
        res['randIndex'].append(ri)
        res['time'].append(t)
    ## save result
    df = pd.DataFrame(res)
    df.to_csv(os.path.join(dir_out, 'kmeans_raw.csv'), index=False)
    return df


# if __name__ == '__main__': # unit test
#     dir_data = base.UCR_DIR
#     tag = tag_path(os.path.abspath(__file__), 1)
#
#     data_name_list = ucr.get_data_name_list(dir_data)
#     # data_name_list = ucr.const.DATASETS_44
#     # data_name_list = ['Trace', 'synthetic_control', 'Gun_Point', 'ECG200', 'ECG5000', 'ECGFiveDays'] # for test
#
#     # ## for raw data
#     # dir_out = 'cache/{}/{}'.format(tag, 'raw')
#     # if os.path.exists(dir_out) is False:
#     #     os.makedirs(dir_out)
#     # run_raw(data_name_list, dir_data, dir_out)
#
#     # ## for raw data, reduce
#     # dir_out = 'cache/{}/{}'.format(tag, 'raw-reduced')
#     # if os.path.exists(dir_out) is False:
#     #     os.makedirs(dir_out)
#     # run_raw_reduced(data_name_list, dir_data, dir_out)
#
#     #### tsgan
#     dir_gan = 'cache/main_gan_all'
#     tag_exp = os.path.basename(dir_gan)
#     dir_out = 'cache/{}/{}'.format(tag, tag_exp)
#     if os.path.exists(dir_out) is False:
#         os.makedirs(dir_out)
#     ## full feature
#     # run_feature('local-max', 'znorm', data_name_list, dir_gan, dir_data, dir_out)
#     ## reduced feature
#     run_feature_reduced('local-max', 'tanh', data_name_list, dir_gan, dir_data, dir_out, n_reduced=3)

if __name__ == '__main__': # multiple runs
    dir_data = base.UCR_DIR
    tag = tag_path(os.path.abspath(__file__), 1)

    # tag_tsgan = 'bn.tf.keras_batch16_kernel10'
    tag_tsgan = 'bn.tf.keras_batch16_kernel10_adapt75'
    dir_gan_list = ['cache/tsgan/{}/run{}'.format(tag_tsgan, i) for i in range(4)]
    for dir_gan in dir_gan_list:
        dir_out = 'cache/{}/{}/{}'.format(tag, tag_tsgan, os.path.basename(dir_gan))
        if os.path.exists(dir_out) is False:
            os.makedirs(dir_out)

        data_name_list = ucr.get_data_name_list(dir_data)
        run_feature('local-max', 'znorm', data_name_list, dir_gan, dir_data, dir_out)
        # run_feature_reduced(
        #     'local-max', 'znorm', data_name_list, dir_gan, dir_data, dir_out, n_reduced=3)
        # nonlinear-quantile


"""
tsgan, just train on half dataset
0.8756218905472637
0.9870438158624515
0.5032835820895523
0.6123880597014926
0.7208988602279545 
0.5011401692358821

tsgan, train on whole dataset?
0.8758208955223881
0.9945479755962285 
0.5024875621890548
0.6370149253731343
0.7270912217556489
0.5038781092619578
"""

"""raw data, nmi
arithmetic
Trace	0.502479697
synthetic_control	0.808513389
Gun_Point	-1.92E-15
ECG200	0.128986665
ECG5000	0.512436019
ECGFiveDays	0.001067531

min
Trace	0.503153181
synthetic_control	0.880872591
Gun_Point	-1.92E-15
ECG200	0.134868279
ECG5000	0.639327418
ECGFiveDays	0.001067581

max
Trace	0.501807115
synthetic_control	0.742098129
Gun_Point	-1.92E-15
ECG200	0.123361549
ECG5000	0.410729566
ECGFiveDays	0.001067482

geometric
Trace	0.502479697
synthetic_control	0.808513389
Gun_Point	-1.92E-15
ECG200	0.128986665
ECG5000	0.512436019
ECGFiveDays	0.001067531

"""