"""

"""

""" Constant parameters
"""
UCR_DIR = '../../dataset/UCR_TS_Archive_2015'
DATASET_LIST_INSPECT_TSGAN = [
    'Wine', 'ChlorineConcentration', 'uWaveGestureLibrary_X', 'CinC_ECG_torso', 'OSULeaf',
    'LargeKitchenAppliances', 'SonyAIBORobotSurfaceII']


""" Naive classifiers
    
    C4.5 (C45), naive Bayes (NB), LR, SVM with linear (SVML) and quadratic kernel (SVMQ), 
    multilayer perceptron (MLP), random forest (with 500 trees) (RandF) and rotation forest (with 50 trees) (RotF)
    k-NN, Naive Bayes, C4.5 decision tree, Support Vector Machines with linear and quadratic basis function kernels, Random Forest, Rotation Forest and a Bayesian network.
    Rotation Forest and a Bayesian network can't be found in Python.
    refer to: ﻿The Great Time Series Classification Bake Off: An Experimental Evaluation of Recently Proposed Algorithms. Extended Version
"""
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import LinearSVC, SVC
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
StandardClassifierDic = {
    'C45': DecisionTreeClassifier(),
    'NaiveBayes': GaussianNB(),
    'LR': LogisticRegression(),
    'LSVC': LinearSVC(),
    'QSVC': SVC(kernel='poly', degree=2),
    'MLP': MLPClassifier(),
    'RF10': RandomForestClassifier(),
    'RF500': RandomForestClassifier(n_estimators=500),
    'RF1000': RandomForestClassifier(n_estimators=1000), # add by myself to test whether the increase of n_estimators will lead to the increase of acc.
    '5NN': KNeighborsClassifier(),
    '1NN': KNeighborsClassifier(n_neighbors=1)
}
StandardClassifierDicV2 = {
    'C45': DecisionTreeClassifier(),
    'NaiveBayes': GaussianNB(),
    'LR': LogisticRegression(),
    'LSVC': LinearSVC(),
    'QSVC': SVC(kernel='poly', degree=2),
    'MLP': MLPClassifier(),
    'RF500': RandomForestClassifier(n_estimators=500),
    '1NN': KNeighborsClassifier(n_neighbors=1)
}
LinearClassifiers = ['LR', 'LSVC', '1NN']

from time import time
from sklearn import metrics
def classify(model, x_tr, y_tr, x_te, y_te):
    ## train
    t_start = time()
    model.fit(x_tr, y_tr)
    time_tr = time() - t_start
    acc_tr = metrics.accuracy_score(y_tr, model.predict(x_tr))
    ## test
    t_start = time()
    acc_te = metrics.accuracy_score(y_te, model.predict(x_te))
    time_te = time() - t_start

    return [acc_tr, acc_te], [time_tr, time_te]