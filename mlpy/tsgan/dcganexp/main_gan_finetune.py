"""
Objective: fine tune TSGAN based on the classification results.
1. Train GAN;
2. Classify based on features extracted from GAN.
"""

import matplotlib
matplotlib.use('Agg')
import os
import sklearn

from mlpy.data import ucr
from mlpy.lib.utils import tag_path
from mlpy.tsgan.dcganexp import base
from mlpy.tsgan.dcganexp import main_encoder_aaai19
from mlpy.tsgan.dcganexp import main_gan

if __name__ == '__main__':
    dir_data_root = base.UCR_DIR
    tag = '{}'.format(tag_path(os.path.abspath(__file__), 1))
    tag += '_bn.tf.keras_batch16_kernel10_adapt70'

    ## set up dataset list for experiment
    # data_name_list = ['Adiac', 'ArrowHead', 'Beef', 'BeetleFly', 'BirdChicken']
    data_name_list = ucr.get_data_name_list(dir_data_root)
    # data_name_list = ['ArrowHead']
    # data_name_list = base.DATASET_LIST_INSPECT_TSGAN

    ## train gan
    dir_gan = 'cache/{}'.format(tag)
    mode = 'half'
    main_gan.start_training(data_name_list, dir_data_root, mode, tag)
    main_gan.reduce_results(data_name_list, dir_gan)

    # ## classification
    # param_dic = {
    #     'model_name':['LR', 'LSVC', '1NN'],
    #     'feature_type': ['local-max'],
    #     'norm_type': main_encoder.Normalization_Types}
    # param_list = list(sklearn.model_selection.ParameterGrid(param_dic))
    # n_params = len(param_list)
    # for i, param in enumerate(param_list):
    #     print("********* [{}/{}]processing: {}".format(i+1, n_params, param))
    #     main_encoder.run_standard_classifier(
    #         param['model_name'], param['feature_type'], param['norm_type'],
    #         data_name_list, dir_gan, dir_data_root, dir_gan)




