"""
    Realization of feature extraction based on GAN's discriminator.
    The approaches are based on various basic pooling operations in DNN.
"""

from mlpy.lib.utils.base import tag_path
from mlpy.data import ucr
from mlpy.data import ts
from mlpy.tsgan.lib.ops import *
from mlpy.tsgan.dcgan.model import TSGAN
from mlpy.tsgan.dcgan.config import Config
from mlpy.tsgan.dcganexp import base

import numpy as np
import os
import pandas as pd
import tensorflow as tf
tf_conf = tf.ConfigProto()
tf_conf.gpu_options.allow_growth = True

Feature_Types_Layers = ['h0', 'h1', 'h2', 'h3']
Feature_Types = ['flat', 'gap', 'gmp', 'local-avg', 'local-max', 'local-max-abs', 'local-max-int',
                 'local-max-stride']
Normalization_Types = [None, 'znorm', 'sigmoid', 'tanh']
class EncoderGAN:
    def __init__(self, gan, input_shape, is_training=False, type='local-max'):
        self.gan = gan
        self.input_shape = input_shape
        self.is_training = is_training
        self.x = tf.placeholder(tf.float32, shape=[None] + self.input_shape, name='x')
        self.layers = self.gan.discriminator_layers(self.x, self.is_training)
        self.features = self.get_features(type)

    def encode(self, sess, X, batch_size, norm=None):
        res = []
        n_samples = len(X)
        n_batches = n_samples // batch_size
        for i in range(n_samples // batch_size):
            x_batch = X[i * batch_size:(i + 1) * batch_size]
            feature_batch = sess.run(self.features, feed_dict={self.x: x_batch})
            res.append(feature_batch)
        n_samples_left = n_samples - n_batches * batch_size
        if n_samples_left > 0:
            x_left = X[-n_samples_left:]
            feature_left = sess.run(self.features, feed_dict={self.x: x_left})
            res.append(feature_left)
        res = np.concatenate(res, axis=0)
        if norm is None:
            return res
        else:
            return self.normalize(res, norm)

    def get_features(self, type='h0'):
        ## flat feature map from specific layer
        if type.startswith('h'):
            index = int(type[1:])
            features = flatten(self.layers[index])
        ## flat and concat
        elif type == 'flat':
            features = self.get_features_flat()
        ## flat, concat, then pool
        elif type == 'local-max':
            features = self.get_features_local_max()
        elif type == 'local-max-abs':
            features = self.get_features_local_max_abs()
        elif type == 'local-max-int':
            features = self.get_features_local_max_int()
        elif type == 'local-max-stride':
            features = self.get_features_local_max_stride()
        elif type == 'local-avg':
            features = self.get_features_local_avg()
        elif type == 'gap':
            features = self.get_features_gap()
        elif type == 'gmp':
            features = self.get_features_gmp()
        else:
            raise ValueError('The type of features, {}, no found!'.format(type))

        return features

    def get_features_flat(self):
        h0, h1, h2, h3 = self.layers
        return tf.concat([flatten(h1), flatten(h2), flatten(h3)], axis=1)

    def get_features_local_max(self):
        h0, h1, h2, h3 = self.layers
        dim_0 = h0.get_shape()[1].value
        dim_1 = h1.get_shape()[1].value
        dim_2 = h2.get_shape()[1].value
        dim_3 = h3.get_shape()[1].value

        f1 = flatten(tf.nn.max_pool(h1, [1, dim_1 // 2, 1, 1], [1, 1, 1, 1], 'VALID'))
        f2 = flatten(tf.nn.max_pool(h2, [1, dim_2 // 3, 1, 1], [1, 1, 1, 1], 'VALID'))
        f3 = flatten(tf.nn.max_pool(h3, [1, max(2, dim_3 // 4), 1, 1], [1, 1, 1, 1], 'VALID'))

        return tf.concat([f1, f2, f3], axis=1)

    def get_features_local_max_abs(self):
        h0, h1, h2, h3 = self.layers
        dim_0 = h0.get_shape()[1].value
        dim_1 = h1.get_shape()[1].value
        dim_2 = h2.get_shape()[1].value
        dim_3 = h3.get_shape()[1].value

        h1, h2, h3 = tf.abs(h1), tf.abs(h2), tf.abs(h3)
        f1 = flatten(tf.nn.max_pool(h1, [1, dim_1 // 2, 1, 1], [1, 1, 1, 1], 'VALID'))
        f2 = flatten(tf.nn.max_pool(h2, [1, dim_2 // 3, 1, 1], [1, 1, 1, 1], 'VALID'))
        f3 = flatten(tf.nn.max_pool(h3, [1, max(2, dim_3 // 4), 1, 1], [1, 1, 1, 1], 'VALID'))

        return tf.concat([f1, f2, f3], axis=1)

    def get_features_local_max_int(self):
        h0, h1, h2, h3 = self.layers
        dim_0 = h0.get_shape()[1].value
        dim_1 = h1.get_shape()[1].value
        dim_2 = h2.get_shape()[1].value
        dim_3 = h3.get_shape()[1].value

        h1, h2, h3 = tf.cast(h1, tf.int32), tf.cast(h2, tf.int32), tf.cast(h3, tf.int32)
        f1 = flatten(tf.nn.max_pool(h1, [1, dim_1 // 2, 1, 1], [1, 1, 1, 1], 'VALID'))
        f2 = flatten(tf.nn.max_pool(h2, [1, dim_2 // 3, 1, 1], [1, 1, 1, 1], 'VALID'))
        f3 = flatten(tf.nn.max_pool(h3, [1, max(2, dim_3 // 4), 1, 1], [1, 1, 1, 1], 'VALID'))

        return tf.concat([f1, f2, f3], axis=1)

    def get_features_local_max_stride(self):
        h0, h1, h2, h3 = self.layers
        dim_0 = h0.get_shape()[1].value
        dim_1 = h1.get_shape()[1].value
        dim_2 = h2.get_shape()[1].value
        dim_3 = h3.get_shape()[1].value
        w1 = dim_1 // 2
        w2 = dim_2 // 3
        w3 = max(2, dim_3 // 4)
        f1 = flatten(tf.nn.max_pool(h1, [1, w1, 1, 1], [1, min(5, w1), 1, 1], 'VALID'))
        f2 = flatten(tf.nn.max_pool(h2, [1, w2, 1, 1], [1, min(5, w2), 1, 1], 'VALID'))
        f3 = flatten(tf.nn.max_pool(h3, [1, w3, 1, 1], [1, min(5, w3), 1, 1], 'VALID'))

        return tf.concat([f1, f2, f3], axis=1)

    def get_features_local_avg(self):
        h0, h1, h2, h3 = self.layers
        dim_0 = h0.get_shape()[1].value
        dim_1 = h1.get_shape()[1].value
        dim_2 = h2.get_shape()[1].value
        dim_3 = h3.get_shape()[1].value
        f1 = flatten(tf.nn.avg_pool(h1, [1, dim_1 // 2, 1, 1], [1, 1, 1, 1], 'VALID'))
        f2 = flatten(tf.nn.avg_pool(h2, [1, dim_2 // 3, 1, 1], [1, 1, 1, 1], 'VALID'))
        f3 = flatten(tf.nn.avg_pool(h3, [1, max(2, dim_3 // 4), 1, 1], [1, 1, 1, 1], 'VALID'))

        return tf.concat([f1, f2, f3], axis=1)

    def get_features_gap(self):
        h0, h1, h2, h3 = self.layers
        f1 = flatten(tf.layers.average_pooling2d(
            h1, pool_size=[h1.get_shape()[1].value, h1.get_shape()[2].value], strides=1))
        f2 = flatten(tf.layers.average_pooling2d(
            h2, pool_size=[h2.get_shape()[1].value, h2.get_shape()[2].value], strides=1))
        f3 = flatten(tf.layers.average_pooling2d(
            h3, pool_size=[h3.get_shape()[1].value, h3.get_shape()[2].value], strides=1))
        flat = tf.concat([f1, f2, f3], axis=1)
        return flat

    def get_features_gmp(self):
        h0, h1, h2, h3 = self.layers
        f1 = flatten(tf.layers.max_pooling2d(
            h1, pool_size=[h1.get_shape()[1].value, h1.get_shape()[2].value], strides=1))
        f2 = flatten(tf.layers.max_pooling2d(
            h2, pool_size=[h2.get_shape()[1].value, h2.get_shape()[2].value], strides=1))
        f3 = flatten(tf.layers.max_pooling2d(
            h3, pool_size=[h3.get_shape()[1].value, h3.get_shape()[2].value], strides=1))
        flat = tf.concat([f1, f2, f3], axis=1)
        return flat

    def normalize(self, features, mode='znorm'):
        # common-used: znorm, sigmoid, thanh
        return ts.normalize(features, mode)

def encode(data_name, dir_data, feature_type, norm_type, dir_gan):
    tf.reset_default_graph()
    ## load data
    x_tr, y_tr, x_te, y_te, n_classes = ucr.load_ucr_flat(data_name, dir_data)
    x_tr = np.reshape(x_tr, x_tr.shape + (1, 1))
    x_te = np.reshape(x_te, x_te.shape + (1, 1))
    ## set up GAN
    dir_checkpoint = os.path.join(dir_gan, data_name, 'checkpoint')
    conf = Config(x_tr, '', '', x_tr.shape[1], x_tr.shape[2], x_tr.shape[3], state='test')
    gan = TSGAN(conf.dim_z, conf.dim_h, conf.dim_w, conf.dim_c, conf.random_seed,
                conf.g_lr, conf.d_lr, conf.g_beta1, conf.d_beta1, conf.gf_dim, conf.df_dim)
    ## start to run
    with tf.Session(config=tf_conf) as sess:
        isload, counter = gan.load(sess, dir_checkpoint)
        if not isload:
            raise Exception("[!] Train a model first, then run test mode")
        input_shape = [x_tr.shape[1], x_tr.shape[2], x_tr.shape[3]]
        encoder = EncoderGAN(gan, input_shape, type=feature_type)
        features_tr = encoder.encode(sess, x_tr, conf.batch_size, norm=norm_type)
        features_te = encoder.encode(sess, x_te, conf.batch_size, norm=norm_type)
        return features_tr, y_tr, features_te, y_te, n_classes


def run_classifier(model, model_name, feature_type, norm_type, data_name_list, dir_gan, dir_data, dir_out):
    print("******** processing {}, {}".format(model_name, feature_type, norm_type))
    res = {'dataset': [], 'acc': [], 'acc_te': [], 'time': [], 'time_te': []}
    n_datasets = len(data_name_list)
    for j, data_name in enumerate(data_name_list):
        print("******** [{}/{}] processing {}".format(j, n_datasets, data_name))
        ## load data
        features_tr, y_tr, features_te, y_te, n_classes = encode(
            data_name, dir_data, feature_type, norm_type, dir_gan)
        acc, t = base.classify(model, features_tr, y_tr, features_te, y_te)
        res['dataset'].append(data_name)
        res['acc'].append(acc[0])
        res['acc_te'].append(acc[1])
        res['time'].append(t[0])
        res['time_te'].append(t[1])
        print(acc, t)
    ## save result
    df = pd.DataFrame(res)
    df.to_csv(
        os.path.join(dir_out, '{}_{}_{}.csv'.format(model_name, feature_type, norm_type)),
        index=False)
    return df

def run_standard_classifier(
        model_name, feature_type, norm_type, data_name_list, dir_gan, dir_data, dir_out):
    model = base.StandardClassifierDic[model_name]
    return run_classifier(model, model_name, feature_type, norm_type, data_name_list, dir_gan, dir_data, dir_out)


def cal_length_of_features(data_name_list, feature_type_list, dir_gan, dir_data, dir_out):
    print("********** start cal_length_of_features: ")
    ## initialize result
    res = {'dataset':[], 'ts':[]}
    for feature_type in feature_type_list:
        res[feature_type] = []
    ## start to run
    n_datasets = len(data_name_list)
    for i, data_name in enumerate(data_name_list):
        tf.reset_default_graph()
        print("[{}/{}] ***** {}".format(i, n_datasets, data_name))
        # load data and
        x_tr, y_tr, _, _, _ = ucr.load_ucr_flat(data_name, dir_data)
        x_tr = np.reshape(x_tr, x_tr.shape + (1, 1))
        dir_checkpoint = os.path.join(dir_gan, data_name, 'checkpoint')
        conf = Config(x_tr, '', '', x_tr.shape[1], x_tr.shape[2], x_tr.shape[3], state='test')
        gan = TSGAN(conf.dim_z, conf.dim_h, conf.dim_w, conf.dim_c, conf.random_seed,
                    conf.g_lr, conf.d_lr, conf.g_beta1, conf.d_beta1, conf.gf_dim, conf.df_dim)
        # set up session and extract features
        sess = tf.Session(config=tf_conf)
        isload, counter = gan.load(sess, dir_checkpoint)
        if not isload:
            raise Exception("[!] Train a model first, then run test mode")
        res['dataset'].append(data_name)
        res['ts'].append(x_tr.shape[1])
        for feature_type in feature_type_list:
            input_shape = [x_tr.shape[1], x_tr.shape[2], x_tr.shape[3]]
            encoder = EncoderGAN(gan, input_shape, type=feature_type)
            features_tr = encoder.encode(sess, x_tr, conf.batch_size)
            res[feature_type].append(features_tr.shape[1])
        sess.close()
    ## save results
    df = pd.DataFrame(res)
    df.to_csv(os.path.join(dir_out, 'length_of_features.csv'), index=False)


if __name__ == '__main__':
    dir_data = base.UCR_DIR
    dir_gan = 'cache/tsgan/bn.tf.keras_batch16_kernel10/run0'
    tag = tag_path(os.path.abspath(__file__), 1)
    dir_out = 'cache/{}/'.format(tag)
    if os.path.exists(dir_out) is False:
        os.makedirs(dir_out)

    data_name_list = ucr.get_data_name_list(dir_data)
    # data_name_list = ucr.const.DATASETS_44
    # data_name_list = ['ArrowHead'] # for test

    run_standard_classifier('LR', 'local-max', 'tanh', data_name_list, dir_gan, dir_data, dir_out)
    # cal_length_of_features(data_name_list, Feature_Types_Layers+Feature_Types, dir_gan, dir_data, dir_out)





