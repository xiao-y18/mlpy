import matplotlib
matplotlib.use('Agg')

from mlpy.lib.utils.base import tag_path
from mlpy.tsgan.dcganexp import base
from mlpy.data import ucr, utils
from mlpy.data import ts
from mlpy.tsgan.dcgan.config import Config
from mlpy.tsgan.dcgan.model import TSGAN
from mlpy.tsgan.lib.ops import *

import os
import pandas as pd
import numpy as np
from sklearn import metrics
from IPython import embed

import tensorflow as tf
tf_conf = tf.ConfigProto()
tf_conf.gpu_options.allow_growth = True


class EncoderGANLayerwise:
    def __init__(self, gan, input_shape, is_training=False, type='local-max'):
        self.gan = gan
        self.input_shape = input_shape
        self.is_training = is_training
        self.x = tf.placeholder(tf.float32, shape=[None] + self.input_shape, name='x')
        self.layers = self.gan.discriminator_layers(self.x, self.is_training)
        self.features = self.get_features(type)

    def encode(self, sess, X, batch_size, norm=None):
        res_on_batch = [[] for _ in range(len(self.features))]
        n_samples = len(X)
        n_batches = n_samples // batch_size
        for i in range(n_samples // batch_size):
            x_batch = X[i * batch_size:(i + 1) * batch_size]
            v_batch = sess.run(self.features, feed_dict={self.x: x_batch})
            for r, v in zip(res_on_batch, v_batch):
                r.append(v)
        n_samples_left = n_samples - n_batches * batch_size
        if n_samples_left > 0:
            x_left = X[-n_samples_left:]
            v_left = sess.run(self.features, feed_dict={self.x: x_left})
            for r, v in zip(res_on_batch, v_left):
                r.append(v)
        res = [self.normalize(np.vstack(r), norm) for r in res_on_batch]
        return res

    def get_features(self, type):
        if type == 'flat':
            features = self.get_features_flat()
        elif type == 'local-max':
            features = self.get_features_local_max()
        elif type == 'local-max-stride':
            features = self.get_features_local_max_stride()
        elif type == 'local-avg':
            features = self.get_features_local_avg()
        else:
            raise ValueError("The feature type = {} can not be found!".format(type))
        return features

    def get_features_flat(self):
        layers_flat = [flatten(l) for l in self.layers]
        return layers_flat

    def get_features_local_max(self):
        h0, h1, h2, h3 = self.layers
        dim_0 = h0.get_shape()[1].value
        dim_1 = h1.get_shape()[1].value
        dim_2 = h2.get_shape()[1].value
        dim_3 = h3.get_shape()[1].value

        f0 = flatten(tf.nn.max_pool(h0, [1, dim_0 // 3, 1, 1], [1, 1, 1, 1], 'SAME'))
        f1 = flatten(tf.nn.max_pool(h1, [1, dim_1 // 3, 1, 1], [1, 1, 1, 1], 'SAME'))
        f2 = flatten(tf.nn.max_pool(h2, [1, dim_2 // 3, 1, 1], [1, 1, 1, 1], 'SAME'))
        f3 = flatten(tf.nn.max_pool(h3, [1, max(2, dim_3 // 3), 1, 1], [1, 1, 1, 1], 'SAME'))

        return [f0, f1, f2, f3]

    def get_features_local_avg(self):
        h0, h1, h2, h3 = self.layers
        dim_0 = h0.get_shape()[1].value
        dim_1 = h1.get_shape()[1].value
        dim_2 = h2.get_shape()[1].value
        dim_3 = h3.get_shape()[1].value

        f0 = flatten(tf.nn.avg_pool(h1, [1, dim_0 // 3, 1, 1], [1, 1, 1, 1], 'SAME'))
        f1 = flatten(tf.nn.avg_pool(h1, [1, dim_1 // 3, 1, 1], [1, 1, 1, 1], 'SAME'))
        f2 = flatten(tf.nn.avg_pool(h2, [1, dim_2 // 3, 1, 1], [1, 1, 1, 1], 'SAME'))
        f3 = flatten(tf.nn.avg_pool(h3, [1, max(2, dim_3 // 3), 1, 1], [1, 1, 1, 1], 'SAME'))

        return [f0, f1, f2, f3]

    def get_features_local_max_stride(self):
        h0, h1, h2, h3 = self.layers
        dim_0 = h0.get_shape()[1].value
        dim_1 = h1.get_shape()[1].value
        dim_2 = h2.get_shape()[1].value
        dim_3 = h3.get_shape()[1].value
        w0 = dim_0 // 2
        w1 = dim_1 // 2
        w2 = dim_2 // 3
        w3 = max(2, dim_3 // 4)
        f0 = flatten(tf.nn.max_pool(h0, [1, w1, 1, 1], [1, min(5, w0), 1, 1], 'VALID'))
        f1 = flatten(tf.nn.max_pool(h1, [1, w1, 1, 1], [1, min(5, w1), 1, 1], 'VALID'))
        f2 = flatten(tf.nn.max_pool(h2, [1, w2, 1, 1], [1, min(5, w2), 1, 1], 'VALID'))
        f3 = flatten(tf.nn.max_pool(h3, [1, w3, 1, 1], [1, min(5, w3), 1, 1], 'VALID'))

        return [f0, f1, f2, f3]

    def normalize(self, features, mode):
        # common-used: znorm, sigmoid, thanh
        return ts.normalize(features, mode)


def run_majority_vote(data_name, dir_data, dir_gan):
    norm_type = 'tanh'
    model_name = 'LR'
    tf.reset_default_graph()

    x_tr, y_tr, x_te, y_te, n_classes = ucr.load_ucr_flat(data_name, dir_data)
    x_tr = np.reshape(x_tr, x_tr.shape + (1, 1))
    x_te = np.reshape(x_te, x_te.shape + (1, 1))
    ## set up GAN
    dir_checkpoint = os.path.join(dir_gan, data_name, 'checkpoint')
    conf = Config(x_tr, '', '', x_tr.shape[1], x_tr.shape[2], x_tr.shape[3], state='test')
    gan = TSGAN(conf.dim_z, conf.dim_h, conf.dim_w, conf.dim_c, conf.random_seed,
                conf.g_lr, conf.d_lr, conf.g_beta1, conf.d_beta1, conf.gf_dim, conf.df_dim)
    ## start to run
    with tf.Session(config=tf_conf) as sess:
        isload, counter = gan.load(sess, dir_checkpoint)
        if not isload:
            raise Exception("[!] Train a model first, then run test mode")
        input_shape = [x_tr.shape[1], x_tr.shape[2], x_tr.shape[3]]

        encoder = EncoderGANLayerwise(gan, input_shape, type='local-max')
        features_tr_list = encoder.encode(sess, x_tr, conf.batch_size, norm=norm_type)
        features_te_list = encoder.encode(sess, x_te, conf.batch_size, norm=norm_type)
        weights = [1, 1, 1, 1]

        i_layer = 1
        features_tr_list = features_tr_list[i_layer:]
        features_te_list = features_te_list[i_layer:]
        weights = weights[i_layer:]

        model_list = []
        y_pred_list = []
        acc_tr = []
        acc_te = []
        for i in range(len(features_tr_list)):
            feat_tr = features_tr_list[i]
            feat_te = features_te_list[i]
            # train
            model = base.StandardClassifierDic[model_name]
            model.fit(feat_tr, y_tr)
            model_list.append(model)
            acc_tr.append(metrics.accuracy_score(y_tr, model.predict(feat_tr)))
            # predict
            y_pred = model.predict(feat_te)
            y_pred_list.append(y_pred)
            acc_te.append(metrics.accuracy_score(y_te, y_pred))
        # ensemble
        y_en = []
        for i in range(len(y_te)):
            y_pred = [y[i] for y in y_pred_list]
            label = np.unique(y_pred)
            count = np.zeros(len(label))
            for i, l in enumerate(label):
                for y, w in zip(y_pred, weights):
                    if y == l:
                        count[i]+=w
            y_en.append(label[np.argmax(count)])
        acc_en = metrics.accuracy_score(y_te, y_en)

        print(acc_tr)
        print(acc_te)
        print(acc_en)

        # df = pd.DataFrame()
        # for i, y in enumerate(y_pred_list):
        #     df['y{}'.format(i)] = y
        # df['y_te'] = y_te
        # df['y_en'] = y_en
        # df.to_csv('cache/test.csv', index=False)


def run_probability_vote(data_name, dir_data, dir_gan):
    norm_type = 'tanh'
    model_name = 'LR'
    tf.reset_default_graph()

    x_tr, y_tr, x_te, y_te, n_classes = ucr.load_ucr_flat(data_name, dir_data)
    x_tr = np.reshape(x_tr, x_tr.shape + (1, 1))
    x_te = np.reshape(x_te, x_te.shape + (1, 1))
    ## set up GAN
    dir_checkpoint = os.path.join(dir_gan, data_name, 'checkpoint')
    conf = Config(x_tr, '', '', x_tr.shape[1], x_tr.shape[2], x_tr.shape[3], state='test')
    gan = TSGAN(conf.dim_z, conf.dim_h, conf.dim_w, conf.dim_c, conf.random_seed,
                conf.g_lr, conf.d_lr, conf.g_beta1, conf.d_beta1, conf.gf_dim, conf.df_dim)
    ## start to run
    with tf.Session(config=tf_conf) as sess:
        isload, counter = gan.load(sess, dir_checkpoint)
        if not isload:
            raise Exception("[!] Train a model first, then run test mode")
        input_shape = [x_tr.shape[1], x_tr.shape[2], x_tr.shape[3]]

        encoder = EncoderGANLayerwise(gan, input_shape, type='local-max')
        # features_tr_list = encoder.encode(sess, x_tr, conf.batch_size, norm=norm_type)
        # features_te_list = encoder.encode(sess, x_te, conf.batch_size, norm=norm_type)
        # weights = [0.1, 0.3, 0.3, 0.3]

        features_tr_list = encoder.encode(sess, x_tr, conf.batch_size, norm=norm_type)[1:]
        features_te_list = encoder.encode(sess, x_te, conf.batch_size, norm=norm_type)[1:]
        weights = [0.3, 0.3, 0.4]

        model_list = []
        y_proba_list = []
        acc_tr = []
        acc_te = []
        for i in range(len(features_tr_list)):
            feat_tr = features_tr_list[i]
            feat_te = features_te_list[i]
            # train
            model = base.StandardClassifierDic[model_name]
            model.fit(feat_tr, y_tr)
            model_list.append(model)
            acc_tr.append(metrics.accuracy_score(y_tr, model.predict(feat_tr)))
            # predict
            y_proba = model.predict_proba(feat_te)
            y_proba_list.append(y_proba)
            y_pred = np.argmax(y_proba, axis=1)
            acc_te.append(metrics.accuracy_score(y_te, y_pred))
        # ensemble
        y_en = []
        for i in range(len(y_te)):
            y_proba = [y[i] for y in y_proba_list]
            y_proba_en = np.zeros(len(y_proba[0]))
            for y, w in zip(y_proba, weights):
                y_proba_en = y_proba_en + w*y
            label = np.argmax(y_proba_en)
            y_en.append(label)
        acc_en = metrics.accuracy_score(y_te, y_en)

        print(acc_tr)
        print(acc_te)
        print(acc_en)


def run_on_layers(data_name, dir_data, dir_gan):
    norm_type = 'tanh'
    model_name = 'LR'
    smooth_type = 'flat'
    tf.reset_default_graph()

    x_tr, y_tr, x_te, y_te, n_classes = ucr.load_ucr_flat(data_name, dir_data)
    x_tr = np.reshape(x_tr, x_tr.shape + (1, 1))
    x_te = np.reshape(x_te, x_te.shape + (1, 1))
    ## set up GAN
    dir_checkpoint = os.path.join(dir_gan, data_name, 'checkpoint')
    conf = Config(x_tr, '', '', x_tr.shape[1], x_tr.shape[2], x_tr.shape[3], state='test')
    gan = TSGAN(conf.dim_z, conf.dim_h, conf.dim_w, conf.dim_c, conf.random_seed,
                conf.g_lr, conf.d_lr, conf.g_beta1, conf.d_beta1, conf.gf_dim, conf.df_dim)
    ## start to run
    with tf.Session(config=tf_conf) as sess:
        isload, counter = gan.load(sess, dir_checkpoint)
        if not isload:
            raise Exception("[!] Train a model first, then run test mode")
        input_shape = [x_tr.shape[1], x_tr.shape[2], x_tr.shape[3]]

        encoder = EncoderGANLayerwise(gan, input_shape, type=smooth_type)
        features_tr_list = encoder.encode(sess, x_tr, conf.batch_size, norm=norm_type)
        features_te_list = encoder.encode(sess, x_te, conf.batch_size, norm=norm_type)

        # i_layer = 1
        # features_tr_list = features_tr_list[i_layer:]
        # features_te_list = features_te_list[i_layer:]

        model_list = []
        y_pred_list = []
        acc_tr = []
        acc_te = []
        for i in range(len(features_tr_list)):
            feat_tr = features_tr_list[i]
            feat_te = features_te_list[i]
            # train
            model = base.StandardClassifierDic[model_name]
            model.fit(feat_tr, y_tr)
            model_list.append(model)
            acc_tr.append(metrics.accuracy_score(y_tr, model.predict(feat_tr)))
            # predict
            y_pred = model.predict(feat_te)
            y_pred_list.append(y_pred)
            acc_te.append(metrics.accuracy_score(y_te, y_pred))

        print(acc_tr)
        print(acc_te)
        return acc_tr, acc_te


if __name__ == '__main__':
    dir_data = base.UCR_DIR
    tag = tag_path(os.path.abspath(__file__), 1)
    dir_out = 'cache/{}'.format(tag)
    if os.path.exists(dir_out) is False:
        os.makedirs(dir_out)

    # dir_gan = 'cache/tsgan/bn.tf.keras_batch16_kernel10/run0'
    dir_gan = 'cache/tsgan/bn.tf.keras_batch16_kernel10_adapt75/run3'
    data_name_list = ucr.get_data_name_list(dir_data)
    # data_name_list = ['ArrowHead']

    # data_name = 'ArrowHead'
    # run_majority_vote(data_name, dir_data, dir_gan)
    # run_probability_vote(data_name, dir_data, dir_gan)

    res = {'dataset':[]}
    n_dataset = len(data_name_list)
    for i, data_name in enumerate(data_name_list):
        print("[{}/{}] processing dataset {}: ".format(i, n_dataset, data_name))
        acc_tr, acc_te = run_on_layers(data_name, dir_data, dir_gan)
        if i == 0: # initialize
            for j in range(len(acc_te)):
                res['te_{}'.format(j)] = []
            for j in range(len(acc_tr)):
                res['tr_{}'.format(j)] = []
        res['dataset'].append(data_name)
        for j in range(len(acc_te)):
            res['te_{}'.format(j)].append(acc_te[j])
            res['tr_{}'.format(j)].append(acc_tr[j])
    df = pd.DataFrame(res)
    df.to_csv(os.path.join(dir_out, 'acc_each_layer_{}.csv'.format(os.path.basename(dir_gan))), index=False)








