"""
    Visualize presentation.
"""

from mlpy.lib.utils.base import tag_path
from mlpy.data import ucr
from mlpy.tsgan.dcganexp import base

import json
import numpy as np
import os
import matplotlib
matplotlib.use('Agg')
matplotlib.rc('xtick', labelsize=15)
matplotlib.rc('ytick', labelsize=15)
import matplotlib.pyplot as plt

SEED = 86
np.random.seed(SEED)

"""
    Plot manifold
"""

def plot_manifold(dir_in, dir_out, mode):
    x_tr = np.loadtxt(os.path.join(dir_in, 'tr{}'.format(mode)), delimiter=',')
    y_tr = np.loadtxt(os.path.join(dir_in, 'try'), delimiter=',')
    x_te = np.loadtxt(os.path.join(dir_in, 'te{}'.format(mode)), delimiter=',')
    y_te = np.loadtxt(os.path.join(dir_in, 'tey'), delimiter=',')

    dim = 2
    assert x_tr.shape[1] == dim and x_te.shape[1] == dim, \
        "dimension of tr{} or te{} do not equal to {}".format(x_tr.shape[0], x_te.shape[0], dim)

    plt.scatter(x_tr[:, 0], x_tr[:, 1], c=y_tr)
    plt.savefig(os.path.join(dir_out, 'tr_{}.png'.format(mode)))
    plt.clf()

    plt.scatter(x_te[:, 0], x_te[:, 1], c=y_te)
    plt.savefig(os.path.join(dir_out, 'te_{}.png'.format(mode)))
    plt.clf()

    x = np.vstack([x_tr, x_te])
    y = np.hstack([y_tr, y_te])
    plt.scatter(x[:, 0], x[:, 1], c=y)
    plt.savefig(os.path.join(dir_out, 'all_{}.png'.format(mode)))
    plt.clf()

def plot_manifold_tsgan(dir_out, data_name, mode):
    tag = 'tsgan'
    dir_in = 'cache/main_manifold/{}/{}'.format(data_name, tag)
    path_out = os.path.join(dir_out, 'manifold', data_name, tag)
    if os.path.exists(path_out) is False:
        os.makedirs(path_out)
    plot_manifold(dir_in, path_out, mode)

def plot_manifold_raw(dir_out, data_name, mode):
    tag = 'raw'
    dir_in = 'cache/main_manifold/{}/{}'.format(data_name, tag)
    path_out = os.path.join(dir_out, 'manifold', data_name, tag)
    if os.path.exists(path_out) is False:
        os.makedirs(path_out)
    plot_manifold(dir_in, path_out, mode)

def run_manifold(dir_out):
    data_name_list = ['FordA', 'Two_Patterns', 'Trace', 'synthetic_control']
    for data_name in data_name_list:
        plot_manifold_tsgan(dir_out, data_name, 'mds')
        plot_manifold_tsgan(dir_out, data_name, 'pca')
        plot_manifold_tsgan(dir_out, data_name, 'tsne')

    for data_name in data_name_list:
        plot_manifold_raw(dir_out, data_name, 'mds')
        plot_manifold_raw(dir_out, data_name, 'pca')
        plot_manifold_raw(dir_out, data_name, 'tsne')

"""
    Plot samples of tsgan
"""

def subplots(samples, out_path, linewidth=2.5):
    n = samples.shape[0]
    n_col = 2
    n_oneside = n // n_col

    f, axes = plt.subplots(n_oneside, n_col)
    plt.subplots_adjust(wspace=0)

    s = 0
    for r in range(axes.shape[0]-1):
        axl, axr = axes[r][0], axes[r][1]
        axl.plot(samples[s], linewidth=linewidth)
        plt.setp(axl.get_xticklabels(), visible=False)
        s += 1
        axr.plot(samples[s], linewidth=linewidth)
        axr.yaxis.tick_right()
        plt.setp(axr.get_xticklabels(), visible=False)
        s += 1
    r = axes.shape[0]-1
    axl, axr = axes[r][0], axes[r][1]
    axl.plot(samples[s], linewidth=linewidth)
    s +=1
    axr.plot(samples[s], linewidth=linewidth)
    axr.yaxis.tick_right()

    plt.savefig(out_path)
    plt.close(f)

def plot_tsgan(data_name, dir_data, dir_in, dir_out):
    path_in = os.path.join(dir_in, data_name)
    path_out = os.path.join(dir_out, 'tsgan', data_name)
    if os.path.exists(path_out) is False:
        os.makedirs(path_out)

    ## plot samples
    n_samples = 6
    # real samples
    x_tr, _, _, _, n_classes = ucr.load_ucr_flat(data_name, dir_data)
    samples_real = x_tr[:n_samples]
    subplots(samples_real, os.path.join(path_out, 'real.png'))
    # generated samples
    samples_fake = np.loadtxt(
        os.path.join(path_in, 'samples', 'train_0300'), delimiter=',')
    subplots(samples_fake, os.path.join(path_out, 'fake.png'))

    ## metrics
    with open(os.path.join(path_in, 'logs', 'metrics.json'), 'r') as f:
        metrics = json.load(f)
        g_loss = np.round(np.array(metrics['g_loss']).astype(np.float), 4)
        d_loss = np.round(np.array(metrics['d_loss']).astype(np.float), 4)
        d_loss_fake = np.round(np.array(metrics['d_loss_fake']).astype(np.float), 4)
        d_loss_real = np.round(np.array(metrics['d_loss_real']).astype(np.float), 4)
        nnd = np.round(np.array(metrics['nnd']).astype(np.float), 4)
        mmd = np.round(np.array(metrics['mmd']).astype(np.float), 4)
        t =np.round(np.array(metrics['time']).astype(np.float), 4)
    # loss
    fig = plt.figure()
    plt.plot(g_loss, linewidth=2.5, label='g_loss')
    plt.plot(d_loss, linewidth=2.5, label='d_loss')
    plt.legend(loc='best')
    plt.savefig(os.path.join(path_out, 'loss.png'))
    plt.close(fig)

    ## mmd
    freq_log = 20
    inds = range(0, len(mmd) * freq_log, freq_log)
    fig = plt.figure()
    plt.scatter(inds, mmd, linewidth=1.5)
    plt.plot(inds, mmd, linewidth=2.5, linestyle='-')
    plt.savefig(os.path.join(path_out, 'mmd.png'))
    plt.close(fig)

def run_tsgan(dir_data, dir_out):
    # dir_gan = 'cache/tsgan/bn.tf.keras_batch16_kernel10/run0'
    # dir_gan = 'cache/tsgan/bn.tf.keras_batch16_kernel10_adapt80/run2'
    dir_gan = 'cache/tsgan/bn.tf.keras_batch16_kernel10_adapt75/run0'
    # data_name_list = ['Wine', 'ChlorineConcentration', 'uWaveGestureLibrary_X',
    #                   'CinC_ECG_torso', 'OSULeaf', 'LargeKitchenAppliances',
    #                   'SonyAIBORobotSurfaceII']
    data_name_list = ucr.get_data_name_list(dir_data)
    for data_name in data_name_list:
        plot_tsgan(data_name, dir_data, dir_gan, dir_out)



if __name__ == '__main__':
    dir_data = base.UCR_DIR
    tag = tag_path(os.path.abspath(__file__), 1)
    dir_out = 'cache/{}'.format(tag)
    if os.path.exists(dir_out) is False:
        os.makedirs(dir_out)

    ### plot samples of tsgan
    # run_manifold(dir_out)
    run_tsgan(dir_data, dir_out)





