from mlpy.data import ucr

import os
import argparse
import pandas as pd

from mlpy.tsgan.dcganexp import main_gan

reduce_gan = main_gan.reduce_results
if __name__ == '__main__': # for a specific directory.
    parser = argparse.ArgumentParser(description=__file__)
    parser.add_argument(
        'path', type=str,
        help="the path to save results for gan under cache/")
    ARGS, unparsed = parser.parse_known_args()
    dir_log = os.path.join('cache', ARGS.path)
    data_name_list = ucr.get_data_name_list(dir_log)
    reduce_gan(data_name_list, dir_log)

if __name__ == '__main__': # batch version.
    dir_root = 'cache/main_gan_finetune'
    path_list = os.listdir(dir_root)
    for path in path_list:
        dir_log = os.path.join(dir_root, path)
        data_name_list = ucr.get_data_name_list(dir_log)
        reduce_gan(data_name_list, dir_log)



import pandas as pd
import os
def reduce_csv(dir_root):
    files = sorted([f for f in os.listdir(dir_root) if f.endswith('.csv')])
    for f in files:
        df = pd.read_csv(os.path.join(dir_root, f))
        print(f)
        print(df.mean())

import os
from mlpy.data import ucr

def del_checkpoint(dir_log):
    data_name_list = ucr.get_data_name_list(dir_log)
    for data_name in data_name_list:
        dir_check = os.path.join(dir_log, data_name, 'checkpoint')
        files = sorted(os.listdir(dir_check))
        if len(files) <= 4:
            continue
        files = files[1:-3]
        for f in files:
            os.remove(os.path.join(dir_check, f))

if __name__ == '__main__':
    dir_root = 'main_gan_finetune'
    for p in os.listdir(dir_root):
        del_checkpoint(os.path.join(dir_root, p))




