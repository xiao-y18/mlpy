from mlpy.lib.utils.base import tag_path
from mlpy.data import ucr, utils, ts
from mlpy.tsgan.dcganexp import base
from mlpy.tsgan.dcgan.model import TSGAN
from mlpy.tsgan.dcgan.config import Config
from mlpy.tsgan.dcganexp.main_encoder import EncoderGAN

import numpy as np
import os
import pandas as pd
import matplotlib
matplotlib.use('Agg')
matplotlib.rc('xtick', labelsize=15)
matplotlib.rc('ytick', labelsize=15)
import matplotlib.pyplot as plt
import tensorflow as tf
tf_conf = tf.ConfigProto()
tf_conf.gpu_options.allow_growth = True


def encode_on_batch(encoder, sess, X, batch_size):
    layer1, layer2 = encoder.get_features_local_max_detail() # fixed
    res1 = [[] for _ in range(len(layer1))]
    res2 = [[] for _ in range(len(layer2))]

    n_samples = len(X)
    n_batches = n_samples // batch_size
    for i in range(n_samples // batch_size):
        x_batch = X[i * batch_size:(i + 1) * batch_size]
        vals1, vals2 = sess.run([layer1, layer2], feed_dict={encoder.x: x_batch})
        for res, val in zip(res1, vals1):
            res.append(val)
        for res, val in zip(res2, vals2):
            res.append(val)
    n_samples_left = n_samples - n_batches * batch_size
    if n_samples_left > 0:
        x_left = X[-n_samples_left:]
        vals1, vals2 = sess.run([layer1, layer2], feed_dict={encoder.x: x_left})
        for res, val in zip(res1, vals1):
            res.append(val)
        for res, val in zip(res2, vals2):
            res.append(val)

    res1 = [np.vstack(r) for r in res1]
    res2 = [np.vstack(r) for r in res2]

    return res1, res2

def encode(data_name, dir_data, feature_type, dir_gan):
    tf.reset_default_graph()
    ## load data
    x_tr_2d, y_tr, _, _, n_classes = ucr.load_ucr_flat(data_name, dir_data)
    x_tr = np.reshape(x_tr_2d, x_tr_2d.shape + (1, 1))
    ## set up GAN
    dir_checkpoint = os.path.join(dir_gan, data_name, 'checkpoint')
    conf = Config(x_tr, '', '', x_tr.shape[1], x_tr.shape[2], x_tr.shape[3], state='test')
    gan = TSGAN(conf.dim_z, conf.dim_h, conf.dim_w, conf.dim_c, conf.random_seed,
                conf.g_lr, conf.d_lr, conf.g_beta1, conf.d_beta1, conf.gf_dim, conf.df_dim)
    ## start to run
    with tf.Session(config=tf_conf) as sess:
        isload, counter = gan.load(sess, dir_checkpoint)
        if not isload:
            raise Exception("[!] Train a model first, then run test mode")
        input_shape = [x_tr.shape[1], x_tr.shape[2], x_tr.shape[3]]
        encoder = EncoderGAN(gan, input_shape, type=feature_type)

        layer1, layer2 = encode_on_batch(encoder, sess, x_tr, conf.batch_size)

        return x_tr_2d, y_tr, layer1, layer2

def vis_fm_multiple(X_2d, y, d_fm_list, dir_out, tag="", i_channel=0, n_samples=2):
    """ plot feature maps in hidden layer where fist n_samples will be presented for each class.
    :param X_2d: 
    :param y: [0, n_classes-1]
    :param d_fm_list: 
    :param dir_out: 
    :return: 
    """
    colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k']
    n_colors = len(colors)
    n_classes = len(np.unique(y))
    if n_classes > n_colors:
        import warnings
        warnings.warn("Too classes to show. Such a bad visualization has been stopped.")
        return

    # construct data
    i_sample = 0
    rows = []
    convs = {}
    for i in np.arange(len(d_fm_list)):
        convs[i] = []
    classes, classes_count = np.unique(y, return_counts=True)
    classes = list(classes)
    # find a class with multiple samples for visualization.
    clas1 = None
    for cla, co in zip(classes, classes_count):
        if co > 1:
            clas1 = cla
            break
    group = y == clas1
    xc = X_2d[group]
    rows.append(xc[:n_samples])
    for i, h in enumerate(d_fm_list):
        hc = h[group, :, :, i_channel]
        convs[i].append(hc[:n_samples])
    for c in classes: # for each class excepting the first
        if c == clas1:
            continue
        group = y == c
        xc = X_2d[group]
        rows.append(xc[i_sample][np.newaxis, :])
        for i, h in enumerate(d_fm_list):
            hc = h[group, :, :, i_channel]
            convs[i].append(hc[i_sample][np.newaxis, :])

    # plot row data
    plt_lines = []
    plt_labels = []
    for c, samples in zip(classes, rows):
        line = None
        for s in samples:
            line, = plt.plot(s, '.-', color=colors[c])
        plt_lines.append(line)
        plt_labels.append('class-{}'.format(c))
    # plt.legend(plt_lines, plt_labels, loc='best')
    plt.savefig('{}/{}_rows.png'.format(dir_out, tag))
    plt.clf()

    # plot feature maps
    for l, vals in convs.items():
        plt_lines = []
        plt_labels = []
        for c, samples in zip(classes, vals):
            line = None
            for s in samples:
                line, = plt.plot(s, '.-', color=colors[c])
            plt_lines.append(line)
            plt_labels.append('class-{}'.format(c))
        # plt.legend(plt_lines, plt_labels, loc='best')
        plt.savefig("{}/{}_convs_{}.png".format(dir_out, tag, l))
        plt.clf()
    plt.close('all')

def distance(x, y):
    return np.mean(abs(x-y))

def vis_fm_dist(X_2d, y, d_fm_list, dir_out, tag="", i_channel = 0):
    colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k']
    n_colors = len(colors)
    n_classes = len(np.unique(y))
    if n_classes > n_colors:
        import warnings
        warnings.warn("Too classes to show. Such a bad visualization has been stopped.")
        return

    # construct data
    n_samples = 2
    i_sample = 0
    rows = []
    convs = {} # convs[l] correspond to layer l
    for l in np.arange(len(d_fm_list)):
        convs[l] = []
    classes, classes_count = np.unique(y, return_counts=True)
    classes = list(classes)
    # find a class with multiple samples for visualization.
    clas1 = None
    for cla, co in zip(classes, classes_count):
        if co > 1:
            clas1 = cla
            break
    classes.remove(clas1)
    classes.insert(0, clas1)
    group = y == clas1
    xc = X_2d[group]
    rows.append(xc[:n_samples])
    for l, h in enumerate(d_fm_list):
        hc = h[group, :, :, i_channel]
        convs[l].append(hc[:n_samples])
    for c in classes: # for each class excepting the first
        if c == clas1:
            continue
        group = y == c
        xc = X_2d[group]
        rows.append(xc[i_sample][np.newaxis, :])
        for l, h in enumerate(d_fm_list):
            hc = h[group, :, :, i_channel]
            convs[l].append(hc[i_sample][np.newaxis, :])

    ## distance for raw data
    raw_temp = rows[0][0]
    raw_dist = []
    dist0 = distance(rows[0][1], raw_temp)
    raw_dist.append(dist0)
    for i in np.arange(1, len(rows)): # for other classes
        dist = distance(rows[i], raw_temp)
        raw_dist.append(dist)
    ## distance for conv layer
    convs_dist = {}
    for l in convs.keys():
        convs_dist[l] = []
    for l, vals in convs.items():
        conv_temp = vals[0][0]
        dist0 = distance(vals[0][1], conv_temp)
        convs_dist[l].append(dist0)
        for c in np.arange(1, len(vals)):
            dist = distance(vals[c][0], conv_temp)
            convs_dist[l].append(dist)

    print(raw_dist)
    print(convs_dist)
    return raw_dist, convs_dist


def vis_fv_hist(X_2d, y, fv1, fv2, dir_out):
    fv = np.hstack([fv1, fv2])
    fv = ts.normalize(fv, 'tanh')
    distr_x = utils.distribute_dataset(X_2d, y)
    distr_fv1 = utils.distribute_dataset(fv1, y)
    distr_fv2 = utils.distribute_dataset(fv2, y)
    distr_fv = utils.distribute_dataset(fv, y)

    def hist(distr, path_out, n_samples=5):
        if os.path.exists(path_out) is False:
            os.makedirs(path_out)
        for c, x in distr.items():
            for i in range(min(n_samples, x.shape[0])):
                plt.hist(x[i])
                plt.savefig(os.path.join(path_out, '{}_{}'.format(c, i)))
                plt.close()
    hist(distr_x, os.path.join(dir_out, 'x'))
    hist(distr_fv1, os.path.join(dir_out, 'fv1'))
    hist(distr_fv2, os.path.join(dir_out, 'fv2'))
    hist(distr_fv, os.path.join(dir_out, 'fv'))

# if __name__ == '__main__':
#     dir_data = base.UCR_DIR
#     tag = tag_path(os.path.abspath(__file__), 1)
#     dir_out = 'cache/{}'.format(tag)
#     if os.path.exists(dir_out) is False:
#         os.makedirs(dir_out)
#
#     # dir_gan = 'cache/main_gan_half'
#     # data_name_list = ['ArrowHead']
#     dir_gan = 'cache/tsgan/bn.tf.keras_batch16_kernel10_adapt70/run0'
#     # data_name_list = ucr.get_data_name_list(dir_data)
#     data_name_list = ['DiatomSizeReduction', 'Strawberry']
#     for data_name in data_name_list:
#         x_2d, y_tr, layer1, layer2 = encode(data_name, dir_data, 'local-max', dir_gan)
#         path_out = os.path.join(dir_out, data_name)
#         if os.path.exists(path_out) is False:
#             os.makedirs(path_out)
#         vis_fm_dist(x_2d, y_tr, layer1[:-2], path_out, tag='layer1')
#         vis_fm_dist(x_2d, y_tr, layer2[:-2], path_out, tag='layer2')

if __name__ == '__main__':
    dir_data = base.UCR_DIR
    tag = tag_path(os.path.abspath(__file__), 1)
    dir_out = 'cache/{}/hist'.format(tag)

    # dir_gan = 'cache/main_gan_half'
    # data_name_list = ['ArrowHead']
    dir_gan = 'cache/tsgan/bn.tf.keras_batch16_kernel10_adapt70/run0'
    # data_name_list = ucr.get_data_name_list(dir_data)
    data_name_list = ['DiatomSizeReduction', 'Strawberry']
    for data_name in data_name_list:
        x_2d, y_tr, layer1, layer2 = encode(data_name, dir_data, 'local-max', dir_gan)
        path_out = os.path.join(dir_out, data_name)
        if os.path.exists(path_out) is False:
            os.makedirs(path_out)
        vis_fv_hist(x_2d, y_tr, layer1[-1],layer2[-1], path_out)


