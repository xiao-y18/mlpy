"""
    Compare different settings for TSGAN based on the performances of the cascading classifiers.
"""
import pandas as pd
import numpy as np
import os
import shutil

if __name__ == '__main__':
    dir_root = 'cache/results/main_gan_finetune'
    dir_run = os.path.join(dir_root, 'runs')
    n_runs = 3

    ## average the results of several runs
    dir_avg = os.path.join(dir_root, 'avg')
    # subdirs = np.array(sorted(os.listdir(dir_run)))
    # prefix_runs = subdirs[np.arange(0, len(subdirs), n_runs)]
    # for prefix in prefix_runs:
    #     dir_out = os.path.join(dir_avg, prefix)
    #     if os.path.exists(dir_out):
    #         shutil.rmtree(dir_out)
    #     os.makedirs(dir_out)
    #     csvfiles = [f for f in os.listdir(os.path.join(dir_run, prefix)) if f.endswith('.csv')]
    #     for f in csvfiles:
    #         df0 = pd.read_csv(os.path.join(dir_run, prefix, f))
    #         cols = list(df0.columns)
    #         cols.remove('dataset')
    #         for i in range(1, n_runs):
    #             df = pd.read_csv(os.path.join(dir_run, prefix+"_{}".format(i), f))
    #             df0[cols] = df0[cols] + df[cols]
    #         df0[cols] = df0[cols] / n_runs
    #         df0.to_csv(os.path.join(dir_out, f), index=False)

    ## concat avg results for different options
    dir_avg2 = os.path.join(dir_root, 'avg2')
    # subdirs = sorted(os.listdir(dir_avg))
    # csv_gan_metrics = 'metrics_train.csv'
    # csvfiles = [f for f in os.listdir(os.path.join(dir_avg, subdirs[0]))
    #             if f.endswith('.csv') and f != csv_gan_metrics]
    # # concat accuracies
    # for f in csvfiles: # for each csv files
    #     df = pd.DataFrame()
    #     for i, s in enumerate(subdirs): # for each options
    #         df_unit = pd.read_csv(os.path.join(dir_avg, s, f))
    #         if i == 0:
    #             df['dataset'] = df_unit['dataset']
    #         df[s] = df_unit['acc_te']
    #     df.to_csv(os.path.join(dir_avg2, f), index=False)
    # # concat gan's metrics
    # df_mmd = pd.DataFrame()
    # df_nnd = pd.DataFrame()
    # for i, s in enumerate(subdirs):
    #     df_unit = pd.read_csv(os.path.join(dir_avg, s, csv_gan_metrics))
    #     if i == 0:
    #         df_mmd['dataset'] = df_unit['dataset']
    #         df_nnd['dataset'] = df_unit['dataset']
    #     df_mmd[s] = df_unit['mmd']
    #     df_nnd[s] = df_unit['nnd']
    # df_mmd.to_csv(os.path.join(dir_avg2, 'mmd.csv'), index=False)
    # df_nnd.to_csv(os.path.join(dir_avg2, 'nnd.csv'), index=False)

    ## analysis
    dir_avg3 = os.path.join(dir_root, 'avg3')
    csvs_gan_metrics = ['mmd.csv', 'nnd.csv']
    csvs_clf = sorted(
        [f for f in os.listdir(dir_avg2) if f.endswith('.csv') and f not in csvs_gan_metrics])
    # # compute mean
    # df = pd.read_csv(os.path.join(dir_avg2, csvs_clf[0]))
    # cols = list(df.columns)
    # cols.remove('dataset')
    # df_mean = pd.DataFrame(columns=['name']+cols)
    # for f in csvs_clf:
    #     df = pd.read_csv(os.path.join(dir_avg2, f))
    #     agg = df.mean().to_dict()
    #     agg['name'] = f.split('.')[0]
    #     ind = df_mean.shape[0]
    #     for k, v in agg.items():
    #         df_mean.loc[ind, k] = v
    # df_mean.to_csv(os.path.join(dir_avg3, 'reduced_mean.csv'), index=False)
    # compare different normalization for tsgan
    csv_clf = 'LR_local-max_tanh.csv'
    csv_mmd = 'mmd.csv'
    csv_nnd = 'nnd.csv'
    settings = ['bn.tf.keras', 'g.bn.tf.keras_d.ln', 'in', 'ln']
    df_acc = pd.read_csv(os.path.join(dir_avg2, csv_clf))
    df_dataset = df_acc['dataset']
    df_acc = df_acc[settings]
    df_mmd = pd.read_csv(os.path.join(dir_avg2, csv_mmd))
    df_mmd = df_mmd[settings]
    df_nnd = pd.read_csv(os.path.join(dir_avg2, csv_nnd))
    df_nnd = df_nnd[settings]
    def max_name(x):
        inds = np.argsort(x)
        argmax = inds[-1]
        return settings[argmax]
    def first_second_acc(x):
        xs = np.sort(x)
        return (xs[-1]-xs[-2])*100
    df_acc['max'] = df_acc.max(axis=1)
    df_acc['max_name'] = df_acc[settings].apply(max_name, axis=1)
    df_acc['diff'] = df_acc[settings].apply(first_second_acc, axis=1)
    def min_name(x):
        inds = np.argsort(x)
        return settings[inds[0]]
    def first_second_min(x):
        xs = np.sort(x)
        return (xs[0] - xs[1])
    df_mmd['min'] = df_mmd.min(axis=1)
    df_mmd['min_name'] = df_mmd[settings].apply(min_name, axis=1)
    df_mmd['diff'] = df_mmd[settings].apply(first_second_min, axis=1)
    df_nnd['min'] = df_nnd.min(axis=1)
    df_nnd['min_name'] = df_nnd[settings].apply(min_name, axis=1)
    df_nnd['diff'] = df_nnd[settings].apply(first_second_min, axis=1)
    df_comp = pd.DataFrame()
    df_comp['acc=mmd'] = df_acc['max_name'] == df_mmd['min_name']
    df_comp['acc=mmd'] = df_comp['acc=mmd'].astype(np.int)
    df_comp['acc=nnd'] = df_acc['max_name'] == df_nnd['min_name']
    df_comp['acc=nnd'] = df_comp['acc=nnd'].astype(np.int)
    df_reduced = pd.DataFrame()
    df_reduced['dataset'] = df_dataset
    for col in df_acc.columns:
        df_reduced['acc_{}'.format(col)] = df_acc[col]
    for col in df_mmd.columns:
        df_reduced['mmd_{}'.format(col)] = df_mmd[col]
    for col in df_nnd.columns:
        df_reduced['nnd_{}'.format(col)] = df_nnd[col]
    for col in df_comp.columns:
        df_reduced[col] = df_comp[col]
    df_reduced.to_csv(os.path.join(dir_avg3, 'reduced_diff_tsgan_norms.csv'), index=False)


























