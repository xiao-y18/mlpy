from mlpy.lib.utils.base import tag_path
from mlpy.data import ucr
from mlpy.tsgan.dcganexp import base
from mlpy.tsgan.dcgan.model import TSGAN
from mlpy.tsgan.dcgan.config import Config
from mlpy.tsgan.dcganexp.main_encoder import EncoderGAN

import numpy as np
import os
import pandas as pd
import matplotlib
matplotlib.use('Agg')
matplotlib.rc('xtick', labelsize=15)
matplotlib.rc('ytick', labelsize=15)
import matplotlib.pyplot as plt
import tensorflow as tf
tf_conf = tf.ConfigProto()
tf_conf.gpu_options.allow_growth = True


def encode_on_batch(encoder, sess, X, batch_size):
    layer1, layer2 = encoder.get_features_local_max_detail() # fixed
    res1 = [[] for _ in range(len(layer1))]
    res2 = [[] for _ in range(len(layer2))]

    n_samples = len(X)
    n_batches = n_samples // batch_size
    for i in range(n_samples // batch_size):
        x_batch = X[i * batch_size:(i + 1) * batch_size]
        vals1, vals2 = sess.run([layer1, layer2], feed_dict={encoder.x: x_batch})
        for res, val in zip(res1, vals1):
            res.append(val)
        for res, val in zip(res2, vals2):
            res.append(val)
    n_samples_left = n_samples - n_batches * batch_size
    if n_samples_left > 0:
        x_left = X[-n_samples_left:]
        vals1, vals2 = sess.run([layer1, layer2], feed_dict={encoder.x: x_left})
        for res, val in zip(res1, vals1):
            res.append(val)
        for res, val in zip(res2, vals2):
            res.append(val)

    res1 = [np.vstack(r) for r in res1]
    res2 = [np.vstack(r) for r in res2]

    return res1, res2

def encode(data_name, dir_data, feature_type, dir_gan):
    tf.reset_default_graph()
    ## load data
    x_tr_2d, y_tr, _, _, n_classes = ucr.load_ucr_flat(data_name, dir_data)
    x_tr = np.reshape(x_tr_2d, x_tr_2d.shape + (1, 1))
    ## set up GAN
    dir_checkpoint = os.path.join(dir_gan, data_name, 'checkpoint')
    conf = Config(x_tr, '', '', x_tr.shape[1], x_tr.shape[2], x_tr.shape[3], state='test')
    gan = TSGAN(conf.dim_z, conf.dim_h, conf.dim_w, conf.dim_c, conf.random_seed,
                conf.g_lr, conf.d_lr, conf.g_beta1, conf.d_beta1, conf.gf_dim, conf.df_dim)
    ## start to run
    with tf.Session(config=tf_conf) as sess:
        isload, counter = gan.load(sess, dir_checkpoint)
        if not isload:
            raise Exception("[!] Train a model first, then run test mode")
        input_shape = [x_tr.shape[1], x_tr.shape[2], x_tr.shape[3]]
        encoder = EncoderGAN(gan, input_shape, type=feature_type)

        layer1, layer2 = encode_on_batch(encoder, sess, x_tr, conf.batch_size)

        return x_tr_2d, y_tr, layer1, layer2

def vis_fm_unit(X_2d, y, d_fm_list, dir_out, tag="", i_channel=0, i_sample=0):
    # construct data
    rows = []
    convs = {}
    for i in np.arange(len(d_fm_list)):
        convs[i] = []
    for c in np.unique(y):
        group = y == c
        xc = X_2d[group]
        rows.append(xc[i_sample])
        for i, h in enumerate(d_fm_list):
            hc = h[group, :, :, i_channel]
            convs[i].append(hc[i_sample])

    # plot row data
    for i, val in enumerate(rows):
        plt.plot(val, '.-', label='class-{}'.format(i))
    plt.legend(loc='best')
    plt.savefig('{}/{}_rows.png'.format(dir_out, tag))
    plt.clf()

    # plot feature maps
    for i, vals in convs.items():
        for ii, val in enumerate(vals):
            plt.plot(val, '.-', label='class-{}'.format(ii))
        plt.legend(loc='best')
        plt.savefig("{}/{}_convs_{}.png".format(dir_out, tag, i))
        plt.clf()

    plt.close('all')


def vis_fm_multiple(X_2d, y, d_fm_list, dir_out, tag="", i_channel=0, n_samples=2):
    """ plot feature maps in hidden layer where fist n_samples will be presented for each class.
    :param X_2d: 
    :param y: [0, n_classes-1]
    :param d_fm_list: 
    :param dir_out: 
    :return: 
    """
    colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k']
    n_colors = len(colors)
    n_classes = len(np.unique(y))
    if n_classes > n_colors:
        import warnings
        warnings.warn("Too classes to show. Such a bad visualization has been stopped.")
        return

    # construct data
    i_sample = 0
    rows = []
    convs = {} # convs[l] correspond to layer l
    for l in np.arange(len(d_fm_list)):
        convs[l] = []
    classes, classes_count = np.unique(y, return_counts=True)
    classes = list(classes)
    # find a class with multiple samples for visualization.
    clas1 = None
    for cla, co in zip(classes, classes_count):
        if co > 1:
            clas1 = cla
            break
    classes.remove(clas1)
    classes.insert(0, clas1)
    group = y == clas1
    xc = X_2d[group]
    rows.append(xc[:n_samples])
    for l, h in enumerate(d_fm_list):
        hc = h[group, :, :, i_channel]
        convs[l].append(hc[:n_samples])
    for c in classes: # for each class excepting the first
        if c == clas1:
            continue
        group = y == c
        xc = X_2d[group]
        rows.append(xc[i_sample][np.newaxis, :])
        for l, h in enumerate(d_fm_list):
            hc = h[group, :, :, i_channel]
            convs[l].append(hc[i_sample][np.newaxis, :])

    # plot row data
    plt_lines = []
    plt_labels = []
    for c, samples in zip(classes, rows):
        line = None
        for s in samples:
            line, = plt.plot(s, '.-', color=colors[c])
        plt_lines.append(line)
        plt_labels.append('class-{}'.format(c))
    # plt.legend(plt_lines, plt_labels, loc='best')
    plt.savefig('{}/{}_rows.png'.format(dir_out, tag))
    plt.clf()

    # plot feature maps
    for l, vals in convs.items():
        plt_lines = []
        plt_labels = []
        for c, samples in zip(classes, vals):
            line = None
            for s in samples:
                line, = plt.plot(s, '.-', color=colors[c])
            plt_lines.append(line)
            plt_labels.append('class-{}'.format(c))
        # plt.legend(plt_lines, plt_labels, loc='best')
        plt.savefig("{}/{}_convs_{}.png".format(dir_out, tag, l))
        plt.clf()
    plt.close('all')

if __name__ == '__main__':
    dir_data = base.UCR_DIR
    tag = tag_path(os.path.abspath(__file__), 1)
    dir_out = 'cache/{}'.format(tag)
    if os.path.exists(dir_out) is False:
        os.makedirs(dir_out)

    # dir_gan = 'cache/main_gan_half'
    # data_name = 'ArrowHead'
    # dir_gan = 'cache/tsgan/bn.tf.keras_batch16_kernel10/run0'
    dir_gan = 'cache/tsgan/bn.tf.keras_batch16_kernel10_adapt75/run0'
    # data_name_list = ucr.get_data_name_list(dir_data)
    data_name_list = ['DiatomSizeReduction', 'Strawberry']
    for data_name in data_name_list:
        x_2d, y_tr, layer1, layer2 = encode(data_name, dir_data, 'local-max', dir_gan)
        for i_channel in range(1, 64):
            path_out = os.path.join(dir_out, data_name, '{}'.format(str(i_channel).zfill(2)))
            if os.path.exists(path_out) is False:
                os.makedirs(path_out)
            vis_fm_multiple(x_2d, y_tr, layer1[:-2], path_out, tag='layer1', i_channel=i_channel)
            vis_fm_multiple(x_2d, y_tr, layer2[:-2], path_out, tag='layer2', i_channel=i_channel)




