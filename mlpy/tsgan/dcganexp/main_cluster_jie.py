"""
    From Jie
"""
import numpy as np
from sklearn.cluster import KMeans
from sklearn.metrics.cluster import adjusted_rand_score

from mlpy.data import ucr
from mlpy.data import ts
from mlpy.data import utils
from mlpy.tsgan.dcganexp import base

DATA_ROOT = base.UCR_DIR

def prepare_dataset(filename):
    datasets = ucr.load_ucr(filename, DATA_ROOT, one_hot=False)
    X_train = datasets.train.X
    y_train = datasets.train.y
    X_test = datasets.test.X
    y_test = datasets.test.y

    distr = utils.distribute_dataset(X_train, y_train)

    X = np.concatenate((X_train, X_test), axis=0)
    y = np.concatenate((y_train, y_test), axis=0)

    y = y.ravel()

    return X, y, len(distr.keys())


def normalization(X):
    X_mean = np.mean(X, axis=0)
    X_std = np.std(X, axis=0)
    X_norm = (X - X_mean) / X_std

    return X_norm

def rand_index(y_actual, y_pred):
    n = len(y_actual)
    tp, tn, fp, fn = 0, 0, 0, 0

    for i in range(n):
        for j in range(i, n):
            if y_actual[i] == y_actual[j] and y_pred[i] == y_pred[j]:
                tp += 1
            elif y_actual[i] == y_actual[j] and y_pred[i] != y_pred[j]:
                fn += 1
            elif y_actual[i] != y_actual[j] and y_pred[i] == y_pred[j]:
                fp += 1
            elif y_actual[i] != y_actual[j] and y_pred[i] != y_pred[j]:
                tn += 1

    return (tp + tn) / (tp + fp + fn + tn)


def test(filename):
    X, y, n_clusters = prepare_dataset(filename)
    # X_norm = normalization(X)
    # X_norm = ts.normalize(X, mode='znorm')
    X_norm = X

    y_pred = KMeans(n_clusters=n_clusters).fit_predict(X_norm)

    print(filename, n_clusters, rand_index(y, y_pred), adjusted_rand_score(y, y_pred))


filenames = ['Trace', 'synthetic_control', 'Gun_Point', 'ECG200', 'ECG5000', 'ECGFiveDays']

for f in filenames:
    test(f)


"""
    The result accord with the Time Series ED listed in Table VI of the paper 
    "Zakaria, Jesin, Abdullah Mueen, and Eamonn Keogh. "Clustering time series using unsupervised-shapelets." 2012 IEEE 12th International Conference on Data Mining. IEEE, 2012."
"""

"""
no znorm  (all data are normalize, this results can not be derived by Jie, maybe her datasets are too old.)
Trace 4 0.7524875621890548 0.3282213854982266
synthetic_control 6 0.870942872989462 0.61718034412851
Gun_Point 2 0.5024875621890548 -0.005050505050504977
ECG200 2 0.6219402985074627 0.21935545792004507
ECG5000 5 0.7541731653669266 0.4933371309030899
CGFiveDays 2 0.5013037809647979 0.00034732532830314124

znorm, axis=1
Trace 4 0.7524875621890548 0.3282213854982266
synthetic_control 6 0.870942872989462 0.61718034412851
Gun_Point 2 0.5024875621890548 -0.005050505050504977
ECG200 2 0.6219402985074627 0.21935545792004507
ECG5000 5 0.7541731653669266 0.4933371309030899
ECGFiveDays 2 0.5013037809647979 0.00034732532830314124

znorm, axis=0
Trace 4 0.755273631840796 0.3381417982148616
synthetic_control 6 0.870942872989462 0.61718034412851
Gun_Point 2 0.5024875621890548 -0.005050505050504977
ECG200 2 0.6370149253731343 0.2526232419540319
ECG5000 5 0.7596837432513497 0.5057704482974512
ECGFiveDays 2 0.5222026229005292 0.04232782568555566
"""