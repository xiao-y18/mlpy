import numpy as np
import os
import matplotlib
matplotlib.use('Agg')
# matplotlib.rcParams['font.family'] = 'sans-serif'
# matplotlib.rcParams['font.sans-serif'] = 'Arial'
# font = {'family' : 'normal',
#         'weight' : 'bold',
#         'size'   : 10}
# matplotlib.rc('font', **font)
# matplotlib.rc('xtick', labelsize=15)
# matplotlib.rc('ytick', labelsize=15)
import matplotlib.pyplot as plt

size=15
params = {'legend.fontsize': 'large',
          'axes.labelsize': size,
          'axes.titlesize': size,
          'xtick.labelsize': size*0.75,
          'ytick.labelsize': size*0.75,
          'axes.titlepad': 25}
plt.rcParams.update(params)

def plot(ratios, resnet, fcn, lr_tsgan, lsvc_tsgan, data_name, dir_out):
    inds = range(len(ratios))

    plt.plot(inds, resnet, linewidth=2.0, marker='o', label='ResNet')
    plt.plot(inds, fcn, linewidth=2.0, marker='D', label='ResNet')
    plt.plot(inds, lr_tsgan, linewidth=2.0, marker='s', label='LR-TSGAN')
    plt.plot(inds, lsvc_tsgan, linewidth=2.0, marker='^', label='LSVC-TSGAN')

    plt.xticks(inds, ratios)
    plt.ylim((min(min(resnet), min(fcn), min(lr_tsgan), min(lsvc_tsgan)) - 0.1), 1.0)
    plt.ylabel('Accuracy')
    plt.xlabel('Ratio of samples to the training split')
    plt.grid(True)
    plt.legend(loc='best')
    plt.savefig(os.path.join(dir_out, '{}.png'.format(data_name)))
    plt.close('all')


def plot_FordA(dir_out):
    data_name = 'FordA'
    ratios = [1.0, 0.8, 0.6, 0.4, 0.2, 0.15, 0.1, 0.05, 0.03, 0.01]
    resnet = [0.918383782, 0.914968064, 0.89583449, 0.909108581, 0.912024438, 0.873646209,
              0.863093585, 0.889336295, 0.848569842, 0.512468759]
    fcn = [0.889808387, 0.877117467, 0.86098306, 0.886003888, 0.866537073, 0.87817273, 0.828769786,
           0.784920855, 0.752901972, 0.474895862]
    lr_tsgan = [0.936545404, 0.934434879, 0.931213552, 0.928519856, 0.9217995, 0.917134129,
             0.907220217, 0.873090808, 0.816078867, 0.737156345]
    lsvc_tsgan = [0.933212996, 0.930186059, 0.927520133, 0.925409608, 0.920077756,
                  0.915440156, 0.906748126, 0.877034157, 0.82599278, 0.745709525]
    plot(ratios, resnet, fcn, lr_tsgan, lsvc_tsgan, data_name, dir_out)

def plot_wafer(dir_out):
    data_name = 'wafer'
    ratios = [1.0, 0.8, 0.6, 0.4, 0.2, 0.15, 0.1, 0.05, 0.03, 0.01]
    resnet = [0.997680078, 0.87279364, 0.995846853, 0.992326411, 0.987589228, 0.917602206,
              0.889016872,0.937832576, 0.9016061, 0.892115509]
    fcn = [0.972160934, 0.984393251, 0.984003894, 0.966028553, 0.964179104, 0.86440623, 0.798604802,
           0.872404283, 0.880986372, 0.892310188]
    lr_tsgan = [0.996398443, 0.995619728, 0.994516548, 0.992780661, 0.990103829, 0.989828034,
             0.979737183, 0.977725503, 0.979201817, 0.934149903]
    lsvc_tsgan = [0.996106424, 0.995635951, 0.994467878, 0.992910448, 0.990752758, 0.990493186,
                  0.98247891, 0.981408177, 0.984620376, 0.950194679]
    plot(ratios, resnet, fcn, lr_tsgan, lsvc_tsgan, data_name, dir_out)



if __name__ == '__main__':
    dir_out = 'cache/semi-supervised'
    plot_FordA(dir_out)
    plot_wafer(dir_out)

