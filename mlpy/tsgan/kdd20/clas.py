from mlpy.data.ucr.const import DATASETS
from mlpy.lib.metrics.comparing import wilcoxon, rank, compute_CD, graph_ranks

import pandas as pd
import os
import numpy as np

def get_subset(df, subdataset):
    datasetall = list(df['dataset'].values)
    inds = []
    for dataset in subdataset:
        inds.append(datasetall.index(dataset))
    inds = np.sort(inds)
    subdf = df.loc[inds, :]
    subdf.reset_index(drop=True, inplace=True)
    return subdf

def reduce_tsgan(dir_in, tag_tsgan, fname_tsgan, decimals=4):
    path_tsgan = os.path.join(dir_in, 'tsgan', tag_tsgan)
    n_runs = len([f for f in os.listdir(path_tsgan)
                  if os.path.isdir(os.path.join(path_tsgan, f))])
    df_tsgan = pd.DataFrame()
    for i in range(n_runs):
        df = pd.read_csv(os.path.join(path_tsgan, 'run{}'.format(i), fname_tsgan))
        cols = list(df.columns)
        if i == 0:
            for c in cols:
                df_tsgan[c] = df[c]
                df_tsgan[c] = df[c]
        else:
            cols.remove('dataset')
            for c in cols:
                df_tsgan[c] += df[c]
    cols = list(df_tsgan.columns)
    cols.remove('dataset')
    for c in cols:
        df_tsgan[c] = df_tsgan[c] / n_runs
    df_tsgan = df_tsgan.round(decimals)
    df_tsgan.to_csv(os.path.join(path_tsgan, '{}_mean.csv'.format(fname_tsgan.split('_')[0])),
                    index=False)

def reduce_raw(dir_in, fname_model, decimals=4):
    path_in = os.path.join(dir_in, 'tsgan', 'raw')
    n_runs = len([f for f in os.listdir(path_in)
                  if os.path.isdir(os.path.join(path_in, f))])
    df_raw = pd.DataFrame()
    for i in range(n_runs):
        df = pd.read_csv(os.path.join(path_in, 'run{}'.format(i), fname_model))
        cols = list(df.columns)
        if i == 0:
            for c in cols:
                df_raw[c] = df[c]
                df_raw[c] = df[c]
        else:
            cols.remove('dataset')
            for c in cols:
                df_raw[c] += df[c]
    cols = list(df_raw.columns)
    cols.remove('dataset')
    for c in cols:
        df_raw[c] = df_raw[c] / n_runs
    df_raw = df_raw.round(decimals)
    df_raw.to_csv(os.path.join(path_in, '{}_mean.csv'.format(fname_model.split('.')[0])),
                    index=False)


def single_statistics(df_dic, dir_out, tag):
    path_out = os.path.join(dir_out, 'single_statistics')
    if os.path.exists(path_out) is False:
        os.makedirs(path_out)
    fout = open(os.path.join(path_out, tag), 'w')
    for name, df in df_dic.items():
        fout.write("****** {}: \n".format(name))
        describe = df.describe()
        fout.write(describe.to_string())
        fout.write("\n\n")

def pair_comparisons(name1, df1, name2, df2, dir_out):
    assert df1.shape[0] >= df2.shape[0], \
        'length of df1:{} should be larger than df2:{}!'.format(df1.shape[0], df2.shape[0])

    datasets = df2['dataset'].values
    df1_sub = get_subset(df1, datasets)
    values1 = np.round(df1_sub['acc'].values, 4)
    values2 = np.round(df2['acc'].values, 4)

    fout = open(os.path.join(dir_out, 'compare_{}2{}'.format(name1, name2)), 'w')
    fout.write("****** The results of comparing {} to {}: \n\n".format(name1, name2))
    fout.write("Counts: {}\n\n".format(datasets.shape[0]))

    datasets_win = []
    datasets_loss = []
    datasets_tie = []
    for dataset, val1, val2 in zip(datasets, values1, values2):
        if val1 > val2:
            datasets_win.append(dataset)
        elif val1 < val2:
            datasets_loss.append(dataset)
        else:
            datasets_tie.append(dataset)
    fout.write("#Win: {}\n".format(len(datasets_win)))
    fout.write("#Loss: {}\n".format(len(datasets_loss)))
    fout.write("#Tie: {}\n".format(len(datasets_tie)))
    fout.write("\n")
    fout.write("Win on these datasets: {} \n".format(",".join(datasets_win)))
    fout.write("Loss on these datasets: {} \n".format(",".format(datasets_loss)))
    fout.write("Tie on these datasets: {} \n".format(",".format(datasets_tie)))
    fout.write("\n")

    statistic, pvalue = wilcoxon(values1, values2)
    fout.write("The result of wilcoxon test: \n")
    fout.write("statistic: {}\n".format(statistic))
    fout.write("pvalue: {}\n".format(pvalue))
    fout.write("\n")

def global_comparisons(df_dic, dir_out):
    interset = set(DATASETS)
    for name, df in df_dic.items():
        datasets = set(df['dataset'].values)
        interset = interset.intersection(datasets)

    interset = list(interset)
    df_dic_inter = {}
    for name, df in df_dic.items():
        df_dic_inter[name] = get_subset(df, interset).round(4)

    df_acc = pd.DataFrame()
    model_name_list = []
    for name, df in df_dic_inter.items():
        if df_acc.empty:
            df_acc['dataset'] = df['dataset']
        df_acc[name] = df['acc']
        model_name_list.append(name)
    df_acc.to_csv(os.path.join(dir_out, 'acc.csv'), index=False)
    df_acc_des = df_acc.describe()
    df_acc_des.to_csv(os.path.join(dir_out, 'acc_describe.csv'))

    ranks_list = df_acc[model_name_list].apply(rank, axis=1)
    rank_dic = {}
    for m in model_name_list:
        rank_dic[m] = []
    for ranks in ranks_list:
        for m, r in zip(model_name_list, ranks):
            rank_dic[m].append(r)
    df_rank = pd.DataFrame(rank_dic)
    df_rank['dataset'] = df_acc['dataset']
    df_rank = df_rank[['dataset'] + model_name_list]
    df_rank.to_csv(os.path.join(dir_out, 'rank.csv'), index=False)
    df_rank_des = df_rank.describe()
    df_rank_des.to_csv(os.path.join(dir_out, 'rank_describe.csv'))

    avranks = df_rank[model_name_list].mean()
    cd = compute_CD(avranks, df_rank.shape[0], alpha="0.05", test="nemenyi")
    graph_ranks(avranks, model_name_list, cd=cd, width=6, textspace=1.5,
                filename=os.path.join(dir_out, 'fredman_avrank'))


def mkdirs(path):
    if os.path.exists(path) is False:
        os.makedirs(path)

if __name__ == '__main__':
    DIR_IN = 'cache/clas'
    DIR_OUT = 'cache/clas/results'
    if os.path.exists(DIR_OUT) is False:
        os.makedirs(DIR_OUT)

    ### baselines
    ## single models
    dir_baselines = os.path.join(DIR_IN, 'baselines')
    df_dtw = pd.read_csv(os.path.join(dir_baselines, 'UCR2015', 'dtw.csv'))
    df_wdtw = pd.read_csv(os.path.join(dir_baselines, 'singleTrainTestBakeOff17', 'WDTW_1NN.csv'))
    df_ciddtw = pd.read_csv(os.path.join(dir_baselines, 'singleTrainTestBakeOff17', 'CID_DTW.csv'))
    df_ls = pd.read_csv(os.path.join(dir_baselines, 'singleTrainTestBakeOff17', 'LS.csv'))
    df_tsf = pd.read_csv(os.path.join(dir_baselines, 'singleTrainTestBakeOff17', 'TSF.csv'))
    df_tsbf = pd.read_csv(os.path.join(dir_baselines, 'singleTrainTestBakeOff17', 'TSBF.csv'))
    df_dtwf = pd.read_csv(os.path.join(dir_baselines, 'singleTrainTestBakeOff17', 'DTW_F.csv'))
    df_msm = pd.read_csv(os.path.join(dir_baselines, 'singleTrainTestBakeOff17', 'MSM_1NN.csv'))
    df_lps = pd.read_csv(os.path.join(dir_baselines, 'singleTrainTestBakeOff17', 'LPS.csv'))
    df_dtdc = pd.read_csv(os.path.join(dir_baselines, 'singleTrainTestBakeOff17', 'DTD_C.csv'))
    df_ddtw = pd.read_csv(os.path.join(dir_baselines, 'singleTrainTestBakeOff17', 'DD_DTW.csv'))
    df_rotf = pd.read_csv(os.path.join(dir_baselines, 'singleTrainTestBakeOff17', 'RotF.csv'))
    df_twe = pd.read_csv(os.path.join(dir_baselines, 'singleTrainTestBakeOff17', 'TWE_1NN.csv'))
    df_dic_single = {
        # 'ED': df_ed,
        'DTW': df_dtw,
        'WDTW': df_wdtw,
        'CID-DTW': df_ciddtw,
        'LS': df_ls,
        'TSF': df_tsf,
        'TSBF': df_tsbf,
        'DTWF': df_dtwf,
        'MSM': df_msm,
        'LPS': df_lps,
        'DTDC': df_dtdc,
        'DDTW': df_ddtw,
        'RotF': df_rotf,
        'TWE': df_twe
    }

    ## ensemble models
    df_prop = pd.read_csv(os.path.join(dir_baselines, 'singleTrainTestBakeOff17', 'EE.csv'))
    df_boss = pd.read_csv(os.path.join(dir_baselines, 'singleTrainTestBakeOff17', 'BOSS.csv'))
    df_st = pd.read_csv(os.path.join(dir_baselines, 'singleTrainTestBakeOff17', 'ST.csv'))
    df_flatcote = pd.read_csv(os.path.join(dir_baselines, 'singleTrainTestBakeOff17', 'Flat-COTE.csv'))
    df_hivecote = pd.read_csv(os.path.join(dir_baselines, 'singleTrainTestBakeOff17', 'HIVE-COTE.csv'))
    df_dic_ensemble = {
        'PROP': df_prop,
        'BOSS': df_boss,
        'ST': df_st,
        'Flat-COTE': df_flatcote,
        'Hive-COTE': df_hivecote}

    ## nn models
    df_encoder = pd.read_csv(os.path.join(dir_baselines, 'DNN', 'Encoder.csv'))
    df_fcn = pd.read_csv(os.path.join(dir_baselines, 'DNN', 'FCN.csv'))
    df_mcdcnn = pd.read_csv(os.path.join(dir_baselines, 'DNN', 'MCDCNN.csv'))
    df_mcnn = pd.read_csv(os.path.join(dir_baselines, 'DNN', 'MCNN.csv'))
    df_mlp = pd.read_csv(os.path.join(dir_baselines, 'DNN', 'MLP.csv'))
    df_resnet = pd.read_csv(os.path.join(dir_baselines, 'DNN', 'ResNet.csv'))
    df_tlenet = pd.read_csv(os.path.join(dir_baselines, 'DNN', 't-LeNet.csv'))
    df_tcnn = pd.read_csv(os.path.join(dir_baselines, 'DNN', 'Time-CNN.csv'))
    df_twiesn = pd.read_csv(os.path.join(dir_baselines, 'DNN', 'TWIESN.csv'))
    df_dic_dnn = {
        'Encoder': df_encoder,
        'FCN': df_fcn,
        'MCDCNN': df_mcdcnn,
        'MCNN': df_mcnn,
        'MLP': df_mlp,
        'ResNet': df_resnet,
        't-LeNet': df_tlenet,
        'Time-CNN': df_tcnn,
        'TWIESN': df_twiesn}
    df_fcn_rcf = pd.read_csv(os.path.join(dir_baselines, 'mWDN', 'FCN-RCF.csv'))
    df_mlp_rcf = pd.read_csv(os.path.join(dir_baselines, 'mWDN', 'MLP-RCF.csv'))
    df_resnet_rcf = pd.read_csv(os.path.join(dir_baselines, 'mWDN', 'ResNet-RCF.csv'))
    df_wavelet_rcf = pd.read_csv(os.path.join(dir_baselines, 'mWDN', 'Wavelet-RCF.csv'))
    df_dic_dnn_rcf = {
        'FCN-RCF': df_fcn_rcf,
        'MLP-RCF': df_mlp_rcf,
        'ResNet-RCF': df_resnet_rcf,
        'Wavelet-RCF': df_wavelet_rcf}

    ### standard classifiers
    def read_raw(model_name):
        fname_model = '{}.csv'.format(model_name)
        reduce_raw(DIR_IN, fname_model)
        df = pd.read_csv(
            os.path.join(DIR_IN, 'tsgan', 'raw', '{}_mean.csv'.format(model_name)))
        df = df[['dataset', 'acc_te']]
        df.rename(columns={'acc_te': 'acc'}, inplace=True)
        return df
    df_lr = read_raw('LR')
    df_lsvc = read_raw('LSVC')
    df_1nn = read_raw('1NN')
    df_dic_raw = {
        'LR': df_lr,
        'LSVC': df_lsvc,
        '1NN': df_1nn}

    ### tsgan model
    # tag_tsgan = 'bn.tf.keras_batch16_kernel10'
    tag_tsgan = 'bn.tf.keras_batch16_kernel10_adapt75'
    feature_type = 'local-max'
    norm_type = 'tanh'
    def read_tsgan(model_name):
        fname_tsgan = '{}_{}_{}.csv'.format(model_name, feature_type, norm_type)
        reduce_tsgan(DIR_IN, tag_tsgan, fname_tsgan)
        df = pd.read_csv(
            os.path.join(DIR_IN, 'tsgan', tag_tsgan, '{}_mean.csv'.format(model_name)))
        df = df[['dataset', 'acc_te']]
        df.rename(columns={'acc_te':'acc'}, inplace=True)
        return df
    df_lr_tsgan = read_tsgan('LR')
    df_lsvc_tsgan = read_tsgan('LSVC')
    df_1nn_tsgan = read_tsgan('1NN')
    df_dic_tsgan = {
        'LR-TSGAN': df_lr_tsgan,
        'LSVC-TSGAN': df_lsvc_tsgan,
        '1NN-TSGAN': df_1nn_tsgan}
    df_dic_tsgan_strong = {
        'LR-TSGAN': df_lr_tsgan,
        'LSVC-TSGAN': df_lsvc_tsgan}

    ### single statistics
    single_statistics(df_dic_single, DIR_OUT, 'single')
    single_statistics(df_dic_ensemble, DIR_OUT, 'ensemble')
    single_statistics(df_dic_dnn, DIR_OUT, 'dnn')
    single_statistics(df_dic_dnn_rcf, DIR_OUT, 'dnn_rcf')
    single_statistics(df_dic_raw, DIR_OUT, 'raw')
    single_statistics(df_dic_tsgan, DIR_OUT, 'tsgan')

    ### pairwise comparisons
    path_pair_comp = os.path.join(DIR_OUT, 'pair_comparisons')
    for name1, df1 in df_dic_tsgan.items():
        # raw
        path_pair_comp_raw = os.path.join(path_pair_comp, 'raw')
        mkdirs(path_pair_comp_raw)
        for name2, df2 in df_dic_raw.items():
            pair_comparisons(name1, df1, name2, df2, path_pair_comp_raw)
        # single
        path_pair_comp_single = os.path.join(path_pair_comp, 'single')
        mkdirs(path_pair_comp_single)
        for name2, df2 in df_dic_single.items():
            pair_comparisons(name1, df1, name2, df2, path_pair_comp_single)
        # ensemble
        path_pair_comp_ensemble = os.path.join(path_pair_comp, 'ensemble')
        mkdirs(path_pair_comp_ensemble)
        for name2, df2 in df_dic_ensemble.items():
            pair_comparisons(name1, df1, name2, df2, path_pair_comp_ensemble)
        # dnn
        path_pair_comp_dnn = os.path.join(path_pair_comp, 'dnn')
        mkdirs(path_pair_comp_dnn)
        for name2, df2 in df_dic_dnn.items():
            pair_comparisons(name1, df1, name2, df2, path_pair_comp_dnn)
        # dnn_rcf
        path_pair_comp_rcf = os.path.join(path_pair_comp, 'dnn_rcf')
        mkdirs(path_pair_comp_rcf)
        for name2, df2 in df_dic_dnn_rcf.items():
            pair_comparisons(name1, df1, name2, df2, path_pair_comp_rcf)

    ### global comparisons
    path_global_comp = os.path.join(DIR_OUT, 'global_comparisons')
    df_dic_comp_single = {**df_dic_tsgan_strong, **df_dic_single}
    df_dic_comp_ensemble = {**df_dic_tsgan_strong,**df_dic_single, **df_dic_ensemble}
    df_dic_comp_dnn = {**df_dic_tsgan_strong, **df_dic_dnn}
    path_global_comp_single = os.path.join(path_global_comp, 'single')
    mkdirs(path_global_comp_single)
    global_comparisons(df_dic_comp_single, path_global_comp_single)
    path_global_comp_ensemble = os.path.join(path_global_comp, 'ensemble')
    mkdirs(path_global_comp_ensemble)
    global_comparisons(df_dic_comp_ensemble, path_global_comp_ensemble)
    path_global_comp_dnn = os.path.join(path_global_comp, 'dnn')
    mkdirs(path_global_comp_dnn)
    global_comparisons(df_dic_comp_dnn, path_global_comp_dnn)


# path = 'singleTrainTestBakeOff17'
# cols = list(dfs.columns)
# cols.remove('dataset')
# for c in cols:
#     df = pd.DataFrame()
#     df['dataset'] = dfs['dataset']
#     df['acc'] = dfs[c]
#     df.to_csv(os.path.join(path, '{}.csv'.format(c)), index=False)