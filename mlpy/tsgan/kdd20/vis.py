"""Reference
    front size: http://ishxiao.com/blog/python/2017/07/23/how-to-change-the-font-size-on-a-matplotlib-plot.html
    line width: https://stackoverflow.com/questions/10297220/linewidth-is-added-to-the-length-of-a-line
"""
import os
import numpy as np
import matplotlib
matplotlib.use('Agg')
# matplotlib.rcParams['font.family'] = 'sans-serif'
# matplotlib.rcParams['font.sans-serif'] = 'Arial'
# font = {'family' : 'normal',
#         'weight' : 'bold',
#         'size'   : 10}
# matplotlib.rc('font', **font)
matplotlib.rc('xtick', labelsize=18)
matplotlib.rc('ytick', labelsize=18)
import matplotlib.pyplot as plt

np.random.seed(86)

def subplots(samples, out_path, linewidth=2.5):
    n = samples.shape[0]
    n_col = 2
    n_oneside = n // n_col

    f, axes = plt.subplots(n_oneside, n_col)
    axes = axes.flat[:]
    for s, ax in zip(samples[:-n_col], axes[:-n_col]):
        ax.plot(s, linewidth=linewidth)
        plt.setp(ax.get_xticklabels(), visible=False)
    for s, ax in zip(samples[-n_col:], axes[-n_col:]):
        ax.plot(s, linewidth=linewidth)

    plt.savefig(out_path)
    plt.close(f)

if __name__ == '__main__':
    dir_out = 'cache/vis'
    if os.path.exists(dir_out) is False:
        os.makedirs(dir_out)

    noise = np.random.uniform(0, 5, 20)
    inds = range(len(noise))
    plt.scatter(inds, noise, linewidth=1.5)
    plt.plot(inds, noise, linewidth=2.5, linestyle='-')
    plt.savefig(os.path.join(dir_out, 'point_line.png'))
    plt.clf()

    plt.plot(inds, noise, linewidth=2.5)
    plt.savefig(os.path.join(dir_out, 'line.png'))
    plt.clf()

    x = np.random.uniform(0, 5, 50)
    y = np.random.uniform(0, 5, 50)
    plt.scatter(x, y, linewidth=1.0)
    plt.savefig(os.path.join(dir_out, 'scatter.png'))

    samples = []
    length = 150
    for i in range(6):
        s = np.random.uniform(-2, 2, length)
        samples.append(s)
    samples = np.array(samples)
    subplots(samples, os.path.join(dir_out, 'subplots.png'))