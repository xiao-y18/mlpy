from mlpy.lib.metrics.comparing import wilcoxon

import pandas as pd
import os
import numpy as np

##
# the 48 datasets are used in k-shape.
# I corrected the follow datasets:
#   Motes-->MoteStrain, Fish-->FISH, Synthetic_control-->synthetic_control, Wafer-->wafer,
#   Yoga-->yoga, Insect-->InsectWingbeatSound
DATASETS48 = ['ChlorineConcentration', 'CinC_ECG_torso', 'DiatomSizeReduction', 'ECGFiveDays',
             'FacesUCR',  'Haptics', 'InlineSkate', 'ItalyPowerDemand', 'MALLAT', 'MedicalImages',
             'MoteStrain', 'SonyAIBORobotSurface', 'SonyAIBORobotSurfaceII', 'Symbols', 'TwoLeadECG',
             'WordsSynonyms', 'Cricket_X', 'Cricket_Y', 'Cricket_Z','uWaveGestureLibrary_X',
             'uWaveGestureLibrary_Y', 'uWaveGestureLibrary_Z', '50words', 'Adiac', 'Beef',
             'CBF', 'Coffee', 'ECG200', 'FaceAll', 'FaceFour', 'FISH', 'Gun_Point', 'Lighting2',
              'Lighting7', 'Plane', 'OliveOil', 'OSULeaf', 'SwedishLeaf', 'synthetic_control',
              'Trace', 'Two_Patterns', 'wafer', 'yoga', 'Car', 'StarLightCurves',
              'InsectWingbeatSound', 'NonInvasiveFatalECG_Thorax1', 'NonInvasiveFatalECG_Thorax2']
def get_subset(df, subdataset):
    datasetall = list(df['dataset'].values)
    inds = []
    for dataset in subdataset:
        inds.append(datasetall.index(dataset))
    inds = np.sort(inds)
    subdf = df.loc[inds, :]
    return subdf


def reduce_tsgan(dir_in, tag_tsgan, fname_tsgan, decimals=4):
    path_tsgan = os.path.join(dir_in, 'tsgan', tag_tsgan)
    n_runs = len([f for f in os.listdir(path_tsgan)
                  if os.path.isdir(os.path.join(path_tsgan, f))])
    df_tsgan = pd.DataFrame()
    for i in range(n_runs):
        df = pd.read_csv(os.path.join(path_tsgan, 'run{}'.format(i), fname_tsgan))
        if i == 0:
            df_tsgan['dataset'] = df['dataset']
            df_tsgan['randIndex'] = df['randIndex']
            df_tsgan['time'] = df['time']
        else:
            df_tsgan['randIndex'] += df['randIndex']
            df_tsgan['time'] += df['time']
    df_tsgan['randIndex'] = df_tsgan['randIndex'] / n_runs
    df_tsgan['time'] = df_tsgan['time'] / n_runs
    df_tsgan = df_tsgan.round(decimals)
    df_tsgan.to_csv(os.path.join(path_tsgan, 'mean.csv'), index=False)
    df_tsgan_48 = get_subset(df_tsgan, DATASETS48)
    df_tsgan_48.to_csv(os.path.join(path_tsgan, 'mean48.csv'), index=False)


def single_statistics(df_dic, dir_out):
    fout = open(os.path.join(dir_out, 'single_statistics'), 'w')
    for name, df in df_dic.items():
        fout.write("****** {}: \n".format(name))
        describe = df.describe()
        fout.write(describe.to_string())
        fout.write("\n\n")

def pair_comparisons(name1, df1, name2, df2, dfd, dir_out):
    assert (df1.shape[0] == df2.shape[0]) and (df1.shape[0] == dfd.shape[0]), \
        "length of df1:{}, df2:{}, dfd:{}, should be equal!".format(
            df1.shape[0], df2.shape[0], dfd.shape[0])

    fout = open(os.path.join(dir_out, 'compare_{}2{}'.format(name1, name2)), 'w')
    fout.write("****** The results of comparing {} to {}: \n\n".format(name1, name2))

    datasets_win = []
    datasets_loss = []
    datasets_tie = []
    for dataset, val1, val2 in zip(dfd, df1, df2):
        if val1 > val2:
            datasets_win.append(dataset)
        elif val1 < val2:
            datasets_loss.append(dataset)
        else:
            datasets_tie.append(dataset)
    fout.write("#Win: {}\n".format(len(datasets_win)))
    fout.write("#Loss: {}\n".format(len(datasets_loss)))
    fout.write("#Tie: {}\n".format(len(datasets_tie)))
    fout.write("\n")
    fout.write("Win on these datasets: {} \n".format(",".join(datasets_win)))
    fout.write("Loss on these datasets: {} \n".format(",".format(datasets_loss)))
    fout.write("Tie on these datasets: {} \n".format(",".format(datasets_tie)))
    fout.write("\n")

    statistic, pvalue = wilcoxon(df1.values, df2.values)
    fout.write("The result of wilcoxon test: \n")
    fout.write("statistic: {}\n".format(statistic))
    fout.write("pvalue: {}\n".format(pvalue))
    fout.write("\n")

if __name__ == '__main__':
    DIR_IN = 'cache/clus'
    DIR_OUT = 'cache/clus/results'
    if os.path.exists(DIR_OUT) is False:
        os.makedirs(DIR_OUT)

    ## baselines
    df_raw = pd.read_csv(os.path.join(DIR_IN, 'kmeans_raw.csv'))
    df_raw = df_raw.round(4)
    df_raw_48 = get_subset(df_raw, DATASETS48)
    df_raw_48.to_csv(os.path.join(DIR_IN, 'kmeans_raw48.csv'), index=False)

    df_kshape = pd.read_csv(os.path.join(DIR_IN, 'kshape_85_runs10.csv'))

    ## tsgan
    # tag_tsgan = 'bn.tf.keras_batch16_kernel10'
    tag_tsgan = 'bn.tf.keras_batch16_kernel10_adapt75'
    fname_tsgan = 'kmeans_local-max_znorm.csv'
    reduce_tsgan(DIR_IN, tag_tsgan, fname_tsgan)
    df_tsgan = pd.read_csv(os.path.join(DIR_IN, 'tsgan', tag_tsgan, 'mean.csv'))

    #### analysis
    single_statistics({'tsgan':df_tsgan ,'kmeans':df_raw, 'kshape':df_kshape}, DIR_OUT)

    pair_comparisons(
        'tsgan', df_tsgan['randIndex'], 'kmeans', df_raw['randIndex'],
        df_tsgan['dataset'], DIR_OUT)
    pair_comparisons(
        'tsgan', df_tsgan['randIndex'], 'kshape', df_kshape['randIndex'],
        df_tsgan['dataset'], DIR_OUT)
    pair_comparisons(
        'kshape', df_kshape['randIndex'], 'kmeans', df_raw['randIndex'],
        df_kshape['dataset'], DIR_OUT)


