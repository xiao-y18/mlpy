import pandas as pd
import numpy as np

if __name__ == '__main__':
    columns = ['dataset', 'knn', 'knn_gan', 'lsvc', 'lsvc_gan', 'lr', 'lr_gan']
    df = pd.DataFrame(columns=columns)
    df.loc[0] = ['test', 0.5, 0.6, 0.8, 0.9, 0.3, 0.2]
    print(df)
    for i, t in enumerate(['test', 0.5, 0.6, 0.8, 0.9, 0.3, 0.2]):
        print(i, t)


    print(','.join(columns) + '\n\n')
    print(','.join([str(val) for val in ['test', 0.5, 0.6, 0.8, 0.9, 0.3, 0.2]]))

    print([e for e in df.loc[0].values if (type(e) is not str and type(e) is not object)])
    columns_val = list(df.columns)
    columns_val.remove('dataset')
    print(columns_val)
    print(df.loc[0, columns_val].values)
    df.loc[1, 'dataset'] = 'test'
    df.loc[1, columns_val] = df.loc[0, columns_val].values
    print(df)
    print(df.shape[0])

    import json
    r = {'is_claimed': 'True', 'rating': 3.5}
    r = json.dumps(r)
    loaded_r = json.loads(r)
    print(loaded_r['rating'])

    print(df['dataset'].values)

