- two mode correspond directory name below `cache/`
	- `uncond_dcgan_base_mmd`
	- `uncond_dcgan_base_mmd_norm`
- train tsgan with spercific data set and output result to `cache/` directory:
	- `main_tsgan*.py` 
- time series classification based on the features extracted from TSGAN
	- `main_classify*.py` 
- construct reslut
	1. `main_result_clean.py`
	2. `main_result_classify_acc_plot.py` 
- others
	- The TSGAN architecure refer the [DCGAN](https://github.com/carpedm20/DCGAN-tensorflow). 
	- The Semisupervised Classification Architecture was designed by intuitive.

### Buglist

- [ ] `h0 = lrelu(conv2d(x, self.df_dim, name='d_h0'))` --> h0 = lrelu(conv2d(x, self.df_dim, name='d_h0_conv')). **But it has to be trained again, because the model parameter didn't match.**