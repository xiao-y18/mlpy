from mlpy.lib.metrics import *

import matplotlib.pyplot as plt

if __name__ == '__main__':
    # t = np.array([1, 4, 3, 2])
    # print("metrics")
    # print(mean_arithmetic(t))
    # print(mean_geometric(t))
    # print(mean_harmonic(t))
    #
    # y = [1, 0, 1, 1, 1]
    # y_hat = [1, 1, 0, 0, 1]
    # print(per_class_error(y, y_hat))
    #
    # print(rank(t))

    # critical figure
    names = ["first", "third", "second", "fourth"]
    avranks = [1.9, 3.2, 2.8, 3.3]
    cd = compute_CD(avranks, 30)  # tested on 30 datasets
    graph_ranks(avranks, names, cd=cd, width=6, textspace=1.5, filename='cache/fredman')

    # # test t-test
    # from scipy import stats
    #
    # np.random.random_seed(7654567)
    # rvs = stats.norm.rvs(loc=5, scale=10, size=(50, 2))
    # t, prob = ttest(rvs, 5.0)
    # print(t, prob)
    #
    # np.random.random_seed(12345678)
    # rvs1 = stats.norm.rvs(loc=5, scale=10, size=500)
    # rvs2 = (stats.norm.rvs(loc=5, scale=10, size=500) + stats.norm.rvs(scale=0.2, size=500))
    # statistic, pvalue = ttest_paired(rvs1, rvs2)
    # print(statistic, pvalue)

    # # mcnemar
    # # code refer: https://machinelearningmastery.com/mcnemars-test-for-machine-learning/
    # # test example from: https://rasbt.github.io/mlxtend/user_guide/evaluate/mcnemar/
    # y_target = np.array([0, 0, 0, 0, 0, 1, 1, 1, 1, 1])
    # y_model1 = np.array([0, 1, 0, 0, 0, 1, 1, 0, 0, 0])
    # y_model2 = np.array([0, 0, 1, 1, 0, 1, 1, 0, 0, 0])
    # print(mcnemar(y_target, y_model1, y_model2))
