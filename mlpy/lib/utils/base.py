import os
import sys
import shutil
from datetime import datetime
import logging


class AvgrageMeter(object):
    """copy from https://github.com/quark0/darts/blob/master/cnn/utils.py"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.avg = 0
        self.sum = 0
        self.cnt = 0

    def update(self, val, n=1):
        self.sum += val * n
        self.cnt += n
        self.avg = self.sum / self.cnt


def get_strftime_now(fmt='%Y%m%d_%H%M'):
    return datetime.now().strftime(fmt)


""" path operations """


def makedirs(path, clean=False):
    if clean:
        if os.path.exists(path) is True:
            shutil.rmtree(path)

    if os.path.exists(path) is False:
        os.makedirs(path)

    return path


def makedirs_clean(path):
    if os.path.exists(path) is True:
        shutil.rmtree(path)
    os.makedirs(path)
    return path


def path_split(path):
    folders = []
    while 1:
        path, folder = os.path.split(path)

        if folder != "":
            folders.append(folder)
        else:
            if path != "":
                folders.append(path)
            break
    folders.reverse()
    return folders


def tag_path(path, nback=1):
    """
    example:
        tag_path(os.path.abspath(__file__), 1) # return file name
    :param path: 
    :param nback: 
    :return: 
    """
    folders = path_split(path)
    nf = len(folders)

    assert nback >= 1, "nback={} should be larger than 0.".format(nback)
    assert nback <= nf, "nback={} should be less than the number of folder {}!".format(nback, nf)

    tag = folders[-1].split('.')[0]
    if nback > 0:
        for i in range(2, nback+1):
            tag = folders[-i] + '_' + tag
    return tag


""" log settings """

LOG_FORMAT = '%(asctime)s, %(levelname)s, %(name)s: %(message)s'
LOG_DATE_FORMAT = '%m/%d %I:%M:%S %p'


def set_logging(tag, dir_log=None):
    logging.basicConfig(stream=sys.stdout, filemode='w', level=logging.INFO, format=LOG_FORMAT, datefmt=LOG_DATE_FORMAT)
    log = logging.getLogger(tag)
    if dir_log is not None:
        fh = logging.FileHandler(os.path.join(dir_log, 'log.txt'))
        fh.setFormatter(logging.Formatter(fmt=LOG_FORMAT, datefmt=LOG_DATE_FORMAT))
        log.addHandler(fh)
    return log


