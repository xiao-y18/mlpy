from .base import path_split
from .base import tag_path
from .base import makedirs, makedirs_clean
from .base import get_strftime_now
from .base import set_logging

from .vote import vote_majority

from .image import create_gif
