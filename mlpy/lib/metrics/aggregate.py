""" Measures to aggregate a series results 
    
    These results can be metrics of multiple runs on a dataset or multiple datasets.
    
"""
import numpy as np
from sklearn.utils import check_array, check_consistent_length


#####################################################
# Mean Per Class Error (MPCE)
#   evaluate the classification performance of the specific models on multiple datasets.
#   is proposed by Wang Z, Yan W, Oates T. Time series classification from scratch with deep neural networks: A strong baseline[C]//Neural Networks (IJCNN), 2017 International Joint Conference on. IEEE, 2017: 1578-1585.
#   is always used to compare different tsc models on multiple datasets.
#
#   PCE(k) = e(k)/c(k),
#        where k refers to each dataset as well as the corresponding error is e(k), number of classes is c(k)
#   MPCE(i) = (1/K)sum(PCE(k)),
#        where i denotes each model.
#
#   Advantage: More robust because the number of classes are taken into concerned.
#
# Some works apply such a metrics:
#   Wang, Jingyuan, et al. "Multilevel wavelet decomposition network for interpretable time series analysis." Proceedings of the 24th ACM SIGKDD International Conference on Knowledge Discovery & Data Mining. ACM, 2018.
#
def per_class_error(y_true, y_pred, n_classes=None):
    y_true = check_array(y_true, ensure_2d=False)
    y_pred = check_array(y_pred, ensure_2d=False)
    check_consistent_length(y_true, y_pred)
    if n_classes is None:
        n_classes = len(np.unique(y_true))
    acc = np.equal(y_true, y_pred).astype(np.float).mean()
    pce = (1.0 - acc) / n_classes
    return pce
def mean_per_class_error(errors, classes):
    check_consistent_length(errors, classes)
    mpce = 0
    for err, n_classes in zip(errors, classes):
        pce = err / n_classes
        mpce += pce
    return mpce / len(errors)



def mean_arithmetic(x):
    """
    
    :param x: 
    :return: (1/n)sum(x)
    """
    x = check_array(x, ensure_2d=False)
    return np.mean(x)


def mean_geometric(x):
    """
    
    :param x: 
    :return: [x(0)*...*x(n-1)]^{1/n}
    """
    x = check_array(x, ensure_2d=False)
    return np.power(np.prod(np.abs(x)), 1.0 / len(x))


def mean_harmonic(x):
    """
    
    :param x: 
    :return: n/(sum(1/x))
    """
    x = check_array(x, ensure_2d=False)
    x_reciprocal = np.array([1.0 / e for e in x])
    return 1.0 / (np.sum(x_reciprocal) / len(x_reciprocal))