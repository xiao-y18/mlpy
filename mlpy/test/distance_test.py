import matplotlib.pyplot as plt
import numpy as np

from mlpy.test.distance import dtw_basic, euclidean
from mlpy.data.synthesis_ts.sine_wave import sine_wave


def dtw_distance(first, second, **kwargs):

    def dtw_single_channel(first, second, **kwargs):
        cutoff = np.inf
        try:
            window = kwargs["window"]
        except Exception:
            window = 1.0
        n = len(first)
        m = len(second)

        warp_matrix = np.full([n, m], np.inf)
        if n > m:
            window_size = n * window
        else:
            window_size = m * window
        window_size = int(window_size)

        def dist(x1, x2):
            return (x1 - x2) ** 2

        pairwise_distances = np.asarray(
            [[dist(x1, x2) for x2 in second] for x1 in first])

        # initialise edges of the warping matrix
        warp_matrix[0][0] = pairwise_distances[0][0]
        for i in range(1, window_size):
            warp_matrix[0][i] = pairwise_distances[0][i] + warp_matrix[0][
                i - 1]
            warp_matrix[i][0] = pairwise_distances[i][0] + warp_matrix[i - 1][
                0]

        # now visit all allowed cells, calculate the value as the distance
        # in this cell + min(top, left, or top-left)
        # traverse each row,
        for row in range(1, n):
            cutoff_beaten = False
            # traverse left and right by the allowed window
            for column in range(row - window_size, row + 1 + window_size):
                if column < 1 or column >= m:
                    continue

                # find smallest entry in the warping matrix, either above,
                # to the left, or diagonally left and up
                above = warp_matrix[row - 1][column]
                left = warp_matrix[row][column - 1]
                diag = warp_matrix[row - 1][column - 1]

                # add the pairwise distance for [row][column] to the minimum
                # of the three possible potential cells
                warp_matrix[row][column] = pairwise_distances[row][
                                               column] + min(
                    [above, left, diag])

                # check for evidence that cutoff has been beaten on this row
                # (if using)
                if cutoff is not None and warp_matrix[row][column] < cutoff:
                    cutoff_beaten = True

            # if using a cutoff, at least one calculated value on this row
            # MUST be less than the cutoff otherwise the
            # final distance is guaranteed not to be less. Therefore,
            # if the cutoff has not been beaten, early-abandon
            if cutoff is not None and cutoff_beaten is False:
                return float("inf")

        return warp_matrix[n - 1][m - 1]

    if isinstance(first, np.ndarray) and isinstance(first[0], float) is True:
        return dtw_single_channel(first, second, **kwargs)

    dist = 0
    for dim in range(0, len(first)):
        dist += dtw_single_channel(first[dim], second[dim], **kwargs)

    return dist

if __name__ == '__main__':
    samples = sine_wave()

    ts1 = samples[0]
    ts2 = samples[1]

    print(euclidean(ts1, ts2))
    print(dtw_basic(ts1, ts2))
    print(dtw_distance(ts1, ts2))

    from IPython import embed; embed()