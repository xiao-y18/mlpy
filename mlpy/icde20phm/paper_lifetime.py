from mlpy.icde20phm import data

import pandas as pd
import os
import matplotlib.pyplot as plt

if __name__ == '__main__':
    dir_data = data.DIR_DATA_REFINED

    fname_healthy = '1704_2018-10-30.csv'
    fname_degrading = '1704_2018-10-01.csv'
    fname_failure = '1704_2018-10-12.csv'

    df_healthy = pd.read_csv(os.path.join(dir_data, fname_healthy))
    df_degrading = pd.read_csv(os.path.join(dir_data, fname_degrading))
    df_failure = pd.read_csv(os.path.join(dir_data, fname_failure))

    df_healthy = df_healthy[data.COLUMNS_TEMP_TC2]
    df_degrading = df_degrading[data.COLUMNS_TEMP_TC2]
    df_failure = df_failure[data.COLUMNS_TEMP_TC2]

    plt.figure(figsize=(20,5))
    plt.subplot(1, 3, 1)
    plt.plot(df_healthy.values)
    plt.ylabel('Temperature (C)')
    plt.title(fname_healthy.split('.')[0])
    plt.subplot(1, 3, 2)
    plt.plot(df_degrading.values)
    plt.ylabel('Temperature (C)')
    plt.title(fname_degrading.split('.')[0])
    plt.subplot(1, 3, 3)
    plt.plot(df_failure.values)
    plt.ylabel('Temperature (C)')
    plt.title(fname_failure.split('.')[0])
    plt.savefig('cache/lifetime_1704.png')
    plt.close()
    #
    # plt.figure(figsize=(20, 5))
    # plt.subplot(1, 3, 1)
    # plt.plot(df_degrading.values)
    # plt.title(fname_degrading.split('.')[0])
    # plt.subplot(1, 3, 2)
    # plt.plot(df_failure.values)
    # plt.title(fname_failure.split('.')[0])
    # plt.subplot(1, 3, 3)
    # plt.plot(df_healthy.values)
    # plt.title(fname_healthy.split('.')[0])
    # plt.savefig('cache/lifetime_1704_recovery.png')
    # plt.close()

    # print(df_healthy['timestamp'][0], df_healthy['timestamp'][df_healthy.shape[0]-1])
    # print(df_degrading['timestamp'][0], df_degrading['timestamp'][df_degrading.shape[0] - 1])
    # print(df_failure['timestamp'][0], df_failure['timestamp'][df_failure.shape[0] - 1])
