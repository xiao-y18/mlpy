import numpy as np
import pandas as pd
import os
import warnings
import shutil
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from tqdm import tqdm

#DIR_DATA_ROOT = '/data/hfl/icde20phm'
DIR_DATA_ROOT = '/Users/huangfanling/workspace/bitbucket/mlpy/mlpy/dataset/icde20phm'
DATASET = 'subway_temperature_no17'
DIR_DATA_PRIMITIVE = os.path.join(DIR_DATA_ROOT, DATASET)
DIR_DATA_DENOISED = os.path.join(DIR_DATA_ROOT, '{}_denoise'.format(DATASET))
DIR_DATA_CONSISTENT = os.path.join(DIR_DATA_ROOT, '{}_consistent'.format(DATASET))
DIR_DATA_REFINED = os.path.join(DIR_DATA_ROOT, '{}_refined'.format(DATASET))
DIR_DATA_DEL_STATIC_END = os.path.join(DIR_DATA_ROOT, '{}_del_static_end'.format(DATASET))
DIR_DATA_ROLLING = os.path.join(DIR_DATA_ROOT, '{}_rolling'.format(DATASET))
DIR_DATA_RESAMPLE = os.path.join(DIR_DATA_ROOT, '{}_resample'.format(DATASET))

COLUMNS = [
    '车号', '时间戳',
    'TC1车1轴温度值', 'TC1车2轴温度值', 'TC1车3轴温度值', 'TC1车4轴温度值', 'TC1车5轴温度值',
    'TC1车6轴温度值', 'TC1车7轴温度值', 'TC1车8轴温度值',
    'M1车1轴温度值', 'M1车2轴温度值', 'M1车3轴温度值', 'M1车4轴温度值', 'M1车5轴温度值', 'M1车6轴温度值',
    'M1车7轴温度值', 'M1车8轴温度值',
    'M2车1轴温度值', 'M2车2轴温度值', 'M2车3轴温度值', 'M2车4轴温度值', 'M2车5轴温度值', 'M2车6轴温度值',
    'M2车7轴温度值', 'M2车8轴温度值',
    'M3车1轴温度值', 'M3车2轴温度值', 'M3车3轴温度值', 'M3车4轴温度值', 'M3车5轴温度值', 'M3车6轴温度值',
    'M3车7轴温度值', 'M3车8轴温度值',
    'M4车1轴温度值', 'M4车2轴温度值', 'M4车3轴温度值', 'M4车4轴温度值', 'M4车5轴温度值', 'M4车6轴温度值',
    'M4车7轴温度值', 'M4车8轴温度值',
    'TC2车1轴温度值', 'TC2车2轴温度值', 'TC2车3轴温度值', 'TC2车4轴温度值', 'TC2车5轴温度值',
    'TC2车6轴温度值', 'TC2车7轴温度值', 'TC2车8轴温度值',
    '列车综合速度(1=0.1km/h)',
    'TC1车室外温度', 'M1车室外温度', 'M2车室外温度', 'M3车室外温度', 'M4车室外温度', 'TC2车室外温度',
    'TC1_AXM1司控器级位',
    'M1车实际牵引力(1=0.01kN)', 'M2车实际牵引力(1=0.01kN)', 'M3车实际牵引力(1=0.01kN)',
    'M4车实际牵引力(1=0.01kN)'
]
COLUMNS_DIC = {
    '车号':'ntrain', '时间戳':'timestamp',

    'TC1车1轴温度值':'temp_tc1_01', 'TC1车2轴温度值':'temp_tc1_02', 'TC1车3轴温度值':'temp_tc1_03',
    'TC1车4轴温度值':'temp_tc1_04', 'TC1车5轴温度值':'temp_tc1_05', 'TC1车6轴温度值':'temp_tc1_06',
    'TC1车7轴温度值':'temp_tc1_07', 'TC1车8轴温度值':'temp_tc1_08',

    'M1车1轴温度值':'temp_m1_01', 'M1车2轴温度值':'temp_m1_02', 'M1车3轴温度值':'temp_m1_03',
    'M1车4轴温度值':'temp_m1_04', 'M1车5轴温度值':'temp_m1_05', 'M1车6轴温度值':'temp_m1_06',
    'M1车7轴温度值':'temp_m1_07', 'M1车8轴温度值':'temp_m1_08',

    'M2车1轴温度值':'temp_m2_01', 'M2车2轴温度值':'temp_m2_02', 'M2车3轴温度值':'temp_m2_03',
    'M2车4轴温度值':'temp_m2_04', 'M2车5轴温度值':'temp_m2_05', 'M2车6轴温度值':'temp_m2_06',
    'M2车7轴温度值':'temp_m2_07', 'M2车8轴温度值':'temp_m2_08',

    'M3车1轴温度值':'temp_m3_01', 'M3车2轴温度值':'temp_m3_02', 'M3车3轴温度值':'temp_m3_03',
    'M3车4轴温度值':'temp_m3_04', 'M3车5轴温度值':'temp_m3_05', 'M3车6轴温度值':'temp_m3_06',
    'M3车7轴温度值':'temp_m3_07', 'M3车8轴温度值':'temp_m3_08',

    'M4车1轴温度值':'temp_m4_01', 'M4车2轴温度值':'temp_m4_02', 'M4车3轴温度值':'temp_m4_03',
    'M4车4轴温度值':'temp_m4_04', 'M4车5轴温度值':'temp_m4_05', 'M4车6轴温度值':'temp_m4_06',
    'M4车7轴温度值':'temp_m4_07', 'M4车8轴温度值':'temp_m4_08',

    'TC2车1轴温度值':'temp_tc2_01', 'TC2车2轴温度值':'temp_tc2_02', 'TC2车3轴温度值':'temp_tc2_03',
    'TC2车4轴温度值':'temp_tc2_04', 'TC2车5轴温度值':'temp_tc2_05', 'TC2车6轴温度值':'temp_tc2_06',
    'TC2车7轴温度值':'temp_tc2_07', 'TC2车8轴温度值':'temp_tc2_08',

    'TC1车室外温度':'temp_tc1_outdoor', 'M1车室外温度':'temp_m1_outdoor',
    'M2车室外温度':'temp_m2_outdoor', 'M3车室外温度':'temp_m3_outdoor',
    'M4车室外温度':'temp_m4_outdoor', 'TC2车室外温度':'temp_tc2_outdoor',

    '列车综合速度(1=0.1km/h)':'speed',
    'TC1_AXM1司控器级位':'controller_level_tc1_axm1',
    'M1车实际牵引力(1=0.01kN)':'traction_m1', 'M2车实际牵引力(1=0.01kN)':'traction_m2',
    'M3车实际牵引力(1=0.01kN)':'traction_m3', 'M4车实际牵引力(1=0.01kN)':'traction_m4'
}

COLUMNS_TEMP_TC1 = ['temp_tc1_0{}'.format(i) for i in range(1, 9)]
COLUMNS_TEMP_TC2 = ['temp_tc2_0{}'.format(i) for i in range(1, 9)]
COLUMNS_TEMP_M1 = ['temp_m1_0{}'.format(i) for i in range(1, 9)]
COLUMNS_TEMP_M2 = ['temp_m2_0{}'.format(i) for i in range(1, 9)]
COLUMNS_TEMP_M3 = ['temp_m3_0{}'.format(i) for i in range(1, 9)]
COLUMNS_TEMP_M4 = ['temp_m4_0{}'.format(i) for i in range(1, 9)]
COLUMNS_TEMP_OUTDOOR = ['temp_tc{}_outdoor'.format(i) for i in range(1, 3)] \
                       + ['temp_m{}_outdoor'.format(i) for i in range(1, 5)]
COLUMNS_TEMP_TC = COLUMNS_TEMP_TC1 + COLUMNS_TEMP_TC2
COLUMNS_TEMP_M = COLUMNS_TEMP_M1 + COLUMNS_TEMP_M2 + COLUMNS_TEMP_M3 + COLUMNS_TEMP_M4
COLUMNS_TEMP = COLUMNS_TEMP_TC + COLUMNS_TEMP_M + COLUMNS_TEMP_OUTDOOR
COLUMNS_TRACTION = ['traction_m{}'.format(i) for i in range(1, 5)]

COLUMNS_CATEGORIZED_DIC = {
    'temp_tc1': COLUMNS_TEMP_TC1, 'temp_tc2': COLUMNS_TEMP_TC2,
    'temp_m1': COLUMNS_TEMP_M1, 'temp_m2': COLUMNS_TEMP_M2,
    'temp_m3': COLUMNS_TEMP_M3, 'temp_m4': COLUMNS_TEMP_M4,
    'temp_outdoor': COLUMNS_TEMP_OUTDOOR,
    'temp_tc': COLUMNS_TEMP_TC, 'temp_m': COLUMNS_TEMP_M, 'temp': COLUMNS_TEMP,
    'traction': COLUMNS_TRACTION
}

STATE_STATIC = -55

FAILURE_NTRAIN = '1704'
FAILURE_FILE = '1704_2018-10-12.csv'
FAILURE_SIGNAL = 'temp_tc2_07'


def get_fnames(dir_root):
    fname_list = [fname for fname in os.listdir(dir_root) if fname.endswith('.csv')]
    #fname_list = [fname for fname in os.listdir(dir_root) if fname.endswith('.csv') and fname.startswith('1704_2018-09')]
    #fname_list = ['1704_2018-10-30.csv']
    return sorted(fname_list)

def split_train_test(X, y, mode, ratio_train=None):
    n_all = len(X)
    if mode == 'ratio':
        n_train = np.floor(ratio_train * n_all)
        X_train = X[:n_train, :]
        X_test = X[n_train:]
        y_train = y[:n_train]
        y_test = y[n_train:]
    elif mode == 'half':
        n_train = np.floor(n_all/2)
        X_train = X[:n_train, :]
        X_test = X[n_train:]
        y_train = y[:n_train]
        y_test = y[n_train:]
    elif mode == 'half_skip':
        X_train = X[::2, :]
        X_test = X[1::2, :]
        y_train = y[::2]
        y_test = y[1::2]
    elif mode == 'random':
        n_train = np.floor(ratio_train * n_all)
        inds_all = range(n_all)
        inds_train = random.sample(inds_all, n_train)
        inds_train = np.sort(inds_train)
        inds_test = np.setdiff1d(inds_all, inds_train)
        X_train = X[inds_train, :]
        X_test = X[inds_test, :]
        y_train = y[inds_train]
        y_test = y[inds_test]
    else:
        raise ValueError("Can't find the mode = {}".format(mode))

    return X_train, y_train, X_test, y_test

""" Denoise
    1. rename attributes: chinese --> english, 
    2. delete useless values (e.g. -55),
    3. skip null files.
"""
def denoise(dir_root, dir_out):
    # make output directory
    if os.path.exists(dir_out) is False:
        os.makedirs(dir_out)
    else:
        warnings.warn("Output directory {} has existed".format(dir_out))

    # iterate all data files
    fname_list = get_fnames(dir_root)
    n = len(fname_list)
    files_skipped = []
    for i, fname in enumerate(fname_list):
        print("*** [{}/{}] processing file {}: ".format(i, n, fname))
        # load data
        df = pd.read_csv(os.path.join(dir_root, fname))
        # rename
        df.rename(columns=COLUMNS_DIC, inplace=True)
        # filter out inherent useless value, just consider temperature related variables.
        cols_val = list(df.columns)
        cols_val.remove('ntrain')
        cols_val.remove('timestamp')
        for col in cols_val:
            df[col].replace(STATE_STATIC, np.nan, inplace=True)
        df_new = df.dropna(subset=COLUMNS_TEMP)
        print("[{}/{}], delete {} rows. ".format(
            df_new.shape[0], df.shape[0], df.shape[0] - df_new.shape[0])
        )
        # save new data
        if df_new.shape[0] == 0: # skipped null file
            print("skip this null file!")
            files_skipped.append(fname)
        else:
            df_new.to_csv(os.path.join(dir_out, fname), index=False)
    print("*** Finished. Skip {} files that are null files or all signals in which are constant.".
          format(len(files_skipped)))
    print(files_skipped)
def denoise_main():
    denoise(DIR_DATA_PRIMITIVE, DIR_DATA_DENOISED)


""" Check consistency: time granularity = 1s.
   1. delete duplicate,
   2. fill missing.
"""
def consistent_unit(fname, dir_root, dir_out):
    df = pd.read_csv(os.path.join(dir_root, fname))
    if df.shape[0] == 0:
        print("[Warning]: the size of file '{}' is 0. Skipped!".format(fname))
        return

    df['timestamp'] = pd.to_datetime(df['timestamp'])
    timedelta = np.diff(df['timestamp'])
    timedelta = timedelta.astype('timedelta64[s]')
    timedelta = np.insert(timedelta, 0, np.timedelta64(1, 's'))

    # delete duplicate
    filter_duplicated = (timedelta == np.timedelta64(0, 's'))
    df_new = df.drop(df.index[filter_duplicated])
    if df_new.shape[0] == 0:
        print("after deleting duplicates, file {} become null! skip!".format(fname))
        return

    # fill missing
    # (1) fill missing value locating [i] with the median in range [i-win, i+win];
    # (2) append filled values to the end for df;
    # (3) sort df by timestamp to get final result.
    filter_missed = (timedelta > np.timedelta64(1, 's'))
    win = 60  # 60 seconds
    cols_val = list(df.columns)
    cols_val.remove('ntrain')
    cols_val.remove('timestamp')
    ntrain = df['ntrain'][0]
    for i, d in zip(df.index[filter_missed], timedelta[filter_missed]):
        d = d.astype(int)
        i_start = max(i - win, 0)
        i_end = min(i + win, df.shape[0] - 1)
        timestap = df['timestamp'][i]
        values_filled = {}
        for c in cols_val:
            values_filled[c] = np.median(df[c][i_start:i_end])
        for _ in range(d - 1): # missing (path-1) observations.
            timestap = timestap - np.timedelta64(1, 's')
            row_values = {}
            row_values['ntrain'] = ntrain
            row_values['timestamp'] = timestap
            for c in cols_val:
                row_values[c] = values_filled[c]
            # append the missing values to the end of df.
            df_new = df_new.append(row_values, ignore_index=True)
    # sort by timestamp
    df_new.sort_values(by=['timestamp'], inplace=True)
    # double check
    timedelta = np.diff(df_new['timestamp'])
    if np.sum(timedelta != np.timedelta64(1, 's')) != 0:
        print("[Error]: the timestamp of file '{}' still miss match!".format(fname))
    else: # save result
        df_new.to_csv(os.path.join(dir_out, fname), index=False)
def consistent_batch(dir_root, dir_out):
    if os.path.exists(dir_out) is False:
        os.makedirs(dir_out)
    else:
        warnings.warn("Output directory '{}' has existed".format(dir_out))
    fname_list = get_fnames(dir_root)
    n = len(fname_list)
    for i, fname in enumerate(fname_list):
        print("**** [{}/{}] processing file {}".format(i, n, fname))
        consistent_unit(fname, dir_root, dir_out)
def consistent_main():
    consistent_batch(DIR_DATA_DENOISED, DIR_DATA_CONSISTENT)

""" Remove extreme files
    1. remove the files too small: 
       the size of file (the number of samples in file) < given threshold
    2. remove the files static most of time: 
       running ratio (count samples whose speed larger than 0 / size of file)  > given threshold.
"""
def files_select_size(dir_root):
    fname_list = get_fnames(dir_root)

    # calculate size for each file, then calculate comprehensive statistics.
    sizes = np.zeros(len(fname_list))
    for i in tqdm(range(len(fname_list))):
        fname = fname_list[i]
        df = pd.read_csv(os.path.join(dir_root, fname))
        sizes[i] = df.shape[0]
    mu = np.mean(sizes)
    sigma = np.std(sizes)
    print("Statistics of size of file: mean={}, median={}, std={}, max={}, min={}".format(
        mu, np.median(sizes), sigma, np.max(sizes), np.min(sizes),
    ))
    # mean=74035.09589041096, median=81610.5, std=17958.80384401835, max=107676.0, min=6819.0
    plt.hist(sizes)
    plt.savefig('cache/histogram_file_sizes.png')

    # get selected files
    fname_list_selected = []
    for fname, size in zip(fname_list, sizes):
        if size > (mu - sigma):
            fname_list_selected.append(fname)

    return fname_list_selected
def files_select_speed(dir_root):
    fname_list = get_fnames(dir_root)

    # calculate running ratios, then calculate comprehensive statistics.
    threshold_static = 0
    run_ratios = np.zeros(len(fname_list))
    for i in tqdm(range(len(fname_list))):
        fname = fname_list[i]
        df = pd.read_csv(os.path.join(dir_root, fname))
        df_speed = df['speed']
        ratio = 1.0 * df_speed[df_speed > threshold_static].shape[0] / df_speed.shape[0]
        run_ratios[i] = ratio
    mu = np.mean(run_ratios)
    sigma = np.std(run_ratios)
    print("Statistics of running ratio: mean={}, median={}, std={}, max={}, min={}".format(
       mu , np.median(run_ratios), sigma, np.max(run_ratios), np.min(run_ratios)
    ))
    # mean=0.4198223850549312, median=0.49501329148415896, std=0.202609209246414, max=0.7520021771246403, min=0.0
    plt.hist(run_ratios)
    plt.savefig('cache/histogram_run_ratios.png')

    # get selected files
    fname_list_selected = []
    for fname, ratio in zip(fname_list, run_ratios):
        #if ratio > (mu - sigma):
        if ratio > (mu):
            fname_list_selected.append(fname)

    return fname_list_selected
def skip_extreme_files():
    from shutil import copyfile

    dir_root = DIR_DATA_CONSISTENT
    dir_out = DIR_DATA_REFINED
    if os.path.exists(dir_out) is False:
        os.makedirs(dir_out)

    files_filtered_by_size = files_select_size(dir_root)
    files_filtered_by_speed = files_select_speed(dir_root)
    fname_list = list(set(files_filtered_by_size).intersection(files_filtered_by_speed))
    print("skip {} files.".format(len(fname_list)))
    print(fname_list)

    for fname in fname_list:
        copyfile(os.path.join(dir_root, fname), os.path.join(dir_out, fname))


""" Delete static(speed=0) points locating in the beginning and ending for each files.
"""
def del_static_end_unit(fname, dir_root, dir_out):
    df = pd.read_csv(os.path.join(dir_root, fname))
    df_static = df[df['speed'] == 0].copy()

    index_delta = np.diff(df_static.index)
    index_delta = np.insert(index_delta, 0, 1)
    index_delta_larger_than_1unit = (index_delta > 1)
    index_break_points = df_static.index[index_delta_larger_than_1unit]

    # if fname == '1704_2018-10-07.csv':
    #     from IPython import embed; embed()

    if len(index_break_points) == 0:
        if df_static.shape[0] == df.shape[0]: # all static
            df_new = pd.DataFrame(columns=df.columns)
        elif df_static.shape[0] == 0: # all running
            df_new = df.copy()
        else: # static just in the beginning or ending
            df_new = df.drop(df_static.index)
    else:
        seg_beginning_end = df_static.index[list(df_static.index).index(index_break_points[0]) - 1]
        seg_ending_start = index_break_points[-1]

        index_drop = df.index[list(range(seg_beginning_end + 1)) +
                              list(range(seg_ending_start, df.index[-1] + 1))]
        df_new = df.drop(index_drop)

    print("Finish to processing file {}, and the retention ratio is {}/{}={}".format(
        fname, df_new.shape[0], df.shape[0], df_new.shape[0]/df.shape[0]
    ))
    if df_new.shape[0] > 0:
        df_new.to_csv(os.path.join(dir_out, fname), index=False)
    else:
        print("Don't select static file {} !".format(fname))
def del_static_end_batch(dir_root, dir_out):
    if os.path.exists(dir_out) is False:
        os.makedirs(dir_out)
    else:
        warnings.warn("Output directory '{}' has existed".format(dir_out))

    fname_list = get_fnames(dir_root)
    n = len(fname_list)
    for i, fname in enumerate(fname_list):
        print("[{}/{}] processing file {} ".format(i, n, fname))
        del_static_end_unit(fname, dir_root, dir_out)
def del_static_end_main():
    del_static_end_batch(DIR_DATA_REFINED, DIR_DATA_DEL_STATIC_END)


def resample_unit(fname, rule, path_in, dir_out, how):
    # load data and set datetime index
    df = pd.read_csv(path_in)
    df['timestamp'] = pd.DatetimeIndex(df['timestamp'])
    # resample
    if how == 'mean':
        df_res = df.resample(rule, on='timestamp').mean()
    elif how == 'median':
        df_res = df.resample(rule, on='timestamp').median()
    elif how == 'last':
        last = lambda x: x.iloc[-1,:]
        df_res = df.resample(rule, on='timestamp').apply(last)
    else:
        raise ValueError('The how = {} undefined!'.format(how))
    # recover columns
    df_res['timestamp'] = df_res.index
    df_res = df_res[df.columns]
    # save result
    df_res.to_csv(os.path.join(dir_out, fname), index=False)
def resample_batch(dir_root, rule):
    fname_list = get_fnames(dir_root)
    n = len(fname_list)
    hows = ['mean', 'median', 'last']
    for how in hows:
        print("*** processing how = {}".format(how))
        # make output directory
        dir_out = os.path.join(DIR_DATA_RESAMPLE, '{}_{}'.format(rule, how))
        if os.path.exists(dir_out) is False:
            os.makedirs(dir_out)
        else:
            warnings.warn("Output directory '{}' has existed".format(dir_out))
        # iterate all datasets and resample
        for i, fname in enumerate(fname_list):
            print("*** [{}/{}] processing file {}".format(i, n, fname))
            resample_unit(fname, rule, os.path.join(dir_root, fname), dir_out, how)
def resample_main():
    dir_root = DIR_DATA_REFINED
    rule = '10S'
    resample_batch(dir_root, rule)
    rule = '60S'
    resample_batch(dir_root, rule)


def rolling_unit(fname, win, path_in, dir_out, type):
    # load data
    df = pd.read_csv(path_in)
    # calculate rolling statistics for values column
    cols_val = list(df.columns)
    cols_val.remove('ntrain')
    cols_val.remove('timestamp')
    if type == 'mean':
        df_ret = df[cols_val].rolling(win).mean()
    elif type == 'median':
        df_ret = df[cols_val].rolling(win).median()
    elif type == 'last':
        last = lambda x: x.iloc[-1, :]
        df_ret = df[cols_val].rolling(win).apply(last)
    else:
        raise ValueError("The type={} undefined!".format(type))
    # assign non-real value columns
    df_ret['ntrain'] = df['ntrain']
    df_ret['timestamp'] = df['timestamp']
    # drop NaN result in rolling calculation
    n_primitive = df_ret.shape[0]
    df_ret.dropna(inplace=True)
    n_na = n_primitive - df_ret.shape[0]
    if n_na != (win - 1):
        warnings.warn("The NaN values should be equal to {}, however it is {} now.".format(
            win - 1, n_na
        ))
    # recover the order of columns
    df_ret = df_ret[df.columns]
    # save data
    df_ret.to_csv(os.path.join(dir_out, fname), index=False)
def rolling_batch(dir_root, win):
    types = ['mean', 'median', 'last']
    # iterating to calculate each type of rolling statistics
    for t in types:
        print("*** processing type of {}".format(t))
        # make output directory
        dir_out = os.path.join(DIR_DATA_ROLLING, '{}_{}'.format(win, t))
        if os.path.exists(dir_out) is False:
            os.makedirs(dir_out)
        else:
            warnings.warn("Output directory '{}' has existed".format(dir_out))
        # iterating to calculate rolling statistics for all datasets
        fname_list = get_fnames(dir_root)
        n = len(fname_list)
        for i, fname in enumerate(fname_list):
            print("*** [{}/{}] processing file {}".format(i, n, fname))
            rolling_unit(fname, win, os.path.join(dir_root, fname), dir_out, t)
def rolling_main():
    rolling_batch(DIR_DATA_CONSISTENT, 10)


def vis_unit(fname, columns, dir_root, dir_out):
    df = pd.read_csv(os.path.join(dir_root, fname))
    if df.shape[0] > 0:
        if len(columns) <= 8:
            df[columns].plot(title=fname)
        else:
            df[columns].plot(legend=False, title=fname)
        plt.savefig('{}/{}.png'.format(dir_out, fname))
        plt.close()
def vis_batch(dir_root):
    tag = os.path.basename(dir_root)
    dir_out_root = 'cache/vis'
    for key, columns in COLUMNS_CATEGORIZED_DIC.items():
        print("*** visualize {}".format(key))
        dir_out = os.path.join(dir_out_root, '{}_{}'.format(tag, key))
        if os.path.exists(dir_out) is False:
            os.makedirs(dir_out)
        else:
            warnings.warn("Output directory {} has existed".format(dir_out))
        fname_list = os.listdir(dir_root)
        for fname in fname_list:
            vis_unit(fname, columns, dir_root, dir_out)
def vis_main():
    tag = '10S_median'
    dir_root = os.path.join(DIR_DATA_RESAMPLE, tag)
    vis_batch(dir_root)


if __name__ == '__main__':
    skip_extreme_files()
    del_static_end_main()












