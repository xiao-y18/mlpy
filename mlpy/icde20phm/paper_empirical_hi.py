import matplotlib
import matplotlib.pyplot as plt
from numpy.random import RandomState
import numpy as np

font = {'family' : 'normal',
        'size'   : 15}
matplotlib.rc('font', **font)

if __name__ == '__main__':
    seed = 42
    np_rng = RandomState(seed)

    stage1 = np_rng.normal(0.0, 0.05, size=500)
    stage2 = np_rng.normal(3, 0.05, size=300)
    stage3 = np.exp(0.5*np.arange(2, 6, 0.05))
    stage3 = stage3 + np_rng.normal(0.0, 0.1, size=len(stage3))

    print(len(stage1), len(stage2), len(stage3))
    x = np.concatenate([stage1, stage2, stage3])
    plt.plot(x, 'black')
    plt.ylabel('Health Value')
    plt.xlabel('Time')
    plt.savefig('cache/empirical_health_indicator.png')