from mlpy.icde20phm import data

import os
import pandas as pd
import matplotlib.pyplot as plt

if __name__ == '__main__':
    dir_data = data.DIR_DATA_REFINED
    train_no = '1703'
    fname_list = data.get_fnames(dir_data)
    fname_list = [f for f in fname_list if f.startswith(train_no)]

    df1 = pd.read_csv(os.path.join(dir_data, fname_list[0]))
    df2 = pd.read_csv(os.path.join(dir_data, fname_list[1]))
    df3 = pd.read_csv(os.path.join(dir_data, fname_list[2]))
    df = pd.concat([df1, df2, df3], keys=data.COLUMNS_TEMP_TC2)

    plt.figure(figsize=(15, 5))
    plt.plot(df[data.COLUMNS_TEMP_TC2].values)
    plt.ylabel('Temperature (C)')
    plt.axvline(df1.shape[0], linestyle='--', color='grey')
    plt.axvline(df1.shape[0] + df2.shape[0], linestyle='--', color='grey')
    plt.savefig('cache/running_cycle_{}.png'.format(train_no))
    plt.close()

    # print(df1['timestamp'][0], df1['timestamp'][df1.shape[0]-1])
    # print(df2['timestamp'][0], df2['timestamp'][df2.shape[0]-1])
    # print(df3['timestamp'][0], df3['timestamp'][df3.shape[0]-1])