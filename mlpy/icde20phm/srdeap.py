from mlpy.tsa.sr import common as srcom
from mlpy.icde20phm import metrics

from deap import tools, creator, base
import random
import operator
from mlpy.tsa.sr.deap.base import *

import os
import pickle
import matplotlib.pyplot as plt
import pandas as pd


class DeapModel(object):
    def __init__(self, var_names, target_name, mid, dir_log, verbose=None):
        self.var_names = var_names
        self.target_name = target_name
        self.mid = mid
        self.dir_log = dir_log
        self.verbose = verbose
        self.funset, self.toolbox = self.setup_model(self.var_names)

    def set_mid(self, mid):
        self.mid = mid

    def fit(self, X_train, y_train, X_test, y_test):
        #hyper = {'gen': 92, 'mu': 500, 'lambda_': 0.5, 'cxpb': 0.5, 'mutp': 1}
        hyper = {'gen': 92, 'mu': 500, 'lambda_': 0.5, 'cxpb': 1, 'mutp': 1}
        #hyper = {'gen': 92, 'mu': 1000, 'lambda_': 0.5, 'cxpb': 1, 'mutp': 1}
        seed = 43

        ## set evaluating metrics
        self.toolbox.register(
            "evaluate", self._evaluate, compile=self.toolbox.compile, X=X_test, y=y_test
        )

        ## optimization setting
        options = {'maxiter': 5}
        context = generate_context(self.funset, X_train)
        constraints = ({'type': 'ineq', 'fun': lambda x: 10.0 - np.abs(x)})
        cost = lambda args, yp: np.sum((y_train - yp(*args)) ** 2)
        optimizer = lambda ind: optimize_constants(
            ind=ind, cost=cost, context=context, options=options, constraints=constraints)

        ## training
        population, logbook = self._evolve(
            toolbox=self.toolbox, optimizer=optimizer, seed=seed, **hyper)
        pareto_front = tools.ParetoFront()
        pareto_front.update(population)

        ## construct result
        complexities = [ind.fitness.values[1] for ind in pareto_front]
        models_compiled = [self.toolbox.compile(m) for m in pareto_front]
        nrmses_train = [self.nrmse(m, X_train, y_train) for m in models_compiled]
        rmses_train = [self.rmse(m, X_train, y_train) for m in models_compiled]
        nrmses_test = [self.nrmse(m, X_test, y_test) for m in models_compiled]
        rmses_test = [self.rmse(m, X_test, y_test) for m in models_compiled]
        result = (complexities, [nrmses_test, nrmses_train], [rmses_test, rmses_train], pareto_front)
        self._save_result(result, self.dir_log)

    def predict(self, X, mid=None):
        if mid is None:
            mid = self.mid
        model = self.load_model(mid)
        model_compiled = self.compile_model(model)
        yp = model_compiled(*X.T)
        try:
            len(yp)
        except Exception:
            yp = yp*np.ones(X.shape[0])

        return yp

    def get_operands(self, f, prefix):
        string = ""
        op = []
        stack = []
        for node in f:
            stack.append((node, []))
            while len(stack[-1][1]) == stack[-1][0].arity:
                prim, args = stack.pop()
                string = convert_inverse_prim(prim, args)
                if string.startswith(prefix):
                    op.append(string)
                if len(stack) == 0:
                    break  # If stack is empty, all nodes should have been seen
                stack[-1][1].append(string)
        return op

    def get_variety(self, f, prefix):
        return len(np.unique(self.get_operands(f, prefix)))

    def get_metrics(self, X, y, prefix):
        with open(os.path.join(self.dir_log, 'result.pkl'), 'rb') as f:
            result = pickle.load(f)
        numBases, nrmses, rmses, models = result
        nrmses_test, nrmses_train = nrmses
        rmses_test, rmses_train = rmses

        n = len(models)
        table = {'model':[], 'model_str':[], 'err_te':[], 'err_tr':[], 'complexity':[], 'variety':[]}
        for key in metrics.METRICS_FUNCTION.keys():
            table[key] = []
        for mid in np.arange(n):
            table['model'].append(models[mid])
            table['model_str'].append(str(self.simplify_model(models[mid])))
            table['err_te'].append(nrmses_test[mid])
            table['err_tr'].append(nrmses_train[mid])
            table['complexity'].append(numBases[mid])
            table['variety'].append(self.get_variety(models[mid], prefix))
            yp = self.predict(X, mid)
            for key, func in metrics.METRICS_FUNCTION.items():
                val = func(y, yp)
                table[key].append(val)
        table = pd.DataFrame(table)
        return table

    def get_optimal_model(self, metrics_df, complex_max):
        n = metrics_df.shape[0]
        err_mean = np.mean(metrics_df['err_te'])
        opt_id = None
        opt_variety = -np.inf
        # initialize, only consider complexity
        for i, com in enumerate(metrics_df['complexity']):
            if com < complex_max:
                opt_id = i
                break
        # comprehensive strategy
        for i, row in metrics_df.iterrows():
            if row['err_te'] < err_mean and row['complexity']<=complex_max:
                if row['variety'] > opt_variety:
                    opt_variety = row['variety']
                    opt_id = i
        # default setting when strategy fail
        if opt_id is None:
            opt_id = 0

        row = metrics_df.loc[opt_id]
        print("opt_id={}, com={}, nrmse={}, variety={}, model={}".format(
            opt_id, row['complexity'], row['err_te'], opt_variety, row['model_str']
        ))

        return opt_id

    def load_model(self, mid):
        with open(os.path.join(self.dir_log, 'result.pkl'), 'rb') as f:
            result = pickle.load(f)

        numBases, nrmses, rmses, models = result
        nrmses_test, nrmses_train = nrmses
        rmses_test, rmses_train = rmses

        if self.verbose:
            print("Your model is: ")
            print(str(models[mid]))
            print(str(self.simplify_model(models[mid])))
            print("numBases={}, nrmse_test={}, nrmse_train={}, rmse_test={}, rmse_train={}".format(
                numBases[mid], nrmses_test[mid], nrmses_train[mid], rmses_test[mid],
                rmses_train[mid]
            ))

        return models[mid]

    def compare_plot(self, yp, y, dir_out):
        srcom.compare_plot_english(y, yp, self.target_name, dir_out)

    def hist_plot(self, yp, y, dir_out):
        error = y - yp
        plt.hist(error)
        plt.savefig(os.path.join(dir_out, 'hist.png'))
        plt.close()

    def setup_model(self, varnames):
        # strat = ['exp', 'symc', 'pot', 'trigo']
        # strat = ['symc', 'pot']
        # strat = ['symc', 'exp']
        strat = ['symc', 'exp', 'pot']
        funcset = prepare_funcset(strat=strat, arity=len(varnames))
        new_varnames = {'ARG%i' % i: var for i, var in enumerate(varnames)}
        funcset.renameArguments(**new_varnames)

        # setting types of problem
        creator.create("Fitness", base.Fitness, weights=(-1.0, -1.0))
        creator.create("Individual", MyGPTree, fitness=creator.Fitness)

        toolbox = base.Toolbox()
        toolbox.register("compile", gp.compile, pset=funcset)
        toolbox.register("expr", gp.genHalfAndHalf, pset=funcset, min_=1, max_=4)
        toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.expr)
        toolbox.register("population", tools.initRepeat, list, toolbox.individual)
        # inherit
        toolbox.register("select", tools.selNSGA2)
        toolbox.register("mate", gp.cxOnePoint)
        toolbox.register("expr_mut", gp.genHalfAndHalf, pset=funcset, min_=1, max_=4)
        toolbox.register("mutate", gp.mutUniform, expr=toolbox.expr_mut, pset=funcset)
        # cross
        toolbox.decorate("mate", gp.staticLimit(key=operator.attrgetter("height"), max_value=17))
        toolbox.decorate("mutate", gp.staticLimit(key=operator.attrgetter("height"), max_value=17))

        return funcset, toolbox

    def compile_model(self, model):
        return self.toolbox.compile(model)

    def simplify_model(self, model):
        return simplify_this(model)

    def _save_result(self, result, dir_out):
        with open(os.path.join(dir_out, 'result.pkl'), 'wb') as f:
            pickle.dump(result, f)

        numBases, nrmses, rmses, models = result
        nrmses_test, nrmses_train = nrmses
        rmses_test, rmses_train = rmses
        with open(os.path.join(dir_out, 'result'), 'w') as f:
            strline = "{0} {1} {0}".format("*" * 20, self.target_name)
            f.write(strline + '\n')
            print(strline)
            strline = "original model:"
            f.write(strline + '\n')
            print(strline)
            for model in models:
                strline = str(model)
                f.write(strline + '\n')
                print(strline)
            strline = "symplified model:"
            f.write(strline + '\n')
            print(strline)
            for model in models:
                strline = str(self.simplify_model(model))
                f.write(strline + '\n')
                print(strline)

            strline = "{0}".format("-" * 25)
            f.write(strline + '\n')
            print(strline)
            strline = "complexity\t\tnrmse_test\t\tnrmse_train\t\trmse_test\t\trmse_train"
            f.write(strline + '\n')
            print(strline)
            for com, nrmse_test, nrmse_train, rmse_test, rmse_train in \
                    zip(numBases, nrmses_test, nrmses_train, rmses_test, rmses_train):
                strline = "{0}\t\t{1}\t\t{2}\t\t{3}\t\t{4}".format(
                    com, nrmse_test, nrmse_train, rmse_test, rmse_train)
                f.write(strline + '\n')
                print(strline)
            strline = "{0}".format("*" * 40)
            f.write(strline + '\n')
            print(strline)

        # ﻿Pareto front, nrmse on test set
        plt.figure()
        plt.plot(numBases, nrmses_test, 'o-')
        plt.title("NRMSE Pareto Front of {}".format(self.target_name))
        plt.xlabel("Complexity")
        plt.ylabel("Error")
        plt.tight_layout()
        plt.savefig(os.path.join(dir_out, 'pareto_nrmse.png'))
        plt.close()
        # Pareto front, rmse on test set
        plt.figure()
        plt.plot(numBases, rmses_test, 'o-')
        plt.title("RMSE Pareto Front of {}".format(self.target_name))
        plt.xlabel("Complexity")
        plt.ylabel("Error")
        plt.tight_layout()
        plt.savefig(os.path.join(dir_out, 'pareto_rmse.png'))
        plt.close()

    def _evolve(self, toolbox, optimizer, seed, gen=0, mu=1, lambda_=1, cxpb=1, mutp=1):
        random.seed(seed)
        np.random.seed(seed)

        population = remove_twins(toolbox.population(n=mu))
        population = list(toolbox.map(optimizer, population))

        invalid_ind = [ind for ind in population if not ind.fitness.valid]
        fitness = list(toolbox.map(toolbox.evaluate, invalid_ind))
        for ind, fit in zip(invalid_ind, fitness):
            ind.fitness.values = fit

        population = toolbox.select(population, mu)

        stats = tools.Statistics(lambda ind: ind.fitness.values)
        stats.register("min", np.nanmin, axis=0)
        stats.register("max", np.nanmax, axis=0)
        stats.register("diversity", lambda pop: len(set(map(str, pop))))

        logbook = tools.Logbook()
        logbook.header = "gen", 'evals', 'min', 'max', 'diversity'

        record = stats.compile(population)
        logbook.record(gen=0, evals=(len(invalid_ind)), **record)
        print(logbook.stream)
        if record['min'][0] == 0.0:
            return population, logbook

        for g in range(1, gen):
            offspring = tools.selRandom(population, int(lambda_*mu))
            offspring = [toolbox.clone(ind) for ind in offspring]

            # crossover
            for child1, child2 in zip(offspring[::2], offspring[1::2]):
                if random.random() <= cxpb:
                    toolbox.mate(child1, child2)
                    del child1.fitness.values
                    del child2.fitness.values
            # mutation
            for mutant in offspring:
                if random.random() <= mutp:
                    toolbox.mutate(mutant)
                    del mutant.fitness.values
            # evaluate the individuals
            invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
            fitness = list(toolbox.map(toolbox.evaluate, invalid_ind))
            for ind, fit in zip(invalid_ind, fitness):
                ind.fitness.values = fit

            # selection
            population = toolbox.select(remove_twins(population + offspring), mu)
            record = stats.compile(population)
            logbook.record(gen=g, evals=len(invalid_ind), **record)
            print(logbook.stream)
            if record['min'][0] < 1E-4: # accuracy good enough
                break

        return population, logbook

    def _error(self, ind, compile, X, y):
        model = compile(expr=ind)
        yp = model(*X.T)
        er = np.sqrt(np.nanmean((y - yp) ** 2))
        return er

    def _evaluate(self, ind, compile, X, y):
        er = self._error(ind, compile, X, y)
        com = len(ind)
        return er, com

    def nrmse(self, model_compiled, X, y):
        return np.sqrt(np.nanmean((model_compiled(*X.T) - y) ** 2)) / (max(y) - min(y))

    def rmse(self, model_compiled, X, y):
        return np.sqrt(np.nanmean((model_compiled(*X.T) - y) ** 2))