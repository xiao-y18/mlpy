from mlpy.icde20phm import data

import os
import pandas as pd
import numpy as np

if __name__ == '__main__':
    dir_data = data.DIR_DATA_REFINED

    train_no_failure = '1704'
    var_names = data.COLUMNS_TEMP_TC2.copy()
    target_name = 'temp_tc2_07'
    var_names.remove(target_name)

    fname_list = data.get_fnames(dir_data)
    train_no_list = np.unique([f.split('_')[0] for f in fname_list])
    train_no_list = [t for t in train_no_list if t != train_no_failure] # exclude failure train

    df_result = {'locomotive':[], 'earliest_oc':[], 'latest_oc':[], 'number_of_oc':[]}

    for t in train_no_list:
        fnames = [f for f in fname_list if f.startswith(t)]
        fname1, fname2, fname3 = fnames[0], fnames[1], fnames[-1]

        df1 = pd.read_csv(os.path.join(dir_data, fname1))
        X1 = df1[var_names].values
        y1 = df1[target_name].values
        X1_train, y1_train, X1_test, y1_test = data.split_train_test(X1, y1, 'half_skip')

        df2 = pd.read_csv(os.path.join(dir_data, fname2))
        X2 = df2[var_names].values
        y2 = df2[target_name].values
        X2_train, y2_train, X2_test, y2_test = data.split_train_test(X2, y2, 'half_skip')

        df3 = pd.read_csv(os.path.join(dir_data, fname3))

        X_train = np.vstack([X1_train, X2_train])
        y_train = np.concatenate([y1_train, y2_train])
        X_test = np.vstack([X1_test, X2_test])
        y_test = np.concatenate([y1_test, y2_test])

        df_result['locomotive'].append(t)
        df_result['earliest_oc'].append((df1['timestamp'][0], df1['timestamp'][df1.shape[0]-1]))
        df_result['latest_oc'].append((df3['timestamp'][0], df3['timestamp'][df3.shape[0]-1]))
        df_result['number_of_oc'].append(len(fnames))

    fname1 = '1704_2018-10-28.csv'
    fname2 = '1704_2018-10-30.csv'
    fnames = [f for f in fname_list if f.startswith(train_no_failure)]
    fname3 = fnames[-1]

    df1 = pd.read_csv(os.path.join(dir_data, fname1))
    X1 = df1[var_names].values
    y1 = df1[target_name].values
    X1_train, y1_train, X1_test, y1_test = data.split_train_test(X1, y1, 'half_skip')

    df2 = pd.read_csv(os.path.join(dir_data, fname2))
    X2 = df2[var_names].values
    y2 = df2[target_name].values
    X2_train, y2_train, X2_test, y2_test = data.split_train_test(X2, y2, 'half_skip')

    df3 = pd.read_csv(os.path.join(dir_data, fname3))

    df_result['locomotive'].append(train_no_failure)
    df_result['earliest_oc'].append((df1['timestamp'][0], df1['timestamp'][df1.shape[0] - 1]))
    df_result['latest_oc'].append((df3['timestamp'][0], df3['timestamp'][df3.shape[0] - 1]))
    df_result['number_of_oc'].append(len(fnames))

    df_result = pd.DataFrame(df_result)
    df_result.to_csv('cache/paper/dataset.csv', index=False)



