from mlpy.icde20phm import data
from mlpy.icde20phm import paper_sr

import os
import numpy as np
import pandas as pd
import shutil
from time import time

def train(dir_data, dir_log):
    fname_list = data.get_fnames(dir_data)
    train_no_list = sorted(np.unique([f.split('_')[0] for f in fname_list]))
    train_no_list = [t for t in train_no_list if t != '1704']  # exclude fail locomotives

    # category files
    fname_dic = {}
    for train in train_no_list:
        fname_dic[train] = [f for f in fname_list if f.startswith(train)]
        print(train, len(fname_dic[train]))
    print()

    # train model
    df_result = {'locomotive':[], 't_train':[], 't_test':[], 'size_train':[], 'size_test':[]}
    for key, fnames in fname_dic.items():
        if len(fnames) < 2:
            print("The number of files less than 2!")
            continue
        print("**** processing {}".format(key))
        t_train, t_test, size_train, size_test = paper_sr.train_two_file_mix(
            dir_data, dir_log, fnames[0], fnames[1], 0, train=True
        )
        df_result['locomotive'].append(key)
        df_result['t_train'].append(t_train)
        df_result['t_test'].append(t_test)
        df_result['size_train'].append(size_train)
        df_result['size_test'].append(size_test)

    df_result['locomotive'].append('Average')
    df_result['t_train'].append(np.mean(df_result['t_train']))
    df_result['t_test'].append(np.mean(df_result['t_test']))
    df_result['size_train'].append(np.mean(df_result['size_train']))
    df_result['size_test'].append(np.mean(df_result['size_test']))
    df_result = pd.DataFrame(df_result)
    df_result.to_csv('cache/paper/efficiency.csv', index=False)

def infer(dir_data, dir_model, dir_log):
    fnames_model = os.listdir(dir_model)
    fnames_data = data.get_fnames(dir_data)

    for fname_model in fnames_model:
        train_no = fname_model.split('_')[0]
        print("*** processing {}".format(train_no))
        fnames = [f for f in fnames_data if f.split('_')[0] == train_no]
        t_start = time()
        paper_sr.infer_persist(dir_data, dir_log, os.path.join(dir_model, fname_model), fnames)
        t_end = time()
        print("train_no={}, infer={}, time={}, average_time={}".format(
            train_no, len(fnames), t_end-t_start, (t_end-t_start)/len(fnames)
        ))


if __name__ == '__main__':
    dir_data = data.DIR_DATA_REFINED
    dir_log_train = 'cache/paper/train_batch'

    if os.path.exists(dir_log_train):
        shutil.rmtree(dir_log_train)
    os.makedirs(dir_log_train)
    train(dir_data, dir_log_train)

    dir_log_infer = 'cache/paper/infer_batch'

    # if os.path.exists(dir_log_infer):
    #     shutil.rmtree(dir_log_infer)
    # os.makedirs(dir_log_infer)
    # infer(dir_data_clean, dir_log_train, dir_log_infer)

    dir_log_hi = 'cache/paper/hi_batch'

    # if os.path.exists(dir_log_hi):
    #     shutil.rmtree(dir_log_hi)
    # os.makedirs(dir_log_hi)
    # paper_sr.extract_hi(dir_log_infer, dir_log_hi)

    # file_result = 'cache/paper/optimal_table.csv'
    # dirs_model = sorted(os.listdir(dir_log_train))
    # df_result = pd.DataFrame()
    # locomotives = []
    # for i, path in enumerate(dirs_model):
    #     dir_model = os.path.join(dir_log_train, path)
    #     with open(os.path.join(dir_model, 'optimal'), 'r') as f:
    #         opt_id = int(f.readline().strip())
    #     df = pd.read_csv(os.path.join(dir_model, 'metrics.csv'))
    #     if i == 0:
    #         df_result = pd.DataFrame(columns=df.columns)
    #     df_result.loc[df_result.shape[0]] = df.loc[opt_id]
    #     locomotives.append(path.split('_')[0])
    # colums = list(df_result.columns)
    # df_result['locomotives'] = np.array(locomotives)
    # df_result = df_result[['locomotives']+colums]
    # df_result.to_csv(file_result, index=False)






