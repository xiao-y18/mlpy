from sklearn.metrics import explained_variance_score, mean_squared_error, \
    mean_squared_log_error, median_absolute_error, r2_score
import numpy as np


def residual(y_true, y_pred):
    return y_true - y_pred


def residual_abs(y_true, y_pred):
    return np.abs(residual(y_true, y_pred))


def mean_error(y_true, y_pred):
    return np.nanmean(residual(y_true, y_pred))

def median_error(y_true, y_pred):
    return np.nanmedian(residual(y_true, y_pred))


def max_absolute_error(y_true, y_pred):
    """https://scikit-learn.org/stable/modules/model_evaluation.html#max-error"""
    return np.nanmax(residual_abs(y_true, y_pred))

def mean_absolute_error(y_true, y_pred):
    return np.nanmean(residual_abs(y_true, y_pred))


METRICS_FUNCTION = {
    'mean_error': mean_error,
    'mean_absolute_error': mean_absolute_error,
    'median_error': median_error,
    'median_absolute_error': median_absolute_error,
    'max_absolute_error': max_absolute_error,
    'explained_variance_score': explained_variance_score,
    'mean_squared_error': mean_squared_error,
    'mean_squared_log_error': mean_squared_log_error,
    'r2_score': r2_score
}
def run(yp, y, dir_out=None):
    import os
    import json

    scores = {}
    for key, func in METRICS_FUNCTION.items():
        scores[key] = func(y, yp)

    if dir_out is None:
        return scores
    else:
        with open(os.path.join(dir_out, 'metrics.json'), 'w') as f:
            json.dump(scores, f)
