import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os

def hi_normal():
    dir_log = 'cache/paper'
    file_hi_normal = 'cache/paper/hi.csv'

    df = pd.read_csv(file_hi_normal)

    print("Inf file")
    filter_inf = df['hi'] == np.inf
    print(df['fname'][filter_inf])

    # del inf
    df = df[~filter_inf]

    mu = np.mean(df['hi'])
    sigma = np.std(df['hi'])
    plt.scatter(np.arange(df.shape[0]), df['hi'], label='HI')
    plt.ylabel('Health Value')
    plt.axhline(mu, linestyle='--', color='r', label='mean')
    plt.axhline(mu + sigma, linestyle='--', color='grey', label='mean+std')
    plt.axhline(mu + 2 * sigma, linestyle='--', color='grey', label='mean+2*std')
    plt.axhline(mu + 3 * sigma, linestyle='--', color='grey', label='mean+3*std')
    plt.savefig(os.path.join(dir_log, 'hi.png'))
    plt.close()

    plt.hist(df['hi'])
    plt.savefig(os.path.join(dir_log, 'hi_hist.png'))
    plt.close()

    print(mu, sigma, mu + sigma, mu + 2 * sigma, mu + 3 * sigma, np.min(df['hi']), np.max(df['hi']))
    # 0.4204197936515976 0.15573353826617628 0.5761533319177738 0.7318868701839502 0.8876204084501265 0.1034375 1.95196298846

def hi_failure():
    dir_log = 'cache/paper'
    file_hi_failure = 'cache/paper/failure/hi.csv'

    df = pd.read_csv(file_hi_failure)
    plt.scatter(np.arange(df.shape[0]), df['hi'], label='HI')
    plt.savefig(os.path.join(dir_log, 'hi_failure.png'))
    plt.close()

    mu = np.mean(df['hi'])
    sigma = np.std(df['hi'])
    print(mu, sigma, mu+sigma, mu+2*sigma, mu+3*sigma, np.min(df['hi']), np.max(df['hi']))
    # 2.6526619779108995 1.840206492649098 4.492868470559998 6.3330749632090955 8.173281455858193 0.481513907396 11.3951843316

def hi_mix():
    dir_log = 'cache/paper'
    file_hi_normal = 'cache/paper/hi.csv'
    file_hi_failure = 'cache/paper/failure/hi.csv'

    df = pd.read_csv(file_hi_normal)
    # del inf
    filter_inf = df['hi'] == np.inf
    df = df[~filter_inf]
    mu = np.mean(df['hi'])
    sigma = np.std(df['hi'])
    df = df[-20::]

    df_failure = pd.read_csv(file_hi_failure)

    hi = np.concatenate([df['hi'].values, df_failure['hi'].values])
    x = np.arange(len(hi))
    x_failure = df.shape[0] + np.arange(df_failure.shape[0])

    plt.scatter(x, hi, label='HI Normal')
    plt.scatter(x_failure, hi[x_failure], color='r')
    plt.ylabel('Health Value')
    plt.ylim((0, 12))
    # plt.axhline(mu, linestyle='--', color='r', label='mean')
    # plt.axhline(mu + sigma, linestyle='--', color='grey', label='mean+std')
    # plt.axhline(mu + 2 * sigma, linestyle='--', color='grey', label='mean+2*std')
    plt.axhline(mu + 3 * sigma, linestyle='--', color='grey', label='mean+3*std')
    plt.savefig(os.path.join(dir_log, 'hi_mix.png'))
    plt.close()

    print(mu, sigma, mu+sigma, mu+2*sigma, mu+3*sigma, np.max(hi))
    # 0.4204197936515976 0.15573353826617628 0.5761533319177738 0.7318868701839502 0.8876204084501265 11.3951843316

if __name__ == '__main__':
    hi_normal()
