"""Processing new data that are saved in different format."""

from mlpy.icde20phm import data
from mlpy.icde20phm import paper_sr

import os
import pandas as pd
import matplotlib.pyplot as plt
import shutil
import numpy as np

DIR_DATA = os.path.join(data.DIR_DATA_ROOT, '1704')

def extract_temp_tc2():
    temp_name_dic = {
        'TC2车1轴温度值_ZT1013': 'temp_tc2_01',
        'TC2车2轴温度值_ZT1012': 'temp_tc2_02',
        'TC2车3轴温度值_ZT1015': 'temp_tc2_03',
        'TC2车4轴温度值_ZT1014': 'temp_tc2_04',
        'TC2车5轴温度值_ZT1017': 'temp_tc2_05',
        'TC2车6轴温度值_ZT1016': 'temp_tc2_06',
        'TC2车7轴温度值_ZT1019': 'temp_tc2_07',
        'TC2车8轴温度值_ZT1018': 'temp_tc2_08',
    }
    ntrain = '1704'

    dir_plot = 'cache/1704/vis_temp_tc2'
    if os.path.exists(dir_plot) is False:
        os.makedirs(dir_plot)
    dir_data_out = 'cache/1704/data_temp_tc2'
    if os.path.exists(dir_data_out) is False:
        os.makedirs(dir_data_out)

    temp_name = list(temp_name_dic.keys())
    fname_list = [f for f in os.listdir(os.path.join(DIR_DATA, temp_name[0])) if f.endswith('.csv')]
    fname_list = sorted(fname_list)
    for fname in fname_list:
        df = {}
        df['timestamp'] = []
        for temp in temp_name:
            df_ = pd.read_csv(os.path.join(DIR_DATA, '{}/{}'.format(temp, fname)),
                              header=None, names=['timestamp', 'number', 'temp'])
            df[temp_name_dic[temp]] = df_['temp']
            df['timestamp'] = df_['timestamp']
        df = pd.DataFrame(df)
        if df.empty:
            print("!! {} is empty".format(fname))
            continue
        df[data.COLUMNS_TEMP_TC2].plot()
        plt.savefig(os.path.join(dir_plot, '{}.png'.format(fname)))
        plt.close()
        df['ntrain'] = np.array([ntrain]*df.shape[0])
        df.to_csv(os.path.join(dir_data_out, fname), index=False)


if __name__ == '__main__':
    dir_in = 'cache/1704/data_temp_tc2'

    fnames = ['1704_2018-06-01.csv', '1704_2018-06-05.csv', '1704_2018-06-06.csv',
              '1704_2018-06-11.csv', '1704_2018-06-12.csv', '1704_2018-06-13.csv',
              '1704_2018-06-14.csv', '1704_2018-06-16.csv', '1704_2018-06-18.csv',
              '1704_2018-06-21.csv', '1704_2018-07-10.csv', '1704_2018-07-11.csv',
              '1704_2018-07-12.csv', '1704_2018-07-13.csv', '1704_2018-07-14.csv',
              '1704_2018-07-15.csv', '1704_2018-07-16.csv', '1704_2018-07-21.csv',
              '1704_2018-07-22.csv', '1704_2018-07-23.csv', '1704_2018-07-24.csv',
              '1704_2018-07-27.csv', '1704_2018-07-29.csv', '1704_2018-07-30.csv',
              '1704_2018-07-31.csv', '1704_2018-08-01.csv', '1704_2018-08-03.csv',
              '1704_2018-08-04.csv', '1704_2018-08-05.csv', '1704_2018-08-06.csv',
              '1704_2018-08-07.csv', '1704_2018-08-11.csv', '1704_2018-08-12.csv',
              '1704_2018-08-13.csv', '1704_2018-08-14.csv', '1704_2018-08-15.csv',
              '1704_2018-08-16.csv', '1704_2018-08-17.csv', '1704_2018-08-18.csv',
              '1704_2018-08-19.csv', '1704_2018-08-20.csv', '1704_2018-08-21.csv',
              '1704_2018-08-22.csv', '1704_2018-08-24.csv']

    dir_data_clean = 'cache/1704/data_temp_tc2_clean'
    # if os.path.exists(dir_data_clean) is True:
    #     shutil.rmtree(dir_data_clean)
    # os.makedirs(dir_data_clean)
    # for f in fnames:
    #     src = os.path.join(dir_in, f)
    #     dst = os.path.join(dir_data_clean, f)
    #     shutil.copyfile(src, dst)
    #
    # dir_data_consistent = 'cache/1704/data_temp_tc2_consistent'
    # data.consistent_batch(dir_data_clean, dir_data_consistent)

    dir_model = 'cache/paper/failure/train/1704_2018-10-28_1704_2018-10-30_mix'
    dir_res = 'cache/1704/residuals'
    # if os.path.exists(dir_res) is False:
    #     os.makedirs(dir_res)
    # dir_hi = 'cache/1704/hi'
    # if os.path.exists(dir_hi) is False:
    #     os.makedirs(dir_hi)
    # paper_sr.infer_persist(dir_data_clean, dir_res, dir_model, fnames)
    # paper_sr.extract_hi(dir_res, dir_hi)

    df_1704 = pd.read_csv('cache/1704/hi_1704.csv')
    df_normal = pd.read_csv('cache/1704/hi_normal.csv')

    filter_inf = df_normal['hi'] == np.inf
    df_normal = df_normal[~filter_inf]
    mu = np.mean(df_normal['hi'])
    sigma = np.std(df_normal['hi'])
    df_normal = df_normal[-20::]


    hi = np.concatenate([df_normal['hi'].values, df_1704['hi'].values])
    x = np.arange(len(hi))
    x_failure = df_normal.shape[0] + np.arange(df_1704.shape[0])

    plt.figure(figsize=(8, 5))
    plt.scatter(x, hi, label='HI Normal')
    plt.scatter(x_failure, hi[x_failure], color='r')
    plt.ylabel('Health Value')
    plt.ylim((0, 12))
    # plt.axhline(mu, linestyle='--', color='r', label='mean')
    # plt.axhline(mu + sigma, linestyle='--', color='grey', label='mean+std')
    # plt.axhline(mu + 2 * sigma, linestyle='--', color='grey', label='mean+2*std')
    plt.axhline(mu + 3 * sigma, linestyle='--', color='grey', label='mean+3*std')
    plt.savefig('cache/1704/hi.png')
    plt.close()


