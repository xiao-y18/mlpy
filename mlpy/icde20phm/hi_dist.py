from mlpy.icde20phm.data import *

import os
import multiprocessing as mp

def dist(col_target, columns, win, df):
    print("*** processing {}".format(col_target))
    cols_rest = [c for c in columns if c != col_target]
    res = []
    for i in range(win, df.shape[0] + 1):
        value_target = df[col_target][(i - win):i]
        diff = 0
        for col in cols_rest:
            value_rest = df[col][(i - win):i]
            diff += np.abs(np.mean(np.abs(value_target - value_rest)))
        res.append(diff / len(cols_rest))
    return (col_target, res)

def dist_main(fname, dir_data):
    dir_out = 'cache/hi_tc2'
    if os.path.exists(dir_out) is False:
        os.makedirs(dir_out)

    n_cpu = mp.cpu_count()
    print("n_cpu = {}".format(n_cpu))
    with mp.Pool(n_cpu//2) as pool:
        df = pd.read_csv(os.path.join(dir_data, fname))
        win = 600  # 10 minute
        pool_programs = [pool.apply_async(dist, args=(col_target, COLUMNS_TEMP_TC2, win, df))
                         for col_target in COLUMNS_TEMP_TC2]
        results = [p.get() for p in pool_programs]
        results_dic = {}
        for res in results:
            results_dic[res[0]] = res[1]

    result = pd.DataFrame(results_dic)
    result.plot()
    plt.savefig(os.path.join(dir_out, '{}.png'.format(fname)))
    plt.title(fname)
    plt.close()
    agg = result.mean(axis=1)
    plt.plot(agg)
    plt.title(fname)
    plt.savefig(os.path.join(dir_out, '{}_agg.png'.format(fname)))
    plt.close()

if __name__ == '__main__':
    dir_data = DIR_DATA_CONSISTENT
    fname_list = get_fnames(dir_data)
    # select some examples for experiment
    fname_list = [fname for fname in fname_list if fname.startswith('1703') or fname.startswith('1704')]
    n = len(fname_list)
    for i, fname in enumerate(fname_list):
        print("*** [{}/{}] processing {}".format(i, n, fname))
        dist_main(fname, dir_data)