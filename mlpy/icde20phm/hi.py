from mlpy.icde20phm import data
from mlpy.icde20phm import metrics

import os
import pandas as pd
import matplotlib.pyplot as plt

def health_indicators_extract(dir_root, dir_out, tag_model):
    fname_list = data.get_fnames(dir_root)
    indicators = {'fname':[]}
    for key in metrics.METRICS_FUNCTION.keys():
        indicators[key] = []
    for fname in fname_list:
        df = pd.read_csv(os.path.join(dir_root, fname))
        indicators['fname'].append(fname)
        for key, func in metrics.METRICS_FUNCTION.items():
            indicators[key].append(func(df['y'], df['yp']))
    indicators = pd.DataFrame(indicators)
    indicators.to_csv(os.path.join(dir_out, '{}.csv'.format(tag_model)), index=False)


def health_indicators_plot(dir_root):
    fname_list = data.get_fnames(dir_root)
    for fname in fname_list:
        df = pd.read_csv(os.path.join(dir_root, fname))
        filter_failure = df['fname'].apply(lambda x: x.startswith('1704'))
        df_failure = df[filter_failure]
        df_healthy = df[~filter_failure]
        cols_indicator = [col for col in df.columns if col != 'fname']
        fig, axes = plt.subplots(nrows=df.shape[1]-1, ncols=1, sharex=True, figsize=(8,15))
        for ax, col in zip(axes, cols_indicator):
            ax.plot(df_failure[col], 'or', label='failure')
            ax.plot(df_healthy[col], 'ob', label='healthy')
            ax.legend(loc='best')
            ax.set_title(col)
        fig.savefig(os.path.join(dir_root, '{}.png'.format(fname)))


def run(dir_root, dir_out):
    if os.path.exists(dir_out) is False:
        os.makedirs(dir_out)
    from IPython import embed; embed()
    tag_models = [t for t in os.listdir(dir_root) if t.startswith('.') is False]
    for tag_model in tag_models:
        print("processing {}".format(tag_model))
        health_indicators_extract(os.path.join(dir_root, tag_model), dir_out, tag_model)

    health_indicators_plot(dir_out)