from mlpy.icde20phm import data
from mlpy.icde20phm import paper_sr

import os
import shutil
import matplotlib.pyplot as plt

if __name__ == '__main__':
    dir_data = data.DIR_DATA_REFINED
    dir_root = 'cache/paper/failure'
    dir_model = '{}/train/1704_2018-10-28_1704_2018-10-30_mix'.format(dir_root)
    dir_infer = '{}/infer'.format(dir_root)
    train_no = '1704'

    if os.path.exists(dir_infer):
        shutil.rmtree(dir_infer)
    os.makedirs(dir_infer)

    fname_list = data.get_fnames(dir_data)
    fname_list = [f for f in fname_list if f.startswith(train_no)]

    paper_sr.infer_persist(dir_data, dir_infer, dir_model, fname_list)
    paper_sr.extract_hi(dir_infer, dir_root)

    


