from mlpy.icde20phm import data
from mlpy.icde20phm import hi
from mlpy.icde20phm.srdeap import DeapModel

import numpy as np
import pandas as pd
import os
import pickle
import matplotlib.pyplot as plt

from deap import tools, creator, base
import random
import operator
from mlpy.tsa.sr.deap.base import *

#DIR_SR_DEAP_NODELAY = 'cache/sr_deap_nodelay'
DIR_SR_DEAP_NODELAY = 'cache/sr_deap_nodelay_10smean'
DIR_SR_DEAP_NODELAY_TRAIN = '{}/train/'.format(DIR_SR_DEAP_NODELAY)
DIR_SR_DEAP_NODELAY_INFER = '{}/infer/'.format(DIR_SR_DEAP_NODELAY)
DIR_SR_DEAP_NODELAY_INFER_PERSIST = '{}/infer_persist/'.format(DIR_SR_DEAP_NODELAY)
DIR_SR_DEAP_NODELAY_HI = '{}/health_indicators/'.format(DIR_SR_DEAP_NODELAY)

DIR_DATA = '{}/10S_mean'.format(data.DIR_DATA_RESAMPLE)


def train_unit_file_split_half_skip(dir_root, fname, mid):
    dir_log = os.path.join(DIR_SR_DEAP_NODELAY_TRAIN, '{}'.format(fname.split('.')[0]))
    if os.path.exists(dir_log) is False:
        os.makedirs(dir_log)

    var_names = data.COLUMNS_TEMP_TC2.copy()
    target_name = 'temp_tc2_07'
    var_names.remove(target_name)

    df = pd.read_csv(os.path.join(dir_root, fname))
    X = df[var_names].values
    y = df[target_name].values
    X_train, y_train, X_test, y_test = data.split_train_test(X, y, mode='half_skip')

    model_cls = DeapModel(var_names, target_name, mid, dir_log, verbose=True)
    model_cls.fit(X_train, y_train, X_test, y_test)
    yp = model_cls.predict(X)
    model_cls.compare_plot(yp, y, model_cls.dir_log)
def train_two_file(dir_root, fname_train, fname_test, mid):
    dir_log = os.path.join(DIR_SR_DEAP_NODELAY_TRAIN, '{}_{}'.format(
        fname_train.split('.')[0], fname_test.split('.')[0]
    ))
    if os.path.exists(dir_log) is False:
        os.makedirs(dir_log)

    var_names = data.COLUMNS_TEMP_TC2.copy()
    target_name = 'temp_tc2_07'
    var_names.remove(target_name)

    df_train = pd.read_csv(os.path.join(dir_root, fname_train))
    X_train = df_train[var_names].values
    y_train = df_train[target_name].values
    df_test = pd.read_csv(os.path.join(dir_root, fname_test))
    X_test = df_test[var_names].values
    y_test = df_test[target_name].values

    model_cls = DeapModel(var_names, target_name, mid, dir_log)
    model_cls.fit(X_train, y_train, X_test, y_test)
    yp = model_cls.predict(X_test)
    model_cls.compare_plot(yp, y_test, model_cls.dir_log)
def train_two_file_mix(dir_root, fname1, fname2, mid):
    dir_log = os.path.join(DIR_SR_DEAP_NODELAY_TRAIN, '{}_{}_mix'.format(
        fname1.split('.')[0], fname2.split('.')[0]))
    if os.path.exists(dir_log) is False:
        os.makedirs(dir_log)

    var_names = data.COLUMNS_TEMP_TC2.copy()
    target_name = 'temp_tc2_07'
    var_names.remove(target_name)

    df1 = pd.read_csv(os.path.join(dir_root, fname1))
    X1 = df1[var_names].values
    y1 = df1[target_name].values
    X1_train, y1_train, X1_test, y1_test = data.split_train_test(X1, y1, 'half_skip')
    df2 = pd.read_csv(os.path.join(dir_root, fname2))
    X2 = df2[var_names].values
    y2 = df2[target_name].values
    X2_train, y2_train, X2_test, y2_test = data.split_train_test(X2, y2, 'half_skip')

    X_train = np.vstack([X1_train, X2_train])
    y_train = np.concatenate([y1_train, y2_train])
    X_test = np.vstack([X1_test, X2_test])
    y_test = np.concatenate([y1_test, y2_test])

    model_cls = DeapModel(var_names, target_name, mid, dir_log)
    model_cls.fit(X_train, y_train, X_test, y_test)
    yp = model_cls.predict(X_test)
    model_cls.compare_plot(yp, y_test, model_cls.dir_log)
def run_train():
    dir_root = DIR_DATA

    train_unit_file_split_half_skip(dir_root, fname='1704_2018-10-28.csv', mid=0)
    train_unit_file_split_half_skip(dir_root, fname='1704_2018-10-30.csv', mid=0)
    train_unit_file_split_half_skip(dir_root, fname='1703_2018-10-04.csv', mid=0)
    train_unit_file_split_half_skip(dir_root, fname='1703_2018-10-05.csv', mid=0)

    train_two_file(dir_root, fname_train='1704_2018-10-28.csv', fname_test='1704_2018-10-30.csv', mid=0)
    train_two_file(dir_root, fname_train='1703_2018-10-04.csv', fname_test='1703_2018-10-05.csv', mid=0)

    train_two_file_mix(dir_root, fname1='1704_2018-10-28.csv', fname2='1704_2018-10-30.csv', mid=0)
    train_two_file_mix(dir_root, fname1='1703_2018-10-04.csv', fname2='1703_2018-10-05.csv', mid=0)


def infer_unit_model_persist(dir_root, dir_model, dir_log, mid):
    var_names = data.COLUMNS_TEMP_TC2.copy()
    target_name = 'temp_tc2_07'
    var_names.remove(target_name)

    model_cls = DeapModel(var_names, target_name, mid, dir_model)
    fname_list = data.get_fnames(dir_root)
    for fname in fname_list:
        df = pd.read_csv(os.path.join(dir_root, fname))
        X = df[var_names].values
        y = df[target_name].values
        yp = model_cls.predict(X)
        res = {'timestamp': df['timestamp'].values,
               'y': y,
               'yp': yp}
        res_df = pd.DataFrame(res)
        res_df.to_csv(os.path.join(dir_log, fname), index=False)
def infer_paired_model_persist(dir_root, models, dir_log):
    var_names = data.COLUMNS_TEMP_TC2.copy()
    target_name = 'temp_tc2_07'
    var_names.remove(target_name)

    for key, model in models.items():
        dir_model, mid = model
        model_cls = DeapModel(var_names, target_name, mid, dir_model)
        fname_list = data.get_fnames(dir_root)
        fname_list = [fname for fname in fname_list if fname.startswith(key)]
        for fname in fname_list:
            df = pd.read_csv(os.path.join(dir_root, fname))
            X = df[var_names].values
            y = df[target_name].values
            yp = model_cls.predict(X)
            res = {'timestamp': df['timestamp'].values,
                   'y': y,
                   'yp': yp}
            res_df = pd.DataFrame(res)
            res_df.to_csv(os.path.join(dir_log, fname), index=False)
def run_infer_persist():
    dir_root = DIR_DATA

    models = [
        (os.path.join(DIR_SR_DEAP_NODELAY_TRAIN, '1704_2018-10-28'), 0),
        (os.path.join(DIR_SR_DEAP_NODELAY_TRAIN, '1704_2018-10-30'), 0),
        (os.path.join(DIR_SR_DEAP_NODELAY_TRAIN, '1703_2018-10-04'), 0),
        (os.path.join(DIR_SR_DEAP_NODELAY_TRAIN, '1703_2018-10-05'), 0),
        (os.path.join(DIR_SR_DEAP_NODELAY_TRAIN, '1704_2018-10-28_1704_2018-10-30'), 0),
        (os.path.join(DIR_SR_DEAP_NODELAY_TRAIN, '1703_2018-10-04_1703_2018-10-05'), 0),
        (os.path.join(DIR_SR_DEAP_NODELAY_TRAIN, '1704_2018-10-28_1704_2018-10-30_mix'), 0),
        (os.path.join(DIR_SR_DEAP_NODELAY_TRAIN, '1703_2018-10-04_1703_2018-10-05_mix'), 0)
    ]
    for model in models:
        dir_model, mid = model
        dir_out = os.path.join(
            DIR_SR_DEAP_NODELAY_INFER_PERSIST, '{}'.format(os.path.basename(dir_model)))
        if os.path.exists(dir_out) is False:
            os.makedirs(dir_out)
        print("processing {}".format(dir_model))
        infer_unit_model_persist(dir_root, dir_model, dir_out, mid)

    models_paired = [
        (
            {'1703': (os.path.join(DIR_SR_DEAP_NODELAY_TRAIN, '1703_2018-10-04'), 0),
             '1704': (os.path.join(DIR_SR_DEAP_NODELAY_TRAIN, '1704_2018-10-30'), 0)},
            os.path.join(DIR_SR_DEAP_NODELAY_INFER_PERSIST, 'paired_unit_model')
        ),
        (
            {'1703': (os.path.join(DIR_SR_DEAP_NODELAY_TRAIN, '1703_2018-10-04_1703_2018-10-05'), 0),
             '1704': (os.path.join(DIR_SR_DEAP_NODELAY_TRAIN, '1704_2018-10-28_1704_2018-10-30'), 0)},
            os.path.join(DIR_SR_DEAP_NODELAY_INFER_PERSIST, 'paired_cross_valid')
        ),
        (
            {'1703': (os.path.join(DIR_SR_DEAP_NODELAY_TRAIN, '1703_2018-10-04_1703_2018-10-05_mix'), 0),
             '1704': (os.path.join(DIR_SR_DEAP_NODELAY_TRAIN, '1704_2018-10-28_1704_2018-10-30_mix'), 0)},
            os.path.join(DIR_SR_DEAP_NODELAY_INFER_PERSIST, 'paired_mix_valid')
        )
    ]
    for item in models_paired:
        models, dir_out = item
        if os.path.exists(dir_out) is False:
            os.makedirs(dir_out)
        print("processing {}".format(dir_out))
        infer_paired_model_persist(dir_root, models, dir_out)


def run_health_indicators():
    dir_root = os.path.join(DIR_SR_DEAP_NODELAY_INFER_PERSIST)
    dir_out = DIR_SR_DEAP_NODELAY_HI
    hi.run(dir_root, dir_out)


if __name__ == '__main__':
    run_train()
    run_infer_persist()
    run_health_indicators()
