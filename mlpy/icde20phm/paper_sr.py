from mlpy.icde20phm import data
from mlpy.icde20phm.srdeap import DeapModel
from mlpy.icde20phm import metrics

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from time import time


DIR_SR_DEAP_NODELAY = 'cache/paper'
DIR_SR_DEAP_NODELAY_TRAIN = '{}/failure/train'.format(DIR_SR_DEAP_NODELAY)

def train_two_file_mix(dir_data, dir_log, fname1, fname2, mid, train=False):
    dir_log = os.path.join(dir_log, '{}_{}_mix'.format(
        fname1.split('.')[0], fname2.split('.')[0]))
    if os.path.exists(dir_log) is False:
        os.makedirs(dir_log)

    var_names = data.COLUMNS_TEMP_TC2.copy()
    target_name = 'temp_tc2_07'
    var_names.remove(target_name)

    df1 = pd.read_csv(os.path.join(dir_data, fname1))
    X1 = df1[var_names].values
    y1 = df1[target_name].values
    X1_train, y1_train, X1_test, y1_test = data.split_train_test(X1, y1, 'half_skip')
    df2 = pd.read_csv(os.path.join(dir_data, fname2))
    X2 = df2[var_names].values
    y2 = df2[target_name].values
    X2_train, y2_train, X2_test, y2_test = data.split_train_test(X2, y2, 'half_skip')

    X_train = np.vstack([X1_train, X2_train])
    y_train = np.concatenate([y1_train, y2_train])
    X_test = np.vstack([X1_test, X2_test])
    y_test = np.concatenate([y1_test, y2_test])

    t_train, t_test = None, None
    if train is False:
        with open(os.path.join(dir_log, 'optimal'), 'r') as f:
            opt_id = int(f.readline().strip())
        model_cls = DeapModel(var_names, target_name, opt_id, dir_log)
    else:
        t = time()
        model_cls = DeapModel(var_names, target_name, mid, dir_log)
        model_cls.fit(X_train, y_train, X_test, y_test)
        t_train = time() - t
        table = model_cls.get_metrics(X_test, y_test, 'temp')
        table.to_csv(os.path.join(dir_log, 'metrics.csv'), index=False)
        opt_id = model_cls.get_optimal_model(table, complex_max=15)
        with open(os.path.join(dir_log, 'optimal'), 'w') as f:
            f.write(str(opt_id))
        model_cls.set_mid(opt_id)
    t = time()
    yp = model_cls.predict(X_test)
    t_test = time() - t
    model_cls.compare_plot(yp, y_test, model_cls.dir_log)
    model_cls.hist_plot(yp, y_test, model_cls.dir_log)

    return t_train, t_test, X_train.shape[0], X_test.shape[0]


def infer_persist(dir_data, dir_log, dir_model, fname_list):
    var_names = data.COLUMNS_TEMP_TC2.copy()
    target_name = 'temp_tc2_07'
    var_names.remove(target_name)

    with open(os.path.join(dir_model, 'optimal'), 'r') as f:
        opt_id = int(f.readline().strip())
    model_cls = DeapModel(var_names, target_name, opt_id, dir_model)
    for fname in fname_list:
        df = pd.read_csv(os.path.join(dir_data, fname))
        X = df[var_names].values
        y = df[target_name].values
        yp = model_cls.predict(X)
        res = {'timestamp': df['timestamp'].values,
               'y': y,
               'yp': yp}
        res_df = pd.DataFrame(res)
        res_df.to_csv(os.path.join(dir_log, fname), index=False)


def extract_hi(dir_data, dir_log):
    fname_list = data.get_fnames(dir_data)
    his = {'fname':[], 'hi':[]}
    for fname in fname_list:
        df = pd.read_csv(os.path.join(dir_data, fname))
        his['fname'].append(fname)
        his['hi'].append(metrics.mean_absolute_error(df['y'], df['yp']))
    his = pd.DataFrame(his)
    his.to_csv(os.path.join(dir_log, 'hi.csv'), index=False)


if __name__ == '__main__':

    #dir_root = '{}/10S_mean'.format(data.DIR_DATA_RESAMPLE)
    dir_root = data.DIR_DATA_REFINED
    #dir_root = data.DIR_DATA_DEL_STATIC_END

    dir_log = DIR_SR_DEAP_NODELAY_TRAIN

    train_two_file_mix(dir_root, dir_log, fname1='1704_2018-10-28.csv', fname2='1704_2018-10-30.csv', mid=0, train=False)

    #train_two_file_mix(dir_root, fname1='1703_2018-10-04.csv', fname2='1703_2018-10-05.csv', mid=0, train=True)
