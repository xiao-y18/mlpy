from mlpy.icde20phm import data
from mlpy.icde20phm import metrics
from mlpy.tsa.sr.ffx_learn import call_ffx
from mlpy.tsa.sr import common as srcom

import numpy as np
import pandas as pd
import os
import pickle
import random
import matplotlib.pyplot as plt

class FFXModel(object):
    def __init__(self, var_names, target_name, mid, dir_out):
        self.var_names = var_names
        self.target_name = target_name
        self.mid = mid
        self.dir_out = dir_out

    def fit(self, X_train, y_train, X_test, y_test, verbose=None):
        result = call_ffx.run_ffx(X_train, y_train, X_test, y_test, self.var_names, verbose)
        self._save_result(result, self.dir_out)

    def predict(self, X, mid=None):
        if mid is None:
            mid = self.mid
        model = self.load_model(mid)
        yp = model.simulate(X)
        return yp

    def _save_result(self, result, dir_out):
        with open(os.path.join(dir_out, 'result.pkl'), 'wb') as f:
            pickle.dump(result, f)

        numBases, nrmses, rmses, models = result
        nrmses_test, nrmses_train = nrmses
        rmses_test, rmses_train = rmses
        with open(os.path.join(dir_out, 'result'), 'w') as f:
            strline = "{0} {1} {0}".format("*" * 20, self.target_name)
            f.write(strline + '\n')
            print(strline)

            strline = "there are {} models".format(len(models))
            f.write(strline + '\n')
            print(strline)
            for model in models:
                strline = str(model)
                f.write(strline + '\n')
                print(strline)
            strline = "{0}".format("-" * 25)
            f.write(strline + '\n')
            print(strline)

            strline = "complexity\t\tnrmse_test\t\tnrmse_train\t\trmse_test\t\trmse_train"
            f.write(strline)
            print(strline)
            for com, nrmse_test, nrmse_train, rmse_test, rmse_train in \
                    zip(numBases, nrmses_test, nrmses_train, rmses_test, rmses_train):
                strline = "{0}\t\t{1}\t\t{2}\t\t{3}\t\t{4}".format(com, nrmse_test, nrmse_train,
                                                                   rmse_test, rmse_train)
                f.write(strline + '\n')
                print(strline)

            strline = "{0}".format("*" * 40)
            f.write(strline + '\n')
            print(strline)

        # ﻿Pareto front, nrmse on test set
        plt.figure()
        plt.plot(numBases, nrmses_test)
        plt.title("NRMSE Pareto Front of {}".format(self.target_name))
        plt.xlabel("Complexity")
        plt.ylabel("Error")
        plt.tight_layout()
        plt.savefig(os.path.join(dir_out, 'pareto_nrmse.png'))
        plt.close()
        # Pareto front, rmse on test set
        plt.figure()
        plt.plot(numBases, rmses_test)
        plt.title("RMSE Pareto Front of {}".format(self.target_name))
        plt.xlabel("Complexity")
        plt.ylabel("Error")
        plt.tight_layout()
        plt.savefig(os.path.join(dir_out, 'pareto_rmse.png'))
        plt.close()

    def load_model(self, mid, verbose=None):
        with open(os.path.join(self.dir_out, 'result.pkl'), 'rb') as f:
            result = pickle.load(f)

        numBases, nrmses, rmses, models = result
        nrmses_test, nrmses_train = nrmses
        rmses_test, rmses_train = rmses

        if verbose:
            print("Your model is: ")
            print(str(models[mid]))
            print("numBases={}, nrmse_test={}, nrmse_train={}, rmse_test={}, rmse_train={}".format(
                numBases[mid], nrmses_test[mid], nrmses_train[mid], rmses_test[mid], rmses_train[mid]
            ))

        return models[mid]

    def compare_plot(self, yp, y, dir_out):
        srcom.compare_plot_english(y, yp, self.target_name, dir_out)

def split_train_test(X, y, mode, ratio_train=None):
    n_all = len(X)
    if mode == 'ratio':
        n_train = np.floor(ratio_train * n_all)
        X_train = X[:n_train, :]
        X_test = X[n_train:]
        y_train = y[:n_train]
        y_test = y[n_train:]
    elif mode == 'half':
        n_train = np.floor(n_all/2)
        X_train = X[:n_train, :]
        X_test = X[n_train:]
        y_train = y[:n_train]
        y_test = y[n_train:]
    elif mode == 'half_skip':
        X_train = X[::2, :]
        X_test = X[1::2, :]
        y_train = y[::2]
        y_test = y[1::2]
    elif mode == 'random':
        n_train = np.floor(ratio_train * n_all)
        inds_all = range(n_all)
        inds_train = random.sample(inds_all, n_train)
        inds_train = np.sort(inds_train)
        inds_test = np.setdiff1d(inds_all, inds_train)
        X_train = X[inds_train, :]
        X_test = X[inds_test, :]
        y_train = y[inds_train]
        y_test = y[inds_test]
    else:
        raise ValueError("Can't find the mode = {}".format(mode))

    return X_train, y_train, X_test, y_test


def train_unit_file_split_half_skip(dir_root, fname, mid):
    dir_out = 'cache/sr_ffx_nodelay/train/{}'.format(fname.split('.')[0])
    if os.path.exists(dir_out) is False:
        os.makedirs(dir_out)

    var_names = data.COLUMNS_TEMP_TC2.copy()
    target_name = 'temp_tc2_07'
    var_names.remove(target_name)

    df = pd.read_csv(os.path.join(dir_root, fname))
    y = df[target_name].values
    X = df[var_names].values

    X_train, y_train, X_test, y_test = split_train_test(X, y, mode='half_skip')
    ffx = FFXModel(var_names, target_name, mid, dir_out)
    ffx.fit(X_train, y_train, X_test, y_test)
    yp = ffx.predict(X)
    ffx.compare_plot(yp, y, ffx.dir_out)
def train_two_file(dir_root, fname_train, fname_test, mid):
    dir_out = 'cache/sr_ffx_nodelay/train/{}_{}'.format(
        fname_train.split('.')[0], fname_test.split('.')[0])
    if os.path.exists(dir_out) is False:
        os.makedirs(dir_out)

    var_names = data.COLUMNS_TEMP_TC2.copy()
    target_name = 'temp_tc2_07'
    var_names.remove(target_name)

    df_train = pd.read_csv(os.path.join(dir_root, fname_train))
    X_train = df_train[var_names].values
    y_train = df_train[target_name].values
    df_test = pd.read_csv(os.path.join(dir_root, fname_test))
    X_test = df_test[var_names].values
    y_test = df_test[target_name].values

    ffx = FFXModel(var_names, target_name, mid, dir_out)
    ffx.fit(X_train, y_train, X_test, y_test)
    yp = ffx.predict(X_test)
    ffx.compare_plot(yp, y_test, ffx.dir_out)
def train_two_file_mix(dir_root, fname1, fname2, mid):
    dir_out = 'cache/sr_ffx_nodelay/train/{}_{}_mix'.format(
        fname1.split('.')[0], fname2.split('.')[0])
    if os.path.exists(dir_out) is False:
        os.makedirs(dir_out)

    var_names = data.COLUMNS_TEMP_TC2.copy()
    target_name = 'temp_tc2_07'
    var_names.remove(target_name)

    df1 = pd.read_csv(os.path.join(dir_root, fname1))
    X1 = df1[var_names].values
    y1 = df1[target_name].values
    X_train1, y_train1, X_test1, y_test1 = split_train_test(X1, y1, 'half_skip')
    df2 = pd.read_csv(os.path.join(dir_root, fname2))
    X2 = df2[var_names].values
    y2 = df2[target_name].values
    X_train2, y_train2, X_test2, y_test2 = split_train_test(X2, y2, 'half_skip')

    X_train = np.vstack([X_train1, X_train2])
    y_train = np.concatenate([y_train1, y_train2])
    X_test = np.vstack([X_test1, X_test2])
    y_test = np.concatenate([y_test1, y_test2])

    ffx = FFXModel(var_names, target_name, mid, dir_out)
    ffx.fit(X_train, y_train, X_test, y_test)
    yp = ffx.predict(X_test)
    ffx.compare_plot(yp, y_test, ffx.dir_out)
def run_train():
    dir_root = data.DIR_DATA_REFINED
    fname = '1704_2018-10-28.csv'
    train_unit_file_split_half_skip(dir_root, fname, mid=5)
    fname = '1704_2018-10-30.csv'
    train_unit_file_split_half_skip(dir_root, fname, mid=5)

    fname = '1703_2018-10-04.csv'
    train_unit_file_split_half_skip(dir_root, fname, mid=3)
    fname = '1703_2018-10-05.csv'
    train_unit_file_split_half_skip(dir_root, fname, mid=5)


    fname_train = '1704_2018-10-28.csv'
    fname_test = '1704_2018-10-30.csv'
    train_two_file(dir_root, fname_train, fname_test, mid=3)

    fname_train = '1703_2018-10-04.csv'
    fname_test = '1703_2018-10-05.csv'
    train_two_file(dir_root, fname_train, fname_test, mid=3)


    fname1 = '1704_2018-10-28.csv'
    fname2 = '1704_2018-10-30.csv'
    train_two_file_mix(dir_root, fname1, fname2, mid=3)

    fname1 = '1703_2018-10-04.csv'
    fname2 = '1703_2018-10-05.csv'
    train_two_file_mix(dir_root, fname1, fname2, mid=4)


def infer_unit_model(dir_root, dir_model, dir_out_root, mid):
    fname_list = data.get_fnames(dir_root)

    var_names = data.COLUMNS_TEMP_TC2.copy()
    target_name = 'temp_tc2_07'
    var_names.remove(target_name)

    ffx = FFXModel(var_names, target_name, mid, dir_model)
    residual_dic = {}
    for fname in fname_list:
        dir_out = os.path.join(dir_out_root, fname.split('.')[0])
        if os.path.exists(dir_out) is False:
            os.makedirs(dir_out)

        df = pd.read_csv(os.path.join(dir_root, fname))
        y = df[target_name].values
        X = df[var_names].values
        yp = ffx.predict(X)
        ffx.compare_plot(yp, y, dir_out)
        residual = y - yp
        residual_dic[fname] = residual

    residual_means = {'file': [], 'norm_residual': []}
    for key, values in residual_dic.items():
        residual_means['file'].append(key)
        residual_means['norm_residual'].append(np.mean(values))
    residual_means = pd.DataFrame(residual_means)
    residual_means.to_csv(os.path.join(dir_out_root, 'residual_means.csv'), index=False)

    filter_1704 = residual_means['file'].apply(lambda x: x.startswith('1704'))
    residual_means_1704 = residual_means[filter_1704]
    residual_means_not1704 = residual_means[~filter_1704]

    plt.plot(residual_means_1704['norm_residual'], 'or', label='1704')
    plt.plot(residual_means_not1704['norm_residual'], 'ob', label='not1704')
    plt.legend(loc='best')
    plt.savefig(os.path.join(dir_out_root, 'residual_means.png'))
    plt.close()
def infer_paired_model(dir_root, models, dir_out_root):
    var_names = data.COLUMNS_TEMP_TC2.copy()
    target_name = 'temp_tc2_07'
    var_names.remove(target_name)

    residual_dic = {}
    for key, value in models.items():
        dir_model, mid = value
        ffx = FFXModel(var_names, target_name, mid, dir_model)

        fname_list = data.get_fnames(dir_root)
        fname_list = [fname for fname in fname_list if fname.startswith(key)]
        for fname in fname_list:
            dir_out = os.path.join(dir_out_root, fname.split('.')[0])
            if os.path.exists(dir_out) is False:
                os.makedirs(dir_out)

            df = pd.read_csv(os.path.join(dir_root, fname))
            y = df[target_name].values
            X = df[var_names].values
            yp = ffx.predict(X)
            ffx.compare_plot(yp, y, dir_out)
            residual = y - yp
            residual_dic[fname] = residual

    residual_means = {'file': [], 'norm_residual': []}
    for key, values in residual_dic.items():
        residual_means['file'].append(key)
        residual_means['norm_residual'].append(np.mean(values))
    residual_means = pd.DataFrame(residual_means)
    residual_means.to_csv(os.path.join(dir_out_root, 'residual_means.csv'), index=False)

    filter_1704 = residual_means['file'].apply(lambda x: x.startswith('1704'))
    residual_means_1704 = residual_means[filter_1704]
    residual_means_not1704 = residual_means[~filter_1704]

    plt.plot(residual_means_1704['norm_residual'], 'or', label='1704')
    plt.plot(residual_means_not1704['norm_residual'], 'ob', label='not1704')
    plt.legend(loc='best')
    plt.savefig(os.path.join(dir_out_root, 'residual_means.png'))
    plt.close()
def run_infer():
    dir_root = data.DIR_DATA_REFINED

    # dir_model = 'cache/sr_ffx_nodelay/train/1704_2018-10-28'
    # mid = 5
    # dir_model = 'cache/sr_ffx_nodelay/train/1704_2018-10-30'
    # mid = 5
    # dir_model = 'cache/sr_ffx_nodelay/train/1703_2018-10-04'
    # mid = 3
    # dir_model = 'cache/sr_ffx_nodelay/train/1703_2018-10-05'
    # mid = 5
    # dir_model = 'cache/sr_ffx_nodelay/train/1704_2018-10-28_1704_2018-10-30'
    # mid = 3
    # dir_model = 'cache/sr_ffx_nodelay/train/1703_2018-10-04_1703_2018-10-05'
    # mid = 3
    # dir_model = 'cache/sr_ffx_nodelay/train/1704_2018-10-28_1704_2018-10-28_mix'
    # mid = 3
    # dir_model = 'cache/sr_ffx_nodelay/train/1703_2018-10-04_1703_2018-10-04_mix'
    # mid = 4
    # dir_out_root = 'cache/sr_ffx_nodelay/infer/{}'.format(os.path.basename(dir_model))
    # infer_unit_model(dir_root, dir_model, dir_out_root, mid)


    # models = {
    #     '1703': ('cache/sr_ffx_nodelay/train/1703_2018-10-04', 3),
    #     '1704': ('cache/sr_ffx_nodelay/train/1704_2018-10-30', 5)
    # }
    # dir_out_root = 'cache/sr_ffx_nodelay/infer/two_unit_model'
    # infer_paired_model(dir_root, models, dir_out_root)

    # models = {
    #     '1703': ('cache/sr_ffx_nodelay/train/1703_2018-10-04_1703_2018-10-05', 3),
    #     '1704': ('cache/sr_ffx_nodelay/train/1704_2018-10-28_1704_2018-10-30', 3)
    # }
    # dir_out_root = 'cache/sr_ffx_nodelay/infer/two_cross_valid'
    # infer_paired_model(dir_root, models, dir_out_root)

    models = {
        '1703': ('cache/sr_ffx_nodelay/train/1703_2018-10-04_1703_2018-10-04_mix', 4),
        '1704': ('cache/sr_ffx_nodelay/train/1704_2018-10-28_1704_2018-10-28_mix', 3)
    }
    dir_out_root = 'cache/sr_ffx_nodelay/infer/two_mix_valid'
    infer_paired_model(dir_root, models, dir_out_root)


def infer_persist_unit_model(dir_root, dir_model, dir_out, mid):
    var_names = data.COLUMNS_TEMP_TC2.copy()
    target_name = 'temp_tc2_07'
    var_names.remove(target_name)

    ffx = FFXModel(var_names, target_name, mid, dir_model)
    fname_list = data.get_fnames(dir_root)
    for fname in fname_list:
        df = pd.read_csv(os.path.join(dir_root, fname))
        y = df[target_name].values
        X = df[var_names].values
        yp = ffx.predict(X)
        res = {'timestamp': df['timestamp'].values,
               'y': y,
               'yp': yp}
        res_df = pd.DataFrame(res)
        res_df.to_csv(os.path.join(dir_out, fname), index=False)
def infer_persist_paired_model(dir_root, models, dir_out):
    var_names = data.COLUMNS_TEMP_TC2.copy()
    target_name = 'temp_tc2_07'
    var_names.remove(target_name)

    for key, model in models.items(): # a model correspond to one type of file.
        dir_model, mid = model
        ffx = FFXModel(var_names, target_name, mid, dir_model)
        fname_list = data.get_fnames(dir_root)
        fname_list = [fname for fname in fname_list if fname.startswith(key)]
        for fname in fname_list:
            df = pd.read_csv(os.path.join(dir_root, fname))
            y = df[target_name].values
            X = df[var_names].values
            yp = ffx.predict(X)
            res = {'timestamp': df['timestamp'].values,
                   'y': y,
                   'yp': yp}
            res_df = pd.DataFrame(res)
            res_df.to_csv(os.path.join(dir_out, fname), index=False)
def run_infer_persist():
    dir_root = data.DIR_DATA_REFINED

    models = [
        ('cache/sr_ffx_nodelay/train/1704_2018-10-28', 5),
        ('cache/sr_ffx_nodelay/train/1704_2018-10-30', 5),
        ('cache/sr_ffx_nodelay/train/1703_2018-10-04', 3),
        ('cache/sr_ffx_nodelay/train/1703_2018-10-05', 5),
        ('cache/sr_ffx_nodelay/train/1704_2018-10-28_1704_2018-10-30', 3),
        ('cache/sr_ffx_nodelay/train/1703_2018-10-04_1703_2018-10-05', 3),
        ('cache/sr_ffx_nodelay/train/1704_2018-10-28_1704_2018-10-30_mix', 3),
        ('cache/sr_ffx_nodelay/train/1703_2018-10-04_1703_2018-10-05_mix', 4)
    ]
    for model in models:
        dir_model, mid = model
        dir_out = 'cache/sr_ffx_nodelay/infer_persist/{}'.format(os.path.basename(dir_model))
        if os.path.exists(dir_out) is False:
            os.makedirs(dir_out)
        print("processing {}".format(dir_model))
        infer_persist_unit_model(dir_root, dir_model, dir_out, mid)

    models_paired = [
        (
            {'1703': ('cache/sr_ffx_nodelay/train/1703_2018-10-04', 3),
             '1704': ('cache/sr_ffx_nodelay/train/1704_2018-10-30', 5)},
            'cache/sr_ffx_nodelay/infer_persist/paired_unit_model'
        ),
        (
            {'1703': ('cache/sr_ffx_nodelay/train/1703_2018-10-04_1703_2018-10-05', 3),
             '1704': ('cache/sr_ffx_nodelay/train/1704_2018-10-28_1704_2018-10-30', 3)},
            'cache/sr_ffx_nodelay/infer_persist/paired_cross_valid'
        ),
        (
            {'1703': ('cache/sr_ffx_nodelay/train/1703_2018-10-04_1703_2018-10-05_mix', 4),
             '1704': ('cache/sr_ffx_nodelay/train/1704_2018-10-28_1704_2018-10-30_mix', 3)},
            'cache/sr_ffx_nodelay/infer_persist/paired_mix_valid'
        )
    ]
    for item in models_paired:
        models, dir_out = item
        if os.path.exists(dir_out) is False:
            os.makedirs(dir_out)
        print("processing {}".format(dir_out))
        infer_persist_paired_model(dir_root, models, dir_out)


def health_indicators_extract(dir_root, dir_out, tag_model):
    fname_list = data.get_fnames(dir_root)
    indicators = {'fname':[]}
    for key in metrics.METRICS_FUNCTION.keys():
        indicators[key] = []
    for fname in fname_list:
        df = pd.read_csv(os.path.join(dir_root, fname))
        indicators['fname'].append(fname)
        for key, func in metrics.METRICS_FUNCTION.items():
            indicators[key].append(func(df['y'], df['yp']))
    indicators = pd.DataFrame(indicators)
    indicators.to_csv(os.path.join(dir_out, '{}.csv'.format(tag_model)), index=False)
def health_indicators_plot(dir_root):
    fname_list = data.get_fnames(dir_root)
    for fname in fname_list:
        df = pd.read_csv(os.path.join(dir_root, fname))
        filter_failure = df['fname'].apply(lambda x: x.startswith('1704'))
        df_failure = df[filter_failure]
        df_healthy = df[~filter_failure]
        cols_indicator = [col for col in df.columns if col != 'fname']
        fig, axes = plt.subplots(nrows=df.shape[1]-1, ncols=1, sharex=True, figsize=(8,15))
        for ax, col in zip(axes, cols_indicator):
            ax.plot(df_failure[col], 'or', label='failure')
            ax.plot(df_healthy[col], 'ob', label='healthy')
            ax.legend(loc='best')
            ax.set_title(col)
        fig.savefig(os.path.join(dir_root, '{}.png'.format(fname)))
def run_health_indicators():
    dir_out = 'cache/sr_ffx_nodelay/health_indicators'
    if os.path.exists(dir_out) is False:
        os.makedirs(dir_out)

    dir_root = 'cache/sr_ffx_nodelay/infer_persist/'
    tag_models = os.listdir(dir_root)
    for tag_model in tag_models:
        print("processing {}".format(tag_model))
        health_indicators_extract(os.path.join(dir_root, tag_model), dir_out, tag_model)

    health_indicators_plot(dir_out)

"no use"
def failure_detect():
    tag_metrics = 'residual-mean'
    tag_model = '1704_2018-10-28'
    dir_root = 'cache/sr_ffx_nodelay/infer_persist/{}'.format(tag_model)
    dir_out = 'cache/sr_ffx_nodelay/failure_detect'
    if os.path.exists(dir_out) is False:
        os.makedirs(dir_out)

    fname_list = data.get_fnames(dir_root)
    diff_dic = {}
    for fname in fname_list:
        df = pd.read_csv(os.path.join(dir_root, fname))
        residual = df['y'] - df['yp']
        diff_dic[fname] = residual

    diffs = {'file': [], 'diff': []}
    for key, values in diff_dic.items():
        diffs['file'].append(key)
        diffs['diff'].append(np.mean(values))
    tag_out = '{}_diff_{}'.format(tag_model, tag_metrics)
    diffs = pd.DataFrame(diffs)
    diffs.to_csv(os.path.join(dir_out, '{}.csv'.format(tag_out)), index=False)

    filter_1704 = diffs['file'].apply(lambda x: x.startswith('1704'))
    diffs_1704 = diffs[filter_1704]
    diffs_not1704 = diffs[~filter_1704]
    plt.plot(diffs_1704['diff'], 'or', label='1704')
    plt.plot(diffs_not1704['diff'], 'ob', label='not1704')
    plt.legend(loc='best')
    plt.savefig(os.path.join(dir_out, '{}.png'.format(tag_out)))
    plt.close()
def run_failure_detect():
    print()


if __name__ == '__main__':
    run_health_indicators()



