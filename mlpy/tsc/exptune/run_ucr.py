import argparse
import os
from time import time
import pandas as pd
import numpy as np

import tensorflow as tf

from mlpy.data import ucr
from mlpy.data.utils import split_stratified, dense_to_one_hot
from mlpy.lib.utils import tag_path, makedirs_clean, get_strftime_now
from mlpy.lib.tfops.base import set_gpu_allow_growth
from mlpy.tsc.exp.base import DIR_DATA_UCR15, DIR_LOG
from mlpy.tsc.exptune.resnet import ResNetTune

K = set_gpu_allow_growth(tf)


def run(ModelClass, fname_list, dir_data, dir_log, extend_dim=0, save_model=False):
    res = {'dataset': [], 'acc_train': [], 'acc_test': [], 'acc_valid': [], 'n_params': [], 't_train': [], 't_test': [],
           'lr': [], 'epoch': []}
    n = len(fname_list)
    vald_ratio = 0.1
    for i, data_name in enumerate(fname_list):
        print("********** [{}]/[{}] {} **********".format(i + 1, n, data_name))
        K.clear_session()
        # load data and set parameters
        x_train, y_train, x_test, y_test, n_classes = ucr.load_ucr_flat(data_name, dir_data)
        x_valid, y_valid, x_train, y_train = split_stratified(x_train, y_train, vald_ratio)
        y_train = dense_to_one_hot(y_train, n_classes)
        y_valid = dense_to_one_hot(y_valid, n_classes)
        y_test = dense_to_one_hot(y_test, n_classes)
        if extend_dim > 0:
            extend_shape = ()
            for _ in range(extend_dim):
                extend_shape += (1,)
            x_train = x_train.reshape(x_train.shape + extend_shape)
            x_valid = x_valid.reshape(x_valid.shape + extend_shape)
            x_test = x_test.reshape(x_test.shape + extend_shape)
        # train model
        t0 = time()
        model = ModelClass(x_train.shape[1:], n_classes, name=data_name)
        res_fit = model.fit(x_train, y_train, validation_data=(x_valid, y_valid))
        t_train = time() - t0
        if save_model:
            model.save(dir_log)
        # test model
        t0 = time()
        acc_test = model.evaluate(x_test, y_test)
        t_test = time() - t0
        # log result
        y_pred_tr = model.predict(x_train)
        y_pred_te = model.predict(x_test)
        np.savetxt(os.path.join(dir_log, '{}_y_pred_tr.txt'.format(data_name)), y_pred_tr)
        np.savetxt(os.path.join(dir_log, '{}_y_pred_te.txt').format(data_name), y_pred_te)
        res['dataset'].append(data_name)
        res['acc_train'].append(res_fit['acc'])
        res['acc_valid'].append(res_fit['val_acc'])
        res['acc_test'].append(acc_test)
        res['n_params'].append(model.count_params())
        res['t_train'].append(t_train)
        res['t_test'].append(t_test)
        if 'lr' in res_fit.keys():
            res['lr'].append(res_fit['lr'])
        else:
            res['lr'].append(0)
        if 'epoch' in res_fit.keys():
            res['epoch'].append(res_fit['epoch'])
        else:
            res['epoch'].append(0)
    df_res = pd.DataFrame(res)
    print(df_res.mean())
    df_res.to_csv(os.path.join(dir_log, 'all.csv'), index=False)


if __name__ == '__main__':
    tag = tag_path(os.path.abspath(__file__), 2)
    dir_log_root = os.path.join(DIR_LOG, tag, 'base')

    n_runs = 5
    fname_list = ucr.get_data_name_list(DIR_DATA_UCR15)

    # # for test
    # n_runs = 1
    # fname_list = ['Gun_Point']

    try:
        for _ in range(n_runs):
            strftime_now = get_strftime_now()
            dir_log = os.path.join(dir_log_root, strftime_now)
            ModelClass, extend_dim = ResNetTune, 1
            makedirs_clean(dir_log)
            run(ModelClass, fname_list, DIR_DATA_UCR15, dir_log, extend_dim=extend_dim)
    except ValueError as e:
        print(e)

