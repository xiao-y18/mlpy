# FCN
import keras 
import numpy as np 
import pandas as pd 
import time 

from utils.utils import save_logs

from sklearn.metrics import accuracy_score
from mlpy.tsc.nn.base import BaseClassifierDNNKeras

class Classifier_FCN:

	def __init__(self, output_directory, input_shape, nb_classes, verbose=False):
		self.output_directory = output_directory
		self.model = self.build_model(input_shape, nb_classes)
		if(verbose==True):
			self.model.summary()
		self.verbose = verbose
		self.model.save_weights(self.output_directory+'model_init.hdf5')

		self.base = BaseClassifierDNNKeras(input_shape, nb_classes)
		self.base.model = self.model

	def build_model(self, input_shape, nb_classes):
		input_layer = keras.Input(input_shape)
		conv1 = keras.layers.Conv1D(128, 8, padding='same')(input_layer)
		conv1 = keras.layers.BatchNormalization()(conv1)
		conv1 = keras.layers.Activation('relu')(conv1)

		conv2 = keras.layers.Conv1D(256, 5, padding='same')(conv1)
		conv2 = keras.layers.BatchNormalization()(conv2)
		conv2 = keras.layers.Activation('relu')(conv2)

		conv3 = keras.layers.Conv1D(128, 3, padding='same')(conv2)
		conv3 = keras.layers.BatchNormalization()(conv3)
		conv3 = keras.layers.Activation('relu')(conv3)

		gap_layer = keras.layers.GlobalAveragePooling1D()(conv3)

		output_layer = keras.layers.Dense(nb_classes, activation='softmax')(gap_layer)

		model = keras.models.Model(inputs=input_layer, outputs=output_layer)

		model.compile(loss='categorical_crossentropy', optimizer = keras.optimizers.Adam(), 
			metrics=['accuracy'])

		reduce_lr = keras.callbacks.ReduceLROnPlateau(monitor='loss', factor=0.5, patience=50, 
			min_lr=0.0001)

		file_path = self.output_directory+'best_model.hdf5'

		model_checkpoint = keras.callbacks.ModelCheckpoint(filepath=file_path, monitor='loss', 
			save_best_only=True)

		self.callbacks = [reduce_lr,model_checkpoint]

		return model 

	def fit(self, x_train, y_train, x_val, y_val, y_true):
		# x_val and y_val are only used to monitor the test loss and NOT for training  
		batch_size = 16
		nb_epochs = 2000
		mini_batch_size = int(min(x_train.shape[0]/10, batch_size))

		self.base.batch_size_tr = batch_size

		hist = self.model.fit(x_train, y_train, batch_size=mini_batch_size, epochs=nb_epochs,
							  verbose=1, callbacks=self.callbacks)

		model = keras.models.load_model(self.output_directory+'best_model.hdf5')

		df_hist = pd.DataFrame(hist.history)
		acc = df_hist.loc[df_hist.shape[0] - 1, 'acc']

		# y_pred = model.predict(x_val)
		# y_pred = np.argmax(y_pred , axis=1) # convert the predicted from binary to integer
		# acc_te = accuracy_score(y_true, y_pred)

		# y_pred_proba = self.model.predict(x_val, batch_size)
		# y_pred = np.argmax(y_pred_proba, axis=1)
		# y_true_ = np.argmax(y_val, axis=1)
		# acc_te = accuracy_score(y_true_, y_pred)

		acc_te = self.base.evaluate(x_val, y_val)

		keras.backend.clear_session()

		return acc, acc_te

	
