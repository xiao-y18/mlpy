from mlpy.data import ucr

from utils.utils import generate_results_csv
from utils.utils import transform_labels
from utils.utils import create_directory
from utils.utils import transform_mts_to_ucr_format
from utils.utils import visualize_filter
from utils.utils import viz_for_survey_paper
from utils.utils import viz_cam
from utils.utils import readucr

import numpy as np
import sys
import sklearn
import os
import pandas as pd

import keras.backend as kb
import tensorflow as tf
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)
kb.set_session(sess)

from mlpy.data import ucr
from mlpy.data.utils import z_normalize

def fit_classifier(data_name, model_name, out_dir):
    x_train, y_train, x_test, y_test, nb_classes = ucr.load_ucr_flat(
        data_name, DATA_ROOT, one_hot=True
    )
    y_true = np.argmax(y_test, axis=1)

    x_train = z_normalize(x_train)
    x_test = z_normalize(x_test)

    if len(x_train.shape) == 2: # if univariate
        # add a dimension to make it multivariate with one dimension
        x_train = x_train.reshape((x_train.shape[0],x_train.shape[1],1))
        x_test = x_test.reshape((x_test.shape[0],x_test.shape[1],1))

    input_shape = x_train.shape[1:]

    classifier = create_classifier(model_name,input_shape, nb_classes, out_dir)
    return classifier.fit(x_train,y_train,x_test,y_test, y_true)



def create_classifier(model_name, input_shape, nb_classes, output_directory, verbose = False):
    if model_name== 'fcn':
        from classifiers import fcn
        return fcn.Classifier_FCN(output_directory,input_shape, nb_classes, verbose)
    if model_name== 'mlp':
        from  classifiers import  mlp
        return mlp.Classifier_MLP(output_directory,input_shape, nb_classes, verbose)
    if model_name== 'resnet':
        from  classifiers import resnet
        return resnet.Classifier_RESNET(output_directory,input_shape, nb_classes, verbose)
    if model_name== 'mcnn':
        from  classifiers import mcnn
        return mcnn.Classifier_MCNN(output_directory,verbose)
    if model_name== 'tlenet':
        from  classifiers import tlenet
        return tlenet.Classifier_TLENET(output_directory,verbose)
    if model_name== 'twiesn':
        from classifiers import twiesn
        return twiesn.Classifier_TWIESN(output_directory,verbose)
    if model_name== 'encoder':
        from classifiers import encoder
        return encoder.Classifier_ENCODER(output_directory,input_shape, nb_classes, verbose)
    if model_name== 'mcdcnn':
        from classifiers import mcdcnn
        return mcdcnn.Classifier_MCDCNN(output_directory,input_shape, nb_classes, verbose)
    if model_name== 'cnn': # Time-CNN
        from classifiers import cnn
        return cnn.Classifier_CNN(output_directory,input_shape, nb_classes, verbose)

############################################### main



if __name__ == '__main__':
    DATA_ROOT = '../../../dataset/UCR_TS_Archive_2015'
    OUT_ROOT = 'cache/'
    create_directory(OUT_ROOT)

    model_name = sys.argv[1]

    res = {'dataset': [], 'acc': [], 'acc_test': []}
    data_name_list = ucr.get_data_name_list(DATA_ROOT)
    #data_name_list = data_name_list[:1] # for test
    data_name_list = ['Trace']  # for test
    n = len(data_name_list)
    for i, data_name in enumerate(data_name_list):
        print("****** [{}/{}] processing {}".format(i, n, data_name))
        dir_out = '{}{}/{}/'.format(OUT_ROOT, model_name, data_name)
        create_directory(dir_out)
        acc, acc_te = fit_classifier(data_name, model_name, dir_out)
        res['dataset'].append(data_name)
        res['acc'].append(acc)
        res['acc_test'].append(acc_te)
        print(acc, acc_te)

    df = pd.DataFrame(res)
    df.to_csv(os.path.join(OUT_ROOT, '{}.csv'.format(model_name)), index=False)
    print(df.mean())


