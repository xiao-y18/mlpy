from sklearn.svm import LinearSVC as LSVC
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import KFold # the dataset of UCR is too small to support stratifiedKFold.
import sklearn.metrics as skl_metrics
import numpy as np
from time import time


def knn1(X_train, y_train, X_test, y_test):
    knn = KNeighborsClassifier(n_neighbors=1)

    t_start = time()
    knn.fit(X_train, y_train)
    t_train = time() - t_start
    y_pred_train = knn.predict(X_train)
    acc_train = skl_metrics.accuracy_score(y_true=y_train, y_pred=y_pred_train)

    t_start = time()
    y_pred_test = knn.predict(X_test)
    t_test = time() - t_start
    acc_test = skl_metrics.accuracy_score(y_true=y_test, y_pred=y_pred_test)

    return [acc_train, acc_test], [t_train, t_test]


def linearSVC(X_train, y_train, X_test, y_test):
    model = LSVC()
    t_start = time()
    model.fit(X_train, y_train)
    t_train = time() - t_start
    y_pred_train = model.predict(X_train)
    acc_train = skl_metrics.accuracy_score(y_true=y_train, y_pred=y_pred_train)

    t_start = time()
    y_pred_test = model.predict(X_test)
    t_test = time() - t_start
    acc_test = skl_metrics.accuracy_score(y_true=y_test, y_pred=y_pred_test)

    return [acc_train, acc_test], [t_train, t_test]

def linearSVCSelectC(X_train, y_train, X_test, y_test,
                     Cs = [0.0001, 0.0002, 0.0005, 0.001, 0.002, 0.005, 0.01, 0.05, 0.1, 0.5, 1.],
                     random_state=42):
    """ refer to: Refer to: https://github.com/Newmu/dcgan_code/blob/master/svhn/svhn_semisup_analysis.py
    But we didn't adopt the parameters set by the reference. 
    Because the parameter they setting seemingly too small: Cs = [0.0001, 0.0002, 0.0005, 0.001, 0.002, 0.005, 0.01].
    """
    K = np.min((10, X_train.shape[0]))
    skf = KFold(n_splits=K, shuffle=True, random_state=random_state)
    mean_accs = []
    t_start = time()
    for C in Cs:
        accs = []
        for inds_tr, inds_val in skf.split(X_train, y_train):
            model = LSVC(C=C)
            model.fit(X_train[inds_tr], y_train[inds_tr])
            accs.append(skl_metrics.accuracy_score(
                y_true=y_train[inds_val], y_pred=model.predict(X_train[inds_val])))
        mean_accs.append(np.mean(accs))
    C_best = Cs[np.argmax(mean_accs)]
    # train model with best parameter
    model = LSVC(C=C_best)
    model.fit(X_train, y_train)
    acc_train = skl_metrics.accuracy_score(y_true=y_train, y_pred=model.predict(X_train))
    t_train = time() - t_start

    t_start = time()
    acc_test = skl_metrics.accuracy_score(y_true=y_test, y_pred=model.predict(X_test))
    t_test = time() - t_start

    return [acc_train, acc_test, C_best], [t_train, t_test]


def logisticRegression(X_train, y_train, X_test, y_test):
    model = LogisticRegression()
    t_start = time()
    model.fit(X_train, y_train)
    t_train = time() - t_start
    acc_train = skl_metrics.accuracy_score(y_train, model.predict(X_train))
    t_start = time()
    acc_test = skl_metrics.accuracy_score(y_test, model.predict(X_test))
    t_test = time() - t_start

    return [acc_train, acc_test], [t_train, t_test]

def logisticRegressionSelectC(X_train, y_train, X_test, y_test,
                              Cs=[0.0001, 0.0002, 0.0005, 0.001, 0.002, 0.005, 0.01, 0.05, 0.1, 0.5, 1.],
                              random_state=42):
    # search best parameter via stratified K-fold validation
    K = np.min((10, X_train.shape[0]))
    skf = KFold(n_splits=K, shuffle=True, random_state=random_state)
    mean_accs = []
    t_start = time()
    for C in Cs:
        accs = []
        for inds_tr, inds_val in skf.split(X_train, y_train):
            model = LogisticRegression(C=C)
            model.fit(X_train[inds_tr], y_train[inds_tr])
            accs.append(skl_metrics.accuracy_score(y_train[inds_val], model.predict(X_train[inds_val])))
        mean_accs.append(np.mean(accs))
    C_best = Cs[np.argmax(mean_accs)]
    # train model with best parameter
    model = LogisticRegression(C=C_best)
    model.fit(X_train, y_train)
    acc_train = skl_metrics.accuracy_score(y_train, model.predict(X_train))
    t_train = time()-t_start

    t_start = time()
    acc_test = skl_metrics.accuracy_score(y_test, model.predict(X_test))
    t_test = time() - t_start

    return [acc_train, acc_test, C_best], [t_train, t_test]