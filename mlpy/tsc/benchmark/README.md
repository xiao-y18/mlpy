## `ucr85/`

There are some results on [UCR repository (including 85 datasets)](https://www.cs.ucr.edu/~eamonn/time_series_data/) copied from published papers. 

- **`DNN/`**: Manually copied from [the website](https://github.com/hfawaz/dl-4-tsc). _Fawaz, Hassan Ismail, Forestier, Germain, Weber, Jonathan,et al. Deep learning for time series classification: a review, DMKD, 2019._ **10 runs cross validations.** 
- **`Hive-COTE/`**: Mannually Copied from the paper of _Jason Lines, Sarah Taylor, Anthony Bagnall. HIVE-COTE: The Hierarchical Vote Collective of Transformation-Based Ensembles for Time Series Classification,ICDM/TKDD, 2018._
- **`mWDN`**: Manualy Copied from the paper of _Wang, Jingyuan , et al. Multilevel Wavelet Decomposition Network for Interpretable Time Series Analysis, KDD, 2018._ **10 cross validations.** **40 datasets.**
- **`CSL/`**: manually Copied from the paper of _Yuanduo He, Jialiang Pei, Xu Chu et al. Characteristic Subspace Learning for Time Series Classification, ICDM, 2018_. **with default training and testing splits.** **48 datasets.**
- **`singleTrainTestBakeOff17`**: Copy from the spreadsheet supplied by the [web page](http://www.timeseriesclassification.com/results.php). _Bagnall A , Lines J , Bostrom A , et al. The great time series classification bake off: a review and experimental evaluation of recent algorithmic advances, DMKD, 2016._ I choose the results derived on the **default training/testing splits**.
- **`BOSSVS/`**: Copied from the spreadsheet supplied by the proposer on the [web page](https://www2.informatik.hu-berlin.de/~schaefpa/bossVS/). _Patrick Schäfer. Scalable time series classification[J]. DMKD, 2016._ **with default training/testing splits**
- **`Flat-COTE/`**: Copied manually from the paper of _Bagnall A , Lines J , Hills J , et al. Time-Series Classification with COTE: The Collective of Transformation-Based Ensembles[J]. IEEE Transactions on Knowledge and Data Engineering, 2015_. **with default training/testing splits**
- `UCR2015`: Copied manually from the website of UCR repository. **with default training/testing splits.**