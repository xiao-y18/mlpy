import numpy as np


def cal_class_weight(y):
    """ 
    ----------- References:        
    The primitive paper to propose the weighting strategy:
    @article{king2001logistic,
      title={Logistic regression in rare events data},
      author={King, Gary and Zeng, Langche},
      journal={Political analysis},
      volume={9},
      number={2},
      pages={137--163},
      year={2001},
      publisher={Cambridge University Press}
    }
        This paper focus on solving binary classification by logistic regression, in which the 
        positive samples labeled by y=1 is rare. 
        To address the problem, they proposed a weighted ﻿log-likelihood:
            ln(L_{w}(beta|y)) = w_{1}\sum_{Yi=1}(ln(pi)) + w_{0}\sum_{Yi=0}(ln(1-pi))
            where pi is the probability of sample i be labeled as positive, y=1. 
            w_{1} = τ/n, w_{0} = (1-τ)/(1-n), where τ is ﻿prior information about the fraction of 
            ones in the population; n is ﻿the observed fraction of ones in the sample. 
            "﻿Knowledge of τ can come from census data, a random sample from the population measuring 
            Y only, a case-cohort sample, or other sources."
    -----
    This code is adapted from the paper:
    @article{Karim2017LSTM,
      title={LSTM Fully Convolutional Networks for Time Series Classification},
      author={Karim, Fazle and Majumdar, Somshubra and Darabi, Houshang and Chen, Shun},
      journal={IEEE Access},
      volume={PP},
      number={99},
      pages={1662-1669},
      year={2017},
    }
        W_{c} = (N/C)/N_{c}, where W_{c} is the weight of the class c, N is the number of total samples,
                             C is the number of total classes, N_{c} is the number of samples of class c.
        Differently from the primitive paper, I think, this work:
            - set the set τ to be N/C, i.e. assign an equal probability to the presence of each class. 
            - generalize the weighting strategy from binary classification to multi-classification.
    -----
    :param y: 
    :return: 
    """
    from sklearn.preprocessing import LabelEncoder
    le = LabelEncoder() # Encode labels with value between 0 and n_classes-1.
    y_ind = le.fit_transform(y)
    classes = np.unique(y_ind)
    class_weight = len(y_ind) / (len(classes) * np.bincount(y_ind).astype(np.float))
    class_weight = class_weight[le.transform(classes)]
    return class_weight