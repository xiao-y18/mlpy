**A package to realize time series classification.** 

## Add early stopping strategy

Preliminary experiment is conducted with nn2/FCN.py on ArrowHead dataset. The parameter patience is
hard to choose based on the following results:

**Apply early stop strategy based on accuracy**
base, 1.000000, 0.845714
es(patience=50), 0.833333,0.577143
es(patience=100), 1.000000, 0.714286
es(patience=200), 1.000000, 0.822857
es(patience=300), 1.000000, 0.868571 ----- some where get best result.
es(patience=400) 1.000000, 0.840000

I think: The early stopping strategy should not apply to.

TODO:

- [ ] just set up experiment on specific dataset, then generalize it. 
