from mlpy.tsc.nn import vis
from mlpy.tsc.exp.base import *


def test_cam():
    data_name = 'Gun_Point'
    dir_data = DIR_DATA_UCR15
    dir_model = '../cache/exp_run_ucr_FCN/{}.hdf5'.format(data_name)
    dir_log = 'cache/'
    vis.cam_ucr(data_name, dir_data, dir_model, dir_log)

def test_filter():
    data_name = 'Gun_Point'
    dir_data = DIR_DATA_UCR15
    dir_model = '../cache/exp_run_ucr_FCN/{}.hdf5'.format(data_name)
    dir_log = 'cache/'
    vis.filter_ucr(data_name, dir_data, dir_model, dir_log)

if __name__ == '__main__':
    test_cam()
    test_filter()
