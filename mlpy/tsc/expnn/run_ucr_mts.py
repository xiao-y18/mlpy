from mlpy.data import ucr
from mlpy.lib.utils import tag_path, makedirs_clean

from mlpy.tsc.nn import *
from mlpy.tsc.exp.base import Seed, DIR_DATA_MTS, DIR_LOG

import argparse
import os
from time import time
import numpy as np
import pandas as pd

import tensorflow as tf
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)
K = tf.keras.backend
K.set_session(sess)

np.random.seed(Seed)


MODELS_FOR_RUN = {
    'encoder': Encoder,
    'fcn': FCN,
    'mcdcnn': MCDCNN,
    'mcnn': MCNN,
    'mlp': MLP,
    'resnet': ResNet,
    'tcnn': TCNN,
    'tlenet': TLeNet,
    'tscnn': TSCNN,
    'twiesn': TWIESN,
    'lstmfcn': LSTMFCN,
    'lstmfcna': LSTMFCNAttention,
    'lstmfcnaug': LSTMFCNAug,
    'lstmfcnauga': LSTMFCNAugAttention
}



def run(ModelClass, fname_list, dir_data, dir_log, save_model=False):
    res = {'dataset':[], 'acc_train': [], 'acc_test': [], 't_train':[], 't_test':[]}
    n = len(fname_list)
    for i, data_name in enumerate(fname_list):
        print("********** [{}]/[{}] {} **********".format(i + 1, n, data_name))
        K.clear_session()
        # load data and set parameters
        x_train, y_train, x_test, y_test, n_class = ucr.load_ucr_mts_flat(
            data_name, dir_data, one_hot=True)
        if ModelClass.__name__ == 'MLP':
            # the input must be scaled to a two-dimensional matrix, i.e. each instance is a vector.
            x_train = np.reshape(x_train, [x_train.shape[0], x_train.shape[1]*x_train.shape[2]])
            x_test = np.reshape(x_test, [x_test.shape[0], x_test.shape[1]*x_test.shape[2]])
        # train model and log
        t0 = time()
        model = ModelClass(x_train.shape[1:], n_class, name=data_name)
        df_metrics = model.fit(x_train, y_train)
        t_train = time() - t0
        if save_model:
            model.save(dir_log)
        # test model
        t0 = time()
        acc_test = model.evaluate(x_test, y_test)
        t_test = time() - t0
        # log batch result
        df_metrics.to_csv(os.path.join(dir_log, '{}.csv'.format(data_name)))
        last = df_metrics.loc[df_metrics.shape[0] - 1, :]
        res['dataset'].append(data_name)
        res['acc_train'].append(last['acc'])
        res['acc_test'].append(acc_test)
        res['t_train'].append(t_train)
        res['t_test'].append(t_test)
    df_res = pd.DataFrame(res)
    print(df_res.mean())
    df_res.to_csv(os.path.join(dir_log, 'all.csv'), index=False)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'model', type=str,
        help="The name of the model you want to use.")
    FLAGS, unparsed = parser.parse_known_args()

    tag = tag_path(os.path.abspath(__file__), 1)

    fname_list = ucr.get_data_name_list(DIR_DATA_MTS)
    fname_list = ['RacketSports'] # for test
    # fname_list = ['ArticularyWordRecognition']

    try:
        ModelClass = MODELS_FOR_RUN[FLAGS.model]
        dir_log = os.path.join(DIR_LOG, tag, FLAGS.model)
        makedirs_clean(dir_log)
        run(ModelClass, fname_list, DIR_DATA_MTS, dir_log, save_model=True)
    except ValueError as e:
        print(e)


"""
CharacterTrajectories: ValueError: could not broadcast input array from shape (131) into shape (116)
This dataset hasn't meet the introduction published on http://www.timeseriesclassification.com/description.php?Dataset=CharacterTrajectories
    The original data has different length cases. The class label is one of 20 characters 
    'a' 'b' 'c' 'd' 'e' 'g' 'h' 'l' 'm' 'n' 'o' 'p' 'q' 'r' 's' 'u' 'v' 'w' 'y' 'z' To conform with the 
    repository, we have truncated all series to the length of the shortest, which is 182.
The realities are that:
    The length of mts range from 60 to 182

"""