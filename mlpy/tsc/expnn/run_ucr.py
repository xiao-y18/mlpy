import argparse
import os
from time import time
import pandas as pd
import numpy as np

import tensorflow as tf

from mlpy.data import ucr
from mlpy.lib.utils import tag_path, makedirs_clean, get_strftime_now
from mlpy.lib.tfops.base import set_gpu_allow_growth, set_gpu_allow_growth_keras
from mlpy.tsc.nn import *
from mlpy.tsc.exp.base import DIR_DATA_UCR15, DIR_LOG

MODELS_FOR_RUN = {
    'mlp': (MLP, 0),  # (model class, extend_dim)
    'fcn': (FCN, 1),
    'resnet': (ResNet, 1),
    'encoder': (Encoder, 1),
    'mcnn': (MCNN, 1),
    'tlenet': (TLeNet, 1),
    'mcdcnn': (MCDCNN, 1),
    'tcnn': (TCNN, 1),
    'twiesn': (TWIESN, 1),
    'tscnn': (TSCNN, 1),
    'lstmfcn': (LSTMFCN, 1),
    'lstmfcna': (LSTMFCNAttention, 1),
    'lstmfcnaug': (LSTMFCNAug, 1),  # ing, current
    'lstmfcnauga': (LSTMFCNAugAttention, 1),
    'inceptionv4': (InceptionNetV4, 1)
}


def run(ModelClass, fname_list, dir_data, dir_log, extend_dim=0, save_model=False):
    if ModelClass in MODELS_REALIZED_BY_KERAS:
        K = set_gpu_allow_growth_keras(tf)
    else:
        K = set_gpu_allow_growth(tf)

    res = {'dataset': [], 'acc_train': [], 'acc_test': [], 'n_params': [], 't_train': [], 't_test': []}
    n = len(fname_list)
    for i, data_name in enumerate(fname_list):
        print("********** [{}]/[{}] {} **********".format(i + 1, n, data_name))
        K.clear_session()
        # load data and set parameters
        x_train, y_train, x_test, y_test, n_classes = ucr.load_ucr_flat(
            data_name, dir_data, one_hot=True)
        if extend_dim > 0:
            extend_shape = ()
            for _ in range(extend_dim):
                extend_shape += (1,)
            x_train = x_train.reshape(x_train.shape + extend_shape)
            x_test = x_test.reshape(x_test.shape + extend_shape)
        # train model and log
        t0 = time()
        model = ModelClass(x_train.shape[1:], n_classes, name=data_name)
        df_metrics = model.fit(x_train, y_train)
        t_train = time() - t0
        if save_model:
            model.save(dir_log)
        # test model
        t0 = time()
        acc_test = model.evaluate(x_test, y_test)
        t_test = time() - t0
        # log batch result
        y_pred_tr = model.predict(x_train)
        y_pred_te = model.predict(x_test)
        np.savetxt(os.path.join(dir_log, '{}_y_pred_tr.txt'.format(data_name)), y_pred_tr)
        np.savetxt(os.path.join(dir_log, '{}_y_pred_te.txt').format(data_name), y_pred_te)
        # df_metrics.to_csv(os.path.join(dir_log, '{}.csv'.format(data_name)))
        last = df_metrics.loc[df_metrics.shape[0] - 1, :]
        res['dataset'].append(data_name)
        res['acc_train'].append(last['acc'])
        res['acc_test'].append(acc_test)
        res['n_params'].append(model.count_params())
        res['t_train'].append(t_train)
        res['t_test'].append(t_test)
    df_res = pd.DataFrame(res)
    print(df_res.mean())
    df_res.to_csv(os.path.join(dir_log, 'all.csv'), index=False)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'model', type=str,
        help="The name of the model you want to use.")
    FLAGS, unparsed = parser.parse_known_args()

    tag = tag_path(os.path.abspath(__file__), 1)

    n_runs = 5
    fname_list = ucr.get_data_name_list(DIR_DATA_UCR15)

    # # for test
    # n_runs = 1
    # fname_list = ['Gun_Point']

    try:
        for _ in range(n_runs):
            strftime_now = get_strftime_now()
            dir_log = os.path.join(DIR_LOG, tag, FLAGS.model, strftime_now)
            ModelClass, extend_dim = MODELS_FOR_RUN[FLAGS.model]
            makedirs_clean(dir_log)
            run(ModelClass, fname_list, DIR_DATA_UCR15, dir_log, extend_dim=extend_dim)
            print()
            print("Finish to run the model {}".format(FLAGS.model))
            print()
    except ValueError as e:
        print(e)

