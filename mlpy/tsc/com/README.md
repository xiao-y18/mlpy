## Opensource packages


- [sktime](https://github.com/alan-turing-institute/sktime). A comprehensive package supplied by the the  most senior team [UEA & UCR](http://www.timeseriesclassification.com/) in which a Java realization is supplied.
	- `sktime/`: traditional approaches.
	- `sktime-dl/`: the dnn models are adpated from [hfawaz/dl-4-tsc](https://github.com/hfawaz/dl-4-tsc).
- [pyts](https://pyts.readthedocs.io/en/latest/user_guide.html). This package mainly focus on the dictionary based approaches, e.g. ShapeletTransform, BagOfPatterns, BOSS.