"""
Network originally proposed by:
@inproceedings{tanisaro2016time,
  title={Time series classification using time warping invariant echo state networks},
  author={Tanisaro, Pattreeya and Heidemann, Gunther},
  booktitle={2016 15th IEEE International Conference on Machine Learning and Applications (ICMLA)},
  pages={831--836},
  year={2016},
  organization={IEEE}
}

﻿Echo State Network (ESN) is a type of RNNs of which the weights are left untrained.Only the output weights
are to be trained for the desired target at the read out connection where no cyclic dependencies are
created. This paper use a modification of the original ESN, called time warping invariant echo state
network [20].

Realization Limitation
- The feedback from target y proposed in primitive paper didn't be realized. (Equation 1)

"""

__author__ = 'Fanling Huang'

import numpy as np
import pandas as pd
import os
import gc
from scipy import sparse
from scipy.sparse import linalg as slinalg
from sklearn.linear_model import Ridge

from mlpy.lib import metrics

from .base import BaseClassifierDNNKeras


class TWIESN(BaseClassifierDNNKeras):
    def __init__(self, input_shape, n_classes, verbose=1, name='twiesn'):
        super(TWIESN, self).__init__(input_shape, n_classes, verbose, name)

        # default parameters
        self.batch_size = 16
        self.n_epochs = 2000
        self.length = self.input_shape[0]
        self.dim = self.input_shape[1]

    def fit(self, x, y,
            batch_size=None,
            n_epochs=None,
            validation_data=None,
            shuffle=True,
            **kwargs):
        # hyper parameters, which correspond the parameter table in primitive paper.
        #
        # N_x: the ﻿reservoir size.
        # Connect: decide the sparsity of the reservoir.
        # ScaleW_in: input scaling should be small for the linear network in order to drive the
        #           network around the resting state. Primitive paper presume most of the datasets
        #           in the UCR archive are nonlinear, therefore input scaling to ScaleW_in is set
        #           to 2.0 as a baseline.
        # lambda: the regularization of ridge regression at the readout to insure a stable W_out.
        config1 = {'N_x':250, 'Connect':0.5, 'ScaleW_in':1.0, 'lambda':0.0}
        config2 = {'N_x':250, 'Connect':0.5, 'ScaleW_in':2.0, 'lambda':0.05}
        config3 = {'N_x':500, 'Connect':0.1, 'ScaleW_in':2.0, 'lambda':0.05}
        config4 = {'N_x':800, 'Connect':0.1, 'ScaleW_in':2.0, 'lambda':0.05}
        self.configs = [config1, config2, config3, config4]
        # spectral radius rho(W): is computed from the maximum of absolute eigenvalue of weight
        #       matrix W. It is considered to be a scaling factor of W.
        self.rhos = [0.55, 0.9, 2.0, 5.0]
        # leaky rate alpha: can be regarded as a time warping of the input signal or the speed
        #       of the dynamics of input and output.
        self.alpha = 0.1

        # limit the hyper parameter search if dataset is too large.
        if x.shape[0] > 1000:
            for config in self.configs:
                config['N_x'] = 100
            self.configs = [self.configs[0], self.configs[1], self.configs[2]]

        # grid search for best model
        best_acc = -np.inf
        best_params = {}
        for i_config, config in enumerate(self.configs):
            for rho in self.rhos:
                # initialize model-related parameters
                self.rho = rho
                self.N_x = config['N_x']
                self.Connect = config['Connect']
                self.ScaleW_in = config['ScaleW_in']
                self.lambda_ = config['lambda']
                self.esn_init()
                # train model with the features transformed by ESN.
                x_train_new, y_train_new = self.esn_encode(x, y)
                self.model = Ridge(alpha=self.lambda_)
                self.model.fit(x_train_new, y_train_new)
                self.W_out = self.model.coef_ # weight matrix for output connections.
                y_pred_train = self.predict(x_train_new)
                acc_train = self.accuracy_score(y, y_pred_train)
                # validate model
                str_print = 'i_config={}, rho={}, acc_train={:4}'.format(i_config, rho, acc_train)
                if validation_data is None:
                    acc = acc_train
                else:
                    x_valid, y_valid = validation_data
                    x_valid_new, y_valid_new = self.esn_encode(x_valid, y_valid)
                    y_pred_valid = self.predict(x_valid_new)
                    acc_valid = self.accuracy_score(y_valid, y_pred_valid)
                    acc = acc_valid
                    str_print += 'acc_valid={:4}'.format(acc_valid)
                print(str_print)
                # log best
                if best_acc < acc:
                    best_acc = acc
                    best_params = {
                        'rho':self.rho, 'N_x':self.N_x, 'Connect':self.Connect,
                        'ScaleW_in':self.ScaleW_in, 'lambda':self.lambda_,
                        'W_in':self.W_in, 'W':self.W, 'W_out':self.W_out,
                        'model':self.model
                    }
                # free memory
                gc.collect()

        # set best model to as the final model
        self.rho = best_params['rho']
        self.N_x = best_params['N_x']
        self.Connect = best_params['Connect']
        self.ScaleW_in = best_params['ScaleW_in']
        self.lambda_ = best_params['lambda']
        self.W_in = best_params['W_in']
        self.W = best_params['W']
        self.W_out = best_params['W_out']
        self.model = best_params['model']

        df_metrics = pd.DataFrame({'acc':[best_acc]})
        return df_metrics

    def predict_proba(self, x, batch_size=None, **kwargs):
        raise NotImplementedError('The model does not support this function.')

    def predict(self, x, batch_size=None, **kwargs):
        import warnings
        if batch_size is not None:
            warnings.warn('The parameter batch_size={} is useless.'.format(batch_size))
        y_pred = self.model.predict(x)
        y_pred = y_pred.reshape(x.shape[0]//self.length, self.length, y_pred.shape[-1])
        y_pred = np.average(y_pred, axis=1)  # (n_instances, dim)
        return y_pred

    def evaluate(self, x, y, batch_size=None, **kwargs):
        x_new, y_new = self.esn_encode(x, y)
        y_pred= self.predict(x_new)
        acc = self.accuracy_score(y, y_pred)
        print(acc)
        return acc

    def save(self, dir, fname):
        import joblib
        import json
        # save parameters
        params = json.dumps({'rho':self.rho, 'N_x':self.N_x, 'Connect':self.Connect,
         'ScaleW_in':self.ScaleW_in, 'lambda':self.lambda_})
        with open(os.path.join(dir, '{}_params.json'.format(fname)), 'w') as f:
            f.write(params)
        # save weights
        np.savetxt(os.path.join(dir, '{}_W_in.txt'.format(fname)), self.W_in)
        np.savetxt(os.path.join(dir, '{}_W.txt'.format(fname)), self.W)
        np.savetxt(os.path.join(dir, '{}_W_out.txt'.format(fname)), self.W_out)
        # save model
        joblib.dump(self.model, os.path.join(dir, '{}_sklearnmodel.joblib'.format(fname)))

    def esn_encode(self, x, y_in):
        """
        (1) iteratively update internal unit activities from t-1 to t
            s(t) = f(w_in*x(t) + w*s(t-1))
            s(t) = (1-alpha)*s(t-1) + alpha*s(t)
        (2) concatenate directly input to output layer
            [x; s]
        :param x: 
        :param y_in: 
        :return: 
            x_new(n * self.length, self.dim + self.N_x), y_new(self.length * n)
        """
        n = x.shape[0]
        # compute the state matrix
        s = np.zeros((n, self.length, self.N_x), dtype=np.float64)
        # previous state
        s_prev = np.zeros((n, self.N_x), dtype=np.float64)
        for t in range(self.length):  # update internal unit activities from t-1 to t
            x_cur = x[:, t, :]  # (n, self.dim)
            s_cur = np.tanh(self.W_in.dot(x_cur.T) + self.W.dot(s_prev.T)).T  # (n, self.N_x)
            s_cur = (1 - self.alpha) * s_prev + self.alpha * s_cur  # apply leakage
            # save current state
            s_prev = s_cur
            s[:, t, :] = s_cur
        # concatenate directly input to output layer
        s = np.concatenate((x, s), axis=2)  # (n, self.length, self.dim + self.N_x)
        x_new = s.reshape(n * self.length, self.dim + self.N_x)
        y_new = np.repeat(y_in, self.length, axis=0)

        # x_state = self.compute_state_matrix(x)
        # x_new = x_state.reshape(n * self.length, self.dim + self.N_x)
        # y_new = np.repeat(y_in, self.length, axis=0)
        return x_new, y_new

    def esn_init(self):
        """ initialize weight matrices for input connections and internal connections.
        :return: 
        """
        # initialize the weight matrix for input connections.
        self.W_in = (2.0 * np.random.rand(self.N_x, self.dim) - 1.0) / (2.0 * self.ScaleW_in)

        # find eigenvalues
        converged = False
        i = 0
        while (not converged):
            i += 1
            # the weight matrix for internal connections (sparse and uniformly distributed weights).
            self.W = sparse.rand(self.N_x, self.N_x, density=self.Connect).todense()
            # ensure that the non-zero values are uniformly distributed, in the range of [-5, 5].
            self.W[np.where(self.W > 0)] -= 0.5

            try:
                # get the largest eigenvalue
                eig, _ = slinalg.eigs(self.W, k=1, which='LM')
                # adjust the spectral radius
                self.W /= np.abs(eig) / self.rho
                converged = True
            except:
                print("not converged", i)
                continue

    def accuracy_score(self, y_ture, y_pred):
        acc = metrics.accuracy_score(np.argmax(y_ture, axis=1), np.argmax(y_pred, axis=1))
        return acc