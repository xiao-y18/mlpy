"""
An augmentation of LSTMFCN in:
@article{karim2018multivariate,
  title={Multivariate lstm-fcns for time series classification},
  author={Karim, Fazle and Majumdar, Somshubra and Darabi, Houshang and Harford, Samuel},
  journal={arXiv preprint arXiv:1801.04503},
  year={2018}
}
Source Cote: https://github.com/houshd/MLSTM-FCN

Compared to previous work, this version introduces following augmentations:
- introduce the squeeze_excite_block to the end of the first tow conv layers in FCN. (improve accuracy.)
- apply the model to multi-variate time series that are ﻿padded by zeros at the end to make their size consistent.
- ﻿the dimension shuffle operation is only applied when the number of time steps is greater than the number of variables. (improve efficiency.)

Discoveries
- this work still select hyperparameters over test set.
"""

__author__ = 'Fanling Huang'

import keras # !! can not be changed to tf.keras as some modules in attention LSTM need the support of keras.

from .lstmfcn import LSTMFCN
from .lstmfcnlib import AttentionLSTM

def squeeze_excite_block(input):
    ''' Copy from the primitive paper.
    Create a squeeze-excite block
    Args:
        input: input tensor
        filters: number of output filters
        k: width factor

    Returns: a keras tensor
    '''
    filters = input._keras_shape[-1] # channel_axis = -1 for TF

    se = keras.layers.GlobalAveragePooling1D()(input)
    se = keras.layers.Reshape((1, filters))(se)
    se = keras.layers.Dense(filters // 16,  activation='relu', kernel_initializer='he_normal', use_bias=False)(se)
    se = keras.layers.Dense(filters, activation='sigmoid', kernel_initializer='he_normal', use_bias=False)(se)
    se = keras.layers.multiply([input, se])
    return se


class LSTMFCNAug(LSTMFCN):
    def __init__(self, input_shape, n_classes, verbose=1, name='lstmfcnaug'):
        super(LSTMFCNAug, self).__init__(input_shape, n_classes, verbose, name)

    def build_model(self):
        ## Basic LSTM block
        if self.input_shape[0] > self.input_shape[1]:
            # ﻿the dimension shuffle operation is only applied when the number of time steps is greater than the number of variables.
            y1 = keras.layers.Permute((2, 1))(self.x) # [length, variable] --> [variable, length]
        else:
            y1 = self.x
        y1 = keras.layers.LSTM(self.n_cells)(y1)
        y1 = keras.layers.Dropout(0.8)(y1)

        ## FCN block
        conv1 = keras.layers.Conv1D(128, 8, padding='same', kernel_initializer='he_uniform')(self.x)
        conv1 = keras.layers.BatchNormalization()(conv1)
        conv1 = keras.layers.Activation('relu')(conv1)
        conv1 = squeeze_excite_block(conv1)

        conv2 = keras.layers.Conv1D(256, 5, padding='same', kernel_initializer='he_uniform')(conv1)
        conv2 = keras.layers.BatchNormalization()(conv2)
        conv2 = keras.layers.Activation('relu')(conv2)
        conv2 = squeeze_excite_block(conv2)

        conv3 = keras.layers.Conv1D(128, 3, padding='same', kernel_initializer='he_uniform')(conv2)
        conv3 = keras.layers.BatchNormalization()(conv3)
        conv3 = keras.layers.Activation('relu')(conv3)

        y2 = keras.layers.GlobalAveragePooling1D()(conv3)

        ## concatenate two blocks
        y = keras.layers.concatenate([y1, y2])

        out = keras.layers.Dense(self.n_classes, activation='softmax')(y)

        return out


class LSTMFCNAugAttention(LSTMFCN):
    def __init__(self, input_shape, n_classes, verbose=1, name='lstmfcnauga'):
        super(LSTMFCNAugAttention, self).__init__(input_shape, n_classes, verbose, name)

    def build_model(self):
        ## Basic LSTM block
        if self.input_shape[0] > self.input_shape[1]:
            # ﻿the dimension shuffle operation is only applied when the number of time steps is greater than the number of variables.
            y1 = keras.layers.Permute((2, 1))(self.x) # [length, variable] --> [variable, length]
        else:
            y1 = self.x
        y1 = AttentionLSTM(self.n_cells)(y1)
        y1 = keras.layers.Dropout(0.8)(y1)

        ## FCN block
        conv1 = keras.layers.Conv1D(128, 8, padding='same', kernel_initializer='he_uniform')(self.x)
        conv1 = keras.layers.BatchNormalization()(conv1)
        conv1 = keras.layers.Activation('relu')(conv1)
        conv1 = squeeze_excite_block(conv1)

        conv2 = keras.layers.Conv1D(256, 5, padding='same', kernel_initializer='he_uniform')(conv1)
        conv2 = keras.layers.BatchNormalization()(conv2)
        conv2 = keras.layers.Activation('relu')(conv2)
        conv2 = squeeze_excite_block(conv2)

        conv3 = keras.layers.Conv1D(128, 3, padding='same', kernel_initializer='he_uniform')(conv2)
        conv3 = keras.layers.BatchNormalization()(conv3)
        conv3 = keras.layers.Activation('relu')(conv3)

        y2 = keras.layers.GlobalAveragePooling1D()(conv3)

        ## concatenate two blocks
        y = keras.layers.concatenate([y1, y2])

        out = keras.layers.Dense(self.n_classes, activation='softmax')(y)

        return out