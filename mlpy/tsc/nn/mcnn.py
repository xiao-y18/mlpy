"""
Network originally proposed by
@article{cui2016multi,
  title={Multi-scale convolutional neural networks for time series classification},
  author={Cui, Zhicheng and Chen, Wenlin and Chen, Yixin},
  journal={arXiv preprint arXiv:1603.06995},
  year={2016}
}
Open source: https://www.cse.wustl.edu/~z.cui/projects/mcnn/
    python2.7. 
    the code is too old. 
    the fully convolution stage didn't be realized in the public code.

Core idea: 
A multi-scale strategy is proposed to map a series to multiple types of series, then they are fed 
into a customized CNN with multiple local channels to extract local features.

The realization of (fawaz2019deep) is significant worse thant the primitive results that can't be 
verified because the code of (cui2016multi) is old and the corresponding running environment is 
hard to configure.
I sent email to (fawaz2019deep) and got a reply: "I was not able to reproduce all results in the 
paper since as you said Cui's code is very obselete and hard to follow."

Based on the code of (fawaz2019deep), I:
- remove the procedure of cross-validation to select parameters.
- change the learning rate from 0.1 to 1e-4 and got improvement, but still worse than the primitive.
- change `concat_layer = keras.layers.Concatenate(axis=-1)(stage1_layers)` 
    to `concat_layer = keras.layers.Concatenate(axis=1)(stage1_layers)`, as I think we should concat 
    the feature maps derived by local conv along the dimension of length/time. Furthermore, for 
    multiple time series, the length of feature maps are different after multi-scale operation, 
    therefore, we have to concatenate local feature maps on the length dimension. 

TODO:
- shuffle data for the batch training process where the tail data are always ignored.
- realize the learning_rate decay proposed in primitive paper.
- realize the cross-validation to select parameters according to the primitive.
- verify the contribution/effectiveness of multi-scale strategy.

"""

__author__ = 'Fanling Huang'

import tensorflow as tf
import numpy as np
import pandas as pd

from mlpy.lib import metrics
from mlpy.lib.utils import vote_majority
from mlpy.data.ts import slice, down_sample_batch, moving_avg_batch

from .base import BaseClassifierDNNKeras


class MCNN(BaseClassifierDNNKeras):
    def __init__(self, input_shape, n_classes, verbose=1, name='mcnn'):
        super(MCNN, self).__init__(input_shape, n_classes, verbose, name)

        # default parameters
        self.n_batches_tr = 10
        self.batch_size_tr = None
        self.batch_size_max_tr = 256
        self.n_epochs = 200
        self.pool_factor = 5
        self.slice_ratio = 0.9
        self.win_size = 0.2
        self.n_inc = None

    def build_model(self, input_shapes, pool_factor, kernel_size):
        input_layers = []
        stage1_layers = []
        #
        # padding='same' is to insure that 'pool_size' isn't zero.
        #
        # local convolution stage
        # the output of each channel is constricted to the same length that is controlled by the parameter pool_factor
        for input_shape in input_shapes:
            input_layer = tf.keras.layers.Input(input_shape)
            input_layers.append(input_layer)

            conv_layer = tf.keras.layers.Conv1D(
                filters=256, kernel_size=kernel_size, padding='same', activation='sigmoid')(input_layer)
            pool_size = int(int(conv_layer.shape[1]) / pool_factor)
            max_layer = tf.keras.layers.MaxPooling1D(pool_size=pool_size)(conv_layer) # strides=None, it will default to pool_size.
            stage1_layers.append(max_layer)

        #
        # full convolution stage
        concat_layer = tf.keras.layers.Concatenate(axis=1)(stage1_layers)
        kernel_size = int(min(kernel_size, int(concat_layer.shape[1]))) # kernel shouldn't exceed the length.
        full_conv = tf.keras.layers.Conv1D(
            filters=256, kernel_size=kernel_size, padding='same', activation='sigmoid')(concat_layer)
        pool_size = int(int(full_conv.shape[1]) / pool_factor)
        full_max = tf.keras.layers.MaxPooling1D(pool_size=pool_size)(full_conv)
        full_max = tf.keras.layers.Flatten()(full_max)

        fully_connected = tf.keras.layers.Dense(units=256, activation='sigmoid')(full_max)
        out = tf.keras.layers.Dense(units=self.n_classes, activation='softmax')(fully_connected)

        return input_layers, out

    def fit(self, x, y,
            batch_size=None,
            n_epochs=None,
            validation_data=None,
            shuffle=True,
            **kwargs):
        # check parameters
        if batch_size is not None:
            self.batch_size_tr = batch_size
        if n_epochs is not None:
            self.n_epochs = n_epochs

        self.model, df_metrics = self.train(x, y)
        return df_metrics

    def train(self, x_train, y_train):
        ############################################################
        ##### prepare data set
        length_origin = x_train.shape[1]

        # data augmentation by slicing the length of the series
        # extend size of data set.
        if length_origin > 500:
            # restrict slice ratio when data length is too large, i.e. to restrict the number of augmented samples.
            self.slice_ratio = self.slice_ratio if self.slice_ratio > 0.98 else 0.98
        # n_inc can be used as the batch size of validation routine to realize voting strategy.
        x_train, y_train, self.n_inc = self.slice_data(x_train, y_train, self.slice_ratio)

        self.n_inc = length_origin - int(length_origin * self.slice_ratio) + 1

        # transform data by moving average(ma) and down sample(ds),
        # extend length of series in data set.
        self.win_size = int(x_train.shape[1] * self.win_size) if self.win_size<1 else int(self.win_size)
        x_train, lengths_data = self.preprocess(x_train, self.win_size, self.pool_factor)

        # set batch size for training
        self.batch_size_tr = int(x_train.shape[0] / self.n_batches_tr)
        if self.batch_size_tr > self.batch_size_max_tr: # limit the batch size
            self.n_batches_tr = int(x_train.shape[0] / self.batch_size_max_tr)
            self.batch_size_tr = self.batch_size_max_tr

        ############################################################
        ##### build model and train
        dim = x_train.shape[2]  # for MTS
        self.input_shapes, length_max = self.get_list_of_input_shapes(lengths_data, dim)
        self.inputs, self.output = self.build_model(
            self.input_shapes, self.pool_factor, self.win_size)
        model = tf.keras.models.Model(inputs=self.inputs, outputs=self.output)
        model.compile(
            loss='categorical_crossentropy', optimizer=tf.keras.optimizers.Adam(lr=1e-4),
            metrics=['accuracy'])
        if (self.verbose == 1):
            model.summary()

        res = {'acc': [], 'loss': []}
        for epoch in range(self.n_epochs):
            acc_tr = 0
            loss_tr = 0.

            for id_batch in range(self.n_batches_tr):
                x_batch = x_train[id_batch*self.batch_size_tr: (id_batch+1)*self.batch_size_tr]
                y_batch = y_train[id_batch*self.batch_size_tr: (id_batch+1)*self.batch_size_tr]
                x_batch = self.split_input_for_model(x_batch, self.input_shapes)
                loss_train, acc_train = model.train_on_batch(x_batch, y_batch)
                acc_tr = acc_tr + acc_train
                loss_tr = loss_tr + loss_train

            avg_loss_tr = loss_tr / self.n_batches_tr
            avg_acc_tr = acc_tr / self.n_batches_tr
            res['acc'].append(avg_acc_tr)
            res['loss'].append(avg_loss_tr)
            print('[%d/%d] acc %.4f, loss %.4f' % (epoch+1, self.n_epochs, avg_acc_tr, avg_loss_tr))

        return model, pd.DataFrame(res)

    def evaluate(self, x, y, batch_size=None, **kwargs):
        y_true_origin = np.argmax(y, axis=1)  # save the original labels
        x, y, n_inc = self.slice_data(x, y, self.slice_ratio)
        x, _ = self.preprocess(x, self.win_size, self.pool_factor)

        # general accuracy
        y_true = np.argmax(y, axis=1)
        y_pred = self.model.predict(self.split_input_for_model(x, self.input_shapes))
        y_pred = np.argmax(y_pred, axis=1)
        acc_general = metrics.accuracy_score(y_true, y_pred)

        # vote accuracy
        y_pred_vote = vote_majority(y_pred, n_inc)
        acc_vote = metrics.accuracy_score(y_true_origin, y_pred_vote)

        print(acc_general, acc_vote)
        return acc_vote

    def slice_data(self, x, y, slice_ratio):
        return slice(x, y, slice_ratio)

    def down_sample(self, x, base, step_size, num):
        return down_sample_batch(x, base, step_size, num)

    def moving_avg(self, x, win_base, step_size, num):
        return moving_avg_batch(x, win_base, step_size, num)

    def preprocess(self, x, win_size, pool_factor):
        length = x.shape[1]
        ma_base, ma_step, ma_num = 5, 6, 1
        ds_base, ds_step, ds_num = 2, 1, 4
        ds_num_max = length / (pool_factor * win_size)
        ds_num = int(min(ds_num, ds_num_max))

        x_ma, lengths_ma = self.moving_avg(x, ma_base, ma_step, ma_num)
        x_ds, lengths_ds = self.down_sample(x, ds_base, ds_step, ds_num)
        lengths_data = [x.shape[1]]
        if lengths_ma != []:
            lengths_data = lengths_data + lengths_ma
            x = np.concatenate([x, x_ma], axis=1)
        if lengths_ds != []:
            lengths_data = lengths_data + lengths_ds
            x = np.concatenate([x, x_ds], axis=1)

        # data are extended toward the length direction.

        return x, lengths_data

    def get_list_of_input_shapes(self, lengths, dim):
        input_shapes = []
        length_max = 0
        for l in lengths:
            input_shapes.append((l, dim))
            length_max = max(length_max, l)

        return input_shapes, length_max

    def split_input_for_model(self, x, input_shapes):
        res = []
        indx = 0
        for input_shape in input_shapes:
            res.append(x[:, indx:(indx+input_shape[0]), :])
            indx = indx + input_shape[0]
        return res



