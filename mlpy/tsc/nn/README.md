### Time Series Classification from Scratch with Deep Neural Networks: A Strong Baseline. [Zhiguang Wang et al., 2017](https://arxiv.org/abs/1611.06455)
[Paper](https://arxiv.org/abs/1611.06455). [Code](https://github.com/cauchyturing/UCR_Time_Series_Classification_Deep_Learning_Baseline).

`mlp.py`, `fcn.py`, `resnet.py` are the realization of Multilayer Perceptron(MLP), Fully convolutional Networks (FCN), Residual Networks(ResNet) respectively.

This paper didn't give any reference for these model, so I scan further by myself.

#### MLP
MLP is a class of feedforward artificial neural network, and sometimes colloquially referred to as "vanilla" neural networks, especially when they have a single hidden layer.  
A MLP consists of, at least, three layers of nodes: an input layer, a hidden layer and an output layer. Except for the input nodes, each node is a neuron that uses a nonlinear activation function.  
[Go to wikipedia for more details](https://en.wikipedia.org/wiki/Multilayer_perceptron).

#### FCN  
[Jonathan Long, Evan Shelhamer and Trevor Darrell, 2015](https://arxiv.org/abs/1411.4038): Fully Convolutional Networks for Semantic Segmentation.

#### RestNet
[Kaiming He, Xiangyu Zhang,  Shaoqing Ren, and Jian Sun, 2015](https://arxiv.org/pdf/1512.03385.pdf): Deep Residual Learning for Image Recognition.

### Deep learning for time series classification: a review. [Fawaz et al., 2018](https://arxiv.org/abs/1809.04356)

[code](https://github.com/hfawaz/dl-4-tsc/tree/master/classifiers), which realize MLP, FCN and Restnet from [Zhiguang Wang et al., 2017](https://arxiv.org/abs/1611.06455), what's more they also realize Encoder, Multi-scale Convolutional Neural Network, Time Le-Net, Multi Channel Deep Convolutional Neural Network, Time Convolutional Neural Network, Time Warping Invariant Echo State Network.

This paper focus on three main DNN architectures, which are Multi Layer Perceptron (MLP), Convolutional Neural Network (CNN) and Echo State Network (ESN).
