import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from scipy.interpolate import spline

from mlpy.data import ucr

"""
CAM
Reference:
    https://github.com/cauchyturing/UCR_Time_Series_Classification_Deep_Learning_Baseline/blob/master/FCN.py
    https://github.com/hfawaz/dl-4-tsc/tree/master/utils
Notification:
    The reasonability of interpolation operation, i.e. spline, should be confirmed by comparing 
    current realization to more works.
"""

def cam(model, data_dic, dir_log, length_expand = 2000, is_training=False, tag=""):
    inputs = model.inputs
    output_last_conv = model.layers[-3].output
    layer_softmax = model.layers[-1]
    learning_phase = tf.keras.backend.learning_phase()

    get_last_conv = tf.keras.backend.function(inputs + [learning_phase], [output_last_conv])
    for c, x in data_dic.items():
        length = x.shape[1]
        # extract features
        last_conv = get_last_conv([x, is_training])[0]
        softmax_weight = layer_softmax.get_weights()[0] # the weights from GAP to softmax, [n_GAP, n_classes]
        CAM = np.dot(last_conv, softmax_weight)  # [n_samples, length, n_classes]
        # plot
        plt.figure()
        for k in range(x.shape[0]):
            # min-max scale to range of [0, 1]
            CAM = (CAM - CAM.min(axis=1, keepdims=True)) / \
                  (CAM.max(axis=1, keepdims=True) - CAM.min(axis=1, keepdims=True))
            # linear interpolation to smooth for visualizing
            inter_inds = np.linspace(0, length - 1, length_expand, endpoint=True)
            inter_values = spline(range(length), x[k, :, 0], inter_inds)
            inter_colour = spline(range(length), CAM[k, :, c], inter_inds)
            plt.scatter(np.arange(len(inter_inds)), inter_values, cmap='jet', c=inter_colour,
                        marker='.', s=1)
        plt.title('Class_{}'.format(c))
        plt.colorbar()
        plt.savefig('{}/{}_{}.png'.format(dir_log, tag, c))
        plt.close()

def cam_ucr(data_name, dir_data, dir_model, dir_log, extend_dim=1):
    # load data
    data = ucr.load_ucr(data_name, dir_data, one_hot=True)
    x = data.train.X
    y = data.train.y
    y_true = np.argmax(y, axis=1)
    n_classes = data.nclass
    if extend_dim > 0:
        extend_shape = ()
        for _ in range(extend_dim):
            extend_shape += (1,)
        x = x.reshape(x.shape + extend_shape)

    # load model
    model = tf.keras.models.load_model(dir_model)
    # make predicting
    y_pred = np.argmax(model.predict(x), axis=1)

    # CAM for the samples predicted correctly.
    n_samples = 20
    data_dic = {}
    for c in range(n_classes):
        x_vis = x[(y_true == c) & (y_true == y_pred)]
        if x_vis.shape[0] > n_samples:
            x_vis = x_vis[:n_samples]
        if x_vis.shape[0] > 0:
            data_dic[c] = x_vis.copy()
    if not data_dic:
        cam(model, data_dic, dir_log, tag=data_name)

    # CAM for the samples predicted wrongly.
    data_dic = {}
    for c in range(n_classes):
        x_vis = x[(y_true == c) & (y_true != y_pred)]
        if x_vis.shape[0] > n_samples:
            x_vis = x_vis[:n_samples]
        if x_vis.shape[0] > 0:
            data_dic[c] = x_vis.copy()
    if not data_dic:
        cam(model, data_dic, dir_log, tag=data_name + "_missed")


"""
Filters
Reference:
    https://github.com/hfawaz/dl-4-tsc/tree/master/utils
Notification
    This function just work for binary classification problem so that it need to be improved to 
    address more scenarios.
"""

def filter_ucr(data_name, dir_data, dir_model, dir_log, extend_dim=1):
    ## load data
    data = ucr.load_ucr(data_name, dir_data, one_hot=True)
    x = data.train.X
    y = data.train.y
    y_true = np.argmax(y, axis=1)
    n_classes = data.nclass
    if extend_dim > 0:
        extend_shape = ()
        for _ in range(extend_dim):
            extend_shape += (1,)
        x = x.reshape(x.shape + extend_shape)

    ## load model
    model = tf.keras.models.load_model(dir_model)

    ## constant parameter
    idx = 10
    idx_filter = 1
    idx_layer = 1
    # set colors for two classes
    colors = [(255 / 255, 160 / 255, 14 / 255), (181 / 255, 87 / 255, 181 / 255)]
    colors_conv = [(210 / 255, 0 / 255, 0 / 255), (27 / 255, 32 / 255, 101 / 255)]

    # filters
    filters = model.layers[idx_layer].get_weights()[0]
    inputs = model.inputs
    conv_output = [model.layers[idx_layer].output]
    get_conv_output = tf.keras.backend.function(inputs, conv_output)

    filter = filters[:, 0, idx_filter]
    plt.figure()
    plt.plot(filter + 0.5, color='gray', label='filter')
    for c in range(n_classes):
        x_c = x[np.where(y_true == c)]
        conv_values = get_conv_output([x_c])[0]

        idx_c = c

        plt.plot(x_c[idx], color=colors[idx_c], label='class' + str(idx_c) + '-raw')
        plt.plot(conv_values[idx, :, idx_filter], color=colors_conv[idx_c],
                 label='class' + str(idx_c) + '-conv')
        plt.legend()

    plt.savefig('{}/convolution-{}.pdf'.format(dir_log, data_name))