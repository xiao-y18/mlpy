"""
Network originally proposed by:
@inproceedings{zheng2014time,
  title={Time series classification using multi-channels deep convolutional neural networks},
  author={Zheng, Yi and Liu, Qi and Chen, Enhong and Ge, Yong and Zhao, J Leon},
  booktitle={International Conference on Web-Age Information Management},
  pages={298--310},
  year={2014},
  organization={Springer}
}
The network then fine-tine by:
@article{zheng2016exploiting,
  title={Exploiting multi-channels deep convolutional neural networks for multivariate time series classification},
  author={Zheng, Yi and Liu, Qi and Chen, Enhong and Ge, Yong and Zhao, J Leon},
  journal={Frontiers of Computer Science},
  volume={10},
  number={1},
  pages={96--112},
  year={2016},
  publisher={Springer}
}

Abstract:
The MC-DCNN is originally proposed in 2014 by Zheng et al. The network is adapted from traditional 
CNN and consists of two parts, i.e. a stacked local multi-channels convolutional layers that extract 
features for each dimension of TS, and a traditional MLP attached to the end to concatenate 
multi-channel features for classification. Such a model was modified in 2016 by the origin team 
where 3 changes were applied to optimize the MC-DCNN, there are: ﻿(1) replacing activation function 
sigmoid with ReLU [23, 24]; (2) replacing average pooling with max pooling [25, 26]; 
(3) unsupervised pre-training by convolutional auto-encoder [33]. 
It should be noted that MC-DCNN is designed for multi-variate time series and the UCR archive is 
excluded in their experiments because it just contain univariate time series and most of them are 
small.

This realization is the two-stage version of MC-DCNN in the extension at 2016, in which the 
pre-training strategy isn't realized. 

Notes:
    - Inputs have to be pre-processsed to address the inputs of model that is easy to be neglected 
    in realization, in particular for the multiple time series.

"""

__author__ = 'Fanling Huang'

import pandas as pd
import tensorflow as tf


from .base import BaseClassifierDNNKeras


class MCDCNN(BaseClassifierDNNKeras):
    def __init__(self, input_shape, n_classes, verbose=1, name='mcdcnn'):
        super(MCDCNN, self).__init__(input_shape, n_classes, verbose, name)

        # default parameters
        self.batch_size = 16
        self.n_epochs = 120

        # set up model
        self.inputs, self.output = self.build_model(self.input_shape)
        self.model = tf.keras.models.Model(self.inputs, self.output)
        if (self.verbose > 0):
            self.model.summary()

    def build_model(self, input_shape):
        length = input_shape[0]
        dim = input_shape[1]

        padding = 'valid'
        if length < 60: # for ItalyPowerOndemand
            padding = 'same'

        inputs = []
        convs_flat = []
        for i in range(dim):
            input = tf.keras.layers.Input((length, 1))
            inputs.append(input)
            conv1_local = tf.keras.layers.Conv1D(8, 5, activation='relu', padding=padding)(input)
            conv1_local = tf.keras.layers.MaxPooling1D(2)(conv1_local)
            conv2_local = tf.keras.layers.Conv1D(4, 5, activation='relu', padding=padding)(conv1_local)
            conv2_local = tf.keras.layers.MaxPooling1D(2)(conv2_local)
            conv2_local_flat = tf.keras.layers.Flatten()(conv2_local) # [batch, length]
            convs_flat.append(conv2_local_flat)

        if dim == 1: # univariate time series
            concat = convs_flat[0]
        else:
            concat = tf.keras.layers.Concatenate(axis=-1)(convs_flat)
        fc = tf.keras.layers.Dense(units=732, activation='relu')(concat)
        output = tf.keras.layers.Dense(units=self.n_classes, activation='softmax')(fc)

        return inputs, output

    def prepare_input(self, x):
        x_new = []
        dim = x.shape[2]
        for i in range(dim):
            x_new.append(x[:, :, i:(i+1)])
        return x_new


    def fit(self, x, y,
            batch_size=None,
            n_epochs=None,
            validation_data=None,
            shuffle=True,
            **kwargs):
        # check parameters
        if batch_size is None:
            batch_size = self.batch_size
        if n_epochs is None:
            n_epochs = self.n_epochs

        optimizer = tf.keras.optimizers.SGD(lr=0.01, momentum=0.9, decay=0.0005)
        self.model.compile(
            optimizer=optimizer, loss='categorical_crossentropy', metrics=['accuracy'])

        x = self.prepare_input(x)
        if validation_data is None:
            hist = self.model.fit(
                x, y, batch_size=batch_size, epochs=n_epochs, verbose=self.verbose)
        else:
            x_val, y_val = validation_data
            x_val = self.prepare_input(x_val)
            validation_data = (x_val, y_val)
            hist = self.model.fit(
                x, y, batch_size=batch_size, epochs=n_epochs, verbose=self.verbose,
                validation_data=validation_data)
        return pd.DataFrame(hist.history)

    def predict_proba(self, x, batch_size=None, **kwargs):
        x = self.prepare_input(x)
        probas = self.model.predict(x, batch_size, **kwargs)
        return probas



