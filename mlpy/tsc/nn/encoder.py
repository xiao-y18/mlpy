"""
Network originally proposed by:
@inproceedings{serra2018towards,
  title={Towards a Universal Neural Network Encoder for Time Series.},
  author={Serr{\`a}, Joan and Pascual, Santiago and Karatzoglou, Alexandros},
  booktitle={CCIA},
  pages={120--129},
  year={2018}
}

Two version:
1. training the model from scratch in an end-to-end fashion on a target dataset.
2. pre-training this same architecture on source dataset and then fine-tune it on a target dataset.
Here the version 1 is realized.

Model design:
- Network: standard convolutional network that is similar to FCN
- Attention mechanism is applied to derive a fixed length feature vector. 
    Such a design is mainly for multi-task learning / transfer learning, i.e. train on multiple 
    types of datasets at the same time. 
- ﻿Instance normalization is applied to replace the common used batch normalization
    because "﻿We found instance normalization to facilitate training and to provide more consistent value ranges in the encoder’s output."

Experiment result:
- Our performance is worse than the results of primitive paper, compared with ﻿Encoder-NEW.

Notes:
- This model can not to be saved via. The error is similar to "https://blog.csdn.net/Funkdub/article/details/100069905https://blog.csdn.net/Funkdub/article/details/100069905"
    but I did not fix it. 

"""

__author__ = 'Fanling Huang'

import pandas as pd
import tensorflow as tf

from mlpy.lib.tfops.normalizations import InstanceNormalization

from .base import BaseClassifierDNNKeras


class Encoder(BaseClassifierDNNKeras):
    def __init__(self, input_shape, n_classes, verbose=1, name='encoder'):
        super(Encoder, self).__init__(input_shape, n_classes, verbose, name)

        # default parameters
        self.batch_size = 12
        self.n_epochs = 100

        # set up model
        self.x = tf.keras.Input(self.input_shape)
        self.output = self.build_model()
        self.model = tf.keras.models.Model(inputs=self.x, outputs=self.output)
        if (self.verbose == True):
            self.model.summary()

    def build_model(self):
        conv1 = tf.keras.layers.Conv1D(128, 5, padding='same')(self.x)
        conv1 = InstanceNormalization()(conv1)
        conv1 = tf.keras.layers.PReLU(shared_axes=[1])(conv1)  # they have one slope parameter per filter.
        conv1 = tf.keras.layers.Dropout(rate=0.2)(conv1)
        conv1 = tf.keras.layers.MaxPooling1D()(conv1)

        conv2 = tf.keras.layers.Conv1D(256, 11, padding='same')(conv1)
        conv2 = InstanceNormalization()(conv2)
        conv2 = tf.keras.layers.PReLU(shared_axes=[1])(conv2)
        conv2 = tf.keras.layers.Dropout(rate=0.2)(conv2)
        conv2 = tf.keras.layers.MaxPooling1D()(conv2)

        conv3 = tf.keras.layers.Conv1D(512, 21, padding='same')(conv2)
        conv3 = InstanceNormalization()(conv3)
        conv3 = tf.keras.layers.PReLU(shared_axes=[1])(conv3)
        conv3 = tf.keras.layers.Dropout(rate=0.2)(conv3)

        attention_data = tf.keras.layers.Lambda(lambda  x: x[:, :, :256])(conv3)
        attention_softmax = tf.keras.layers.Lambda(lambda x: x[:, :, 256:])(conv3)
        attention_softmax = tf.keras.layers.Activation('softmax')(attention_softmax)
        multiply_layer = tf.keras.layers.Multiply()([attention_softmax, attention_data])

        dense_layer = tf.keras.layers.Dense(256, activation='sigmoid')(multiply_layer)
        dense_layer = InstanceNormalization()(dense_layer)

        full = tf.keras.layers.Flatten()(dense_layer)
        out = tf.keras.layers.Dense(self.n_classes, activation='softmax')(full)

        return out

    def fit(self, x, y,
            batch_size=None,
            n_epochs=None,
            validation_data=None,
            shuffle=True,
            **kwargs):
        # check parameters
        if batch_size is None:
            batch_size = self.batch_size
        if n_epochs is None:
            n_epochs = self.n_epochs
            #n_epochs = 1 # for test

        # start to train
        optimizer = tf.keras.optimizers.Adam(lr=1e-4)
        self.model.compile(
            loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy'])
        if validation_data is None:
            hist = self.model.fit(
                x, y, batch_size=batch_size, epochs=n_epochs, verbose=self.verbose
            )
        else:
            hist = self.model.fit(
                x, y, batch_size=batch_size, epochs=n_epochs, verbose=self.verbose,
                validation_data=validation_data
            )

        return pd.DataFrame(hist.history)