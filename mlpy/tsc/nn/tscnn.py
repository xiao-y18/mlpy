"""
This vanila CNN work will on the audio classification problem, i.e. refitted vehicle identification 
and car horn identification.
This model was realized when I was learning deep learning, but I have forget where I took the model.

Re-realization by Keras
- tf.contrib.layers.xavier_initializer and tf.glorot_uniform_initializer are the same, referring to https://stackoverflow.com/questions/47986662/why-xavier-initializer-and-glorot-uniform-initializer-are-duplicated-to
-  max_iterations = 2000 --> n_epochs = 200
"""

__author__ = 'Fanling Huang'


import pandas as pd
import tensorflow as tf

from .base import BaseClassifierDNNKeras


class TSCNN(BaseClassifierDNNKeras):
    def __init__(self, input_shape, n_classes, verbose=1, name='tscnn'):
        super(TSCNN, self).__init__(input_shape, n_classes, verbose, name)

        # default parameters
        self.n_filters_1 = 16
        self.n_filters_2 = 14
        self.n_fc_1 = 40
        self.batch_size = 10
        self.n_epochs = 300
        self.lr = 2e-5
        self.dropout = 1.0 # useless

        # set up model
        self.x = tf.keras.Input(self.input_shape)
        self.output = self.build_model()
        self.model = tf.keras.models.Model(inputs=self.x, outputs=self.output)
        if (self.verbose > 0):
            self.model.summary()

    def build_model(self, **kwargs):
        conv1 = tf.keras.layers.Conv1D(self.n_filters_1, 5, padding='same')(self.x)
        conv1 = tf.keras.layers.BatchNormalization()(conv1)
        conv1 = tf.keras.layers.Activation('relu')(conv1)

        conv2 = tf.keras.layers.Conv1D(self.n_filters_2, 4, padding='same')(conv1)
        conv2 = tf.keras.layers.BatchNormalization()(conv2)
        conv2 = tf.keras.layers.Activation('relu')(conv2)

        flat = tf.keras.layers.Flatten()(conv2)
        fc1 = tf.keras.layers.Dense(self.n_fc_1, activation='relu')(flat)
        if self.dropout < 1.0: # address the error "keep_prob must be a scalar tensor or a float in the range (0, 1], got 0" that is so strange!
            fc1 = tf.keras.layers.Dropout(self.dropout)(fc1)
        out = tf.keras.layers.Dense(self.n_classes, activation='softmax')(fc1)
        return out

    def fit(self, x,
            y,
            batch_size=None,
            n_epochs=None,
            validation_data=None,
            shuffle=True,
            **kwargs):
        if batch_size is None:
            batch_size = self.batch_size
        if n_epochs is None:
            n_epochs = self.n_epochs

        optimizer = tf.keras.optimizers.Adam(self.lr)
        self.model.compile(
            loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy'])
        if validation_data is None:
            hist = self.model.fit(
                x, y, batch_size=batch_size, epochs=n_epochs, verbose=self.verbose)
        else:
            hist = self.model.fit(
                x, y, batch_size=batch_size, epochs=n_epochs, verbose=self.verbose,
                validation_data=validation_data)

        return pd.DataFrame(hist.history)
