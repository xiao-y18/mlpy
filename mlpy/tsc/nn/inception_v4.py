"""
Network originally proposed by the paper:
@article{fawaz2019inceptiontime,
  title={InceptionTime: Finding AlexNet for Time Series Classification},
  author={Hassan Ismail Fawaz and Benjamin Lucas and Germain Forestier and Charlotte Pelletier and Daniel F. Schmidt and Jonathan Weber and Geoffrey I. Webb and Lhassane Idoumghar and Pierre-Alain Muller and Franccois Petitjean},
  journal={ArXiv},
  year={2019},
  volume={abs/1909.04939}
}
Open source: https://github.com/hfawaz/InceptionTime
Overview: This work is motivated by the Inception-V4 for image classification. Similarly, the inception module in the
    proposed InceptionTime use multiple kernels with different sizes, differently the sizes of kernels become larger as
    the authors though that a longer kernel is helpful for modeling time series.
    Note that, InceptionTime is an ensemble of multiple inception nets with different random initializations. Experiment
    results shown that the proposed ensemble model is competitive to the conventional state-of-the-art HIVE-CODE.
My view: There are multiple points different from the primitive Inception-v4 for image. For example:
    - Inception-v4 use a stack of conv layers with small kernels instead of a conv layer with a large kernel to
    guarantee efficiency. However, this use an opposite strategy.
    - The number of filters InceptionTime is a constant but in the primitive Inception-v4 various numbers are applied
    and the deeper a layer in a stack of conv layers with a larger number of filters.
    - Inception-v4 use 'relu' activation in inception block but InceptionTime use 'linear' activation except the last
    layer in the block.
What I did not realize:
    - model_checkpoint = keras.callbacks.ModelCheckpoint(filepath=file_path, monitor='loss', save_best_only=True)
More references:
    Keras library: tf.keras.applications.InceptionResNetV2()

Latest update: 20200728
"""

__author__ = 'Fanling Huang'

import pandas as pd
import warnings
import tensorflow as tf

from .base import BaseClassifierDNNKeras


class InceptionNetV4(BaseClassifierDNNKeras):
    def __init__(self, input_shape, n_classes, verbose=1, name='inceptionv4'):
        super(InceptionNetV4, self).__init__(input_shape, n_classes, verbose, name)

        self.is_mts = (input_shape[1] > 1)

        self.use_bottleneck = True
        self.use_residual = True
        self.filters_bottleneck = 32
        self.filters = 32
        self.kernel_size = 40
        self.depth = 6

        self.batch_size = 64
        self.n_epochs = 1500

        self.x = tf.keras.layers.Input(self.input_shape)
        self.output = self.build_model()
        self.model = tf.keras.models.Model(inputs=self.x, outputs=self.output)
        if self.verbose > 0:
            self.model.summary()

    def _inception_block(self, x, activation='linear', strides=1):
        x_inception = x
        if self.use_bottleneck and self.is_mts:
            x_inception = tf.keras.layers.Conv1D(filters=self.filters_bottleneck, kernel_size=1, strides=1,
                                                 padding='same', activation=activation, use_bias=False)(x)

        kernel_size_list = [self.kernel_size // (2**i) for i in range(3)]
        conv_list = []
        for kernel_size in kernel_size_list:
            conv = tf.keras.layers.Conv1D(self.filters, kernel_size, strides=strides, padding='same',
                                          activation=activation, use_bias=False)(x_inception)
            conv_list.append(conv)
        max_pool = tf.keras.layers.MaxPool1D(pool_size=3, strides=strides, padding='same')(x_inception)
        conv = tf.keras.layers.Conv1D(self.filters, kernel_size=1, strides=1, padding='same', activation=activation,
                                      use_bias=False)(max_pool)
        conv_list.append(conv)

        y = tf.keras.layers.Concatenate()(conv_list)
        y = tf.keras.layers.BatchNormalization()(y)
        y = tf.keras.layers.Activation('relu')(y)
        return y

    def _shortcut_layer(self, x, y):

        y_shortcut = tf.keras.layers.Conv1D(filters=y.shape[-1].value, kernel_size=1, strides=1, padding='same',
                                            use_bias=False)(x)
        y_shortcut = tf.keras.layers.BatchNormalization()(y_shortcut)

        res = tf.keras.layers.Add()([y_shortcut, y])
        res = tf.keras.layers.Activation('relu')(res)
        return res

    def build_model(self):
        x = self.x
        x_res = self.x
        for dep in range(self.depth):
            x = self._inception_block(x)
            if self.use_residual and dep % 3 == 2:
                x = self._shortcut_layer(x_res, x)
                x_res = x

        gap = tf.keras.layers.GlobalAveragePooling1D()(x)
        out = tf.keras.layers.Dense(self.n_classes, activation='softmax')(gap)
        return out

    def fit(self, x, y,
            batch_size=None,
            n_epochs=None,
            validation_data=None,
            shuffle=True,
            **kwargs):
        # set parameters
        if batch_size is None:
            batch_size = self.batch_size
        batch_size = min(x.shape[0] // 10, batch_size)
        if batch_size == 0:
            batch_size = min(self.batch_size, x.shape[0])
            warnings.warn("Reset the batch size to {} because batch size can not be 0."
                          .format(batch_size))
        if n_epochs is None:
            n_epochs = self.n_epochs
            # n_epochs = 1 # for test

        # start to train
        optimizer = tf.keras.optimizers.Adam()
        self.model.compile(
            loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy'])

        if validation_data is None:
            reduce_lr = tf.keras.callbacks.ReduceLROnPlateau(
                monitor='loss', factor=0.5, patience=50, min_lr=0.0001)
            hist = self.model.fit(
                x, y, batch_size=batch_size, epochs=n_epochs,
                verbose=self.verbose, callbacks=[reduce_lr])
        else:
            reduce_lr = tf.keras.callbacks.ReduceLROnPlateau(
                monitor='val_loss', factor=0.5, patience=50, min_lr=0.0001)
            hist = self.model.fit(
                x, y, batch_size=batch_size, epochs=n_epochs,
                verbose=self.verbose, validation_data=validation_data, callbacks=[reduce_lr])
        return pd.DataFrame(hist.history)
