"""
Network originally proposed by:
@article{zhao2017convolutional,
  title={Convolutional neural networks for time series classification},
  author={Zhao, Bendong and Lu, Huanzhang and Chen, Shangfeng and Liu, Junliang and Wu, Dongya},
  journal={Journal of Systems Engineering and Electronics},
  volume={28},
  number={1},
  pages={162--169},
  year={2017},
  publisher={BIAI}
}

Abstract
﻿This think this is not a good work.
This work is an extension work on MC-DCNN that model multivariate time series independently.
This work modified traditional CNN to jointly model multivariate time series, in which the critical 
parameters, i.e. conv filter size, pooling method and number of conv filters, are experimentally 
determined. However, it is unclear that the process of parameter selection is conducted on 
validation set or not. I can't find any works to explain that. 
The experiments are conducted on synthetic data and 8 UCR datasets with large training size. 
It should be noted that they didn't compare the proposed approach to MC-DCNN while they claim 
their network is better than MC-DCNN.

Realization Limitation:
This realization fixed the parameters to the default settings in section of original paper. 
However, such a configure can't generalize to wide scenarios, so it is a recommendation to realize 
a cross-validation procedure to select parameters.

"""

__author__ = 'Fanling Huang'

import pandas as pd
import tensorflow as tf

from .base import BaseClassifierDNNKeras


class TCNN(BaseClassifierDNNKeras):
    def __init__(self, input_shape, n_classes, verbose=1, name='tcnn'):
        super(TCNN, self).__init__(input_shape, n_classes, verbose, name)

        # default parameters
        self.batch_size = 16
        self.n_epochs = 2000

        # set up model
        self.x = tf.keras.layers.Input(input_shape)
        self.output = self.build_model()
        self.model = tf.keras.models.Model(inputs=self.x, outputs=self.output)
        if self.verbose > 0:
            self.model.summary()

    def build_model(self):
        padding = 'valid'
        if self.input_shape[0] < 60: # for italypowerondemand dataset
            padding = 'same'

        conv1 = tf.keras.layers.Conv1D(6, 7, padding=padding, activation='sigmoid')(self.x)
        conv1 = tf.keras.layers.AveragePooling1D(pool_size=3)(conv1)
        conv2 = tf.keras.layers.Conv1D(12, 7, padding=padding, activation='sigmoid')(conv1)
        conv2 = tf.keras.layers.AveragePooling1D(pool_size=3)(conv2)

        flat = tf.keras.layers.Flatten()(conv2)
        output = tf.keras.layers.Dense(self.n_classes, activation='sigmoid')(flat)

        return output

    def fit(self, x, y,
            batch_size=None,
            n_epochs=None,
            validation_data=None,
            shuffle=True,
            **kwargs):
        # check parameters
        if batch_size is None:
            batch_size = self.batch_size
        if n_epochs is None:
            n_epochs = self.n_epochs

        # start to train
        optimizer = tf.keras.optimizers.Adam()
        self.model.compile(loss='mean_squared_error', optimizer=optimizer, metrics=['accuracy'])
        if validation_data is None:
            hist = self.model.fit(
                x, y, batch_size=batch_size, epochs=n_epochs, verbose=self.verbose)
        else:
            hist = self.model.fit(
                x, y, batch_size=batch_size, epochs=n_epochs, verbose=self.verbose,
                validation_data=validation_data
            )
        return pd.DataFrame(hist.history)