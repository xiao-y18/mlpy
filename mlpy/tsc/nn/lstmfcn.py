"""
Network originally proposed by:
@article{Karim2017LSTM,
  title={LSTM Fully Convolutional Networks for Time Series Classification},
  author={Karim, Fazle and Majumdar, Somshubra and Darabi, Houshang and Chen, Shun},
  journal={IEEE Access},
  volume={PP},
  number={99},
  pages={1662-1669},
  year={2017},
}
Cote: https://github.com/titu1994/LSTM-FCN

Discoveries:
- the authors select hyper-parameters via the default testing split. Specifically, they applied 
    ReduceLROnPlateau, select cells from [8, 64, 128] for the LSTM.
    - I set cell to 8.
    - ReduceLROnPlateau is applied on training set if validation set is not availablle.
- the initialization in FCN is different form the primitive setting, they set kernel_initializer='he_uniform'.
"""

__author__ = 'Fanling Huang'

import pandas as pd
import numpy as np
import keras # !! can not be changed to tf.keras as some modules in attention LSTM need the support of keras.
             # arise Segmentation fault when using keras=2.2.4
             # it works on keras=2.1.0

from .base import BaseClassifierDNNKeras
from .lstmfcnlib import AttentionLSTM
from ..lib.smallsample import cal_class_weight


class LSTMFCN(BaseClassifierDNNKeras):
    def __init__(self, input_shape, n_classes, verbose=1, name='lstmfcn'):
        super(LSTMFCN, self).__init__(input_shape, n_classes, verbose, name)

        # default parameters
        self.batch_size = 128
        self.n_epochs = 2000
        self.n_cells = 8
        self.lr = 1e-3

        # set up model
        self.x = keras.Input(self.input_shape)
        self.output = self.build_model()
        self.model = keras.models.Model(inputs=self.x, outputs=self.output)
        if (self.verbose > 0):
            self.model.summary()

    def build_model(self):
        ## Basic LSTM block
        y1 = keras.layers.Permute((2, 1))(self.x) # [length, variable] --> [variable, length]
        y1 = keras.layers.LSTM(self.n_cells)(y1)
        y1 = keras.layers.Dropout(0.8)(y1)

        ## FCN block
        conv1 = keras.layers.Conv1D(128, 8, padding='same', kernel_initializer='he_uniform')(self.x)
        conv1 = keras.layers.BatchNormalization()(conv1)
        conv1 = keras.layers.Activation('relu')(conv1)

        conv2 = keras.layers.Conv1D(256, 5, padding='same', kernel_initializer='he_uniform')(conv1)
        conv2 = keras.layers.BatchNormalization()(conv2)
        conv2 = keras.layers.Activation('relu')(conv2)

        conv3 = keras.layers.Conv1D(128, 3, padding='same', kernel_initializer='he_uniform')(conv2)
        conv3 = keras.layers.BatchNormalization()(conv3)
        conv3 = keras.layers.Activation('relu')(conv3)

        y2 = keras.layers.GlobalAveragePooling1D()(conv3)

        ## concatenate two blocks
        y = keras.layers.concatenate([y1, y2])

        out = keras.layers.Dense(self.n_classes, activation='softmax')(y)

        return out

    def fit(self, x,
            y,
            batch_size=None,
            n_epochs=None,
            validation_data=None,
            shuffle=True,
            **kwargs):
        ## set parameters
        if batch_size is None:
            batch_size = self.batch_size
        if n_epochs is None:
            n_epochs = self.n_epochs

        ## class imbalance was handled via a class weighting scheme inspired by \cite{king2001logistic}
        y_unit = [np.argmax(one_hot) for one_hot in y]
        class_weight = cal_class_weight(y_unit)

        ## start to train
        optimizer = keras.optimizers.Adam(self.lr)
        self.model.compile(
            loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy'])
        factor = 1.0 / np.cbrt(2)

        if validation_data is None:
            reduce_lr = keras.callbacks.ReduceLROnPlateau(
                monitor='loss', factor=factor, patience=100, mode='auto', cooldown=0, min_lr=1e-4,
                verbose=self.verbose)
            hist = self.model.fit(
                x, y, batch_size=batch_size, epochs=n_epochs, class_weight=class_weight,
                verbose=self.verbose, callbacks=[reduce_lr])
        else:
            reduce_lr = keras.callbacks.ReduceLROnPlateau(
                monitor='val_loss', factor=factor, patience=100, mode='auto', cooldown=0, min_lr=1e-4,
                verbose=self.verbose)
            hist = self.model.fit(
                x, y, batch_size=batch_size, epochs=n_epochs, class_weight=class_weight,
                verbose=self.verbose, validation_data=validation_data, callbacks=[reduce_lr])
        return pd.DataFrame(hist.history)


class LSTMFCNAttention(LSTMFCN):
    def __init__(self, input_shape, n_classes, verbose=1, name='lstmfcna'):
        super(LSTMFCNAttention, self).__init__(input_shape, n_classes, verbose, name)

    def build_model(self):
        ## Basic LSTM block
        y1 = keras.layers.Permute((2, 1))(self.x) # [length, variable] --> [variable, length]
        y1 = AttentionLSTM(self.n_cells)(y1)
        y1 = keras.layers.Dropout(0.8)(y1)

        ## FCN block
        conv1 = keras.layers.Conv1D(128, 8, padding='same', kernel_initializer='he_uniform')(self.x)
        conv1 = keras.layers.BatchNormalization()(conv1)
        conv1 = keras.layers.Activation('relu')(conv1)

        conv2 = keras.layers.Conv1D(256, 5, padding='same', kernel_initializer='he_uniform')(conv1)
        conv2 = keras.layers.BatchNormalization()(conv2)
        conv2 = keras.layers.Activation('relu')(conv2)

        conv3 = keras.layers.Conv1D(128, 3, padding='same', kernel_initializer='he_uniform')(conv2)
        conv3 = keras.layers.BatchNormalization()(conv3)
        conv3 = keras.layers.Activation('relu')(conv3)

        y2 = keras.layers.GlobalAveragePooling1D()(conv3)

        ## concatenate two blocks
        y = keras.layers.concatenate([y1, y2])

        out = keras.layers.Dense(self.n_classes, activation='softmax')(y)

        return out