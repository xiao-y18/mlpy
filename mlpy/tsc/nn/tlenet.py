"""
Network originally proposed by:
@inproceedings{le2016data,
  title={Data augmentation for time series classification using convolutional neural networks},
  author={Le Guennec, Arthur and Malinowski, Simon and Tavenard, Romain},
  year={2016}
}

Abstract:
- t-leNet is a time series specific version of leNet model, proposed in paper "Yann LeCun et al. 1998. “Gradient-based learning applied to document recognition”.
- proposed two strategies to prevent overfitting problem
    1. data augmentation
        a. window slicing (WS), is identical to MCNN's proposed by ﻿Cui et al. (2016).
        b. window warping (WW), employs a warping technique that squeezes or dilates the time series.
        - Note: WS is applied after WW to derive equidistant time series.
    2. training the network in a semi-supervised fashion. (We didn't consider here.)

Fixed bugs
- majority vote: fawaz2019deep vote wrong on all instances instead of on batch instances that are 
    derived from a same time series.

Confusion (waiting to justify)
- activation function, relu, didn't mentioned in the t-leNet paper, and the referred leNet use 
  instead customized tanh function.
- the number of units in FC layer (#FC): to my understanding after reading the paper proposed lenet, 
  #FC isn't 500 used by (fawaz2019deep), but 120.
"""

__author__ = 'Fanling Huang'

import numpy as np
import pandas as pd
import tensorflow as tf

from mlpy.lib import metrics
from mlpy.lib.utils import vote_majority
from mlpy.data.ts import slice, warping

from .base import BaseClassifierDNNKeras


class TLeNet(BaseClassifierDNNKeras):
    def __init__(self, input_shape, n_classes, verbose=1, name='tlenet'):
        super(TLeNet, self).__init__(input_shape, n_classes, verbose, name)

        # default parameters
        self.ratios_warping = [0.5, 2]
        self.ratio_slice = 0.1 # should be less thant the smallest warping ratio.
        self.batch_size = 256
        self.n_epochs = 1000

    def build_model(self):
        conv1 = tf.keras.layers.Conv1D(
            filters=5, kernel_size=5, padding='same', activation='relu')(self.x)
        conv1 = tf.keras.layers.MaxPooling1D(pool_size=2)(conv1)
        conv2 = tf.keras.layers.Conv1D(
            filters=20, kernel_size=5, padding='same', activation='relu')(conv1)
        conv2 = tf.keras.layers.MaxPooling1D(pool_size=4)(conv2)

        # (fawaz2019deep) "they did not mention the number of hidden units in the fully-connected
        # layer so we took the lenet they referenced."
        flat = tf.keras.layers.Flatten()(conv2)
        fc = tf.keras.layers.Dense(500, activation='relu')(flat)
        out = tf.keras.layers.Dense(self.n_classes, activation='softmax')(fc)

        return out

    def fit(self, x, y,
            batch_size=None,
            n_epochs=None,
            validation_data=None,
            shuffle=True,
            **kwargs):
        # check parameters
        if batch_size is None:
            batch_size = self.batch_size
        if n_epochs is None:
            n_epochs = self.n_epochs

        # limit the number of augmented time series if series too long or too many.
        if x.shape[1] > 500 or x.shape[0] > 2000:
            self.ratios_warping = [1]
            self.ratio_slice = 0.9
        # increase the slice if series too short
        if x.shape[1] * self.ratio_slice < 8:
            self.ratio_slice = 8 / x.shape[1]
        # preprocess data
        x, y, _ = self.preprocess(x, y, self.ratio_slice, self.ratios_warping)

        # train
        self.input_shape = x.shape[1:]
        self.x = tf.keras.layers.Input(self.input_shape)
        self.output = self.build_model()
        self.model = tf.keras.models.Model(inputs=self.x, outputs=self.output)
        if (self.verbose == 1):
            self.model.summary()
        optimizer = tf.keras.optimizers.Adam(lr=0.01, decay=0.005)
        self.model.compile(optimizer=optimizer,
            loss='categorical_crossentropy', metrics=['accuracy'])

        if validation_data is None:
            hist = self.model.fit(
                x, y, batch_size=batch_size, epochs=n_epochs, verbose=self.verbose)
        else:
            x_valid, y_valid = validation_data
            x_valid, y_valid, _ = self.preprocess(
                x_valid, y_valid, self.ratio_slice, self.ratios_warping)
            hist = self.model.fit(
                x, y, batch_size=batch_size, epochs=n_epochs,
                verbose=self.verbose, validation_data=(x_valid, y_valid))

        return pd.DataFrame(hist.history)

    def evaluate(self, x, y, batch_size=None, **kwargs):
        x_aug, y_aug, n_unit_inc_tot = self.preprocess(x, y, self.ratio_slice, self.ratios_warping)

        y_pred = np.argmax(self.model.predict(x_aug), axis=1)
        y_pred_vote = vote_majority(y_pred, n_unit_inc_tot)

        acc_general = metrics.accuracy_score(np.argmax(y_aug, axis=1), y_pred)
        acc_vote = metrics.accuracy_score(np.argmax(y, axis=1), y_pred_vote)
        print(acc_general, acc_vote)

        return acc_vote

    def preprocess(self, x, y, ratio_slice, ratios_warping):
        length = x.shape[1]
        length_sliced = int(ratio_slice * length)
        x_aug_list = [x] # include the primitive data
        y_aug_list = [y]
        # data augmentation using Window Warping(WW)
        for ratio in ratios_warping:
            x_aug_list.append(self.warping(x, ratio))
            y_aug_list.append(y)
        # data augmentation using Window Slicing(WS)
        n_unit_inc_list = []
        for i in range(len(x_aug_list)):
            x_aug_list[i], y_aug_list[i], n_unit_inc = self.slice(
                x_aug_list[i], y_aug_list[i], length_sliced)
            n_unit_inc_list.append(n_unit_inc)

        # construct final data
        n_unit_inc_tot = np.array(n_unit_inc_list).sum()
        x_new = np.zeros((x.shape[0]*n_unit_inc_tot, length_sliced, x.shape[2]))
        y_new = np.zeros((y.shape[0]*n_unit_inc_tot,)+y.shape[1:])
        idx = 0
        for i in range(x.shape[0]): # for each primitive ts
            # combine the derived ts in each block
            for x_aug, y_aug, n_inc in zip(x_aug_list, y_aug_list, n_unit_inc_list):
                x_new[idx:(idx + n_inc)] = x_aug[i * n_inc:(i + 1) * n_inc]
                y_new[idx:(idx + n_inc)] = y_aug[i * n_inc:(i + 1) * n_inc]
                idx += n_inc
        return x_new, y_new, n_unit_inc_tot

    def slice(self, x, y, length_sliced):
        return slice(x, y, length_sliced)

    def warping(self, ts, ratio):
        return warping(ts, ratio)



