import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import statsmodels.api as sm
from statsmodels.tsa.arima_model import ARIMA

from tsa.lib import metrics

from statsmodels.tsa.stattools import acf, pacf

def is_stable(ts):
    from statsmodels.tsa.stattools import adfuller as ADF
    adf = ADF(ts)
    return adf[1] < 0.05

def is_white_noise(ts, lags=1):

    assert lags < len(ts), 'lags should be less than length of values.'
    from statsmodels.stats.diagnostic import acorr_ljungbox

    lbs, px = acorr_ljungbox(ts, lags=lags) # p-value < 0.05 indicate there is white noise.
    h = np.sum(px  < 0.05)
    if h > 0: # at least one accept, it is not white noise.
        return False
    else:
        return True

def stabilize_diff(ts, diff_upper=10):
    """
    
    :param ts: 
    :param diff_upper: 
    :return:
        i >=0: difference i times
        i == -1: can't be stabilized
    """
    # difference
    for i in range(diff_upper):
        ret = np.diff(ts, i)[i:]
        if is_stable(ret):
            return i
    return -1

def arima_valid(ts, order, lags=12):
    try:
        arima = ARIMA(ts, order).fit(disp=-1)
    except:
        print("Some error occur in ARIMA, please change model !")
        return False
    pred = arima.predict()
    res = (pred - ts).dropna()
    if is_white_noise(res, lags=lags):
        return True
    else:
        return False

def arima_fit(ts, p_upper, d_upper, q_upper):
    p, d, q = None, None, None
    for d in range(d_upper):
        print("conduct {}-order difference to stabilized time series".format(d))
        if (is_white_noise(np.diff(ts, d)[d::])):
            print("time series is white noise, can't be predicted!")
            return (p, d, q)

        # select arma order
        order = sm.tsa.arma_order_select_ic(np.diff(ts, d)[d::], max_ar=p_upper, max_ma=q_upper,
                                            ic=['aic', 'bic', 'hqic'])
        print("the candidate best order of ARMA as follow: ")
        print('aic-{}'.format(order.aic_min_order))
        print('bic-{}'.format(order.bic_min_order))
        print('hqic-{}'.format(order.hqic_min_order))
        print()

        # get one of candidate model parameters
        if arima_valid(ts, (order.aic_min_order[0], d, order.aic_min_order[1])):
            p, q = order.aic_min_order
        elif arima_valid(ts, (order.bic_min_order[0], d, order.bic_min_order[1])):
            p, q = order.bic_min_order
        elif arima_valid(ts, (order.hqic_min_order[0], d, order.hqic_min_order[1])):
            p, q = order.hqic_min_order
        else:
            print("Didn't found any appropriate ARIMA model!")
            continue
        print("one of best ARIMA order (p, d, q)= ({}, {}, {})".format(p, d, q))
        return (p, d, q)

    return (None, None, None)

if __name__ == '__main__':
    # prepare data
    time_start = 1700
    df = sm.datasets.sunspots.load_pandas().data
    ts = pd.Series(df.SUNACTIVITY.values, index=df.YEAR.values.astype(int))
    n_train = int(ts.shape[0] * 0.9)
    ts_train = ts[:n_train]
    ts_test = ts[n_train::]

    # ARIMA model
    p_upper = 10
    d_upper = 10
    q_upper = 10
    p, d, q = arima_fit(ts_train, p_upper, d_upper, q_upper)
    if p is None or d is None or q is None:
        exit(-1)
    model = ARIMA(ts_train, (p, d, q)).fit(disp=-1)
    print(model.summary())

    ts_pred = model.predict(start=n_train, end=len(df) - 1, dynamic=True)
    print("metrics: ")
    print("RMSE: {}".format(metrics.rmse(ts_test.values, ts_pred.values)))
    print("NRMSE: {}".format(metrics.nrmse(ts_test.values, ts_pred.values)))
    print("MAE: {}".format(metrics.mae(ts_test.values, ts_pred.values)))
    print("MAPE: {}".format(metrics.mape(ts_test.values, ts_pred.values)))
    print()

    fig = plt.figure(figsize=(12, 8))
    plt.plot(np.arange(n_train), ts_train, label='train')
    plt.plot(n_train+np.arange(len(ts_test)), ts_test, label='test')
    plt.plot(n_train+np.arange(len(ts_test)), ts_pred, label='pre')
    plt.legend(loc='best')
    plt.show()