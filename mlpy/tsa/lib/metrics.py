import numpy as np

from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from sklearn.utils import check_array

def rmse(y, y_hat):
    return np.sqrt(mean_squared_error(y, y_hat))

def nrmse(y, y_hat):
    return np.sqrt(mean_squared_error(y, y_hat)) / (max(y) - min(y))

def mae(y, y_hat):
    return mean_absolute_error(y, y_hat)

def mape(y, y_hat):
    """
        reference: https://stats.stackexchange.com/questions/58391/mean-absolute-percentage-error-mape-in-scikit-learn
    """
    y = check_array(y)
    y_hat = check_array(y_hat)
    return np.mean(np.abs(y - y_hat) / y) * 100