from __future__ import print_function

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from ffx_learn import learn_ffx
from deap_learn import learn_deap

def plot_dataset(data, names):
    num_col = len(names)
    f, axis = plt.subplots(num_col, 1, sharex=True, figsize=(10, 8))
    for i, ax, name in zip(range(num_col), axis, names):
        y = data[:, i]
        ax.plot(y)
        ax.set_ylabel(name)
        ax.set_ylim(min(y)-np.std(y), max(y)+np.std(y))

def test_ffx_no_delay(data, var_names, n_var_learn, model_ids):
    results = learn_ffx.learn(data, var_names, n_var_learn)
    learn_ffx.result_summary(results, var_names[0:n_var_learn])
    learn_ffx.compare_plot(data, var_names, n_var_learn, results, model_ids, lg='chinese')
    plt.show()

def test_ffx_delay(data, var_names, n_var_learn, model_ids):
    seq_timedelay = np.arange(16)
    results = learn_ffx.learn(data, var_names, n_var_learn, seq_timedelay)
    learn_ffx.result_summary(results, var_names[0:n_var_learn])
    learn_ffx.compare_plot(data, var_names, n_var_learn, results, model_ids, seq_timedelay, lg='chinese')
    plt.show()


def test_deap_no_delay(data, var_names, n_var_learn, model_ids):
    results = learn_deap.learn(data, var_names, n_var_learn)
    learn_deap.result_summary(results, var_names[0:n_var_learn])
    learn_deap.compare_plot(data, var_names, n_var_learn, results, model_ids, lg='chinese')
    plt.show()


def test_deap_delay(data, var_names, n_var_learn, model_ids):
    seq_timedelay = np.arange(16)
    results = learn_deap.learn(data, var_names, n_var_learn, seq_timedelay)
    learn_deap.result_summary(results, var_names[0:n_var_learn])
    learn_deap.compare_plot(data, var_names, n_var_learn, results, model_ids, seq_timedelay, lg='chinese')
    plt.show()


if __name__ == '__main__':
    data = pd.read_table("data/163_0151_2016-06-30_80_allAxisTemperature_merge2_1_reg_axl1.txt", sep=" ", header=None)
    print("the information of data")
    print(data.info())
    print(data.head())

    data = data.as_matrix()
    var_names = np.array(['ZX_WD_1', 'ZX_WD_2', 'ZX_WD_3', 'ZX_WD_4', 'ZX_WD_5', 'ZX_WD_6',
                         'ZX_HW_1', 'ZX_HW_2', 'ZD_LCG', 'ZD_TFG', 'ZD_JHG', 'ZD_LLJ', 'ZD_SPEED'])
    n_var_learn = 1

    ## test ffx
    # model_ids = [4, 7, 4, 5, 6, 5]
    # test_ffx_no_delay(data, varnames, var_num_learn, model_ids)
    # model_ids = [8, 4, 3, 4, 6, 6]
    # test_ffx_delay(data, varnames, var_num_learn, model_ids)

    ## test deap
    # model_ids = [0, 0, 0, 0, 0, 0]
    # test_deap_no_delay(data, varnames, var_num_learn, model_ids)
    model_ids = [0, 0, 0, 0, 0, 0]
    test_deap_delay(data, var_names, n_var_learn, model_ids)

