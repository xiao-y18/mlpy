import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import os

""" variables 
'BTSJ' 'ZX_WD_1_1' 'ZX_WD_1_2' 'ZX_WD_1_3' 'ZX_WD_1_4'
 'ZX_WD_1_5' 'ZX_WD_1_6' 'ZX_WD_2_1' 'ZX_WD_2_2' 'ZX_WD_2_3' 'ZX_WD_2_4'
 'ZX_WD_2_5' 'ZX_WD_2_6' 'ZX_WD_3_1' 'ZX_WD_3_2' 'ZX_WD_3_3' 'ZX_WD_3_4'
 'ZX_WD_3_5' 'ZX_WD_3_6' 'ZX_WD_4_1' 'ZX_WD_4_2' 'ZX_WD_4_3' 'ZX_WD_4_4'
 'ZX_WD_4_5' 'ZX_WD_4_6' 'ZX_WD_5_1' 'ZX_WD_5_2' 'ZX_WD_5_3' 'ZX_WD_5_4'
 'ZX_WD_5_5' 'ZX_WD_5_6' 'ZX_WD_6_1' 'ZX_WD_6_2' 'ZX_WD_6_3' 'ZX_WD_6_4'
 'ZX_WD_6_5' 'ZX_WD_6_6' 'ZD_FLAG' 'ZD_ALT' 'ZD_CNT' 'ZD_LCG' 'ZD_TFG'
 'ZD_JHG' 'ZD_LLJ' 'ZD_SPEED' 'ZX_HW1_1' 'ZX_HW2_1' 'ZX_HW1_2' 'ZX_HW2_2'
 'ZX_HW1_3' 'ZX_HW2_3' 'ZX_HW1_4' 'ZX_HW2_4' 'ZX_HW1_5' 'ZX_HW2_5'
 'ZX_HW1_6' 'ZX_HW2_6'
"""

varnames = np.array(['ZX_WD_1_1', 'ZX_WD_1_2', 'ZX_WD_1_3', 'ZX_WD_1_4',
                     'ZX_WD_1_5', 'ZX_WD_1_6', 'ZX_WD_2_1', 'ZX_WD_2_2',
                     'ZX_WD_2_3', 'ZX_WD_2_4', 'ZX_WD_2_5', 'ZX_WD_2_6',
                     'ZX_WD_3_1', 'ZX_WD_3_2', 'ZX_WD_3_3', 'ZX_WD_3_4',
                     'ZX_WD_3_5', 'ZX_WD_3_6', 'ZX_WD_4_1', 'ZX_WD_4_2',
                     'ZX_WD_4_3', 'ZX_WD_4_4', 'ZX_WD_4_5', 'ZX_WD_4_6',
                     'ZX_WD_5_1', 'ZX_WD_5_2', 'ZX_WD_5_3', 'ZX_WD_5_4',
                     'ZX_WD_5_5', 'ZX_WD_5_6', 'ZX_WD_6_1', 'ZX_WD_6_2',
                     'ZX_WD_6_3', 'ZX_WD_6_4', 'ZX_WD_6_5', 'ZX_WD_6_6'])

def get_original():
    data_root = "data_ijcai/interpolation_10s_int/"
    len = 5000
    files = os.listdir(data_root)
    data = []
    for f in files:
        df = pd.read_csv("{}/{}".format(data_root, f))
        df = df.loc[:len-1, varnames]
        print(f, df.shape)
        for i in np.arange(df.shape[1]):
            data.append(df.iloc[:, i])

    data = np.array(data)
    return data

def resample(data, step):

    indexs = np.arange(0, data.shape[1], step)
    return data[:, indexs]

if __name__ == '__main__':
    data = get_original()
    step = 5
    data = resample(data, 5)
    out_fname = "locomotive_temperature_{}".format(step)
    np.savetxt(out_fname, data, delimiter=',', newline='\n')