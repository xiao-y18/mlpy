import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt


def cal_nrmse(model, X, y):
    return np.sqrt(np.nanmean((model.simulate(X)-y)**2 / (max(y) - min(y))))


def cal_rmse(model, X, y):
    return np.sqrt(np.nanmean((model.simulate(X)-y)**2))


def get_data_nodelay(data, y_i, cols):
    X = data[:, cols]
    y = data[:, y_i]

    return X, y


def get_data_delay(data, seq_timedelay, y_i, var_names, del_lag_0=True):
    """
    :param data: 2-dimension array-like data, column correspond attributes.
    :param seq_timedelay: the first number must be 0.
    :param y_i: the index of target attribute.
    :param var_names: the variable name in data.
    :param del_lag_0: delete all of lag-0 variables.
    :return:         
    """
    print("---------------prepare time delay data-------------")
    new_data = pd.DataFrame()
    tail = 10
    start = max(seq_timedelay) + 10
    varnum = len(var_names)
    cols = list(np.arange(varnum))
    for i_var in cols:
        for dt in seq_timedelay:
            name = "{0}_dt{1}".format(var_names[i_var], dt)
            new_data[name] = data[(start-dt):(-dt-tail), i_var]

    y_name = "{0}_dt{1}".format(var_names[y_i], 0)  # name can not contain character '.'
    y_value = new_data[y_name]
    if del_lag_0:
        for name in var_names:
            name_lag_0 = "{0}_dt{1}".format(name, 0)
            del new_data[name_lag_0]
    else: # just delete the lag-0 of target variable.
        del new_data[y_name]

    names = list(new_data.columns)
    X = new_data.as_matrix()
    y = y_value.as_matrix()

    print("del the target variable %s" % (y_name))
    print("number of variable: %d" % (len(names)))
    print("the shape of X")
    print(np.shape(X))
    print("the shape of y")
    print(np.shape(y))

    return X, y, names


def compare_plot_english(y, yp, name, dir_out=None):
    f, (ax1, ax2, ax3) = plt.subplots(3, 1, figsize=(8, 5))

    ax1.set_title('{}'.format(name))
    ax1.plot(y)
    ax1.set_ylim(min(y) - np.std(y), max(y) + np.std(y))
    ax1.set_ylabel('Observed Value')
    ax2.plot(yp)
    ax2.set_ylim(min(yp) - np.std(yp), max(yp + np.std(yp)))
    ax2.set_ylabel('Estimated Value')
    errors = y-yp
    ax3.plot(errors)
    ax3.set_ylabel('Residual Value')
    ax3.set_ylim(min(errors) - np.std(errors), max(errors) + np.std(errors))
    if dir_out:
        plt.savefig(os.path.join(dir_out, 'compare_plot_english_{}.png'.format(name)))
        plt.close()

def set_matplot_zh_font():
    """解决中文问题"""
    plt.rcParams['font.sans-serif'] = ['SimHei']  # 用来正常显示中文标签
    plt.rcParams['axes.unicode_minus'] = False  # 用来正常显示负号

def compare_plot_chinese(y, yp, name, dir_out=None):
    from matplotlib.font_manager import FontProperties
    font = FontProperties(fname='/System/Library/Fonts/STHeiti Light.ttc')
    set_matplot_zh_font()
    f, (ax1, ax2, ax3) = plt.subplots(3, 1, figsize=(8, 5))
    ax1.set_title('{}'.format(name))
    ax1.plot(y)
    ax1.set_ylim(min(y) - np.std(y), max(y) + np.std(y))
    ax1.set_ylabel(u"真实值", fontproperties=font)
    ax2.plot(yp)
    ax2.set_ylim(min(yp) - np.std(yp), max(yp + np.std(yp)))
    ax2.set_ylabel(u"预测值", fontproperties=font)
    errors = y-yp
    ax3.plot(errors)
    ax3.set_ylabel(u"残差", fontproperties=font)
    ax3.set_ylim(min(errors) - np.std(errors), max(errors) + np.std(errors))
    if dir_out:
        plt.savefig(os.path.join(dir_out, 'compare_plot_chinese_{}.png'.format(name)))