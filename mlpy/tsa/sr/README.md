# Symbolic regression

There are two implementations which relies on `deap` (a nondeterministic algorithm) and `ffx` (a deterministic algorithm), respectively. 

## deap

Libs of `joblib` and `dill` can be applied to save models trained via `deap`, but `pickle` can not. In addition, those three libs are all fail to save model expression generated via `deap`. Therefore, I save the expression as a string in application. Please refer to the project of `tangche` for more details. 


## TODO

- [ ] deap: hyperparameters should be selected with a cross-validation procedure, for example randomly running the program used `deap` for 5 or 10 times and selecting the setting achieving the best performance on validation dataset. 
	- Motivation: I realized that results derived by `deap` is indeterminate when I revise it to the latest project `project/tangche` in December, 2020. Therefore, it is unstable to use a default heuristic setting. 
- [ ] revise ffx. 
	- For limited time, I just revised and used `deap` in the latest project `project/tangche` but leave `ffx_learn`. I want to revise `ffx_learn` next time.