import pandas as pd
import numpy as np

if __name__ == '__main__':
    varnames = np.array(['ZX_WD_1', 'ZX_WD_2', 'ZX_WD_3', 'ZX_WD_4', 'ZX_WD_5', 'ZX_WD_6'])
    data = pd.read_table("data/163_0151_2016-06-30_80_allAxisTemperature_merge2_1_reg_axl1.txt", sep=" ", header=None)
    print("the information of data")
    print(data.info())
    print(data.head())

    data_0134 = pd.read_csv("data_ijcai/interpolation_10s_int/0134.csv")
    print(data_0134.columns.values)

    # data = data.as_matrix()
    # varnames = np.array(['ZX_WD_1', 'ZX_WD_2', 'ZX_WD_3', 'ZX_WD_4', 'ZX_WD_5', 'ZX_WD_6',
    #                     'ZX_HW_1', 'ZX_HW_2',
    #                      'ZD_LCG', 'ZD_TFG', 'ZD_JHG', 'ZD_LLJ', 'ZD_SPEED'])
    # var_num = 13
    # var_num_learn = 6