import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from ffx_learn import call_ffx
from ffx_learn import learn_ffx

""" variables 
'BTSJ' 'ZX_WD_1_1' 'ZX_WD_1_2' 'ZX_WD_1_3' 'ZX_WD_1_4'
 'ZX_WD_1_5' 'ZX_WD_1_6' 'ZX_WD_2_1' 'ZX_WD_2_2' 'ZX_WD_2_3' 'ZX_WD_2_4'
 'ZX_WD_2_5' 'ZX_WD_2_6' 'ZX_WD_3_1' 'ZX_WD_3_2' 'ZX_WD_3_3' 'ZX_WD_3_4'
 'ZX_WD_3_5' 'ZX_WD_3_6' 'ZX_WD_4_1' 'ZX_WD_4_2' 'ZX_WD_4_3' 'ZX_WD_4_4'
 'ZX_WD_4_5' 'ZX_WD_4_6' 'ZX_WD_5_1' 'ZX_WD_5_2' 'ZX_WD_5_3' 'ZX_WD_5_4'
 'ZX_WD_5_5' 'ZX_WD_5_6' 'ZX_WD_6_1' 'ZX_WD_6_2' 'ZX_WD_6_3' 'ZX_WD_6_4'
 'ZX_WD_6_5' 'ZX_WD_6_6' 'ZD_FLAG' 'ZD_ALT' 'ZD_CNT' 'ZD_LCG' 'ZD_TFG'
 'ZD_JHG' 'ZD_LLJ' 'ZD_SPEED' 'ZX_HW1_1' 'ZX_HW2_1' 'ZX_HW1_2' 'ZX_HW2_2'
 'ZX_HW1_3' 'ZX_HW2_3' 'ZX_HW1_4' 'ZX_HW2_4' 'ZX_HW1_5' 'ZX_HW2_5'
 'ZX_HW1_6' 'ZX_HW2_6'
"""
data_0134 = pd.read_csv("data_ijcai/interpolation_10s_int/0134.csv")
data_0134_pca = pd.read_csv("data_ijcai/pca1/0134.csv")

y = data_0134['ZX_WD_1_1']
X = data_0134[['ZX_WD_1_2', 'ZX_WD_1_3', 'ZX_WD_1_4', 'ZX_WD_1_5', 'ZX_WD_1_6']]
results = list()
result = call_ffx.run_ffx_main_half(X.as_matrix(), y.values, X.columns, verbose=False)
results.insert(len(results), result)
learn_ffx.result_summary(results, 'ZX_WD_1_1')
