import sys

from ..common import *
from .model import *


def result_summary(results, var_names, dir_out=None):
    """
    
    :param results: list, 
    :param var_names: list
    :return: 
    """

    assert len(results) == len(var_names), 'the length of result and variable should be equal.'

    # display result table
    if dir_out is not None:
        orig_stdout = sys.stdout
        f = open(os.path.join(dir_out, 'result.txt'), 'w')
        sys.stdout = f
    for result, name in zip(results, var_names):
        numBases, nrmses, rmses, models, _ = result
        nrmses_test, nrmses_train = nrmses
        rmses_test, rmses_train = rmses

        print("{0} {1} {0}".format("*"*20, name))
        print("original model:")
        for model in models:
            print(model)
        print("symplified model:")
        for model in models:
            print(simplify_this(model))

        print("{0}".format("-"*25))
        print("complexity\t\tnrmse_test\t\tnrmse_train\t\trmse_test\t\trmse_train")
        for com, nrmse_test, nrmse_train, rmse_test, rmse_train in \
                zip(numBases, nrmses_test, nrmses_train, rmses_test, rmses_train):
            print("{0}\t\t{1}\t\t{2}\t\t{3}\t\t{4}".format(com, nrmse_test, nrmse_train, rmse_test, rmse_train))
        print("{0}".format("*"*40))
    if dir_out is not None:
        sys.stdout = orig_stdout
        f.close()

    # Pareto front, nrmse on test set
    plt.figure()
    for result, name in zip(results, var_names):
        coms, nrmses, rmses, _, _ = result
        nrmses_test, _ = nrmses
        plt.plot(coms, '.-', nrmses_test, label=name)
    plt.title("NRMSE Pareto Front")
    plt.xlabel("Complexity")
    plt.ylabel("Error")
    plt.legend()
    plt.tight_layout()
    if dir_out:
        plt.savefig(os.path.join(dir_out, 'pareto_nrmse.png'))
    plt.close()

    # Pareto front, rmse on test set
    plt.figure()
    for result, name in zip(results, var_names):
        coms, nrmses, rmses, _, _ = result
        rmses_test, _ = rmses
        plt.plot(coms, '.-', rmses_test, label=name)
    plt.title("RMSE Pareto Front")
    plt.xlabel("Complexity")
    plt.ylabel("Error")
    plt.legend()
    plt.tight_layout()
    if dir_out:
        plt.savefig(os.path.join(dir_out, 'pareto_rmse.png'))
    plt.close()


def compare_plot(data, var_names, var_target_ids, results, model_ids, seq_timedelay=None, lg='english', dir_out=None):
    """
    :param data: 2 dimension array-like data set.
    :param var_names: all variable names correspond each column `data`
    :param var_target_ids: indexes of targets in var_names
    :param results: learning result list returned from ffx 
    :param model_ids: a sequence of model's id to simulate
    :param seq_timedelay: None or a sequence of time delay
    :param lg: language, {'english', 'chinese'}
    :return: 
    """
    if seq_timedelay is None:
        __compare_plot_nodelay(data, var_names, var_target_ids, results, model_ids, lg, dir_out)
    else:
        __compare_plot_delay(data, var_names, var_target_ids, seq_timedelay, results, model_ids, lg, dir_out)

def __compare_plot_nodelay(data, var_names, var_target_ids, results, model_ids, lg='english', dir_out=None):
    n_var = len(var_names)
    for i_var, i_model, result in zip(var_target_ids, model_ids, results):
        coms, nrmses, rmses, models, models_bin = result
        cols = list(np.arange(0, n_var))
        del cols[i_var]
        X, y = get_data_nodelay(data, i_var, cols)
        model = models_bin[i_model]
        yp = model(*X.T)
        print(simplify_this(models[i_model]))
        print()
        if lg == 'english':
            compare_plot_english(y, yp, var_names[i_var], dir_out)
        elif lg == 'chinese':
            compare_plot_chinese(y, yp, var_names[i_var], dir_out)
        else:
            raise ValueError("Can't find the lg type {}".format(lg))
def __compare_plot_delay(data, var_names, var_target_ids, seq_timedelay, results, model_ids, lg='english', dir_out=None):
    for i_var, i_model, result in zip(var_target_ids, model_ids, results):
        coms, nrmses, rmses, models, models_bin = result
        X, y, names = get_data_delay(data=data, seq_timedelay=seq_timedelay, y_i=i_var, var_names=var_names)
        model = models_bin[i_model]
        yp = model(*X.T)
        print(simplify_this(models[i_model]))
        print()
        if lg == 'english':
            compare_plot_english(y, yp, var_names[i_var], dir_out)
        elif lg == 'chinese':
            compare_plot_chinese(y, yp, var_names[i_var], dir_out)
        else:
            raise ValueError("Can't find the lg type {}".format(lg))


def learn(data, var_names, var_target_ids, seq_timedelay=None):
    """
    
    :param data: 
    :param var_names: 
    :param var_target_ids: 
    :param seq_timedelay: 
    :return: 
    """
    if seq_timedelay is None:
        return __learn_nodelay(data, var_names, var_target_ids)
    else:
        return __learn_delay(data, var_names, var_target_ids, seq_timedelay)


def __learn_nodelay(data, var_names, var_target_ids):
    results = list()
    n_var = len(var_names)
    for i in var_target_ids:
        print("-------------------------learn %s------------" % var_names[i])
        cols = list(np.arange(0, n_var))
        del cols[i]
        X, y = get_data_nodelay(data, i, cols)
        result = train_half(X, y, var_names[cols])
        results.insert(i, result)
        print("--------------------end----------------------------")
    return results
def __learn_delay(data, var_names, var_target_ids, seq_timedelay):
    results = list()
    for i in var_target_ids:
        print("-------------------------learn %s------------" % var_names[i])
        X, y, names = get_data_delay(data, seq_timedelay, i, var_names)
        result = train_half(X, y, names)
        results.insert(i, result)
        print("--------------------end----------------------------")
    return results
