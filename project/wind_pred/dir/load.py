import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt
import sklearn.preprocessing as skpre

DIR_DATA = '../data/obs_update_nwp_allyear.csv'

def load(path, n_pred=1, window=48, features=['dir'], period=24):
    ## Load primitive data
    df = pd.read_csv(path)
    # process anomaly
    VALUE_MISSED = -9999
    for col in df:
        dfc = df[col]
        inds = df.index[dfc == VALUE_MISSED]
        for i in inds:
            l = i-1
            while dfc[l] == VALUE_MISSED:
                l = l-1
            r = i+1
            while dfc[r] == VALUE_MISSED:
                r = r+1
            dfc[i] = np.mean([dfc[l], dfc[r]])
    # extract selected data
    X_base = df[features].values
    y_base = df['dir'].values
    y_nwp_base = df['nwp_dir'].values
    n_features = X_base.shape[1]
    n_total = X_base.shape[0]
    print("Finish to load data: ", X_base.shape, y_base.shape)
    print("y, max={}, mean={}, median={}, min={}".format(
        np.max(y_base), np.mean(y_base), np.median(y_base), np.min(y_base)))

    ## Construct data
    pos_begin = window-1
    pos_end = n_total-n_pred-1
    n_samples = pos_end - pos_begin + 1
    X = np.zeros([n_samples, window, n_features])
    y = np.zeros(n_samples)
    y_nwp = np.zeros(n_samples)
    for i, t in zip(range(n_samples), range(pos_begin, pos_end+1)):
        X[i] = X_base[(t-window+1):(t+1)]
        y[i] = y_base[t+n_pred]
        y_nwp[i] = y_nwp_base[t+n_pred]

    ## Split into train, valid, test
    n_test = 30*period # according to the number of days in a month
    # test set
    X_te = X[(-n_test):]
    y_te = y[(-n_test):]
    y_nwp_te = y_nwp[(-n_test):]
    # validation set
    X_val = X[(-2*n_test):(-n_test)]
    y_val = y[(-2*n_test):(-n_test)]
    y_nwp_val = y_nwp[(-2*n_test):(-n_test)]
    # train set
    X_tr = X[:(-2*n_test)]
    y_tr = y[:(-2*n_test)]
    y_nwp_tr = y_nwp[:(-2*n_test)]
    print("Finish to construct data: ")
    print("train split: ", X_tr.shape, y_tr.shape, y_nwp_tr.shape)
    print("validation split: ", X_val.shape, y_val.shape, y_nwp_val.shape)
    print("test split: ", X_te.shape, y_te.shape, y_nwp_te.shape)

    return (X_tr, y_tr, y_nwp_tr),  (X_val, y_val, y_nwp_val), (X_te, y_te, y_nwp_te)


def load_nwp(path, n_pred=1, window=48, features=['dir'], period=24):
    ## Load primitive data
    df = pd.read_csv(path)
    # process anomaly
    VALUE_MISSED = -9999
    for col in df:
        dfc = df[col]
        inds = df.index[dfc == VALUE_MISSED]
        for i in inds:
            l = i-1
            while dfc[l] == VALUE_MISSED:
                l = l-1
            r = i+1
            while dfc[r] == VALUE_MISSED:
                r = r+1
            dfc[i] = np.mean([dfc[l], dfc[r]])
    # extract selected data
    X_base = df[features].values
    y_base = df['dir'].values
    y_nwp_base = df['nwp_dir'].values
    n_features = X_base.shape[1]
    n_total = X_base.shape[0]
    print("Finish to load data: ", X_base.shape, y_base.shape)
    print("y, max={}, mean={}, median={}, min={}".format(
        np.max(y_base), np.mean(y_base), np.median(y_base), np.min(y_base)))

    ## Construct data
    pos_begin = window-1
    pos_end = n_total-n_pred-1
    n_samples = pos_end - pos_begin + 1
    X = np.zeros([n_samples, window, n_features+1]) # +1, nwp predictions
    y = np.zeros(n_samples)
    y_nwp = np.zeros(n_samples)
    for i, t in zip(range(n_samples), range(pos_begin, pos_end+1)):
        y[i] = y_base[t+n_pred]
        y_nwp[i] = y_nwp_base[t+n_pred]
        x_nwp = np.array([y_nwp[i]]*window)
        x_nwp = x_nwp.reshape(x_nwp.shape + (1,))
        X[i] = np.hstack([X_base[(t - window + 1):(t + 1)], x_nwp])

    ## Split data into train, valid, test
    n_test = 30*period # according to the number of days in a month
    # test set
    X_te = X[(-n_test):]
    y_te = y[(-n_test):]
    y_nwp_te = y_nwp[(-n_test):]
    # validation set
    X_val = X[(-2*n_test):(-n_test)]
    y_val = y[(-2*n_test):(-n_test)]
    y_nwp_val = y_nwp[(-2*n_test):(-n_test)]
    # train set
    X_tr = X[:(-2*n_test)]
    y_tr = y[:(-2*n_test)]
    y_nwp_tr = y_nwp[:(-2*n_test)]
    print("Finish to construct data: ")
    print("train split: ", X_tr.shape, y_tr.shape, y_nwp_tr.shape)
    print("validation split: ", X_val.shape, y_val.shape, y_nwp_val.shape)
    print("test split: ", X_te.shape, y_te.shape, y_nwp_te.shape)

    return (X_tr, y_tr, y_nwp_tr),  (X_val, y_val, y_nwp_val), (X_te, y_te, y_nwp_te)


class Dataset:
    def __init__(self, path, period=24, window=48, features=['dir'], use_nwp=True):
        self.path =  path
        self.period = period
        self.window = window
        self.features = features
        self.use_nwp = use_nwp

        self.name = os.path.basename(path)
        self.df = self.read(path)
        self.n_total = self.df.shape[0]

        self.X = self.df[self.features].values
        self.y = self.df['dir'].values.reshape(-1, 1)
        self.y_nwp = self.df['nwp_dir'].values.reshape(-1, 1)
        self.indexes = np.arange(self.n_total)

        ## split data into train, valid, test
        # [t_tr, ..., t_val, ..., t_te, ..., t_end)
        #   train=[t_tr:t_val), valid=[t_val:t_te), test=[t_te:t_end)
        self.n_test = 30 * period  # according to the number of days in a month
        self.t_te = self.n_total - self.n_test
        self.t_val = self.n_total - 2 * self.n_test

    def read(self, path):
        df = pd.read_csv(path)
        VALUE_MISSED = -9999
        for col in df:
            dfc = df[col]
            inds = df.index[dfc == VALUE_MISSED]
            for i in inds:
                l = i - 1
                while dfc[l] == VALUE_MISSED:
                    l = l - 1
                r = i + 1
                while dfc[r] == VALUE_MISSED:
                    r = r + 1
                dfc[i] = np.mean([dfc[l], dfc[r]])
        return df

    def construct_instances(self, t_base, t_end, n_pred):
        pos_begin = t_base
        pos_end = t_end-n_pred
        n_samples = pos_end - pos_begin + 1
        ret_X = np.zeros([n_samples, self.window, self.X.shape[1]])
        ret_y = np.zeros([n_samples])
        ret_inds = np.zeros([n_samples])
        for i, t in zip(range(n_samples), range(pos_begin, pos_end + 1)):
            ret_X[i] = self.X[(t - self.window + 1):(t + 1)]
            ret_y[i] = self.y[t + n_pred]
            ret_inds[i] = t + n_pred
        return ret_X, ret_y, ret_inds

    def get_train_split(self, n_pred):
        t_base = self.window-1
        t_end = self.t_val-1
        return self.construct_instances(t_base, t_end, n_pred)

    def get_valid_split(self, n_pred):
        t_base = self.t_val-1
        t_end = self.t_te-1
        return self.construct_instances(t_base, t_end, n_pred)

    def get_test_split(self, n_pred):
        t_base = self.t_te-1
        t_end = self.n_total-1
        return self.construct_instances(t_base, t_end, n_pred)

    def get_test_split_all(self):
        t_start = self.t_te
        t_end = self.n_total - 1

        ret_inds = np.arange(t_start, t_end+1)
        n_samples = ret_inds.shape[0]
        ret_X = np.zeros([n_samples, self.window, self.X.shape[1]])
        ret_y = np.zeros([n_samples])
        for i in range(0, n_samples, self.period):
            t = ret_inds[i]
            x = self.X[(t-self.window):t]
            for k in range(self.period):
                ret_y[i+k] = self.y[t+k]
                ret_X[i+k] = x

        return ret_X, ret_y, ret_inds

    def get_dir(self, inds):
        return self.df['dir'].values[inds]

    def get_nwp_dir(self, inds):
        return self.df['nwp_dir'].values[inds]

    def get_wind(self, inds):
        return self.df['wind'].values[inds]


def load_test(path, n_pred=1, window=48, features=['dir'], period=24, mode='valid'): #
    ## Load primitive data
    df = pd.read_csv(path)
    # process anomaly
    VALUE_MISSED = -9999
    for col in df:
        dfc = df[col]
        inds = df.index[dfc == VALUE_MISSED]
        for i in inds:
            l = i-1
            while dfc[l] == VALUE_MISSED:
                l = l-1
            r = i+1
            while dfc[r] == VALUE_MISSED:
                r = r+1
            dfc[i] = np.mean([dfc[l], dfc[r]])
    # extract selected data
    X_base = df[features].values
    y_base = df['dir'].values.reshape(-1, 1)
    y_nwp_base = df['nwp_dir'].values.reshape(-1, 1)
    n_features = X_base.shape[1]
    n_total = X_base.shape[0]
    print("Finish to load data: ", X_base.shape, y_base.shape)
    print("y, max={}, mean={}, median={}, min={}".format(
        np.max(y_base), np.mean(y_base), np.median(y_base), np.min(y_base)))

    ## split data into train, valid, test
    # [t_tr, ..., t_val, ..., t_te, ..., t_end)
    #   train=[t_tr:t_val), valid=[t_val:t_te), test=[t_te:t_end)
    n_test = 30*period  # according to the number of days in a month
    t_te = n_total-n_test
    t_val = n_total-2*n_test
    t_tr = window-1
    t_end = n_total

    ## construct data,
    # two splits, [t_tr, ..., t_spilt, ..., t_end]
    if mode == 'valid':
        t_spilt = t_val
        t_end = t_te
    elif mode == 'test':
        t_spilt = t_te
    else:
        raise ValueError("Can not found mode={}".format(mode))
    # normalization based on train split
    # x
    x_scaler = skpre.StandardScaler()
    X_base[:t_spilt] = x_scaler.fit_transform(X_base[:t_spilt])
    X_base[t_spilt:] = x_scaler.transform(X_base[t_spilt:])
    # y
    # y_scaler = skpre.StandardScaler()
    y_scaler = skpre.MinMaxScaler()
    y_base[:t_spilt] = y_scaler.fit_transform(y_base[:t_spilt])
    y_base[t_spilt:] = y_scaler.transform(y_base[t_spilt:])
    # y_nwp
    # y_nwp_scaler = skpre.StandardScaler()
    y_nwp_scaler = skpre.MinMaxScaler()
    y_nwp_base[:t_spilt] = y_nwp_scaler.fit_transform(y_nwp_base[:t_spilt])
    y_nwp_base[t_spilt:] = y_nwp_scaler.transform(y_nwp_base[t_spilt:])

    ## construct pairwise data
    def _construct_data(pos_begin, pos_end):
        n_samples = pos_end - pos_begin + 1
        X = np.zeros([n_samples, window, n_features])
        y = np.zeros(n_samples)
        y_nwp = np.zeros(n_samples)
        for i, t in zip(range(n_samples), range(pos_begin, pos_end+1)):
            X[i] = X_base[(t-window+1):(t+1)]
            y[i] = y_base[t+n_pred]
            y_nwp[i] = y_nwp_base[t+n_pred]
        return X, y, y_nwp

    X_tr, y_tr, y_nwp_tr = _construct_data(t_tr, t_spilt-1-n_pred)
    X_te, y_te, y_nwp_te = _construct_data(t_spilt, t_end-1-n_pred)

    print("Finish to construct data: ")
    print("train split: ", X_tr.shape, y_tr.shape, y_nwp_tr.shape)
    print("test split: ", X_te.shape, y_te.shape, y_nwp_te.shape)

    return (X_tr, y_tr, y_nwp_tr), (X_te, y_te, y_nwp_te), (x_scaler, y_scaler, y_nwp_scaler)

if __name__ == '__main__':
    tr, val, te = load(DIR_DATA)

    # dir_out = 'cache/eda'
    # if os.path.exists(dir_out) is False:
    #     os.makedirs(dir_out)
    #
    # def plot_dir(dir, nwp_dir, tag, n_samples=24*10):
    #     plt.plot(dir[:n_samples], label='dir')
    #     plt.plot(nwp_dir[:n_samples], label='nwp_dir')
    #     plt.legend(loc='best')
    #     plt.savefig('{}/{}.png'.format(dir_out, tag))
    #     plt.close()
    #
    #
    # df = pd.read_csv(DIR_DATA)
    # plot_dir(df['dir'].values, df['nwp_dir'].values, 'prim')
    #
    # plot_dir(tr[1], tr[2], 'train')
    # plot_dir(val[1], val[2], 'valid')
    # plot_dir(te[1], te[2], 'test')

