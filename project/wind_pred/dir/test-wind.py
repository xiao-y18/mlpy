import os
import time

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import lightgbm as lgb
import tensorflow as tf

from sklearn.svm import SVR
from sklearn.metrics import mean_squared_error, mean_absolute_error, mean_squared_log_error, r2_score
from sklearn.preprocessing import MinMaxScaler, Normalizer


# configs: use past history to predict future_target
past_history = 24 * 1
future_target = 1
TRAIN_SPLIT = 4500
TEST_SPLIT = 5000


def get_wind_dataset():
    path = os.path.join('../data', 'obs_update_nwp_allyear.csv')
    data = pd.read_csv(path)
    data['data-time'] = pd.to_datetime(data['data-time'])
    y = data[['dir']].squeeze()

    return data, y


def multivariate_data(dataset, target, start_index, end_index, history_size, target_size, step,
                      single_step=False, add_nwp_dir=True):
    data = []
    labels = []

    start_index = start_index + history_size
    if end_index is None:
        end_index = len(dataset) - target_size
    end_index = end_index - history_size

    for i in range(start_index, end_index):
        indices = range(i-history_size, i, step)
        if add_nwp_dir:
            nwp_dir = dataset[ range(i, i+history_size, step) ]
            d = np.concatenate((dataset[indices], nwp_dir[:, 1].reshape(-1, 1)), axis=1)
            data.append(d)
        else:
            data.append(dataset[indices])

        if single_step:
            labels.append(target[i+target_size])
        else:
            labels.append(target[i:i+target_size])

    return np.array(data), np.array(labels)


def generate_datasets(data, target, past_history, future_target, TRAIN_SPLIT, TEST_SPLIT, STEP=1):
    x_train, y_train = multivariate_data(data, target, 0, TRAIN_SPLIT, past_history,
                                                       future_target, STEP,
                                                       single_step=False)
    x_val, y_val = multivariate_data(data, target, TRAIN_SPLIT, TEST_SPLIT, past_history,
                                                   future_target, STEP,
                                                   single_step=False)
    x_test, y_test = multivariate_data(data, target, TEST_SPLIT, None, past_history,
                                                   future_target, STEP,
                                                   single_step=False)

    return x_train, y_train, x_val, y_val, x_test, y_test


def embedding(data):
    return data.reshape(data.shape[0], -1)


def lgbt_single(x_train, y_train, x_val, y_val, x_test):
    params = {
        'boosting_type': 'gbdt',
        'objective': 'regression',
        'metric': {'rmse'},
        'subsample': 0.2,
        'learning_rate': 0.9,
        'feature_fraction': 0.9,
        'bagging_fraction': 0.9,
        'alpha': 0.1,
        'lambda': 0.1,
        'verbose': -1
    }

    lgb_train = lgb.Dataset(x_train, y_train)
    lgb_eval = lgb.Dataset(x_val, y_val)

    gbm = lgb.train(params,
                    lgb_train,
                    num_boost_round=300,
                    valid_sets=[lgb_train, lgb_eval],
                    early_stopping_rounds=100,
                    verbose_eval=False)  # 100)

    y_pred = gbm.predict(x_test, num_iteration=gbm.best_iteration)

    return gbm, y_pred


def svr_single(x_train, y_train, x_test):
    # svr = GridSearchCV(SVR(kernel='rbf', gamma=0.1),
    #                    cv=3,
    #                    param_grid={"C": [1e-1, 1e0, 1e1],
    #                                "gamma": np.logspace(-2, 2, 5)})

    svr = SVR(kernel='rbf', gamma=0.1)

    # time cost
    t0 = time.time()
    svr.fit(x_train, y_train)
    svr_fit = time.time() - t0
    # print("SVR complexity and bandwidth selected and model fitted in %.3f s" % svr_fit)

    # sv_ratio = svr.best_estimator_.support_.shape[0] / x_train.shape[0]
    # print("Support vector ratio: %.3f" % sv_ratio)

    t0 = time.time()
    y_pred = svr.predict(x_test)
    svr_predict = time.time() - t0
    # print("SVR prediction for %d inputs in %.3f s" % (x_train.shape[0], svr_predict))

    return svr, y_pred


def lstm(x_train, y_train, x_val, y_val, x_test, plot=False):
    def plot_train_history(history, title):
        loss = history.history['loss']
        val_loss = history.history['val_loss']

        epochs = range(len(loss))

        plt.figure()

        plt.plot(epochs, loss, 'b', label='Training loss')
        plt.plot(epochs, val_loss, 'r', label='Validation loss')
        plt.title(title)
        plt.legend()

        plt.show()

    BATCH_SIZE = 256
    BUFFER_SIZE = 10000
    EVALUATION_INTERVAL = 200
    EPOCHS = 100

    train_data = tf.data.Dataset.from_tensor_slices((x_train, y_train))
    train_data = train_data.cache().shuffle(BUFFER_SIZE).batch(BATCH_SIZE).repeat()

    val_data = tf.data.Dataset.from_tensor_slices((x_val, y_val))
    val_data = val_data.batch(BATCH_SIZE).repeat()

    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.LSTM(12,
                                   input_shape=(x_train.shape[1], x_train.shape[2])))
    model.add(tf.keras.layers.Dense(6))
    model.add(tf.keras.layers.Dense(1, activation='sigmoid'))

    model.compile(optimizer=tf.keras.optimizers.RMSprop(clipvalue=1.0), loss='mean_squared_error')

    step_history = model.fit(train_data, epochs=EPOCHS,
                             steps_per_epoch=EVALUATION_INTERVAL,
                             validation_data=val_data,
                             validation_steps=50)

    model.save("wind-dir-lstm.h5")

    if plot:
        plot_train_history(step_history,
                           'Single Step Training and validation loss')

    y_pred = model.predict(x_test)

    return model, y_pred


def cal_dir_mae(df1, df2):
    delta = df2 - df1
    delta[delta > 180] = delta - 360
    delta[delta < -180] = delta + 360
    bias = delta.mean()
    mae = np.abs(delta).mean()
    rmse = np.sqrt((delta ** 2.).mean())

    return bias.values[0], rmse.values[0], mae.values[0]


def plot(start, length, y_pred_svr_trans, y_pred_gbdt_trans, y_pred_lstm_trans):
    epochs = range(length)

    plt.figure(figsize=(20, 15))

    plt.plot(epochs, y_pred_svr_trans[start:start + length], 'r', label='SVR')
    plt.plot(epochs, y_pred_gbdt_trans[start:start + length], 'y', label='GBDT')
    plt.plot(epochs, y_pred_lstm_trans[start:start + length], 'b', label='LSTM')
    plt.plot(epochs, X[['dir']].values[TEST_SPLIT + start : TEST_SPLIT + start + length], 'g', label='dir')
    plt.plot(epochs, X[['nwp_dir']].values[TEST_SPLIT + start : TEST_SPLIT + start + length], 'black', label='nwp_dir')

    plt.legend(prop={'size': 15})

    plt.show()


X, y = get_wind_dataset()

X = X[['dir', 'nwp_dir']]
X['direction'] = X['dir'] / 90
X['pian'] = X['dir'] % 90

# normalization
X_norm = Normalizer().fit_transform(X)
y_scaler = MinMaxScaler()
y_norm = y_scaler.fit_transform(y.values.reshape(-1, 1))


# train test split
X_train, y_train, X_val, y_val, X_test, y_test = \
    generate_datasets(X_norm, y_norm, past_history, future_target, TRAIN_SPLIT, TEST_SPLIT)

# embedding
X_train_embedding = embedding(X_train)
X_val_embedding = embedding(X_val)
X_test_embedding = embedding(X_test)

# GBDT test
gdbt_model, y_pred_gbdt = lgbt_single(X_train_embedding, y_train.ravel(),
                                 X_val_embedding, y_val.ravel(),
                                 X_test_embedding)
y_pred_gbdt_trans = y_scaler.inverse_transform(y_pred_gbdt.reshape(-1, 1)).ravel()

# SVR Test
X_train_svr = np.concatenate((X_train_embedding, X_val_embedding), axis=0)
y_train_svr = np.concatenate((y_train, y_val), axis=0)
svr_single_models, y_pred_svr = svr_single(X_train_svr, y_train_svr.ravel(), X_test_embedding)
y_pred_svr_trans = y_scaler.inverse_transform(y_pred_svr.reshape(-1, 1)).ravel()

# LSTM Test
model, y_pred_lstm = lstm(X_train, y_train, X_val, y_val, X_test, plot=True)
y_pred_lstm_trans = y_scaler.inverse_transform(y_pred_lstm.reshape(-1, 1)).ravel()

print('GBDT MSE:', mean_squared_error(y_pred_gbdt, y_test.ravel()))
print('GBDT MAE:', cal_dir_mae(pd.DataFrame(y_pred_gbdt_trans), pd.DataFrame(y_test.ravel())))

print('SVR MSE:', mean_squared_error(y_pred_svr, y_test.ravel()))
print('SVR MAE:', cal_dir_mae(pd.DataFrame(y_pred_svr_trans), pd.DataFrame(y_test.ravel())))

print('LSTM MSE:', mean_squared_error(y_pred_lstm.ravel(), y_test.ravel()))
print('LSTM MAE:', cal_dir_mae(pd.DataFrame(y_pred_lstm_trans), pd.DataFrame(y_test.ravel())))

print('nwp MAE:', cal_dir_mae(pd.DataFrame(X[['dir']].values), pd.DataFrame(X[['nwp_dir']].values)))

start = 50
length = 50

plot(start, length, y_pred_svr_trans, y_pred_gbdt_trans, y_pred_lstm_trans)
