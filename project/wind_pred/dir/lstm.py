import numpy as np
import tensorflow as tf
import pandas as pd
import matplotlib.pyplot as plt

from project.wind_pred.dir.load import load_test, DIR_DATA

def cal_metrics_dir(obs, pred):
    obs = pd.DataFrame(obs)
    pred = pd.DataFrame(pred)
    delta = pred - obs
    delta[delta > 180] = delta - 360
    delta[delta < -180] = delta + 360
    bias = delta.mean()
    mae = np.abs(delta).mean()
    rmse = np.sqrt((delta ** 2.).mean())

    return bias.values[0], rmse.values[0], mae.values[0]



class LSTMWind(object):
    def __init__(self, input_shape, window, verbose=1):
        self.input_shape = input_shape
        self.window = window
        self.verbose = verbose
        self.model = self.build_model(self.input_shape, self.window)
        if (self.verbose > 0):
            self.model.summary()

    def build_model(self, input_shape, window):
        model = tf.keras.models.Sequential()
        model.add(tf.keras.layers.LSTM(window, input_shape=input_shape))
        model.add(tf.keras.layers.Dense(window // 2 ))
        model.add(tf.keras.layers.Dense(window // 4))
        model.add(tf.keras.layers.Dense(1))
        return model

    # def build_model(self, input_shape, window):
    #     model = tf.keras.models.Sequential()
    #     model.add(tf.keras.layers.LSTM(12, input_shape=input_shape))
    #     model.add(tf.keras.layers.Dense(6))
    #     model.add(tf.keras.layers.Dense(1))
    #     return model

    def fit(self, x,
            y,
            batch_size=256,
            n_epochs=20,
            validation_data=None,
            shuffle=True,
            **kwargs):

        self.model.compile(loss='mae', optimizer=tf.keras.optimizers.RMSprop())
        # self.model.compile(loss='mae', optimizer='adam')
        if validation_data is None:
            hist = self.model.fit(
                x, y, batch_size=batch_size, epochs=n_epochs, verbose=self.verbose)
        else:
            hist = self.model.fit(
                x, y, batch_size=batch_size, epochs=n_epochs, verbose=self.verbose,
                validation_data=validation_data)

        return pd.DataFrame(hist.history)

# if __name__ == '__main__':
#     dir_out = 'cache'
#     period = 24
#     window = period*2
#     n_pred = 1
#     tr, val, te = load(DIR_DATA, n_pred=n_pred, window=window)
#     # tr, val, te = load_nwp(DIR_DATA, n_pred=n_pred, window=window)
#     x_tr, y_tr, y_nwp_tr = tr
#     x_val, y_val, y_nwp_val = val
#
#     n_epochs = 50
#     lstm = LSTMWind(x_tr.shape[1:], window)
#     hist = lstm.fit(x_tr, y_tr, n_epochs=n_epochs, validation_data=(x_val, y_val))
#
#     y_pred = lstm.model.predict(x_val)
#     bias, rmse, mae = cal_metrics_dir(pd.DataFrame(y_val), pd.DataFrame(y_pred))
#
#     plt.plot(hist['loss'].values, label='loss')
#     plt.plot(hist['val_loss'].values, label='val_loss')
#     plt.legend(loc='best')
#     plt.savefig('{}/loss.png'.format(dir_out))
#     plt.close()
#     n_samples_plot = period*10
#     plt.plot(y_val[:n_samples_plot], label='obs')
#     plt.plot(y_nwp_val[:n_samples_plot], label='nwp')
#     plt.plot(y_pred[:n_samples_plot], label='pre')
#     plt.legend(loc='best')
#     plt.savefig('{}/compare.png'.format(dir_out))
#     plt.close()
#     print("Evaluation results of model:")
#     print("bias={}, rmse={}, mae={}".format(bias, rmse, mae))
#
#     print("Evaluation results of NWP: ")
#     bias, rmse, mae = cal_metrics_dir(y_val, y_nwp_val)
#     print("bias={}, rmse={}, mae={}".format(bias, rmse, mae))


# if __name__ == '__main__':
#     dir_out = 'cache'
#     period = 24
#     window = period*2
#     n_pred = 1
#     tr, val, te = load(DIR_DATA, n_pred=n_pred, window=window)
#     # tr, val, te = load_nwp(DIR_DATA, n_pred=n_pred, window=window)
#     x_tr, y_tr, y_nwp_tr = tr
#     x_val, y_val, y_nwp_val = val
#
#     # x_norm = skpre.Normalizer()
#     # x_tr = x_norm.fit_transform(x_tr)
#     # x_val = x_norm.transform(x_val)
#     from IPython import embed; embed()
#     y_scaler = skpre.StandardScaler()
#     y_tr = y_scaler.fit_transform(y_trc)
#     y_val = y_scaler.transform(y_val.reshape(-1,1))
#
#     n_epochs = 50
#     lstm = LSTMWind(x_tr.shape[1:], window)
#     hist = lstm.fit(x_tr, y_tr, n_epochs=n_epochs, validation_data=(x_val, y_val))
#
#     y_pred = lstm.model.predict(x_val)
#     y_pred = y_scaler.inverse_transform(y_pred)
#     y_val = y_scaler.inverse_transform(y_val)
#     bias, rmse, mae = cal_metrics_dir(y_val, y_pred)
#
#     plt.plot(hist['loss'].values, label='loss')
#     plt.plot(hist['val_loss'].values, label='val_loss')
#     plt.legend(loc='best')
#     plt.savefig('{}/loss.png'.format(dir_out))
#     plt.close()
#     n_samples_plot = period*10
#     plt.plot(y_val[:n_samples_plot], label='obs')
#     plt.plot(y_nwp_val[:n_samples_plot], label='nwp')
#     plt.plot(y_pred[:n_samples_plot], label='pre')
#     plt.legend(loc='best')
#     plt.savefig('{}/compare.png'.format(dir_out))
#     plt.close()
#     print("Evaluation results of model:")
#     print("bias={}, rmse={}, mae={}".format(bias, rmse, mae))
#
#     print("Evaluation results of NWP: ")
#     bias, rmse, mae = cal_metrics_dir(y_val, y_nwp_val)
#     print("bias={}, rmse={}, mae={}".format(bias, rmse, mae))


if __name__ == '__main__':
    dir_out = 'cache'
    period = 24
    window = period*2
    n_pred = 1
    tr, val, scaler = load_test(DIR_DATA, n_pred=n_pred, window=window, mode='valid')
    x_tr, y_tr, y_nwp_tr = tr
    x_val, y_val, y_nwp_val = val
    _, y_scaler, _ = scaler

    n_epochs = 50
    lstm = LSTMWind(x_tr.shape[1:], window)
    hist = lstm.fit(x_tr, y_tr, n_epochs=n_epochs, validation_data=(x_val, y_val))

    y_pred = lstm.model.predict(x_val)
    y_pred = y_scaler.inverse_transform(y_pred)
    y_val = y_scaler.inverse_transform(y_val)

    bias, rmse, mae = cal_metrics_dir(y_val, y_pred)
    plt.plot(hist['loss'].values, label='loss')
    plt.plot(hist['val_loss'].values, label='val_loss')
    plt.legend(loc='best')
    plt.savefig('{}/loss.png'.format(dir_out))
    plt.close()
    n_samples_plot = period*10
    plt.plot(y_val[:n_samples_plot], label='obs')
    # plt.plot(y_nwp_val[:n_samples_plot], label='nwp')
    plt.plot(y_pred[:n_samples_plot], label='pre')
    plt.legend(loc='best')
    plt.savefig('{}/compare.png'.format(dir_out))
    plt.close()
    print("Evaluation results of model:")
    print("bias={}, rmse={}, mae={}".format(bias, rmse, mae))
