from project.wind_pred.base import *
import keras
import pandas as pd
import sklearn
import numpy as np
from keras import backend as K

from project.wind_pred.load import load, DIR_DATA_AUG


def recall_m(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
    recall = true_positives / (possible_positives + K.epsilon())
    return recall


def precision_m(y_true, y_pred):
    true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
    predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
    precision = true_positives / (predicted_positives + K.epsilon())
    return precision


def f1_m(y_true, y_pred):
    precision = precision_m(y_true, y_pred)
    recall = recall_m(y_true, y_pred)
    return 2 * ((precision * recall) / (precision + recall + K.epsilon()))

class LSTMWind(object):
    def __init__(self, input_shape, window, verbose=1):
        self.input_shape = input_shape
        self.window = window
        self.verbose = verbose
        self.model = self.build_model(self.input_shape, self.window)
        if (self.verbose > 0):
            self.model.summary()

    def build_model(self, input_shape, window):
        model = keras.models.Sequential()
        model.add(keras.layers.LSTM(window, input_shape=input_shape))
        model.add(keras.layers.Dense( window // 2 ))
        model.add(keras.layers.Dense( window // 4))
        model.add(keras.layers.Dense(1, activation='sigmoid'))
        return model

    def fit(self, x,
            y,
            batch_size=256,
            n_epochs=20,
            validation_data=None,
            shuffle=True,
            **kwargs):
        self.model.compile(loss=focal_loss_sigmoid, optimizer='adam', metrics=[precision_m])
        # self.model.compile(loss='binary_crossentropy', optimizer='adam', metrics=[precision_m])
        if validation_data is None:
            hist = self.model.fit(
                x, y, batch_size=batch_size, epochs=n_epochs, verbose=self.verbose)
        else:
            hist = self.model.fit(
                x, y, batch_size=batch_size, epochs=n_epochs, verbose=self.verbose,
                validation_data=validation_data)

        return pd.DataFrame(hist.history)


if __name__ == '__main__':
    window = 50
    n_epochs = 50
    feature_columns = ['wind', 'nwp_wind', 'hour']
    X_train, y_train, X_test, y_test = load(
        DIR_DATA_AUG, with_valid=False, window=window, feature_columns=feature_columns,
        one_hot=False, norm='axis0')

    lstm = LSTMWind(X_train.shape[1:], window)
    df_hist = lstm.fit(X_train, y_train, n_epochs=n_epochs, shuffle=False)

    y_pred = lstm.model.predict(X_test)
    y_pred = np.round(np.reshape(y_pred, y_pred.shape[0])).astype(int)
    print(sklearn.metrics.classification_report(y_pred, y_test))





