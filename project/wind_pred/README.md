# Task

```
数据说明
训练期:2018年3月1日至2018年10月31日
检验期:2018年11月1日至2018年12月30日

obs_loc_Initime_Endtime.dat:
loc:站点名称
地面风速观测，单位m/s
训练期5880行=连续245天x24小时
-999 为缺测


NWP1_fcst_Initime_Endtime.dat.dat:
数据源为第一种NWP的预报
所有站点的预报都一样
训练期5880行=连续245天x24小时
检验期:1440行=连续60天x24小时
6列为6个变量预报，分别为
第一列地面风U分量 m/s
第二列地面风V分量 m/s
第三列地面温度  K
第四列地面相对湿度 %
第五列地面气压  hpa
第六列海平	面气压 hpa

目标：
根据检验期的NWP1_fcst_2018110112_2018123012.txt 预测9个站点的风速，以降低RMSE。

```


# Server 

```
登陆协议：ssh
主机ip：52.83.143.70
用户名：ec2-user
public key: key.pem
机器学习数据路径：/home/ec2-user/DATA
```
Connect command: 
> sudo chmod 600 key.pem   
> ssh -i key.pem ec2-user@52.83.143.70

## Python enviroment

> python, pip   
> python3, pip-3.6

