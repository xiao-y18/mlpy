"""
For binary classification

"""
from project.wind_pred.base import *
from mlpy.lib.utils import tag_path
from mlpy.tsc.nn import ResNet
from project.wind_pred.load import load, DIR_DATA_AUG

import pandas as pd
import keras
from datetime import datetime
import os

DIR_LOG = os.path.join('cache', tag_path(os.path.abspath(__file__), 1))
if os.path.exists(DIR_LOG) is False:
    os.makedirs(DIR_LOG)



class ResNetWind(ResNet):
    def __init__(self, input_shape, n_classes, verbose=1):
        super(ResNetWind, self).__init__(input_shape, n_classes, verbose)
        self.model = keras.models.Model(inputs=self.x, outputs=self.output)

    def build_model(self):
        res1 = self.block_restnet(self.x, self.n_conv_filters)
        res2 = self.block_restnet(res1, self.n_conv_filters*2)
        res3 = self.block_restnet(res2, self.n_conv_filters*2)

        gap = keras.layers.GlobalAveragePooling1D()(res3)
        out = keras.layers.Dense(self.n_classes-1, activation='sigmoid')(gap)
        return out

    # def fit(self, x, y,
    #         batch_size=None,
    #         n_epochs=None,
    #         validation_data=None,
    #         shuffle=True,
    #         **kwargs):
    #     # set parameters
    #     if batch_size is None:
    #         batch_size = self.batch_size
    #     batch_size = min(x.shape[0] // 10, batch_size)
    #     if n_epochs is None:
    #         n_epochs = self.n_epochs
    #
    #     # start to train
    #     optimizer = keras.optimizers.Adam()
    #     # self.model.compile(
    #     #     loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy'])
    #     self.model.compile(
    #         loss=focal_loss_sigmoid, optimizer=optimizer, metrics=['accuracy'])
    #     reduce_lr = keras.callbacks.ReduceLROnPlateau(
    #         monitor='loss', factor=0.5, patience=1, min_lr=0.0001, verbose=1)
    #
    #     if validation_data is None:
    #         hist = self.model.fit(
    #             x, y, batch_size=batch_size, epochs=n_epochs, verbose=self.verbose)
    #     else:
    #         hist = self.model.fit(
    #             x, y, batch_size=batch_size, epochs=n_epochs, verbose=self.verbose,
    #             validation_data=validation_data)
    #
    #     return pd.DataFrame(hist.history)

    def fit(self, x, y,
            batch_size=None,
            n_epochs=None,
            validation_data=None,
            shuffle=True,
            **kwargs):
        # set parameters
        if batch_size is None:
            batch_size = self.batch_size
        batch_size = min(x.shape[0] // 10, batch_size)
        if n_epochs is None:
            n_epochs = self.n_epochs

        # start to train
        optimizer = keras.optimizers.Adam()
        self.model.compile(
            loss='binary_crossentropy', optimizer=optimizer, metrics=['accuracy'])
        # self.model.compile(
        #     loss=focal_loss_sigmoid, optimizer=optimizer, metrics=['accuracy'])
        reduce_lr = keras.callbacks.ReduceLROnPlateau(
            monitor='loss', factor=0.5, patience=1, min_lr=0.0001, verbose=1)

        if validation_data is None:
            hist = self.model.fit(
                x, y, batch_size=batch_size, epochs=n_epochs,
                verbose=self.verbose, callbacks=[reduce_lr])
        else:
            es = keras.callbacks.EarlyStopping(patience=2, mode='min', verbose=1)
            mc = keras.callbacks.ModelCheckpoint(
                '{}/best_model.h5'.format(DIR_LOG), mode='min', save_best_only=True, verbose=1)
            hist = self.model.fit(
                x, y, batch_size=batch_size, epochs=n_epochs,
                verbose=1, validation_data=validation_data, callbacks=[reduce_lr, es, mc])

        return pd.DataFrame(hist.history)

    def predict(self, x, batch_size=None, threshold=0.5, **kwargs):
        probas = self.predict_proba(x, batch_size)
        y_pred = (probas >= threshold).astype(int).reshape(-1)
        return y_pred


def run(window=48, feature_columns=['wind'], batch_size=48, n_epochs=10, n_classes=2, n_runs=5,
        verbose=1):
    ## load data
    # X_train, y_train, X_test, y_test = load(
    #     DIR_DATA_AUG, window=window, feature_columns=feature_columns, one_hot=False)
    X_train, y_train, X_valid, y_valid, X_test, y_test = load(
        DIR_DATA_AUG, with_valid=True, lag_pred=2, window=window, feature_columns=feature_columns, one_hot=False)

    ## train model
    cls_eval = Evaluation()
    for _ in range(n_runs):
        # train model
        model = ResNetWind(X_train.shape[1:], n_classes, verbose=verbose)
        df_hist = model.fit(X_train, y_train,
                            n_epochs=n_epochs, batch_size=batch_size, validation_data=(X_valid, y_valid))
        # predict
        model.load('{}/best_model.h5'.format(DIR_LOG)) # load best model
        y_pred = model.predict(X_test, batch_size)
        # evaluate
        cls_eval.append(y_pred, y_test)
    df, agg = cls_eval.get_df_with_agg()
    df.to_csv('{}/{}.csv'.format(DIR_LOG, datetime.now().strftime("%I%M%p-%B%d%Y")))

    return agg

def run_diff_lags(window=48, feature_columns=['wind'], batch_size=48, n_epochs=10, n_classes=2, n_runs=5,
        verbose=1):
    def _run(lag_pred):
        X_train, y_train, X_valid, y_valid, X_test, y_test = load(
            DIR_DATA_AUG, with_valid=True, lag_pred=lag_pred, window=window,
            feature_columns=feature_columns, one_hot=False)
        ## train model
        cls_eval = Evaluation()
        for _ in range(n_runs):
            # train model
            # K.clear_session()
            model = ResNetWind(X_train.shape[1:], n_classes, verbose=verbose)
            df_hist = model.fit(X_train, y_train,
                                n_epochs=n_epochs, batch_size=batch_size, validation_data=(X_valid, y_valid))
            # predict
            # model.load('{}/best_model.h5'.format(DIR_LOG)) # load best model
            y_pred = model.predict(X_test, batch_size)
            # evaluate
            cls_eval.append(y_pred, y_test)
        df, agg = cls_eval.get_df_with_agg()
        df.to_csv('{}/lag_pred_{}.csv'.format(DIR_LOG, lag_pred))

    for i in range(1, 25):
        _run(i)

if __name__ == '__main__':
    param = {'window': 24*2, 'feature_columns': ['wind', 'nwp_wind', 'hour'], 'batch_size':64,
             'n_epochs': 1000,
             'n_runs': 5}
    run(**param)


