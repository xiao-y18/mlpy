"""
Batch run the traditional classifiers 

"""
from project.wind_pred.base import *
from mlpy.lib.utils import tag_path
from project.wind_pred.load import load, DIR_DATA_AUG

from datetime import datetime
import os
import numpy as np

from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import LinearSVC, SVC
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import AdaBoostClassifier, BaggingClassifier, ExtraTreesClassifier, \
    GradientBoostingClassifier, IsolationForest, RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB


DIR_LOG = os.path.join('cache', tag_path(os.path.abspath(__file__), 1))
if os.path.exists(DIR_LOG) is False:
    os.makedirs(DIR_LOG)

def run(models:dict, window=48, feature_columns=['wind']):
    ## load data
    X_train, y_train, X_test, y_test = load(
        DIR_DATA_AUG, lag_pred=24, window=window, feature_columns=feature_columns, one_hot=False)
    X_train = np.reshape(X_train, [X_train.shape[0], -1])
    X_test = np.reshape(X_test, [X_test.shape[0], -1])

    ## train model
    cls_eval = Evaluation()
    n_models = len(models.items())
    for i, val in enumerate(models.items()):
        key, model = val[0], val[1]
        print("[{}/{}] ********** processing {}".format(i+1, n_models, key))
        # train model
        model.fit(X_train, y_train)
        y_pred = model.predict(X_test)
        # evaluate
        cls_eval.append(y_pred, y_test, key=key)
    df = cls_eval.get_df()
    df.to_csv('{}/{}.csv'.format(DIR_LOG, datetime.now().strftime("%I%M%p-%B%d%Y")))

if __name__ == '__main__':
    param = {'window': 24*2, 'feature_columns': ['wind', 'nwp_wind', 'hour']}
    models = {
        '{}'.format(LogisticRegression.__name__): LogisticRegression(),
        '{}'.format(KNeighborsClassifier.__name__): KNeighborsClassifier(),
        '{}'.format(LinearSVC.__name__): LinearSVC(),
        '{}'.format(SVC.__name__): SVC(),
        '{}'.format(MLPClassifier.__name__): MLPClassifier(),
        '{}'.format(AdaBoostClassifier.__name__): AdaBoostClassifier(),
        '{}'.format(BaggingClassifier.__name__): BaggingClassifier(),
        '{}'.format(ExtraTreesClassifier.__name__): ExtraTreesClassifier(),
        '{}'.format(GradientBoostingClassifier.__name__): GradientBoostingClassifier(),
        '{}'.format(IsolationForest.__name__): IsolationForest(),
        '{}'.format(RandomForestClassifier.__name__): RandomForestClassifier(),
        '{}'.format(DecisionTreeClassifier.__name__): DecisionTreeClassifier(),
        '{}'.format(GaussianNB.__name__): GaussianNB()
    }
    run(models, **param)


