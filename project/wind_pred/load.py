import numpy as np
import pandas as pd
import sklearn
import os

from mlpy.data import utils
from mlpy.tsc.nn import FCN, ResNet

DIR_DATA = 'data/obs_update_nwp_allyear.csv'
DIR_DATA_AUG = 'data/obs_update_nwp_allyear_augment.csv'
PERIOD = 24

def ratio_of_positive(y):
    """Positive=1, Negative=0"""
    n = len(y)
    n_positives = np.sum(y)
    ratio_positives = n_positives / n
    print("There are {} positive instances over {}, the positive ratio is {}".format(
        n_positives, n, ratio_positives
    ))
    return ratio_positives

def extract_additional_attributes(path_data, THRESHOLD_BIG_WIND=4.0):
    df = pd.read_csv(path_data)
    df['big_wind'] = [1 if (w > THRESHOLD_BIG_WIND) else 0 for w in df['wind']]
    df['data-time'] = pd.to_datetime(df['data-time'])
    df['hour'] = df['data-time'].apply(lambda x: x.hour)
    dir_name = os.path.dirname(path_data)
    basename = os.path.basename(path_data)
    df.to_csv(os.path.join(dir_name, '{}_augment.csv'.format(basename.split('.')[0])))


def load(path_data, with_valid=False, feature_columns=['wind'],
         lag_pred=1, window=48, one_hot=True, norm=None):
    n_classes = 2
    period = PERIOD
    ## load data and extract label
    df = pd.read_csv(path_data)
    df['label'] = df['big_wind']
    n_total = df.shape[0]
    # just parse the considered features
    X_base = df[feature_columns].values
    if len(X_base.shape) == 1:
        X_base = X_base.reshape((-1, 1))
    y_base = df['label'].values.astype(int)
    n_features = X_base.shape[1]
    print("Finish to load whole dataset:")
    ratio_of_positive(y_base)

    ## construct pair-wise data
    pos_begin = window - 1
    # align data to a period of 24h
    if df['hour'][pos_begin] != 0:
        pos_begin = pos_begin+period-df['hour'][pos_begin]
    pos_end = n_total - lag_pred -1
    if df['hour'][pos_end] != period-1:
        pos_end = pos_end - df['hour'][pos_end] - 1
    # construct array-like data
    n_samples = pos_end - pos_begin + 1
    X = np.zeros([n_samples, window, n_features])  # (n_samples, length, n_features)
    y = np.zeros(n_samples, dtype=int)
    for i, t in zip(range(n_samples), range(pos_begin, pos_end+1)):
        X[i] = X_base[(t-window+1):(t+1)]
        y[i] = y_base[t+lag_pred]
    assert n_samples % period == 0, "the number of samples didn't accord the period."
    n_periods = n_samples // period

    ## split dataset according period
    train_periods = int(np.round(0.8 * n_periods))
    test_size = n_samples - train_periods * period
    X_train, X_test, y_train, y_test = sklearn.model_selection.train_test_split(
        X, y, test_size=test_size, shuffle=False)
    if with_valid:
        valid_size = int(np.round(0.2 * (train_periods))) * period
        X_train, X_valid, y_train, y_valid = sklearn.model_selection.train_test_split(
            X_train, y_train, test_size=valid_size, shuffle=False)
    print("Train set:", X_train.shape, y_train.shape)
    ratio_of_positive(y_train)
    print("Test set: ", X_test.shape, y_test.shape)
    ratio_of_positive(y_test)
    if with_valid:
        print("Valid set: ", X_valid.shape, y_valid.shape)

    ## normalization:
    if norm is not None:
        if norm == 'axis0':
            mean = X_train.mean(axis=0)
            std = X_train.std(axis=0)
            X_train = (X_train - mean) / (std + 1e-8)
            X_test = (X_test - mean) / (std + 1e-8)
            if with_valid:
                X_valid = (X_valid - mean) / (std + 1e-8)
        elif norm == 'axis1':
            def normalize(X):
                for i in range(X.shape[-1]):
                    X[:, :, i] = utils.z_normalize(X[:, :, i])
                return X
            X_train = normalize(X_train)
            X_test = normalize(X_test)
            if with_valid:
                X_valid = normalize(X_valid)
        else:
            raise ValueError('The normalization mode={} can not be found!'.format(norm))

    ## encode label
    if one_hot:
        y_train = utils.dense_to_one_hot(y_train, n_classes)
        y_test = utils.dense_to_one_hot(y_test, n_classes)
        if with_valid:
            y_valid = utils.dense_to_one_hot(y_valid, n_classes)

    ## return
    if with_valid is False:
        return X_train, y_train, X_test, y_test
    else:
        return X_train, y_train, X_valid, y_valid, X_test, y_test



if __name__ == '__main__':
    extract_additional_attributes(DIR_DATA)










