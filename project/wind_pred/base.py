# the following setting is effective for sklearn
from numpy.random import seed
seed(86)
from tensorflow import set_random_seed
set_random_seed(86)
import random as rn
rn.seed(86)

import sklearn
import pandas as pd
import tensorflow as tf

class Evaluation(object):
    def __init__(self):
        self.res = {
            'precision': [], 'recall': [], 'f1-score': [], 'support': [],
            'accuracy': [],
            'macro-avg-precision': [], 'macro-avg-recall': [], 'macro-avg-f1-score': [],
            'macro-avg-support': []}
        self.count = 0
        self.keys = []
    def evaluate(self, y_pred, y_valid):
        metrics = sklearn.metrics.classification_report(y_pred, y_valid, output_dict=True)
        return metrics

    def append(self, y_pred, y_true, key=None):
        metrics = self.evaluate(y_pred, y_true)
        for m in ['precision', 'recall', 'f1-score', 'support']:
            self.res[m].append(metrics['1'][m])
            self.res['macro-avg-{}'.format(m)].append(metrics['macro avg'][m])
        self.res['accuracy'].append(metrics['accuracy'])
        if key is not None:
            self.keys.append(key)
        else:
            self.keys.append(self.count)
        self.count += 1
        return metrics

    def get_df(self):
        return pd.DataFrame(self.res, index=self.keys)

    def get_df_with_agg(self):
        df = pd.DataFrame(self.res)
        keys = self.keys.copy()
        count = self.count
        mean = df.mean().to_dict()
        for key, item in mean.items():
            df.loc[count, key] = item
        count += 1
        keys.append('mean')
        std = df.std().to_dict()
        for key, item in std.items():
            df.loc[count, key] = item
        count += 1
        keys.append('std')
        df.index = keys
        return df, (mean, std)


"""
    Copy from: https://github.com/fudannlp16/focal-loss/blob/master/focal_loss.py
"""
def focal_loss_sigmoid(labels,logits,alpha=0.25,gamma=2):
    """
    Computer focal loss for binary classification
    Args:
      labels: A int32 tensor of shape [batch_size].
      logits: A float32 tensor of shape [batch_size].
      alpha: A scalar for focal loss alpha hyper-parameter. If positive samples number
      > negtive samples number, alpha < 0.5 and vice versa.
      gamma: A scalar for focal loss gamma hyper-parameter.
    Returns:
      A tensor of the same shape as `lables`
    """
    y_pred=tf.nn.sigmoid(logits)
    labels=tf.to_float(labels)
    L=-labels*(1-alpha)*((1-y_pred)*gamma)*tf.log(y_pred)-\
      (1-labels)*alpha*(y_pred**gamma)*tf.log(1-y_pred)
    return L
