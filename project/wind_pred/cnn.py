import numpy as np
import pandas as pd
import sklearn
import keras
from datetime import datetime

from mlpy.tsc.nn import ResNet
from project.wind_pred.load import load, DIR_DATA_AUG

class ResNetWind(ResNet):
    def __init__(self, input_shape, n_classes, verbose=1):
        super(ResNetWind, self).__init__(input_shape, n_classes, verbose)

    # def fit(self, x,
    #         y,
    #         batch_size=None,
    #         n_epochs=None,
    #         validation_data=None,
    #         shuffle=True,
    #         **kwargs):
    #     # set parameters
    #     if batch_size is None:
    #         batch_size = self.batch_size
    #     batch_size = min(x.shape[0] // 10, batch_size)
    #     if n_epochs is None:
    #         n_epochs = self.n_epochs
    #
    #     # start to train
    #     optimizer = keras.optimizers.Adam()
    #     self.model.compile(
    #         loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy'])
    #     reduce_lr = keras.callbacks.ReduceLROnPlateau(
    #         monitor='loss', factor=0.5, patience=50, min_lr=0.0001)
    #
    #     if validation_data is None:
    #         hist = self.model.fit(
    #             x, y, batch_size=batch_size, epochs=n_epochs,
    #             verbose=self.verbose, callbacks=[reduce_lr])
    #     else:
    #         hist = self.model.fit(
    #             x, y, batch_size=batch_size, epochs=n_epochs,
    #             verbose=1, validation_data=validation_data, callbacks=[reduce_lr])
    #
    #     return pd.DataFrame(hist.history)

    def fit(self, x,
            y,
            batch_size=None,
            n_epochs=None,
            validation_data=None,
            shuffle=True,
            **kwargs):
        # set parameters
        if batch_size is None:
            batch_size = self.batch_size
        batch_size = min(x.shape[0] // 10, batch_size)
        if n_epochs is None:
            n_epochs = self.n_epochs

        # start to train
        optimizer = keras.optimizers.Adam()
        self.model.compile(
            loss='categorical_crossentropy', optimizer=optimizer, metrics=['accuracy'])
        reduce_lr = keras.callbacks.ReduceLROnPlateau(
            monitor='loss', factor=0.5, patience=1, min_lr=0.0001, verbose=1)

        if validation_data is None:
            hist = self.model.fit(
                x, y, batch_size=batch_size, epochs=n_epochs,
                verbose=self.verbose, callbacks=[reduce_lr])
        else:
            es = keras.callbacks.EarlyStopping(patience=2, mode='min', verbose=1)
            mc = keras.callbacks.ModelCheckpoint('cache/best_model.h5', mode='min',
                                                 save_best_only=True, verbose=1)

            hist = self.model.fit(
                x, y, batch_size=batch_size, epochs=n_epochs,
                verbose=1, validation_data=validation_data, callbacks=[reduce_lr, es, mc])

        return pd.DataFrame(hist.history)


def run(window=48, feature_columns=['wind'], batch_size=48, n_epochs=10, n_classes=2, n_runs=5,
        verbose=1):
    ## load data
    X_train, y_train, X_valid, y_valid, X_test, y_test = load(
        DIR_DATA_AUG, with_valid=True, window=window, feature_columns=feature_columns)

    ## train model
    res = {'precision': [], 'recall': [], 'f1-score': [], 'support': [], 'accuracy': [],
           'macro-avg-precision': [], 'macro-avg-recall': [], 'macro-avg-f1-score': [],
           'macro-avg-support': []}
    for _ in range(n_runs):
        # train model
        model = ResNetWind(X_train.shape[1:], n_classes, verbose=verbose)
        df_hist = model.fit(X_train, y_train,
                            n_epochs=n_epochs, batch_size=batch_size, validation_data=(X_valid, y_valid))
        # predict
        model.load('cache/best_model.h5') # load best model
        y_pred = model.predict(X_test, batch_size)
        # evaluate
        metrics = sklearn.metrics.classification_report(
            y_pred, np.argmax(y_test, axis=1), output_dict=True)
        for key in ['precision', 'recall', 'f1-score', 'support']:
            res[key].append(metrics['1'][key])
            res['macro-avg-{}'.format(key)].append(metrics['macro avg'][key])
        res['accuracy'].append(metrics['accuracy'])

    ## aggregate results
    df = pd.DataFrame(res)
    mean = df.mean().to_dict()
    add_id = df.shape[0]
    for key, item in mean.items():
        df.loc[add_id, key] = item
    std = df.std().to_dict()
    add_id = df.shape[0]
    for key, item in std.items():
        df.loc[add_id, key] = item
    df.to_csv('cache/{}.csv'.format(datetime.now().strftime("%I%M%p-%B%d%Y")), index=False)

    return mean, std

def run_search():
    param_grid = {'window': [24, 24 * 2, 24 * 3, 24 * 4],
                  'feature_columns':
                      [['wind'], ['wind', 'nwp_wind'], ['wind', 'hour'],
                       ['wind', 'nwp_wind', 'hour']],
                  'batch_size': [24, 34, 48, 64, 72, 24 * 4, 24 * 5]}
    param_list = list(sklearn.model_selection.ParameterGrid(param_grid))
    n_params = len(param_list)
    res = {'param': [], 'f1-score': [], 'f1-score-std': [],
           'macro-avg-f1-score': [], 'macro-avg-f1-score-std': []}
    for i, param in enumerate(param_list):
        param_str = '{}-{}-{}'.format(param['window'], param['feature_columns'],
                                      param['batch_size'])
        print("[{}/{}] processing {}".format(i, n_params, param_str))
        mean, std = run(verbose=0, **param)
        # mean, std = run(param['window'], param['feature_columns'], param['batch_size'])
        res['param'].append(param_str)
        res['f1-score'].append(mean['f1-score'])
        res['f1-score-std'].append(std['f1-score'])
        res['macro-avg-f1-score'].append(mean['macro-avg-f1-score'])
        res['macro-avg-f1-score-std'].append(std['macro-avg-f1-score'])
    df = pd.DataFrame(res)
    df.to_csv('cache/reduce.csv', index=False)

if __name__ == '__main__':
    param = {'window': 24*2, 'feature_columns': ['wind', 'nwp_wind', 'hour'], 'batch_size':256,
             'n_runs': 1}
    run(**param)


