import base
import matplotlib.pyplot as plt


def vis_obs(dir_root, file):
    data, station, time_start, time_end = base.load_obs(dir_root, file, with_info=True)
    tag = 'obs_{}_{}_{}'.format(station, time_start, time_end)
    plt.plot(data)
    plt.title(tag)
    plt.savefig('cache/{}.png'.format(tag))
    plt.close()


def vis_nwp(dir_root, file):
    data, time_start, time_end = base.load_nwp(dir_root, file, with_info=True)
    n = data.shape[1]
    tag = 'nwp_{}_{}'.format(time_start, time_end)
    f, axes = plt.subplots(n, 1)
    axes = axes.flat[:]
    for i, ax in enumerate(axes):
        ax.plot(data[:, i])
        ax.set_ylabel('v{}'.format(i+1))
    plt.savefig('cache/{}.png'.format(tag))
    plt.close()


if __name__ == '__main__':
    for file in base.FILES_OBS:
        vis_obs(base.DIR_ROOT, file)

    for file in base.FILES_NWP1:
        vis_nwp(base.DIR_ROOT, file)
