import base
import pandas as pd

if __name__ == '__main__':
    df = pd.read_csv(base.DIR_DF)
    df_des = df.describe()
    df_des.to_csv('cache/df_describe.csv', index=False)
    df_corr = df.corr()
    df_corr.to_csv('cache/df_corr.csv', index=False)
