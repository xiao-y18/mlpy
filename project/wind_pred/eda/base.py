import numpy as np
import os
from sklearn.metrics import mean_squared_error
import pandas as pd

DIR_DF = '/home/ec2-user/hfl/data.csv'
DIR_ROOT = '/home/ec2-user/DATA'
FILES_NWP1 = 'NWP1_fcst_2018030112_2018103112.txt'
FILES_OBS = ['obs_01_2018030112_2018103112.txt', 'obs_18L_2018030112_2018103112.txt',
             'obs_18R_2018030112_2018103112.txt', 'obs_19_2018030112_2018103112.txt',
             'obs_MID2_2018030112_2018103112.txt', 'obs_01_2018030112_2018103112.txt',
             'obs_36L_2018030112_2018103112.txt', 'obs_MID3_2018030112_2018103112.txt',
             'obs_18L_2018030112_2018103112.txt', 'obs_36R_2018030112_2018103112.txt']


def load_nwp(dir_root, file, with_info=False):
    path = os.path.join(dir_root, file)
    data = np.loadtxt(path, delimiter=' ', dtype='float')
    if with_info == False:
        return data
    else:
        tags = file.split('.')[0].split('_')
        time_start = tags[-2]
        time_end = tags[-1]
        return data, time_start, time_end

def load_obs(dir_root, file, with_info=False):
    path = os.path.join(dir_root, file)
    data = np.loadtxt(path, delimiter=' ', dtype='float')
    if with_info == False:
        return data
    else:
        tags = file.split('.')[0].split('_')
        station = tags[1]
        time_start = tags[-2]
        time_end = tags[-1]
        return data, station, time_start, time_end


def load2df(dir_root, f_nwp, f_obs_list):
    data_nwp = load_nwp(dir_root, f_nwp)
    df = pd.DataFrame(data_nwp, columns=['nwp_v{}'.format(i) for i in range(data_nwp.shape[1])])
    for f in f_obs_list:
        data_obs, station, _, _ = load_obs(dir_root, f, with_info=True)
        df['obs_{}'.format(station)] = data_obs[:]

    return df

### metrics

def rmse(y, y_pred):
    return np.sqrt(mean_squared_error(y, y_pred))


def r_score(y, y_pred_ec, y_pred_ai):
    e_ec = rmse(y, y_pred_ec)
    e_ai = rmse(y, y_pred_ai)
    return ((e_ec - e_ai) / e_ec) * 100