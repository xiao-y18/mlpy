import os

import numpy as np
from mlpy.ijcai19.exp.constant import Constant
from preprocess import load_voltage

from tsa.sr.deap_learn import learn_deap


def test_deap_delay(data, var_names, n_var_learn, model_ids):
    #seq_timedelay = np.arange(16)
    seq_timedelay = [0, 3000, 3100, 3200, 3300, 3400, 3500, 3600, 3700]
    results = learn_deap.learn(data, var_names, n_var_learn, seq_timedelay)
    learn_deap.result_summary(results, var_names[0:n_var_learn])
    learn_deap.compare_plot(data, var_names, n_var_learn, results, model_ids, seq_timedelay, lg='chinese')

if __name__ == '__main__':
    file = 'LHWMJ84D0H1170014_Qiong_A57528_20180321214435842.csv'
    dir_data_file = os.path.join(Constant.dir_data_haikou_25_30s, file)
    voltage_df = load_voltage(dir_data_file)

    var_names = np.array(['BoundUpper', 'BoundLower', 'Unit1', 'Unit2'])
    n_train = voltage_df.shape[0]
    data = voltage_df.loc[:n_train, var_names].as_matrix()
    print(data.shape)

    n_var_learn = 2  # The first 2 variable as target

    model_ids = [2, 2]
    test_deap_delay(data, var_names, n_var_learn, model_ids)


