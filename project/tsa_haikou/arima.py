import os

import matplotlib.pyplot as plt
import preprocess
from tsa.stat import arima

from mlpy.ijcai19.exp.constant import Constant

if __name__ == '__main__':
    file = 'LHWMJ84D0H1170014_Qiong_A57528_20180321214435842.csv'
    dir_data_file = os.path.join(Constant.dir_data_haikou_25_30s, file)
    voltage_df = preprocess.load_voltage(dir_data_file)
    n_train = 3000
    y = voltage_df['BoundUpper'][:n_train]
    # print('is stable = {}'.format(is_stable(y)))
    # print('is white noise = {}'.format(is_white_noise(y, 1)))

    p_upper = 10
    d_upper = 10
    q_upper = 10
    p, d, q = 10, 0, 7
    # p, d, q = arima.arima_fit(y, p_upper, d_upper, q_upper)
    if p is None or d is None or q is None:
        exit(-1)
    model = arima.ARIMA(y, (p, d, q)).fit(disp=-1)
    print(model.summary())
    fig, ax = plt.subplots(figsize=(12, 8))
    ax = y[2800:].plot(ax=ax)
    fig = model.plot_predict(2800, 3010, dynamic=True, ax=ax, plot_insample=False)
    plt.show()

    fig, ax = plt.subplots(figsize=(12, 8))
    ax = y[2800:].plot(ax=ax)
    fig = model.plot_predict(2800, 3010, dynamic=False, ax=ax, plot_insample=False)
    plt.show()