Update time = 2018.09.08

### 当前解决方案：

- 数据源：
    - 电动公交车的传感器数据
- 目的：
    - 电池故障监测
- 关于电池一些故障的判断条件：
    - 电压不均衡阀值，＞0.2V持续10秒报警，＞0.5V持续2秒严重报警；
    - 单体过压故障阀值，＞4.2V持续5秒报警，＞4.25V持续1秒严重报警；
    - 单体欠压故障阀值，＜2.8V持续5秒报警，＜2.5V持续2秒严重报警。
    - 如果上述条件满足，电池即会反馈相应的故障，也可以根据上述故障触发条件来进行故障数据的相应筛选.
 - 其他：
    - 按照电力汽车工程师的说法，车辆电池的单体电压变化和工况、使用电流、SOC有很大关系

### 我的分析

- 关键变量（小勇告知）：`单体1电压`:`Unit1`，`单体2电压`:`Unit2`, `单体最高电压`:`BoundUpper`, `单体最低电压`:`BoundLower`, `数据时间`:`Time`
- 目标：预测电池电压的变化 （无历史故障数据，及时以后可以采集也很少，因此采用 forecast-based 的方法实现故障诊断）

#### 异常数据占比

以`BoundUpper` 和 `BoundLower` 为电压浮值的上、下界，并统计数据集中正常数据点的占比。结果如下：

> LHWMJ84D3H1170007_Qiong_A57522_20180321193917992.csv size=137690 unit1-normal-ratio=0.9259060207712978 unit2-normal-ratio=0.9258624446219769
LHWMJ84D7H1170012_Qiong_A57510_20180321210852163.csv size=147050 unit1-normal-ratio=0.9139068344100646 unit2-normal-ratio=0.9145868752125127
LHWMJ84D2H1170015_Qiong_A57363_20180321215258798.csv size=71685 unit1-normal-ratio=0.9292041570760968 unit2-normal-ratio=0.9286182604450024
LHWMJ84D3H1170024_Qiong_A56927_20180321234058604.csv size=99914 unit1-normal-ratio=0.94471245270933 unit2-normal-ratio=0.9447324699241347
LHWMJ84D8H1170021_Qiong_A57388_20180321231054767.csv size=21043 unit1-normal-ratio=0.9350377797842513 unit2-normal-ratio=0.9365584755025425
LHWMJ84D6H1170017_Qiong_A57267_20180321222019867.csv size=126204 unit1-normal-ratio=0.9467211815790307 unit2-normal-ratio=0.9468321130867484
LHWMJ84D5H1170008_Qiong_A57513_20180321195756950.csv size=133097 unit1-normal-ratio=0.9434773135382465 unit2-normal-ratio=0.9441084321960675
LHWMJ84D5H1170011_Qiong_A57519_20180321205015179.csv size=138181 unit1-normal-ratio=0.9366483091018302 unit2-normal-ratio=0.9378713426592657
LHWMJ84D0H1170014_Qiong_A57528_20180321214435842.csv size=48661 unit1-normal-ratio=0.9093113581718419 unit2-normal-ratio=0.9097223649329031
LHWMJ84D6H1170020_Qiong_A57393_20180321230334769.csv size=38779 unit1-normal-ratio=0.8962840712756904 unit2-normal-ratio=0.8978570875989582
LHWMJ84D3H1170010_Qiong_A57290_20180321203322809.csv size=122802 unit1-normal-ratio=0.9117603947818439 unit2-normal-ratio=0.913796192244426
LHWMJ84D8H1170018_Qiong_A57398_20180321223731823.csv size=46817 unit1-normal-ratio=0.9226563000619433 unit2-normal-ratio=0.9223145438622723
LHWMJ84D4H1170002_Qiong_A57389_20180321180355002.csv size=126734 unit1-normal-ratio=0.9317152461060173 unit2-normal-ratio=0.9309735351208042
LHWMJ84D6H1170003_Qiong_A57399_20180321182156323.csv size=46546 unit1-normal-ratio=0.9065440639367507 unit2-normal-ratio=0.9068233575387789
LHWMJ84D7H1170009_Qiong_A57385_20180321201631415.csv size=121713 unit1-normal-ratio=0.937631970290766 unit2-normal-ratio=0.9368843098107844
LHWMJ84D1H1170006_Qiong_A56521_20180321192137584.csv size=127202 unit1-normal-ratio=0.925040486784799 unit2-normal-ratio=0.9250169022499646
LHWMJ84D1H1170023_Qiong_A57500_20180321233238472.csv size=46257 unit1-normal-ratio=0.8817043906868149 unit2-normal-ratio=0.8825907430226777
LHWMJ84D2H1170001_Qiong_A57503_20180321174640771.csv size=120169 unit1-normal-ratio=0.9302066256688497 unit2-normal-ratio=0.9309222844494004
LHWMJ84D8H1170004_Qiong_A57221_20180321183022760.csv size=4932 unit1-normal-ratio=0.8923357664233577 unit2-normal-ratio=0.8917274939172749
LHWMJ84D5H1170025_Qiong_A57051_20180321235511373.csv size=123084 unit1-normal-ratio=0.9318676676091124 unit2-normal-ratio=0.9326719963602093

可以看到正常样本点在 88% 以上，仔细观察，异常点也仅仅是超出微小范围，大多只超出 0.1，远没有达到认为设定的规则。可以认为所有数据均正常，没有 label 数据进行分类任务。

### 最后的报告

见根目录下的文档