"""
    Extracting time series features via tsfresh library.
    Feature extraction settings:https://tsfresh.readthedocs.io/en/latest/text/feature_extraction_settings.html
        - ComprehensiveFCParameters in tsfresh.feature_extraction list all included features.
    It should be noted that the feature selection procedure of tsfresh needs a supervised target as input because it
        rely on a regression or classification task to pick up relevant features. However, such a supervised can not
        be found in our scenario. Therefore, this script just encodes a time series into a vector that includes all
        features included in tsfresh and thus the resultant is a high-dimensional vector.
"""
import os
import pandas as pd
import logging

import tsfresh
from tsfresh.feature_extraction import feature_calculators
from inspect import getfullargspec

from mlpy.lib.utils import tag_path, makedirs

from project.tangche.lib.data import load_tunnel_data, load_pressure_data, down_sample_mean
from project.tangche.configure import DIR_CACHE, DIR_OPERATIONS

tag = tag_path(os.path.abspath(__file__), 2)
dir_log = os.path.join(DIR_CACHE, tag)
makedirs(dir_log)
logging.basicConfig(filename=os.path.join(dir_log, 'log.log'), filemode='w', level=logging.INFO)
log = logging.getLogger(tag)


class DefaultFeatures(dict):
    # a subset of features manually picked up from tsfresh.feature_extraction.ComprehensiveFCParameters

    def __init__(self):
        name_to_param = {}

        for name, func in feature_calculators.__dict__.items():
            if callable(func) and hasattr(func, "fctype") and len(getfullargspec(func).args) == 1:
                name_to_param[name] = None

        names_to_delete = [
            'variance_larger_than_standard_deviation',  # the values of this feature are always equal to 1.
            # i think following feature are meaningless for the time series with variant lengths.
            'sum_values', 'abs_energy', 'length', 'last_location_of_maximum', 'absolute_sum_of_changes',
            'first_location_of_maximum', 'last_location_of_minimum', 'first_location_of_minimum',
            'longest_strike_below_mean', 'longest_strike_above_mean', 'count_above_mean', 'count_below_mean',
            # I think following features are meaningless for continuous values.
            'has_duplicate_max', 'has_duplicate_min', 'has_duplicate',
            'percentage_of_reoccurring_values_to_all_values', 'percentage_of_reoccurring_datapoints_to_all_datapoints',
            'sum_of_reoccurring_values', 'sum_of_reoccurring_data_points', 'ratio_value_number_to_time_series_length']
        for name in names_to_delete:
            del name_to_param[name]

        name_to_param.update({
            "time_reversal_asymmetry_statistic": [{"lag": lag} for lag in [1]],
            "c3": [{"lag": lag} for lag in [1]],
            "cid_ce": [{"normalize": True}],
            #
            # I think boolean variable is not informative.
            # "symmetry_looking": [{"r": r * 0.05} for r in [1]],   # referring to skewness or kurtosis is better
            # "large_standard_deviation": [{"r": r * 0.05} for r in [1]], # referring to standard_deviation is better
            "quantile": [{"q": q} for q in [.3, .5, .9]],
            "agg_autocorrelation": [{"f_agg": s, "maxlag": 40} for s in ["mean", "median", "var"]],
            "partial_autocorrelation": [{"lag": lag} for lag in [1]],
            #
            # I think the following count numbers are meaningless for the time series with variant length.
            # In addition the statistics of skewness can reflect the properties of these features in some extent.
            # "number_cwt_peaks": [{"n": n} for n in [1, 5]],
            # "number_peaks": [{"n": n} for n in [1, 3, 5, 10, 50]],
            "binned_entropy": [{"max_bins": max_bins} for max_bins in [10]],
            #
            # meaningless for variant time series
            # "index_mass_quantile": [{"q": q} for q in [3, .5, .9]],
            "cwt_coefficients": [{"widths": width, "coeff": coeff, "w": w} for
                                 width in [(2, 5, 10, 20)] for coeff in range(4) for w in (2, 5, 10, 20)],
            "spkt_welch_density": [{"coeff": coeff} for coeff in [2, 5, 8]],
            "ar_coefficient": [{"coeff": coeff, "k": k} for coeff in range(1) for k in [10]],
            #
            # less meaning for variant time series.
            # "change_quantiles": [{"ql": ql, "qh": qh, "isabs": b, "f_agg": f}
            #                      for ql in [0., .2, .4, .6, .8] for qh in [.2, .4, .6, .8, 1.]
            #                      for b in [False, True] for f in ["mean", "var"] if ql < qh],
            "fft_aggregated": [{"aggtype": s} for s in ["centroid", "variance", "skew", "kurtosis"]],
            #
            # I think the following two features need domain knowledge to define the values and range
            # "value_count": [{"value": value} for value in [0, 1, -1]],
            # "range_count": [{"min": -1, "max": 1}, {"min": 1e12, "max": 0}, {"min": 0, "max": 1e12}],
            #
            # Copy from the annotation: For short time-series this method is highly dependent on the parameters.
            # We have no a automated feature selection process, therefore I remove these unstable features.
            # "approximate_entropy": [{"m": 2, "r": r} for r in [.1, .3, .5, .7, .9]],
            # "friedrich_coefficients": (lambda m: [{"coeff": coeff, "m": m, "r": 30} for coeff in range(m + 1)])(3),
            # "max_langevin_fixed_point": [{"m": 3, "r": 30}],
            #
            # Copy from the annotation: This feature assumes the signal to be uniformly sampled.
            #   It will not use the time stamps to fit the model.
            # Therefore, I remove the following feature as I can not confirm where the signal is uniformly sampled.
            # "agg_linear_trend": [{"attr": attr, "chunk_len": i, "f_agg": f}
            #                      for attr in ["rvalue", "intercept", "slope", "stderr"]
            #                      for i in [5, 10, 50]
            #                      for f in ["max", "min", "mean", "var"]],
            "augmented_dickey_fuller": [{"attr": "teststat"}, {"attr": "pvalue"}, {"attr": "usedlag"}],
            #
            # Based on the definition, I think the following two features are meaningless in our scenario.
            # "number_crossing_m": [{"m": 0}, {"m": -1}, {"m": 1}],
            # "energy_ratio_by_chunks": [{"num_segments": 10, "segment_focus": i} for i in range(10)],
            "ratio_beyond_r_sigma": [{"r": x} for x in [0.5, 1]],
            "linear_trend_timewise": [{"attr": "pvalue"}, {"attr": "rvalue"}, {"attr": "intercept"},
                                      {"attr": "slope"}, {"attr": "stderr"}],
            #
            # I think following two features are meaningless for continuous values.
            # "count_above": [{"t": 0}],
            # "count_below": [{"t": 0}],
            "lempel_ziv_complexity": [{"bins": x} for x in [10]],
            "fourier_entropy":  [{"bins": x} for x in [10]],
            #
            # I think the following feature is meaningless for continuous values.
            # "permutation_entropy":  [{"tau": 1, "dimension": x} for x in [5]],

        })

        super().__init__(name_to_param)

        self.name_to_param = name_to_param

    def get_name_to_param(self):
        return self.name_to_param


class FeatureExtractor(object):
    def __init__(self, features=None):
        if features is None:
            self.settings = DefaultFeatures()
        else:
            self.settings = features
        self.extractor = lambda df: tsfresh.extract_features(df, default_fc_parameters=self.settings,
                                                             column_id='id', column_sort="time")
        log.info("Feature Extraction")
        log.info("The number of included features is {}".format(len(self.settings.get_name_to_param())))
        log.info("The features are as follows: ")
        features = [str(feat) for feat in self.settings.get_name_to_param()]
        log.info(features)
        log.info("\n")

    def extract_df(self, df):
        df['id'] = 1
        df_features = self.extractor(df)
        return df_features

    def extract_ts(self, ts):
        ts = pd.DataFrame({'value': ts, 'time': range(len(ts))})
        df_features = self.extract_df(ts)
        dic_rename_columns = {}
        for col in df_features.columns:
            dic_rename_columns[col] = '__'.join(col.split('__')[1:])
        df_features = df_features.rename(columns=dic_rename_columns)

        feature_names = df_features.columns
        feature_values = df_features.values

        return feature_names, feature_values


def get_mean_signals_by_types(df_pressure):
    columns = df_pressure.columns
    columns_inner = [col for col in columns if 'inner' in col]
    columns_external = [col for col in columns if 'external' in col]
    df_inner = df_pressure[columns_inner]
    df_external = df_pressure[columns_external]
    mean_inner = df_inner.mean(axis=1)
    mean_external = df_external.mean(axis=1)
    df_mean = pd.DataFrame({
        'time': df_pressure['time'].values,
        'inner': mean_inner,
        'external': mean_external
    })
    return df_mean


if __name__ == '__main__':

    dir_operation = DIR_OPERATIONS[0]
    df_tunnel = load_tunnel_data(dir_operation)

    # extract features over all tunnels
    df_res = []
    extractor = FeatureExtractor()
    for i, tunnel in enumerate(df_tunnel['tunnel'].values):
        df_pressure = load_pressure_data(dir_operation, tunnel)
        df_pressure = down_sample_mean(df_pressure, downsampling_ratio=100)  # 100 * 0.01s = 1s.
        df_pressure_mean = get_mean_signals_by_types(df_pressure)
        df_features = extractor.extract_df(df_pressure_mean)
        if i == 0:
            df_res = df_features
        else:
            df_res = df_res.append(df_features, ignore_index=True)
    df_res['tunnel'] = df_tunnel['tunnel']
    df_res.to_csv(os.path.join(dir_log, 'feature_{}.csv'.format(tag_path(dir_operation, 3))))



