"""
    There are two types/groups of pressure series, denoted as 'inner' and 'external'.
"""
import logging
import os
import numpy as np

from mlpy.lib.utils import tag_path, makedirs

from project.tangche.lib.data import load_tunnel_data, load_pressure_data, down_sample_mean
from project.tangche.configure import DIR_CACHE, DIR_OPERATIONS

tag = tag_path(os.path.abspath(__file__), 2)
dir_log = os.path.join(DIR_CACHE, tag)
makedirs(dir_log)
logging.basicConfig(filename=os.path.join(dir_log, 'log.log'), filemode='w', level=logging.INFO)
log = logging.getLogger(tag)


def cal_corr_indicator_for_a_tunnel(df_pressure):
    columns = df_pressure.columns
    columns_inner = [col for col in columns if 'inner' in col]
    columns_external = [col for col in columns if 'external' in col]
    df_inner = df_pressure[columns_inner]
    df_external = df_pressure[columns_external]

    def cal_mean_corr(corr):
        assert corr.shape[0] == corr.shape[1]
        sum = 0
        count = 0
        for i in range(corr.shape[0]):
            for j in range(i+1, corr.shape[1]):
                sum += corr[i][j]
                count += 1
        return sum / count
    df_inner_corr = df_inner.corr()
    df_external_corr = df_external.corr()
    mean_corr_inner = cal_mean_corr(df_inner_corr.values)
    mean_corr_external = cal_mean_corr(df_external_corr.values)
    corr = {'inner': mean_corr_inner, 'external': mean_corr_external}

    return corr


def cal_corr_indicator_for_an_operation(dir_an_operation, down_sample_freq=1):
    log.info("cal_corr_indicator_for_an_operation: down_sample_freq={}".format(down_sample_freq))
    df_tunnel = load_tunnel_data(dir_an_operation)
    corr_inner_list = []
    corr_external_list = []
    for tunnel_id in range(df_tunnel.shape[0]):
        tunnel_name = df_tunnel['tunnel'][tunnel_id]
        df_pressure = load_pressure_data(dir_an_operation, tunnel_name)
        df_pressure = down_sample_mean(df_pressure, downsampling_ratio=down_sample_freq)
        corr = cal_corr_indicator_for_a_tunnel(df_pressure)
        corr_inner_list.append(corr['inner'])
        corr_external_list.append(corr['external'])
    df_tunnel_corr = df_tunnel.copy()
    df_tunnel_corr['corr_inner'] = np.array(corr_inner_list)
    df_tunnel_corr['corr_external'] = np.array(corr_external_list)
    df_tunnel_corr.to_csv(os.path.join(dir_log, '{}_tunnel_corr.csv'.format(tag_path(dir_an_operation, 3))))
    log.info("The end!")
    log.info("\n")
    return df_tunnel_corr


def anomaly_detection(df_corr_list, column, kstd=1):
    """a df in df_corr_list corresponds to an operation"""
    log.info("Anomaly detection: indicator/column={}, kstd={}.".format(column, kstd))
    corrs = []
    for df in df_corr_list:
        corrs.extend(df[column].values)
    mean = np.mean(corrs)
    std = np.std(corrs)

    log.info("Some statistics")
    log.info("There are {} operations and {} records.".format(len(df_corr_list), len(corrs)))
    log.info("mean={}, std={}.".format(mean, std))
    log.info("The results of AD are as follows: ")
    for i, df in enumerate(df_corr_list):
        for tunnel, corr in zip(df['tunnel'].values, df[column].values):
            if corr < (mean-std*kstd):  # one side detection because a higher correlation indicate a more healthy state.
                log.info("anomaly occurs in operation=No.{}, tunnel={}, corr={}".format(i, tunnel, corr))
    log.info("The end!")
    log.info("\n")


def run(dir_operations):
    df_indicator_list = []
    for dir_root in dir_operations:
        df_corr = cal_corr_indicator_for_an_operation(dir_root)
        df_indicator_list.append(df_corr)

    anomaly_detection(df_indicator_list, 'corr_inner')
    anomaly_detection(df_indicator_list, 'corr_external')


if __name__ == '__main__':
    run(DIR_OPERATIONS)





