"""
    air impermeability calculation
"""

import logging
import os
import numpy as np
from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.linear_model import LinearRegression

from mlpy.lib.utils import tag_path, makedirs

from project.tangche.lib.data import load_tunnel_data, load_pressure_data, down_sample_mean
from project.tangche.configure import DIR_CACHE, DIR_OPERATIONS

tag = tag_path(os.path.abspath(__file__), 2)
dir_log = os.path.join(DIR_CACHE, tag)
makedirs(dir_log)
logging.basicConfig(filename=os.path.join(dir_log, 'log.log'), filemode='w', level=logging.INFO)
log = logging.getLogger(tag)


if __name__ == '__main__':
    # test_pressure_integrand_run()
    dir_operation = DIR_OPERATIONS[0]
    df_tunnel = load_tunnel_data(dir_operation)
    tunnel = df_tunnel['tunnel'].values[1]
    df_pressure = load_pressure_data(dir_operation, tunnel)
    log.info("EXP: {}/{}".format(dir_operation, tunnel))

    cols = ['c1_inner_mid', 'c1_inner_end', 'c1_external_side_b1', 'c1_external_side_b2']
    for col in cols:
        plt.plot(df_pressure[col])
        plt.savefig(os.path.join(dir_log, '{}.png'.format(col)))
        plt.clf()

    time = df_pressure['time'].values
    p_i = df_pressure[['c1_inner_mid', 'c1_inner_end']].mean(axis=1).values
    p_e = df_pressure[['c1_external_side_b1', 'c1_external_side_b2']].mean(axis=1).values
    plt.plot(p_i)
    plt.savefig(os.path.join(dir_log, 'inner.png'))
    plt.clf()
    plt.plot(p_e)
    plt.savefig(os.path.join(dir_log, 'external.png'))
    plt.clf()

    for deg in range(1, 10):
        p_i_func = np.polyfit(time, p_i, deg=deg)
        p_i_func = np.poly1d(p_i_func)
        p_i_trend = p_i_func(time)
        p_i_residual = p_i - p_i_trend
        plt.plot(p_i_trend)
        plt.savefig(os.path.join(dir_log, '{}_pi_trend.png'.format(deg)))
        plt.clf()
        plt.plot(p_i_residual)
        plt.savefig(os.path.join(dir_log, '{}_pi_residual.png'.format(deg)))
        plt.clf()
        log.info('deg={}, corr={}'.format(deg, np.corrcoef(p_i_residual, p_e)[0, 1]))






