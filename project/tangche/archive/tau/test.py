from scipy.integrate import quad


def integrand(x, a, b):
    return a * x ** 2 + b


if __name__ == '__main__':
    a = 2
    b = 1
    y, err = quad(integrand, 0, 1, args=(a, b))
    print(y, err)