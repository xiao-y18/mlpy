"""
    air impermeability calculation
"""

import logging
import os
import numpy as np
from sklearn.metrics import mean_squared_error
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.linear_model import LinearRegression

from mlpy.lib.utils import tag_path, makedirs

from project.tangche.lib.data import load_tunnel_data, load_pressure_data, down_sample_mean
from project.tangche.configure import DIR_CACHE, DIR_OPERATIONS

tag = tag_path(os.path.abspath(__file__), 2)
dir_log = os.path.join(DIR_CACHE, tag)
makedirs(dir_log)
logging.basicConfig(filename=os.path.join(dir_log, 'log.log'), filemode='w', level=logging.INFO)
log = logging.getLogger(tag)


if __name__ == '__main__':
    # test_pressure_integrand_run()
    dir_operation = DIR_OPERATIONS[3]
    df_tunnel = load_tunnel_data(dir_operation)

    log.info("EXP: {}".format(dir_operation))
    for tunnel in df_tunnel['tunnel'].values:
        df_pressure = load_pressure_data(dir_operation, tunnel)

        time = df_pressure['time'].values
        p_i = df_pressure[['c1_inner_mid', 'c1_inner_end']].mean(axis=1).values
        p_e = df_pressure[['c1_external_side_b1', 'c1_external_side_b2']].mean(axis=1).values

        p_i_func = np.polyfit(time, p_i, deg=1)
        p_i_func = np.poly1d(p_i_func)
        p_i_trend = p_i_func(time)
        p_i_residual = p_i - p_i_trend
        log.info('tunnel={}, corr={}'.format(tunnel, np.corrcoef(p_i_residual, p_e)[0, 1]))







