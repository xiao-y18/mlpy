"""
    air impermeability calculation
"""

import logging
import os
import numpy as np
from sklearn.metrics import mean_absolute_error
import matplotlib.pyplot as plt
import pandas as pd

from mlpy.lib.utils import tag_path, makedirs

from project.tangche.lib.data import load_tunnel_data, load_pressure_data, down_sample_mean
from project.tangche.configure import DIR_CACHE, DIR_OPERATIONS

tag = tag_path(os.path.abspath(__file__), 2)
dir_log = os.path.join(DIR_CACHE, tag)
makedirs(dir_log)
logging.basicConfig(filename=os.path.join(dir_log, 'log.log'), filemode='w', level=logging.INFO)
log = logging.getLogger(tag)


from scipy.integrate import quad


def law_pressure_integrand(tau, p_e_t):
    I = lambda t: np.exp(-t/tau) * p_e_t  # the function I(t).
    return I


def test_pressure_integrand(tau):
    """
        let p_e_t = 1 and thus the analytic_solution is that I(t)=[-tau*exp{-t/tau}]_{0}{t}
    """
    time = np.arange(0, 100, 0.001)

    integrand = law_pressure_integrand(tau, 1)
    integrand_values = []
    for t in time:
        value, err = quad(integrand, 0, t)
        integrand_values.append(value)
    plt.plot(integrand_values)
    plt.savefig(os.path.join(dir_log, 'test_integrand.png'))
    plt.clf()

    def analytic_solution(tau, t):
        return -tau*np.exp(-t/tau)
    integrand_values_analytic_solution = []
    for t in time:
        value = analytic_solution(tau, t) - analytic_solution(tau, 0)
        integrand_values_analytic_solution.append(value)
    plt.plot(integrand_values_analytic_solution)
    plt.savefig(os.path.join(dir_log, 'test_integrand_truth.png'))

    error = mean_squared_error(integrand_values, integrand_values_analytic_solution)
    return error


def test_pressure_integrand_run():
    for tau in np.arange(0.1, 100.0, 0.1):
        error = test_pressure_integrand(tau)
        if error > 1e-8:
            print("The approximate calculation is wrong!")
            print("The error={:10f} is not equal to zero!".format(error))
    print("The approximate calculation pass the test.")


def run(tau, time, p_i, p_e):
    integrand_values = []
    for it, t in enumerate(time):
        p_e_t = p_e[it]
        integrand = law_pressure_integrand(tau, p_e_t)
        value, err = quad(integrand, 0, t)
        integrand_values.append(value)
    plt.plot(integrand_values)
    plt.savefig(os.path.join(dir_log, 'integrand_{:.2f}.png'.format(tau)))
    plt.clf()

    p_i_hat = np.zeros(len(p_i))
    for it, t in enumerate(time):
        p_i_hat[it] = 1.0/tau * np.exp(-t/tau)*integrand_values[it]
    plt.plot(p_i_hat)
    plt.savefig(os.path.join(dir_log, 'inner_{:.2f}.png'.format(tau)))
    plt.clf()

    err = mean_absolute_error(p_i, p_i_hat)

    return err


if __name__ == '__main__':
    # test_pressure_integrand_run()
    dir_operation = DIR_OPERATIONS[0]
    df_tunnel = load_tunnel_data(dir_operation)
    tunnel = df_tunnel['tunnel'].values[1]
    df_pressure = load_pressure_data(dir_operation, tunnel)
    log.info("EXP: {}/{}".format(dir_operation, tunnel))

    cols = ['c1_inner_mid', 'c1_inner_end', 'c1_external_side_b1', 'c1_external_side_b2']
    for col in cols:
        plt.plot(df_pressure[col])
        plt.savefig(os.path.join(dir_log, '{}.png'.format(col)))
        plt.clf()

    time = df_pressure['time'].values
    p_i = df_pressure[['c1_inner_mid', 'c1_inner_end']].mean(axis=1).values
    p_e = df_pressure[['c1_external_side_b1', 'c1_external_side_b2']].mean(axis=1).values
    plt.plot(p_i)
    plt.savefig(os.path.join(dir_log, 'inner.png'))
    plt.clf()
    plt.plot(p_e)
    plt.savefig(os.path.join(dir_log, 'external.png'))
    plt.clf()

    p_i_func = np.polyfit(time, p_i, deg=1)
    p_i_func = np.poly1d(p_i_func)
    p_i_trend = p_i_func(time)
    p_i_residual = p_i - p_i_trend
    p_i = p_i_residual

    errs = []
    taus = []
    # for tau in np.arange(0.1, 5.0, 0.1):
    # for tau in np.arange(5.0, 10.0, 0.1):
    # for tau in np.arange(10.0, 20.0, 0.1):
    # for tau in np.arange(20.0, 40.0, 0.1):
    # for tau in np.arange(40.0, 60.0, 0.1):
    for tau in np.arange(0.1, 200.0, 0.1):
        taus.append(tau)
        err = run(tau, time, p_i, p_e)
        errs.append(err)
    taus = np.array(taus)
    errs = np.array(errs)

    plt.plot(taus, errs)
    plt.savefig(os.path.join(dir_log, 'tau_err.png'))
    df_err = pd.DataFrame({'tau': taus, 'errs': errs})
    df_err.to_csv(os.path.join(dir_log, 'err.csv'))
    idx_min = np.argmin(errs)
    log.info("The minimum, tau={}, err={}".format(taus[idx_min], errs[idx_min]))




