import os
import logging
import sys

DIR_CACHE = '../cache'

DIR_DATA = '../../dataset/tangche'
DIR_DATA_202009 = os.path.join(DIR_DATA, '202009')
DIR_OPERATIONS = [os.path.join(DIR_DATA_202009, '修前', '广州到长沙'),
                  os.path.join(DIR_DATA_202009, '修前', '长沙到广州'),
                  os.path.join(DIR_DATA_202009, '修后', '广州到长沙'),
                  os.path.join(DIR_DATA_202009, '修后', '长沙到广州')]

SAMPLING_RATE = 1000  # Hz
PERIOD = 1.0 / SAMPLING_RATE

LOG_FORMAT = '%(asctime)s, %(levelname)s, %(name)s: %(message)s'
LOG_DATE_FORMAT = '%m/%d %I:%M:%S %p'

N_PROCESSES = max(os.cpu_count() - 2, 1)  # the maximum number of processes used for a multiprocessing pool.


def set_logging(tag, dir_log=None):
    logging.basicConfig(stream=sys.stdout, filemode='w', level=logging.INFO, format=LOG_FORMAT, datefmt=LOG_DATE_FORMAT)
    log = logging.getLogger(tag)
    if dir_log is not None:
        fh = logging.FileHandler(os.path.join(dir_log, 'log.txt'))
        fh.setFormatter(logging.Formatter(fmt=LOG_FORMAT, datefmt=LOG_DATE_FORMAT))
        log.addHandler(fh)
    return log

