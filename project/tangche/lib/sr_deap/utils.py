import sys
import os
import matplotlib.pyplot as plt

from .model import *


def result_summary(results, var_names, dir_out=None):
    """
    
    :param results: list, 
    :param var_names: list
    :return: 
    """

    assert len(results) == len(var_names), 'the length of result and variable should be equal.'

    # display result table
    if dir_out is not None:
        orig_stdout = sys.stdout
        f = open(os.path.join(dir_out, 'result.txt'), 'w')
        sys.stdout = f
    for result, name in zip(results, var_names):
        numBases, nrmses, rmses, models, _ = result
        nrmses_test, nrmses_train = nrmses
        rmses_test, rmses_train = rmses

        print("{0} {1} {0}".format("*"*20, name))
        print("original model:")
        for model in models:
            print(model)
        print("symplified model:")
        for model in models:
            print(simplify_this(model))

        print("{0}".format("-"*25))
        print("complexity\t\tnrmse_test\t\tnrmse_train\t\trmse_test\t\trmse_train")
        for com, nrmse_test, nrmse_train, rmse_test, rmse_train in \
                zip(numBases, nrmses_test, nrmses_train, rmses_test, rmses_train):
            print("{0}\t\t{1}\t\t{2}\t\t{3}\t\t{4}".format(com, nrmse_test, nrmse_train, rmse_test, rmse_train))
        print("{0}".format("*"*40))
    if dir_out is not None:
        sys.stdout = orig_stdout
        f.close()

    # Pareto front, nrmse on test set
    plt.figure()
    for result, name in zip(results, var_names):
        coms, nrmses, rmses, _, _ = result
        nrmses_test, _ = nrmses
        plt.plot(coms, '.-', nrmses_test, label=name)
    plt.title("NRMSE Pareto Front")
    plt.xlabel("Complexity")
    plt.ylabel("Error")
    plt.legend()
    plt.tight_layout()
    if dir_out:
        plt.savefig(os.path.join(dir_out, 'pareto_nrmse.png'))
    plt.close()

    # Pareto front, rmse on test set
    plt.figure()
    for result, name in zip(results, var_names):
        coms, nrmses, rmses, _, _ = result
        rmses_test, _ = rmses
        plt.plot(coms, '.-', rmses_test, label=name)
    plt.title("RMSE Pareto Front")
    plt.xlabel("Complexity")
    plt.ylabel("Error")
    plt.legend()
    plt.tight_layout()
    if dir_out:
        plt.savefig(os.path.join(dir_out, 'pareto_rmse.png'))
    plt.close()

