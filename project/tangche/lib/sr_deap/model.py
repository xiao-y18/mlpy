from deap import tools
from deap import creator
from deap import base
import random
import operator
from .base import *


def _evolve(toolbox, optimizer, seed, gen=0, mu=1, lambda_=1, cxpb=1, mutp=1):
    random.seed(seed)
    np.random.seed(seed)

    pop = remove_twins(toolbox.population(n=mu))
    pop = list(toolbox.map(optimizer, pop))

    invalid_ind = [ind for ind in pop if not ind.fitness.valid]
    fitness = list(toolbox.map(toolbox.evaluate, invalid_ind))
    for ind, fit in zip(invalid_ind, fitness):
        ind.fitness.values = fit

    pop = toolbox.select(pop, mu)

    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("min", np.nanmin, axis=0)
    stats.register("max", np.nanmax, axis=0)
    stats.register("diversity", lambda pop: len(set(map(str, pop))))

    logbook = tools.Logbook()
    logbook.header = "gen", 'evals', 'min', 'max', 'diversity'

    record = stats.compile(pop)
    logbook.record(gen=0, evals=(len(invalid_ind)), **record)
    print(logbook.stream)
    if record['min'][0] == 0.0:
        return pop, logbook

    for g in range(1, gen):
        offspring = tools.selRandom(pop, mu)
        offspring = [toolbox.clone(ind) for ind in offspring]

        # mate
        for child1, child2 in zip(offspring[::2], offspring[1::2]):
            if random.random() <= cxpb:
                toolbox.mate(child1, child2)
                del child1.fitness.values
                del child2.fitness.values
        # mutate
        for mutant in offspring:
            if random.random() <= mutp:
                toolbox.mutate(mutant)
                del mutant.fitness.values
        # re-evaluate
        invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
        fitness = list(toolbox.map(toolbox.evaluate, invalid_ind))
        for ind, fit in zip(invalid_ind, fitness):
            ind.fitness.values = fit

        # select
        pop = toolbox.select(remove_twins(pop + offspring), mu)
        record = stats.compile(pop)
        logbook.record(gen=g, evals=len(invalid_ind), **record)
        print(logbook.stream)
        if record['min'][0] < 1E-4:
            break

    return pop, logbook


def _error(ind, compile, X, y):
    model = compile(expr=ind)
    yp = model(*X.T)
    er = np.sqrt(np.nanmean((y-yp)**2))
    return er


def _evaluate(ind, compile, X, y):
    er = _error(ind, compile, X, y)
    com = len(ind)
    return er, com


def _setup_model(varnames):
    strat = ['exp', 'symc', 'pot', 'trigo']
    #funcset = prepare_funcset(strat=strat, arity=train_X.shape[1])
    funcset = prepare_funcset(strat=strat, arity=len(varnames))
    new_varnames = {'ARG%i' % i: var for i, var in enumerate(varnames)}
    funcset.renameArguments(**new_varnames)

    creator.create("Fitness", base.Fitness, weights=(-1.0, -1.0))
    creator.create("Individual", MyGPTree, fitness=creator.Fitness)

    toolbox = base.Toolbox()
    toolbox.register("compile", gp.compile, pset=funcset)
    toolbox.register("expr", gp.genHalfAndHalf, pset=funcset, min_=1, max_=4)
    toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.expr)
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)
    # inherit
    toolbox.register("select", tools.selNSGA2)
    toolbox.register("mate", gp.cxOnePoint)
    toolbox.register("expr_mut", gp.genHalfAndHalf, pset=funcset, min_=1, max_=4)
    toolbox.register("mutate", gp.mutUniform, expr=toolbox.expr_mut, pset=funcset)
    # cross
    toolbox.decorate("mate", gp.staticLimit(key=operator.attrgetter("height"), max_value=17))
    toolbox.decorate("mutate", gp.staticLimit(key=operator.attrgetter("height"), max_value=17))

    return funcset, toolbox


def cal_nrmse(model, X, y):
    return np.sqrt(np.nanmean((infer(model, X) - y) ** 2)) / (max(y) - min(y))


def cal_rmse(model, X, y):
    return np.sqrt(np.nanmean((infer(model, X) - y) ** 2))


def infer(model, X, fill_nan_with=None):
    """Note: outputs may contain NaNs."""
    pred = model(*X.T)
    if fill_nan_with is not None:
        where_are_nans = np.isnan(pred)
        pred[where_are_nans] = fill_nan_with

    return pred


def train(train_X, train_y, valid_X, valid_y, varnames, seed=43, verbose=None):
    hyper = {'gen': 92, 'mu': 500, 'cxpb': 0.5}

    funcset, toolbox = _setup_model(varnames)
    # set evaluation metrics
    toolbox.register("evaluate", _evaluate, compile=toolbox.compile, X=valid_X, y=valid_y)

    # optimization setting
    options = {'maxiter': 5}
    context = generate_context(funcset, train_X)
    constraints = ({'type': 'ineq', 'fun': lambda x: 10.0-np.abs(x)})
    cost = lambda args, yp: np.sum((train_y - yp(*args)) ** 2)
    optimizer = lambda ind: optimize_constants(
        ind=ind, cost=cost, context=context, options=options, constraints=constraints)

    # start to train
    pop, log = _evolve(toolbox=toolbox, optimizer=optimizer, seed=seed,  **hyper)
    pareto_front = tools.ParetoFront()
    pareto_front.update(pop)

    complexity = [ind.fitness.values[1] for ind in pareto_front]
    models_compiled = [toolbox.compile(m) for m in pareto_front]

    return complexity, pareto_front, models_compiled


def train_tail(X, y, varnames, train_ratio, seed=43, verbose=None):
    nlen = len(X)
    lag = np.round(train_ratio * nlen)
    train_X = X[1:(nlen-lag), :]
    valid_X = X[(nlen-lag)::, :]
    train_y = y[1:(nlen-lag)]
    valid_y = y[(nlen-lag)::]

    result = train(train_X, train_y, valid_X, valid_y, varnames, seed, verbose)
    complexity, pareto_front, models_compiled = result

    valid_nrmse = [cal_nrmse(model, valid_X, valid_y) for model in models_compiled]
    valid_rmse = [cal_rmse(model, valid_X, valid_y) for model in models_compiled]

    train_nrmse = [cal_nrmse(model, train_X, train_y) for model in models_compiled]
    train_rmse = [cal_rmse(model, train_X, train_y) for model in models_compiled]

    return complexity, [valid_nrmse, train_nrmse], [valid_rmse, train_rmse], pareto_front, models_compiled


def train_half(X, y, varnames, seed=43, verbose=None):
    train_X = X[::2, :]
    valid_X = X[1::2, :]
    train_y = y[::2]
    valid_y = y[1::2]

    result = train(train_X, train_y, valid_X, valid_y, varnames, seed, verbose)
    complexity, pareto_front, models_compiled = result

    valid_nrmse = [cal_nrmse(model, valid_X, valid_y) for model in models_compiled]
    valid_rmse = [cal_rmse(model, valid_X, valid_y) for model in models_compiled]

    train_nrmse = [cal_nrmse(model, train_X, train_y) for model in models_compiled]
    train_rmse = [cal_rmse(model, train_X, train_y) for model in models_compiled]

    return complexity, [valid_nrmse, train_nrmse], [valid_rmse, train_rmse], pareto_front, models_compiled


def train_random(X, y, varnames, train_ratio, seed=43, verbose=None):
    n_len = len(X)
    num_train = int(np.round(train_ratio * n_len))
    indexs_all = range(n_len)
    indexs_train = random.sample(indexs_all, num_train)
    indexs_train = np.sort(indexs_train)
    indexs_valid = np.setdiff1d(indexs_all, indexs_train)
    train_X = X[indexs_train, :]
    valid_X = X[indexs_valid, :]
    train_y = y[indexs_train]
    valid_y = y[indexs_valid]

    result = train(train_X, train_y, valid_X, valid_y, varnames, seed, verbose)
    complexity, pareto_front, models_compiled = result

    test_nrmse = [cal_nrmse(model, valid_X, valid_y) for model in models_compiled]
    test_rmse = [cal_rmse(model, valid_X, valid_y) for model in models_compiled]

    train_nrmse = [cal_nrmse(model, train_X, train_y) for model in models_compiled]
    train_rmse = [cal_rmse(model, train_X, train_y) for model in models_compiled]

    return complexity, [test_nrmse, train_nrmse], [test_rmse, train_rmse], pareto_front, models_compiled



