import pandas as pd
import os
import numpy as np

COLS_TUNNEL_DATA = {
    '隧道': 'tunnel',
    '隧道中心公里标': 'center_kilometer',
    '隧道长度': 'tunnel_length',
    '进口速度': 'speed_in',
    '出口速度': 'speed_out',
    '空调开关': 'air_conditioner',
    '会车': 'meeting'
}
FACTORS_CONTINUOUS = ['speed_in', 'speed_out','tunnel_length']
FACTORS_BINARY = ['air_conditioner', 'meeting']
TUNNEL_DATA_TYPE = {
    '隧道': str,
    '隧道中心公里标': str,
    '隧道长度': float,
    '进口速度': float,
    '出口速度': float,
    '空调开关': int,
    '会车': int
}

COLS_PRESSURE_DATA = {
    '时间': 'time',
    '1车内中部': 'c1_internal_mid',
    '1车内端部': 'c1_internal_end',
    '1车外1位侧': 'c1_external_side_b1',
    '1车外2位侧': 'c1_external_side_b2',
    '2车内中部': 'c2_internal_mid',
    '2车内端部': 'c2_internal_end',
    '2车外1位侧': 'c2_external_side_b1',
    '2车外2位侧': 'c2_external_side_b2',
    '7车内中部': 'c7_internal_mid',
    '7车内端部': 'c7_internal_end',
    '7车外1位侧': 'c7_external_side_b1',
    '7车外2位侧': 'c7_external_side_b2'
}


def get_cols_tunnel_data():
    return COLS_TUNNEL_DATA.values()


def get_cols_pressure_data():
    cols = list(COLS_PRESSURE_DATA.values())
    cols.remove('time')
    return cols


def convert_kilometer_mark_to_value(x):
    """Primitive format: K1832+319.22"""
    x = str(x)[1:]
    kilometer, remainder = x.split('+')
    res = float(kilometer)*1000 + float(remainder)
    return res


def load_tunnel_data(dir_root):
    # df = pd.read_excel(os.path.join(dir_root, '隧道信息.xlsx'), dtype=TUNNEL_DATA_TYPE)
    df = pd.read_csv(os.path.join(dir_root, '隧道信息.csv'), dtype=TUNNEL_DATA_TYPE)
    df = df.rename(columns=COLS_TUNNEL_DATA)
    df['center_kilometer'] = df['center_kilometer'].apply(convert_kilometer_mark_to_value)
    return df


def load_pressure_data(dir_root, name):
    # df = pd.read_excel(os.path.join(dir_root, '{}.xlsx'.format(name)), dtype=float)
    df = pd.read_csv(os.path.join(dir_root, '{}.csv'.format(name)), dtype=float)
    df = df.rename(columns=COLS_PRESSURE_DATA)
    return df


def load_data(dir_root):
    df_tunnel = load_tunnel_data(dir_root)
    df_pressure_list = []
    for id, row in df_tunnel.iterrows():
        df_pressure_list.append(load_pressure_data(dir_root, row['tunnel']))

    return df_tunnel, df_pressure_list


def down_sample_mean(df, downsampling_ratio):
    assert downsampling_ratio > 1, 'downsampling_ratio={} should be large than 1.'.format(downsampling_ratio)

    inds = list(range(0, df.shape[0], downsampling_ratio))
    df_ret = pd.DataFrame(columns=df.columns)
    for i in range(1, len(inds)):
        subdf = df.iloc[inds[i - 1]:inds[i]]
        mean = subdf.mean()
        df_ret = df_ret.append(mean, ignore_index=True)
        df_ret.iloc[df_ret.shape[0] - 1]['time'] = subdf.iloc[subdf.shape[0] - 1]['time']

    return df_ret


def down_sample_point(df, downsampling_ratio):
    assert downsampling_ratio > 1, 'downsampling_ratio={} should be large than 1.'.format(downsampling_ratio)

    inds = list(range(0, df.shape[0], downsampling_ratio))
    df_ret = pd.DataFrame(columns=df.columns)
    for i in range(1, len(inds)):
        subdf = df.iloc[inds[i - 1]:inds[i]]
        point = subdf.iloc[subdf.shape[0]-1]
        df_ret = df_ret.append(point, ignore_index=True)
        df_ret.iloc[df_ret.shape[0] - 1]['time'] = subdf.iloc[subdf.shape[0] - 1]['time']

    return df_ret

