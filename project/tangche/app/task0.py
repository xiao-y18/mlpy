"""
    EDA
"""

import logging
import os
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import argparse

from project.tangche.lib.utils import tag_path, makedirs
from project.tangche.lib.data import load_tunnel_data, load_pressure_data
from project.tangche.configure import DIR_CACHE, DIR_OPERATIONS, set_logging


TAG = tag_path(os.path.abspath(__file__), 2)
DIR_LOG = makedirs(os.path.join(DIR_CACHE, TAG))
LOG = set_logging(TAG, DIR_LOG)


def overview(dir_operations):
    dir_log_overview = makedirs(os.path.join(DIR_LOG, 'overview'))
    LOG.info("overview of data to the path={}".format(dir_log_overview))

    res_binary_count = {'meeting': [], 'air_conditioner': []}
    for iop, dir_operation in enumerate(dir_operations):
        df_tunnel = load_tunnel_data(dir_operation)
        res = []
        for tunnel in df_tunnel['tunnel'].values:
            df_pressure = load_pressure_data(dir_operation, tunnel)
            n_sample = df_pressure.shape[0]
            res.append(n_sample)
        df_tunnel['n_sample_pressure'] = np.array(res)
        df_tunnel.to_csv(os.path.join(dir_log_overview, 'tunnel_op-{}.csv'.format(iop)), index=False)
        for col in res_binary_count.keys():
            res_binary_count[col].append(df_tunnel[col].sum())
    res_binary_count = pd.DataFrame(res_binary_count)
    res_binary_count.to_csv(os.path.join(dir_log_overview, 'binary_count.csv'))


def vis_internal_pressure(dir_operations):
    dir_log_vis = makedirs(os.path.join(DIR_LOG, 'vis_in'))
    LOG.info("visualize internal pressure and save the result to the path={}".format(dir_log_vis))
    for iop, dir_operation in enumerate(dir_operations):
        LOG.info("processing {} {}".format(iop, dir_operation))
        df_tunnel = load_tunnel_data(dir_operation)
        for tunnel in df_tunnel['tunnel'].values:
            LOG.info("processing {}".format(tunnel))
            df_pressure = load_pressure_data(dir_operation, tunnel)
            p_i = df_pressure[['c1_internal_mid']].values
            # p_i = df_pressure[['c1_internal_mid', 'c1_internal_end']].mean(axis=1).values
            plt.plot(p_i)
            plt.savefig(os.path.join(dir_log_vis, '{}_{}.png'.format(iop, tunnel)))
            plt.close()


def vis_external_pressure(dir_operations):
    dir_log_vis = makedirs(os.path.join(DIR_LOG, 'vis_ex'))
    LOG.info("visualize external pressure and save the result to the path={}".format(dir_log_vis))
    for iop, dir_operation in enumerate(dir_operations):
        LOG.info("processing {} {}".format(iop, dir_operation))
        df_tunnel = load_tunnel_data(dir_operation)
        for tunnel in df_tunnel['tunnel'].values:
            LOG.info("processing {}".format(tunnel))
            df_pressure = load_pressure_data(dir_operation, tunnel)
            p_e = df_pressure[['c1_external_side_b1', 'c1_external_side_b2']].mean(axis=1).values
            plt.plot(p_e)
            plt.savefig(os.path.join(dir_log_vis, '{}_{}.png'.format(iop, tunnel)))
            plt.close()


def main(args):
    if args.mode == 'over':
        overview(args.dir_operations)
    elif args.mode == 'visin':
        vis_internal_pressure(args.dir_operations)
    elif args.mode == 'visex':
        vis_external_pressure(args.dir_operations)
    else:
        raise ValueError("mode={} can not be found!".format(args.mode))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Exploratory data analysis.')
    parser.add_argument('--mode', default='over', type=str)
    parser.add_argument('--dir_operations', default=DIR_OPERATIONS, type=list)
    args = parser.parse_args()
    main(args)



