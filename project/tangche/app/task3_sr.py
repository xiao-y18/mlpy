import os
import pandas as pd
import joblib
import json

import sklearn.preprocessing as skpre

from project.tangche.lib import sr_deap
from project.tangche.lib.utils import tag_path, makedirs
from project.tangche.configure import DIR_CACHE, DIR_OPERATIONS, set_logging
from project.tangche.app.task3 import DataConstructor, Evaluator

TAG = tag_path(os.path.abspath(__file__), 2)
DIR_LOG = makedirs(os.path.join(DIR_CACHE, TAG))
LOG = set_logging(TAG, DIR_LOG)


class ModelSymbolicRegressionDeep(object):
    def __init__(self, dir_log):
        self.dir_log = dir_log
        self.x_scaler = None
        self.y_scaler = None
        self.model = None
        self.model_exp = None
        self.model_exp_sim = None

    def fit(self, x_train, y_train, save=True):
        self.x_scaler = skpre.StandardScaler()
        x_train_scaled = self.x_scaler.fit_transform(x_train)
        self.y_scaler = skpre.StandardScaler()
        y_train_scaled = self.y_scaler.fit_transform(y_train.reshape(-1, 1)).ravel()

        varnames = ['var{}'.format(i) for i in range(x_train.shape[1])]
        result = sr_deap.model.train_random(x_train_scaled, y_train_scaled, varnames, 0.8)
        sr_deap.utils.result_summary([result], ['target'], self.dir_log)

        # choose the most accurate model
        best_model_id = 0
        self.model = result[-1][best_model_id]
        self.model_exp = str(result[-2][best_model_id])
        self.model_exp_sim = str(sr_deap.base.simplify_this(result[-2][best_model_id]))
        if save:
            self.save()
        return self.model_exp, self.model_exp_sim

    def predict(self, x):
        self.load()
        x_scaled = self.x_scaler.transform(x)
        y_pred = sr_deap.model.infer(self.model, x_scaled, fill_nan_with=0)
        y_pred = self.y_scaler.inverse_transform(y_pred)
        return y_pred

    def save(self):
        assert self.x_scaler is not None and self.y_scaler is not None and self.model is not None and \
               self.model_exp is not None
        joblib.dump(self.x_scaler, os.path.join(self.dir_log, 'x_scaler.bin'))
        joblib.dump(self.y_scaler, os.path.join(self.dir_log, 'y_scaler.bin'))
        joblib.dump(self.model, os.path.join(self.dir_log, 'model.bin'))
        exp = dict(exp=self.model_exp, exp_sim=self.model_exp_sim)
        with open(os.path.join(self.dir_log, 'model_exp.json'), 'w') as f:
            f.write(json.dumps(exp))

    def load(self):
        try:
            self.x_scaler = joblib.load(os.path.join(self.dir_log, 'x_scaler.bin'))
            self.y_scaler = joblib.load(os.path.join(self.dir_log, 'y_scaler.bin'))
            self.model = joblib.load(os.path.join(self.dir_log, 'model.bin'))
            with open(os.path.join(self.dir_log, 'model_exp.json')) as f:
                exp = json.load(f)
            self.model_exp = exp['exp']
            self.model_exp_sim = exp['exp_sim']

        except FileNotFoundError as err:
            print(err)
            print("Please train model first!")
            exit()


def main(dir_op_train, dir_op_test, train=True):
    """
        dir_op_train and dir_op_test should be in the same direction.
    """

    data_constructor = DataConstructor()
    x_list_train, y_list_train, df_tunnel_train = data_constructor.extract_data_an_operation(dir_op_train)
    x_list_test, y_list_test, df_tunnel_test = data_constructor.extract_data_an_operation(dir_op_test)

    dir_vis_train = makedirs(os.path.join(DIR_LOG, os.path.basename(dir_op_train), 'vis-train'))
    DataConstructor.vis(y_list_train, dir_vis_train)
    dir_vis_test = makedirs(os.path.join(DIR_LOG, os.path.basename(dir_op_train), 'vis-test'))
    DataConstructor.vis(y_list_test, dir_vis_test)

    dir_eval = makedirs(os.path.join(DIR_LOG, os.path.basename(dir_op_train), 'evaluate'))
    if train:
        res_expressions = {'tunnel': [], 'origin': [], 'simplified': []}
        for i_tunnel, tunnel in enumerate(df_tunnel_train['tunnel'].values):
            x_train, y_train = x_list_train[i_tunnel], y_list_train[i_tunnel]
            dir_model = makedirs(os.path.join(DIR_LOG, os.path.basename(dir_op_train), 'model', tunnel))
            model = ModelSymbolicRegressionDeep(dir_model)
            exp_origin, exp_simplified = model.fit(x_train, y_train)
            res_expressions['tunnel'].append(tunnel)
            res_expressions['origin'].append(exp_origin)
            res_expressions['simplified'].append(exp_simplified)
        res_expressions = pd.DataFrame(res_expressions)
        res_expressions.to_csv(os.path.join(dir_eval, 'expression.csv'))

    eval = Evaluator(dir_eval)
    for i_tunnel, tunnel in enumerate(df_tunnel_train['tunnel'].values):
        x_test, y_test = x_list_test[i_tunnel], y_list_test[i_tunnel]
        dir_model = os.path.join(DIR_LOG, os.path.basename(dir_op_train), 'model', tunnel)
        model = ModelSymbolicRegressionDeep(dir_model)
        y_pred = model.predict(x_test)
        y_true = y_test
        eval.append(y_true, y_pred, tunnel)


if __name__ == '__main__':
    main(DIR_OPERATIONS[0], DIR_OPERATIONS[2], train=True)
    main(DIR_OPERATIONS[1], DIR_OPERATIONS[3], train=True)




