import pandas as pd
import os
import numpy as np
import matplotlib.pyplot as plt

import tensorflow as tf
import sklearn.preprocessing as skpre
import pickle

from project.tangche.lib.utils import tag_path, makedirs
from project.tangche.configure import DIR_CACHE, DIR_OPERATIONS
from project.tangche.app.task3 import Evaluator

TAG = tag_path(os.path.abspath(__file__), 2)


def visualize(target, signal, dir_in, dir_log):
    for iop, op in enumerate(DIR_OPERATIONS):
        target_op = '{}_op-{}'.format(target, iop)
        dir_in_op = os.path.join(dir_in, target_op)
        dir_log_op = makedirs(os.path.join(dir_log, target_op))
        files = [f for f in os.listdir(dir_in_op) if f.endswith(signal)]
        for f in files:
            arr = np.loadtxt(os.path.join(dir_in_op, f))
            plt.plot(arr)
            plt.savefig(os.path.join(dir_log_op, '{}.png'.format(f)))
            plt.close()


def skip_prefix(arr, pre=0):
    i = 0
    while arr[i] == pre:
        i += 1
    return arr[i:]


def get_data_an_operation(target, iop, signal, dir_in):
    target_op = '{}_op-{}'.format(target, iop)
    dir_in_op = os.path.join(dir_in, target_op)
    files = [f for f in os.listdir(dir_in_op) if f.endswith(signal)]
    res = []
    for f in files:
        arr = np.loadtxt(os.path.join(dir_in_op, f))
        arr = skip_prefix(arr)
        res.append(arr)
    res = np.hstack(res)
    res = res[:, np.newaxis]

    return res


def construct_data_prediction(X, y, window=1, n_pred=1):
    pos_begin = window-1
    pos_end = X.shape[0]-1-n_pred
    n_samples = pos_end - pos_begin + 1
    X_ret = np.zeros([n_samples, window, X.shape[1]])
    y_ret = np.zeros(n_samples)
    for i, t in zip(range(n_samples), range(pos_begin, pos_end+1)):
        X_ret[i] = X[(t-window+1):(t+1)]
        y_ret[i] = y[t+n_pred]

    return X_ret, y_ret


class PredictionModel(object):
    def __init__(self, path, verbose=1):
        self.path = path
        self.path_model = makedirs(os.path.join(self.path, 'model'))
        self.path_scaler = makedirs(os.path.join(self.path, 'scaler'))
        self.path_log = makedirs(os.path.join(self.path, 'log'))
        self.verbose = verbose

        self.model = None
        self.x_scaler = skpre.StandardScaler()
        self.y_scaler = skpre.StandardScaler()

    @staticmethod
    def build_model(input_shape):
        model = tf.keras.models.Sequential()
        model.add(tf.keras.layers.LSTM(64, input_shape=input_shape))
        model.add(tf.keras.layers.Dense(1))
        return model

    def fit(self, x, y,
            batch_size=64,
            n_epochs=20,
            validation_data=None,
            **kwargs):

        # pre-processing
        x = self.x_scaler.fit_transform(x.reshape(-1, 2)).reshape(x.shape)
        y = self.y_scaler.fit_transform(y.reshape(-1, 1))
        if validation_data is not None:
            x_val, y_val = validation_data
            x_val = self.x_scaler.transform(x_val.reshape(-1, 2)).reshape(x_val.shape)
            y_val = self.y_scaler.transform(y_val.reshape(-1, 1))
            validation_data = (x_val, y_val)

        self.model = self.build_model(x.shape[1:])
        if self.verbose > 0:
            self.model.summary()

        # training
        self.model.compile(loss='mae', optimizer=tf.keras.optimizers.RMSprop(lr=0.0001))
        if validation_data is None:
            hist = self.model.fit(
                x, y, batch_size=batch_size, epochs=n_epochs, verbose=self.verbose)
        else:
            es = tf.keras.callbacks.EarlyStopping(patience=10, restore_best_weights=True)
            hist = self.model.fit(
                x, y, batch_size=batch_size, epochs=n_epochs, verbose=self.verbose,
                validation_data=validation_data, callbacks=[es])

        hist = pd.DataFrame(hist.history)
        plt.plot(hist['loss'].values, label='loss')
        plt.plot(hist['val_loss'].values, label='val_loss')
        plt.legend(loc='best')
        plt.tight_layout()
        plt.savefig(os.path.join(self.path_log, 'loss.png'))
        plt.close()
        if validation_data is not None:
            x_val, y_val = validation_data
            y_pred = self.predict(x_val)
            plt.plot(y_pred, label='y_pred')
            plt.plot(y_val, label='y_true')
            plt.legend(loc='best')
            plt.tight_layout()
            plt.savefig(os.path.join(self.path_log, 'pred_val.png'))
            plt.close()

        return hist

    def predict(self, x):
        y_pred = self.model.predict(self.x_scaler.transform(x.reshape(-1, 2)).reshape(x.shape))
        y_pred = self.y_scaler.inverse_transform(y_pred).ravel()

        return y_pred

    def save(self):
        tf.keras.models.save_model(self.model, self.path_model)
        pickle.dump(self.x_scaler, open(os.path.join(self.path_scaler, 'x_scaler.pkl'), 'wb'))
        pickle.dump(self.y_scaler, open(os.path.join(self.path_scaler, 'y_scaler.pkl'), 'wb'))

    def load(self):
        self.model = tf.keras.models.load_model(self.path_model)
        self.x_scaler = pickle.load(open(os.path.join(self.path_scaler, 'x_scaler.pkl'), 'rb'))
        self.y_scaler = pickle.load(open(os.path.join(self.path_scaler, 'y_scaler.pkl'), 'rb'))


def main(mode, target, signal, dir_in):
    # directories
    dir_log = makedirs(os.path.join(DIR_CACHE, TAG, '{}_{}'.format(target, signal)))
    dir_model = makedirs(os.path.join(dir_log, 'model'))

    # hyper-parameters for training
    iop_tr = 0  # the operation id for training
    ratio_tr = 0.9  # the ratio of training set and the rest for validation
    window = 10  # the size of history window
    n_pred = 1  # the number of future horizons to be predicted
    n_epochs = 100

    # actions
    if mode == 'vis':
        visualize(target, signal, dir_in, makedirs(os.path.join(dir_log, 'vis')))
    elif mode == 'train':
        # construct training set
        series = get_data_an_operation(target, iop_tr, signal, dir_in)
        x, y = construct_data_prediction(series, series, window=window, n_pred=n_pred)
        # split a validation set
        len_tr = int(y.shape[0]*ratio_tr)
        y_tr, X_tr = y[:len_tr], x[:len_tr]
        y_te, X_te = y[len_tr:], x[len_tr:]
        print("training set", X_tr.shape, y_tr.shape)
        print("validation set", X_te.shape, y_te.shape)
        # train a prediction model
        model = PredictionModel(dir_model)
        model.fit(X_tr, y_tr, n_epochs=n_epochs, validation_data=(X_te, y_te))
        model.save()
    elif mode == 'eval':
        # load model
        model = PredictionModel(dir_model)
        model.load()
        # evaluate
        dir_eval = makedirs(os.path.join(dir_log, 'evaluate'))
        eval = Evaluator(dir_eval)
        for iop, dir_op in enumerate(DIR_OPERATIONS):
            series = get_data_an_operation(target, iop, signal, dir_in)
            x, y = construct_data_prediction(series, series, window=window, n_pred=n_pred)
            y_pred = model.predict(x)
            eval.append(y, y_pred, str(iop))


if __name__ == '__main__':
    # 1s change rate
    target = 'change-rate_win-100'
    signal = 'c1_internal_mid'
    dir_in = os.path.join(DIR_CACHE, 'app_task1')
    main('vis', target, signal, dir_in)
    main('train', target, signal, dir_in)
    main('eval', target, signal, dir_in)

    # 2s change rate
    target = 'change-rate_win-300'  # 3s
    signal = 'c1_internal_mid'
    dir_in = os.path.join(DIR_CACHE, 'app_task1')
    main('vis', target, signal, dir_in)
    main('train', target, signal, dir_in)
    main('eval', target, signal, dir_in)







