import logging
import os
import sys
import numpy as np
from sklearn.metrics import mean_absolute_error
import matplotlib.pyplot as plt
import pandas as pd
from multiprocessing import get_context
from scipy.integrate import quad
import argparse

from project.tangche.lib.utils import tag_path, makedirs
from project.tangche.lib.data import load_tunnel_data, load_pressure_data
from project.tangche.configure import DIR_CACHE, DIR_OPERATIONS, N_PROCESSES, set_logging

TAG = tag_path(os.path.abspath(__file__), 2)
DIR_LOG = makedirs(os.path.join(DIR_CACHE, TAG))
LOG = set_logging(TAG, DIR_LOG)


class CalculationOfTau(object):
    def __init__(self, time, p_i, p_e, dir_log, verbose=0):
        self.time = time
        self.p_i = p_i
        self.p_e = p_e
        self.dir_log = dir_log
        self.verbose = verbose
        self.n_samples = len(p_i)

        plt.plot(self.p_i)
        plt.savefig(os.path.join(self.dir_log, 'internal.png'))
        plt.clf()
        plt.plot(self.p_e)
        plt.savefig(os.path.join(self.dir_log, 'external.png'))
        plt.clf()

    def run(self, tau_min=0.1, tau_max=200.0, step_size=0.1):
        errs = []
        taus = []
        for tau in np.arange(tau_min, tau_max, step_size):
            taus.append(tau)
            err = self.simulate(tau)
            errs.append(err)
        taus = np.array(taus)
        errs = np.array(errs)

        plt.plot(taus, errs)
        plt.savefig(os.path.join(self.dir_log, 'tau_err.png'))
        df_err = pd.DataFrame({'tau': taus, 'errs': errs})
        df_err.to_csv(os.path.join(self.dir_log, 'err.csv'))
        idx_min = np.argmin(errs)
        return taus[idx_min], errs[idx_min]

    def simulate(self, tau):
        integrand_values = []
        for it, t in enumerate(self.time):
            p_e_t = self.p_e[it]
            integrand = self.law_pressure_integrand(tau, p_e_t)
            value, err = quad(integrand, 0, t)
            integrand_values.append(value)
        if self.verbose:
            plt.plot(integrand_values)
            plt.savefig(os.path.join(self.dir_log, 'integrand_{:.2f}.png'.format(tau)))
            plt.clf()

        p_i_hat = np.zeros(self.n_samples)
        for it, t in enumerate(self.time):
            p_i_hat[it] = 1.0 / tau * np.exp(-t / tau) * integrand_values[it]
        err = mean_absolute_error(self.p_i, p_i_hat)
        if self.verbose:
            plt.plot(p_i_hat)
            plt.savefig(os.path.join(self.dir_log, 'inner_{:.2f}.png'.format(tau)))
            plt.clf()

        return err

    @staticmethod
    def law_pressure_integrand(tau, p_e_t):
        I = lambda t: np.exp(-t / tau) * p_e_t  # the function I(t).
        return I


def run_test_law_pressure_integrand():
    """
        let p_e_t = 1 and thus the analytic solution is that I(t)=[-tau*exp{-t/tau}]_{0}{t}
    """
    def _run(tau):
        time = np.arange(0, 100, 0.001)

        integrand = CalculationOfTau.law_pressure_integrand(tau, 1)
        integrand_values = []
        for t in time:
            value, err = quad(integrand, 0, t)
            integrand_values.append(value)
        plt.plot(integrand_values)
        plt.savefig(os.path.join(DIR_LOG, 'test_integrand.png'))
        plt.clf()

        def analytic_solution(tau, t):
            return -tau*np.exp(-t/tau)
        integrand_values_analytic_solution = []
        for t in time:
            value = analytic_solution(tau, t) - analytic_solution(tau, 0)
            integrand_values_analytic_solution.append(value)
        plt.plot(integrand_values_analytic_solution)
        plt.savefig(os.path.join(DIR_LOG, 'test_integrand_truth.png'))

        error = mean_absolute_error(integrand_values, integrand_values_analytic_solution)
        return error

    LOG.info("in the function: run_test_law_pressure_integrand")
    for tau in np.arange(0.1, 100.0, 0.1):
        error = _run(tau)
        if error > 1e-8:
            print("The approximate calculation is wrong!")
            print("The error={:10f} is not equal to zero!".format(error))
    print("The approximate calculation pass the test.")


def run(params):
    iop, dir_operation = params
    df_tunnel = load_tunnel_data(dir_operation)
    res = {'tunnel': [], 'tau': [], 'err': []}
    for tunnel in df_tunnel['tunnel'].values:
        df_pressure = load_pressure_data(dir_operation, tunnel)
        LOG.info("EXP: {}/{}".format(dir_operation, tunnel))
        time = df_pressure['time'].values
        p_i = df_pressure[['c1_internal_mid']].values
        # p_i = df_pressure[['c1_internal_mid', 'c1_internal_end']].mean(axis=1).values
        p_e = df_pressure[['c1_external_side_b1']].values
        # p_e = df_pressure[['c1_external_side_b1', 'c1_external_side_b2']].mean(axis=1).values

        dir_log_res = os.path.join(DIR_LOG, 'op-{}'.format(iop), tunnel)
        makedirs(dir_log_res)
        cal = CalculationOfTau(time, p_i, p_e, dir_log_res)
        tau, err = cal.run()
        res['tunnel'].append(tunnel)
        res['tau'].append(tau)
        res['err'].append(err)
        LOG.info("The minimum, tau={}, err={}".format(tau, err))
    res = pd.DataFrame(res)
    res.to_csv(os.path.join(DIR_LOG, 'tau_op-{}.csv'.format(iop)))


def main(args):
    if args.mode == 'run':
        # The process to calculate Tau is so slow, therefore we use parallel strategy.
        params = []
        for iop, dir_operation in enumerate(args.dir_operations):
            params.append((iop, dir_operation))

        n_processes = min(N_PROCESSES, len(params))
        with get_context("spawn").Pool(n_processes) as p:
            p.map(run, params)
    elif args.mode == 'test':
        run_test_law_pressure_integrand()
    else:
        raise ValueError("mode={} can not be found!".format(args.mode))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Calculate the dynamic air impermeability index, Tau.')
    parser.add_argument('--mode', default='run', type=str)
    parser.add_argument('--dir_operations', default=DIR_OPERATIONS, type=list)
    args = parser.parse_args()
    main(args)










