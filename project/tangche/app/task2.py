import os
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use('Agg')
matplotlib.rc('xtick', labelsize=20)
matplotlib.rc('ytick', labelsize=20)

from project.tangche.lib.utils import tag_path, makedirs
from project.tangche.lib import data
from project.tangche.configure import DIR_CACHE, DIR_OPERATIONS

TAG = tag_path(os.path.abspath(__file__), 2)


class Analysis(object):
    def __init__(self, dir_operations, dir_feature, dir_log, feature):
        self.dir_operations = dir_operations
        self.n_operations = len(self.dir_operations)
        self.dir_feature = dir_feature
        self.dir_log = os.path.join(dir_log, feature)
        makedirs(self.dir_log)
        self.feature = feature

        self.df_tunnel_list = self.load_tunnel_data()
        self.df_feature_list = self.load_feature_data()

    def load_tunnel_data(self):
        df_tunnel_list = []
        for operation_dir in self.dir_operations:
            df_tunnel = data.load_tunnel_data(operation_dir)
            df_tunnel_list.append(df_tunnel)
        return df_tunnel_list

    def load_feature_data(self):
        df_feature_list = []
        for operation_id, operation_dir in enumerate(self.dir_operations):
            df_feature = pd.read_csv(os.path.join(self.dir_feature, '{}_op-{}.csv'.format(self.feature, operation_id)))
            df_feature_list.append(df_feature)
        return df_feature_list

    def run(self):
        self.correlation()
        self.obs_same_tunnel()
        self.bins()

    def obs_same_tunnel(self):  # by tunnels
        df_tunnel_list = []
        df_feature_list = []
        for i in range(len(self.df_tunnel_list)):
            df_tunnel = self.df_tunnel_list[i].copy()
            df_tunnel['operation_id'] = i
            df_tunnel_list.append(df_tunnel)
            df_feature = self.df_feature_list[i].copy()
            df_feature['operation_id'] = i
            df_feature_list.append(df_feature)
            # df_tunnel_list[i]['operation_id'] = i
            # df_feature_list[i]['operation_id'] = i

        df_tunnel = pd.concat(df_tunnel_list, axis=0, ignore_index=True)
        df_feature = pd.concat(df_feature_list, axis=0, ignore_index=True)
        df_concat = df_tunnel.merge(df_feature, on=['tunnel', 'operation_id'])
        tunnels = self.df_tunnel_list[0]['tunnel'].values
        for tunnel_id, tunnel in enumerate(tunnels):
            df_concat_a_tunnel = df_concat[df_concat['tunnel'] == tunnel]
            df_concat_a_tunnel.to_csv(os.path.join(self.dir_log, 'tunnel-{}_{}.csv'.format(tunnel_id, tunnel)),
                                      index=False)

    def correlation(self):  # by operations
        df_com_list = []
        for i in range(self.n_operations):
            df_tunnel = self.df_tunnel_list[i][data.FACTORS_CONTINUOUS]
            df_feature = self.df_feature_list[i].drop(columns=['tunnel'])
            df_com = pd.concat([df_tunnel, df_feature], axis=1)
            df_com_list.append(df_com)
            df_com_corr = df_com.corr()
            df_com.to_csv(os.path.join(self.dir_log, 'corr_op-{}_raw.csv'.format(i)))  # save raw data.
            df_com_corr.to_csv(os.path.join(self.dir_log, 'corr_op-{}.csv'.format(i)))  # save correlation results.

            # visualization
            dir_vis = makedirs(os.path.join(self.dir_log, 'corr_op-{}_vis'.format(i)))  # save visualization results.
            for y_name in df_feature.columns.values:
                for x_name in df_tunnel.columns.values:
                    y = df_feature[y_name].values
                    x = df_tunnel[x_name].values
                    plt.plot(x, y, '.', markersize=20)
                    plt.grid(True)
                    plt.tight_layout()
                    plt.savefig(os.path.join(dir_vis, '{}_{}.png'.format(x_name, y_name)))
                    plt.close()

        df_com_all = pd.concat(df_com_list, ignore_index=True)
        df_com_all_corr = df_com_all.corr()
        df_com_all_corr.to_csv(os.path.join(self.dir_log, 'corr_op-all.csv'))

    def bins(self):  # by operations
        df_com_list = []
        for i in range(self.n_operations):
            df_tunnel = self.df_tunnel_list[i][data.FACTORS_BINARY]
            df_feature = self.df_feature_list[i].drop(columns=['tunnel'])
            df_com = pd.concat([df_tunnel, df_feature], axis=1)
            df_com_list.append(df_com)
            for col in data.FACTORS_BINARY:
                res = df_com.groupby([col]).mean()
                res.to_csv(os.path.join(self.dir_log, 'bins-mean-{}_op-{}.csv'.format(col, i)))
        df_com_all = pd.concat(df_com_list, ignore_index=True)
        for col in data.FACTORS_BINARY:
            res = df_com_all.groupby([col]).mean()
            res.to_csv(os.path.join(self.dir_log, 'bins-mean-{}_op-all.csv'.format(col)))


class VisualizationByTunnel(object):
    """@note:
        Once visualization is operated on samples collected on a same tunnel.
    """
    factors_continuous = data.FACTORS_CONTINUOUS
    factors_binary = data.FACTORS_BINARY

    def __init__(self, dir_log, targets, factors_continuous=None, factors_binary=None):
        self.dir_log = dir_log
        self.targets = targets
        if factors_continuous is not None:
            self.factors_continuous = factors_continuous
        if factors_binary is not None:
            self.factors_binary = factors_binary

    def run(self):
        tunnel_csvs = [f for f in os.listdir(self.dir_log) if f.startswith('tunnel-') and f.endswith('.csv')]
        for csv in tunnel_csvs:  # for each tunnel
            dir_log_tunnel = makedirs(os.path.join(self.dir_log, csv.split('.')[0]))
            df = pd.read_csv(os.path.join(self.dir_log, csv))
            self.plot_points(df, self.factors_continuous, dir_log_tunnel)
            self.plot_bar(df, self.factors_binary, dir_log_tunnel)

    def plot_points(self, df, factors_continuous, dir_log):
        for p_name in self.targets:
            y = df[p_name].values
            for f_name in factors_continuous:
                x = df[f_name].values
                plt.plot(x, y, '^', markersize=30)
                for i, text in enumerate(df['operation_id'].values):
                    plt.annotate(text, (x[i], y[i]), fontsize=30)
                plt.grid(True)
                plt.tight_layout()
                plt.savefig(os.path.join(dir_log, '{}_{}.png'.format(p_name, f_name)))
                plt.close()

    def plot_bar(self, df, factors_binary, dir_log):
        colors = ['gray', 'red']
        for p_name in self.targets:
            y = df[p_name].values
            labels = df['operation_id'].values.astype(str)
            for f_name in factors_binary:
                c = [colors[v] for v in df[f_name].values]
                plt.bar(labels, y, color=c)
                plt.grid(True)
                plt.tight_layout()
                plt.savefig(os.path.join(dir_log, '{}_{}.png'.format(p_name, f_name)))
                plt.close()


if __name__ == '__main__':
    dir_log = makedirs(os.path.join(DIR_CACHE, TAG))

    dir_log_pi = makedirs(os.path.join(DIR_CACHE, 'app_task1'))
    Analysis(DIR_OPERATIONS, dir_log_pi, dir_log, 'amplitude_win-None').run()
    Analysis(DIR_OPERATIONS, dir_log_pi, dir_log, 'change-rate_win-100').run()  # 1s change rate
    Analysis(DIR_OPERATIONS, dir_log_pi, dir_log, 'change-rate_win-300').run()  # 2s change rate
    dir_log_tau = makedirs(os.path.join(DIR_CACHE, 'app_task1_tau'))
    Analysis(DIR_OPERATIONS, dir_log_tau, dir_log, 'tau').run()

    VisualizationByTunnel(os.path.join(dir_log, 'amplitude_win-None'), ['c1_internal_mid', 'c1_external_side_b1']).run()
    VisualizationByTunnel(os.path.join(dir_log, 'change-rate_win-100'), ['c1_internal_mid']).run()
    VisualizationByTunnel(os.path.join(dir_log, 'change-rate_win-300'), ['c1_internal_mid']).run()
    VisualizationByTunnel(os.path.join(dir_log, 'tau'), ['tau']).run()





