import pandas as pd
import os
import logging
import sys
import numpy as np
import matplotlib.pyplot as plt

from sklearn import metrics

from project.tangche.lib.utils import tag_path
from project.tangche.lib.data import load_tunnel_data, load_pressure_data, down_sample_mean
from project.tangche.configure import set_logging

TAG = tag_path(os.path.abspath(__file__), 2)
LOG = set_logging(TAG)


class Metrics(object):
    r2_score = metrics.r2_score
    rmse = lambda y_true, y_pred: np.sqrt(metrics.mean_squared_error(y_true, y_pred))
    mae = metrics.mean_absolute_error

    @staticmethod
    def hit_rate(y_true, y_pred, targetsize=None, n_std=1):
        if targetsize is None:
            std = np.std(y_true)
            y_true_std_p = y_true + n_std * std
            y_true_std_n = y_true - n_std * std
            hit = (y_true_std_n <= y_pred) & (y_pred <= y_true_std_p)
            hit_rate = np.sum(hit) / len(hit)
        else:
            y_true_std_p = np.zeros_like(y_true)
            y_true_std_n = np.zeros_like(y_true)
            for i in range(0, len(y_true), targetsize):
                std = np.std(y_true[i:(i + targetsize)])
                y_true_std_p[i:(i + targetsize)] = y_true[i:(i + targetsize)] + n_std * std
                y_true_std_n[i:(i + targetsize)] = y_true[i:(i + targetsize)] - n_std * std
            hit = (y_true_std_n <= y_pred) & (y_pred <= y_true_std_p)
            hit_rate = np.sum(hit) / len(hit)

        return hit_rate, y_true_std_p, y_true_std_n


class Evaluator(object):
    metrics = ['r2', 'rmse', 'mae', 'hit_rate']

    def __init__(self, dir_out, n_std=1):
        self.dir_out = dir_out
        self.n_std = n_std
        self.res = self.init_res()
        self.count = 0
        self.keys = []

    def init_res(self):
        res = dict()
        for m in self.metrics:
            res[m] = []
        return res

    def evaluate(self, y_true, y_pred):
        r2 = Metrics.r2_score(y_true, y_pred)
        rmse = Metrics.rmse(y_true, y_pred)
        mae = Metrics.mae(y_true, y_pred)
        hit_rate, _, _ = Metrics.hit_rate(y_true, y_pred, n_std=self.n_std)

        LOG.info('evaluation result: ')
        LOG.info('r2: {0:.4f}'.format(r2))
        LOG.info('rmse: {0:.4f}'.format(rmse))
        LOG.info('mae: {0:.4f}'.format(mae))
        LOG.info('hit_rate: {0:.4f}'.format(hit_rate))

        res = self.init_res()
        res['r2'] = r2
        res['rmse'] = rmse
        res['mae'] = mae
        res['hit_rate'] = hit_rate

        return res

    def evaluate_and_plot(self, y_true, y_pred, key):
        res = self.evaluate(y_true, y_pred)

        plt.plot(y_true, label='y_true')
        plt.plot(y_pred, label='y_pred')
        plt.legend(loc='best')
        plt.savefig(os.path.join(self.dir_out, '{}_pre.png'.format(key)))
        plt.close()

        _, y_true_std_p, y_true_std_n = Metrics.hit_rate(y_true, y_pred, n_std=1)
        plt.plot(y_true, label='y_true')
        plt.plot(y_pred, label='y_pred')
        plt.plot(y_true_std_p, '--', label='y_true+std', color='gray')
        plt.plot(y_true_std_n, '--', label='y_true-std', color='gray')
        plt.legend(loc='best')
        plt.savefig(os.path.join(self.dir_out, '{}_pred_std.png'.format(key)))
        plt.close()

        return res

    def append(self, y_true, y_pred, key, flush=True, plot=True):
        if plot:
            metrics = self.evaluate_and_plot(y_true, y_pred, key)
        else:
            metrics = self.evaluate(y_true, y_pred)
        for k, v in metrics.items():
            self.res[k].append(v)

        self.keys.append(key)
        self.count += 1

        if flush:
            self.save()

        return metrics

    def get_df(self):
        return pd.DataFrame(self.res, index=self.keys)

    def save(self):
        df = self.get_df()
        df.to_csv(os.path.join(self.dir_out, 'metrics.csv'))
        return df


class DataConstructor(object):
    def __init__(self, downsampling_ratio=10):
        self.downsampling_ratio = downsampling_ratio

    def extract_data_a_tunnel(self, tunnel_info, dir_operation):
        tunnel = tunnel_info['tunnel']
        speed = (tunnel_info['speed_in'] + tunnel_info['speed_out']) / 2
        df_pressure = load_pressure_data(dir_operation, tunnel)
        df_pressure = down_sample_mean(df_pressure, downsampling_ratio=self.downsampling_ratio)
        print("EXP: {}/{}".format(dir_operation, tunnel))
        time = df_pressure['time'].values
        p_i = df_pressure[['c1_internal_mid']].values
        # p_i = df_pressure[['c1_internal_mid', 'c1_internal_end']].mean(axis=1).values
        p_e = df_pressure[['c1_external_side_b1', 'c1_external_side_b2']].mean(axis=1).values
        p = time * speed
        x = np.vstack([time, p_e, p]).transpose()
        y = p_i
        return x, y.ravel()

    def extract_data_an_operation(self, dir_operation):
        df_tunnel = load_tunnel_data(dir_operation)
        x_list = []
        y_list = []
        for _, row in df_tunnel.iterrows():
            x, y = self.extract_data_a_tunnel(row, dir_operation)
            x_list.append(x)
            y_list.append(y)

        return x_list, y_list, df_tunnel

    @staticmethod
    def vis(y_list, dir_log):
        for i in range(len(y_list)):
            plt.plot(y_list[i])
            plt.savefig(os.path.join(dir_log, '{}.png'.format(i)))
            plt.close()
