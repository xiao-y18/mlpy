import os
import joblib

from sklearn.neural_network import MLPRegressor
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import RandomizedSearchCV

from project.tangche.lib.utils import tag_path, makedirs
from project.tangche.configure import DIR_CACHE, DIR_OPERATIONS, set_logging
from project.tangche.app.task3 import DataConstructor, Evaluator

TAG = tag_path(os.path.abspath(__file__), 2)
DIR_LOG = makedirs(os.path.join(DIR_CACHE, TAG))
LOG = set_logging(TAG, DIR_LOG)


class ModelMLP(object):
    def __init__(self, random_search=False):
        self.random_search = random_search
        self.model = self.build_model()

    def build_model(self):
        model = MLPRegressor(early_stopping=True, learning_rate='adaptive', max_iter=1000)
        if self.random_search:
            params = {'hidden_layer_sizes': [16, 32, 64, 128],
                      'alpha': [0.0001, 0.001, 0.01, 0.1, 1.0],
                      'batch_size': [16, 32, 63, 128, 200]}
            model = RandomizedSearchCV(model, params, random_state=0)
        model = make_pipeline(StandardScaler(), model)
        return model

    def fit(self, x_train, y_train):
        self.model.fit(x_train, y_train)

    def predict(self, x):
        return self.model.predict(x)

    def save(self, path, name):
        joblib.dump(self.model, os.path.join(path, '{}.bin'.format(name)))

    def load(self, path, name):
        try:
            self.model = joblib.load(os.path.join(path, '{}.bin'.format(name)))
        except FileNotFoundError as err:
            print(err)
            print("Please train model first!")
            exit()


def main(dir_op_train, dir_op_test, train=True):
    """
        dir_op_train and dir_op_test should be in the same direction.
    """
    LOG.info("Start")
    LOG.info("the operation for training: {}".format(dir_op_train))
    LOG.info("the operation for testing: {}".format(dir_op_test))

    data_constructor = DataConstructor()
    x_list_train, y_list_train, df_tunnel_train = data_constructor.extract_data_an_operation(dir_op_train)
    x_list_test, y_list_test, df_tunnel_test = data_constructor.extract_data_an_operation(dir_op_test)

    dir_vis_train = makedirs(os.path.join(DIR_LOG, os.path.basename(dir_op_train), 'vis-train'))
    DataConstructor.vis(y_list_train, dir_vis_train)
    dir_vis_test = makedirs(os.path.join(DIR_LOG, os.path.basename(dir_op_train), 'vis-test'))
    DataConstructor.vis(y_list_test, dir_vis_test)

    dir_eval = makedirs(os.path.join(DIR_LOG, os.path.basename(dir_op_train), 'evaluate'))
    eval = Evaluator(dir_eval)
    for i_tunnel, tunnel in enumerate(df_tunnel_train['tunnel'].values):
        x_train, y_train = x_list_train[i_tunnel], y_list_train[i_tunnel]
        x_test, y_test = x_list_test[i_tunnel], y_list_test[i_tunnel]

        model = ModelMLP()
        if train:
            model.fit(x_train, y_train)
            model.save(dir_eval, tunnel)
        else:
            model.load(dir_eval, tunnel)
        y_pred = model.predict(x_test)
        y_true = y_test
        eval.append(y_true, y_pred, tunnel)


if __name__ == '__main__':
    main(DIR_OPERATIONS[0], DIR_OPERATIONS[2], train=False)
    main(DIR_OPERATIONS[1], DIR_OPERATIONS[3], train=False)

