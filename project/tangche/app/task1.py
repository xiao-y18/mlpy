import os
import sys
import logging
import pandas as pd
import numpy as np
from multiprocessing import get_context
import argparse

from project.tangche.lib.utils import tag_path, makedirs
from project.tangche.lib import data
from project.tangche.configure import DIR_CACHE, DIR_OPERATIONS, SAMPLING_RATE, N_PROCESSES, set_logging

TAG = tag_path(os.path.abspath(__file__), 2)
DIR_LOG = makedirs(os.path.join(DIR_CACHE, TAG))
LOG = set_logging(TAG, DIR_LOG)


class PerformanceIndex(object):
    @staticmethod
    def get_amplitude(series, window=None):
        length = len(series)
        if window is None:
            window = length
        assert window <= length, "window ({}) is out of the length ({}) of series !".format(window, length)

        res_series = np.zeros(length)
        for iw in range(window, length+1):
            subseries = series[(iw-window):iw]
            amplitude = abs(max(subseries) - min(subseries))
            res_series[iw-1] = amplitude

        return res_series

    @staticmethod
    def get_maximum_amplitude(series, window=None, with_series=False):
        series_amplitude = PerformanceIndex.get_amplitude(series, window)
        maximun = max(series_amplitude)
        if with_series:
            return maximun, series_amplitude
        else:
            return maximun

    @staticmethod
    def get_change_rate(series, window=None):
        length = len(series)
        if window is None:
            window = length
        assert window <= length, "window ({}) is out of the length ({}) of series !".format(window, length)

        res_series = np.zeros(length)
        for iw in range(window, length+1):
            subseries = series[(iw-window):iw]
            maximum_change_rate = -np.inf
            length_ss = len(subseries)
            for i in range(length_ss):
                for j in range(i+1, length_ss):
                    cr = abs(subseries[i] - subseries[j])
                    maximum_change_rate = max(maximum_change_rate, cr)
            res_series[iw-1] = maximum_change_rate
        return res_series

    @staticmethod
    def get_maximum_change_rate(series, window=None, with_series=False):
        series_change_rate = PerformanceIndex.get_change_rate(series, window)
        maximum = max(series_change_rate)
        if with_series:
            return maximum, series_change_rate
        else:
            return maximum


class FeatureExtraction(object):
    def __init__(self, operation_id, operation_dir, dir_log, cols_pressure, downsampling_ratio=None):
        self.operation_id = operation_id
        self.operation_dir = operation_dir
        self.dir_log = dir_log
        self.cols_pressure = cols_pressure
        self.downsampling_ratio = downsampling_ratio

        self.df_tunnel = data.load_tunnel_data(self.operation_dir)
        self.n_tunnels = self.df_tunnel.shape[0]
        self.n_processes = min(N_PROCESSES, self.n_tunnels)

    def extract(self, feature, window=None):
        tag = '{}_win-{}_op-{}'.format(feature, str(window), self.operation_id)
        dir_log_detail = makedirs(os.path.join(self.dir_log, tag))
        res = {'tunnel': []}
        for col in self.cols_pressure:
            res[col] = []

        if feature == 'amplitude':
            extract_func = self.amplitude
        elif feature == 'change-rate':
            extract_func = self.change_rate
        else:
            raise ValueError('feature={} can not be found!'.format(feature))

        logging.info("Loading data")
        df_pressure_list = []
        for tunnel in self.df_tunnel['tunnel'].values:
            print(tunnel)
            df_pressure = data.load_pressure_data(self.operation_dir, tunnel)
            if self.downsampling_ratio is not None:
                df_pressure = data.down_sample_mean(df_pressure, self.downsampling_ratio)
            df_pressure_list.append(df_pressure)

        logging.info("Computing in parallel")
        with get_context("spawn").Pool(self.n_processes) as p:
            res_list = p.starmap(extract_func, [(df_pressure, window) for df_pressure in df_pressure_list])

        logging.info("Reducing the results")
        for item, tunnel in zip(res_list, self.df_tunnel['tunnel'].values):
            maximums, series = item
            res['tunnel'].append(tunnel)
            for col in self.cols_pressure:
                res[col].append(maximums[col])
                np.savetxt(os.path.join(dir_log_detail, '{}_{}'.format(tunnel, col)), series[col], fmt='%.4f')
        res = pd.DataFrame(res)
        res.to_csv(os.path.join(self.dir_log, '{}.csv'.format(tag)), index=False)

    def amplitude(self, df_pressure, window):
        res_maximum = {}
        res_series = {}
        for c in self.cols_pressure:
            logging.info("processing column c={}".format(c))
            series = df_pressure[c].values
            maximum, series = PerformanceIndex.get_maximum_amplitude(series, window, with_series=True)
            res_maximum[c] = maximum
            res_series[c] = series
        return res_maximum, res_series

    def change_rate(self, df_pressure, window):
        res_maximum = {}
        res_series = {}
        for c in self.cols_pressure:
            logging.info("processing column c={}".format(c))
            series = df_pressure[c].values
            maximum, series = PerformanceIndex.get_maximum_change_rate(series, window, with_series=True)
            res_maximum[c] = maximum
            res_series[c] = series
        return res_maximum, res_series


def run_amplitude(dir_log, dir_operations):
    cols_pressure = ['c1_internal_mid', 'c1_external_side_b1']
    for operation_id, operation_dir in enumerate(dir_operations):
        logging.info("{} start".format(operation_dir))
        fe = FeatureExtraction(operation_id, operation_dir, dir_log, cols_pressure)
        fe.extract('amplitude', window=None)


def run_change_rate(dir_log, dir_operations, seconds=1, downsampling_ratio=10):
    cols_pressure = ['c1_internal_mid']
    for operation_id, operation_dir in enumerate(dir_operations):
        logging.info("{} start".format(operation_dir))
        fe = FeatureExtraction(operation_id, operation_dir, dir_log, cols_pressure, downsampling_ratio)
        fe.extract('change-rate',
                   window=int(SAMPLING_RATE/downsampling_ratio * seconds))  # seconds=1 corresponds to 1s change rate.


# def main(args):
#     if args.mode == 'amp':
#         run_amplitude(DIR_LOG, args.dir_operations)
#     elif args.mode == 'cr':
#         run_change_rate(DIR_LOG, args.dir_operations, seconds=args.seconds, downsampling_ratio=args.downsampling_ratio)
#     else:
#         raise ValueError("mode={} can not be found!".format(args.mode))


if __name__ == '__main__':

    run_amplitude(DIR_LOG, DIR_OPERATIONS)
    run_change_rate(DIR_LOG, DIR_OPERATIONS, seconds=1, downsampling_ratio=10)
    run_change_rate(DIR_LOG, DIR_OPERATIONS, seconds=3, downsampling_ratio=10)

    # parser = argparse.ArgumentParser(description='Calculate the performance index of air impermeability.')
    # parser.add_argument('--mode', default='amp', type=str)
    # parser.add_argument('--seconds', default=1, type=int)
    # parser.add_argument('--downsampling_ratio', default=10, type=int)
    # parser.add_argument('--dir_operations', default=DIR_OPERATIONS, type=list)
    # args = parser.parse_args()
    # main(args)



