"""
    some case studies for final report.
"""

import matplotlib.pyplot as plt
import seaborn as sns

from project.tangche.lib.utils import tag_path, makedirs
from project.tangche.configure import *
from project.tangche.lib import data

TAG = tag_path(os.path.abspath(__file__), 2)
DIR_LOG = makedirs(os.path.join(DIR_CACHE, TAG))
LOG = set_logging(TAG, DIR_LOG)

if __name__ == '__main__':
    #
    # a case to show down sampling method in the final report.
    #
    dir_root = DIR_OPERATIONS[0]
    df = data.load_pressure_data(dir_root, '丹水岭隧道')

    df_down_sample_mean = data.down_sample_mean(df, downsampling_ratio=100)
    df_down_sample_point = data.down_sample_point(df, downsampling_ratio=100)

    col_name = 'c1_external_side_b1'
    plt.plot(df[col_name])
    plt.tight_layout()
    plt.savefig(os.path.join(DIR_LOG, 'origin.png'))
    plt.close()

    plt.plot(df_down_sample_mean[col_name],)
    plt.tight_layout()
    plt.savefig(os.path.join(DIR_LOG, 'mean.png'))
    plt.close()

    plt.plot(df_down_sample_point[col_name])
    plt.tight_layout()
    plt.savefig(os.path.join(DIR_LOG, 'point'))
    plt.close()

    #
    # histogram
    #
    # plt.hist, bins=10)

    win = 2000
    sns.distplot(df['c1_external_side_b1'][3000:(3000+win)].values, bins=20, kde=True)
    plt.tight_layout()
    plt.savefig(os.path.join(DIR_LOG, 'hist_external.png'))
    plt.close()

    sns.distplot(df['c1_internal_mid'][1500:(1500+win)].values, bins=20, kde=True)
    plt.tight_layout()
    plt.savefig(os.path.join(DIR_LOG, 'hist_internal.png'))
    plt.close()

    plt.plot(df['c1_internal_mid'].values)
    plt.tight_layout()
    plt.savefig(os.path.join(DIR_LOG, 'internal_origin.png'))
    plt.close()

    print(df['c1_external_side_b1'].describe())
    print(df['c1_internal_mid'].describe())


