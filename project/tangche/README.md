# 动车组客室气密性能变化规律研究

## Project structure

```
.
├── README.md         
├── __init__.py
├── app        <- the source code to finish all tasks addressed in this project.
│   ├── __init__.py
│   ├── task0.py      <- exploratory data analysis.
│   ├── task1.py      <- calculating two performance indexes, i.e. pressure amplitude and change rate.
│   ├── task1_tau.py  <- calculating dynamic air impermeability `tau`.
│   ├── task2.py      <- factor analysis. analyzing the relationship between a air impermeability indicator (e.g. pressure amplitude) and its potential impact factors (e.g speed, tunnel length etc.)
│   ├── task3.py      <- internal pressure prediction tool.
│   ├── task3_nn.py   <- the neural network based prediction model.
│   ├── task3_sr.py   <- the symbolic regression based prediction model.
│   └── task4.py      <- trend prediction model. extrapolating future by history.
├── archive     <- some useless codes but that may help future work.
│   ├── __init__.py
│   ├── anomaly_detection.py   <- a basic AD model.
│   ├── feature_extraction.py  <- application of `tsfresh` library that automatically extracts a bunch of statistical features for time series data.
│   └── tau     <- exploration the calculation. 
│       ├── __init__.py
│       ├── detrend.py
│       ├── detrend2.py
│       ├── detrend_tau.py
│       ├── indicator.py
│       └── test.py
├── cache    <- the directory to save the program output. 
├── configure.py   <- configuration for this project, for example the directory of data.
├── lib    <- general library.
│   ├── __init__.py
│   ├── data.py    <- processing data.
│   ├── sr_deap    <- symbolic regression model.
│   │   ├── __init__.py
│   │   ├── base.py
│   │   ├── model.py
│   │   └── utils.py
│   └── utils.py
└── requirements.txt   <- the script to install required packages (e.g. tensorflow).  
```

## Installation and Execution

This project was tested on Ubuntu-18.04 and macOS-10.14.6. It is based on Python-3.6. To install the required packages, you can run the following command: `pip install -r requirements.txt`. Please add the directory saving the `project` into your Python Module Search Path. 

Main programs are implemented in the direction `app/`.
You can run the `.py` scripts in order to get familiar with the implemented functions and then change respective parameters to address your need.  


## Data description. 

There are two types of data: 

1. Primitive data:
	- The path to load data can be set in `configure.py` and the script to process data is `lib/data.py`.
	- Please refer the project documents and/or reports for more details about this data.
2. Intermediate results:
	- These data are saved in `cache/`.
	- Note that some programs need the results outputted by the preceding programs as input. For example `app/task4.py` takes the output of `app/task1.py`. Please refer to source codes and their ouputs for more details about the data format. 


## Notifications. 

