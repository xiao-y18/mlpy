from project.bikece.utils import tag_path, makedirs

from project.bikece.regr.current_20200225 import load
from project.bikece.regr.base import evaluate
from project.bikece.configure import DIR_CACHE

import numpy as np
import tensorflow as tf
import pandas as pd
import os
import matplotlib.pyplot as plt
import sklearn.preprocessing as skpre


class LSTMBase(object):
    def __init__(self, input_shape, verbose=1):
        self.input_shape = input_shape
        self.verbose = verbose
        self.model = self.build_model(self.input_shape)
        if (self.verbose > 0):
            self.model.summary()

    def build_model(self, input_shape):
        model = tf.keras.models.Sequential()
        model.add(tf.keras.layers.LSTM(64, input_shape=input_shape))
        model.add(tf.keras.layers.Dense(1))
        return model

    def fit(self, x,
            y,
            batch_size=64,
            n_epochs=20,
            validation_data=None,
            shuffle=True,
            **kwargs):

        self.model.compile(loss='mae', optimizer=tf.keras.optimizers.RMSprop())
        if validation_data is None:
            hist = self.model.fit(
                x, y, batch_size=batch_size, epochs=n_epochs, verbose=self.verbose)
        else:
            hist = self.model.fit(
                x, y, batch_size=batch_size, epochs=n_epochs, verbose=self.verbose,
                validation_data=validation_data)

        return pd.DataFrame(hist.history)


def construct_data(X, y, window=1, n_pred=1):
    pos_begin = window-1
    pos_end = X.shape[0]-1-n_pred
    n_samples = pos_end - pos_begin + 1
    X_ret = np.zeros([n_samples, window, X.shape[1]])
    y_ret = np.zeros(n_samples)
    for i, t in zip(range(n_samples), range(pos_begin, pos_end+1)):
        X_ret[i] = X[(t-window+1):(t+1)]
        y_ret[i] = y[t+n_pred]

    return X_ret, y_ret


if __name__ == '__main__':
    tag = tag_path(os.path.abspath(__file__), 2)
    dir_out = makedirs(os.path.join(DIR_CACHE, tag))

    df = load()
    df = df[:-2000]
    df = df[:5000]
    col_y = 'current'
    cols_X = ['current']
    # cols_X = ['current', 'coal_ratio', 'oil_pressure']
    y = df[col_y]
    X = df[cols_X]
    X, y = X, y
    X, y = construct_data(X, y, window=100, n_pred=5)
    print("dataset", X.shape, y.shape)

    ratio_tr = 0.95
    len_tr = int(y.shape[0]*ratio_tr)
    y_tr, X_tr = y[:len_tr], X[:len_tr]
    y_te, X_te = y[len_tr:], X[len_tr:]
    print("train set", X_tr.shape, y_tr.shape)
    print("test set", X_te.shape, y_te.shape)

    X_scaler = skpre.StandardScaler()
    X_tr = X_scaler.fit_transform(X_tr.reshape(-1, 2)).reshape(X_tr.shape)
    X_te = X_scaler.transform(X_te.reshape(-1, 2)).reshape(X_te.shape)
    y_scaler = skpre.StandardScaler()
    y_tr = y_scaler.fit_transform(y_tr.reshape(-1, 1))
    y_te = y_scaler.transform(y_te.reshape(-1, 1))

    n_epochs = 80
    lstm = LSTMBase(X_tr.shape[1:])
    hist = lstm.fit(X_tr, y_tr, n_epochs=n_epochs, validation_data=(X_te, y_te))

    plt.plot(hist['loss'].values, label='loss')
    plt.plot(hist['val_loss'].values, label='val_loss')
    plt.legend(loc='best')
    plt.savefig('{}/loss.png'.format(dir_out))
    plt.close()

    y_pred = lstm.model.predict(X_te)
    y_pred = y_scaler.inverse_transform(y_pred)
    y_te = y_scaler.inverse_transform(y_te)
    evaluate(y_te, y_pred, dir_out)







