from project.bikece.utils import tag_path, makedirs

from project.bikece.regr.current_20200225 import load
from project.bikece.regr.base import evaluate
from project.bikece.configure import DIR_CACHE

import numpy as np
import tensorflow as tf
import pandas as pd
import os
import matplotlib.pyplot as plt
import sklearn.preprocessing as skpre


class LSTMBase(object):
    def __init__(self, input_shape, target_size, verbose=1):
        self.input_shape = input_shape
        self.target_size = target_size
        self.verbose = verbose
        self.model = self.build_model(self.input_shape, self.target_size)
        if (self.verbose > 0):
            self.model.summary()

    def build_model(self, input_shape, target_size):
        model = tf.keras.models.Sequential()
        model.add(tf.keras.layers.LSTM(16, input_shape=input_shape, activation='relu'))
        # model.add(tf.keras.layers.LSTM(64, input_shape=input_shape))
        # model.add(tf.keras.layers.LSTM(32, activation='relu'))
        model.add(tf.keras.layers.Dense(32))
        model.add(tf.keras.layers.Dense(16))
        model.add(tf.keras.layers.Dense(target_size))
        return model

    def fit(self, x,
            y,
            batch_size=64,
            n_epochs=20,
            validation_data=None,
            shuffle=True,
            **kwargs):
        # self.model.compile(loss='mae', optimizer=tf.keras.optimizers.RMSprop())
        self.model.compile(loss='mae', optimizer=tf.keras.optimizers.Adam())
        if validation_data is None:
            hist = self.model.fit(
                x, y, batch_size=batch_size, epochs=n_epochs, verbose=self.verbose)
        else:
            hist = self.model.fit(
                x, y, batch_size=batch_size, epochs=n_epochs, verbose=self.verbose,
                validation_data=validation_data)

        return pd.DataFrame(hist.history)


def construct_data_event_process(X, y, target_size, window=1):
    pos_begin = window-1
    pos_end = X.shape[0]-1-target_size
    n_samples = (pos_end - pos_begin + 1) // target_size
    X_ret = np.zeros([n_samples, window, X.shape[1]])
    y_ret = np.zeros([n_samples, target_size])

    for s, t in zip(range(n_samples), range(pos_begin, pos_end+1, target_size)):
        X_ret[s] = X[(t - window + 1):(t + 1)]
        y_ret[s] = y[(t + 1):(t + target_size + 1)]

    return X_ret, y_ret


if __name__ == '__main__':
    tag = tag_path(os.path.abspath(__file__), 2)
    dir_out = makedirs(DIR_CACHE, tag)
    makedirs(dir_out)

    target_size = 5
    window = target_size*10

    df = load()
    df = df[2000:-3000]
    df = df[:5001]
    if df.shape[0]%target_size != 0:
        df = df[:(-(df.shape[0]%target_size))]

    col_y = 'current'
    cols_X = ['current', 'coal_ratio']
    # cols_X = ['current', 'coal_ratio', 'oil_pressure']
    y = df[col_y].values
    X = df[cols_X].values

    n_rounds = X.shape[0] // target_size
    ratio_tr = 0.9
    n_rounds_tr = int(n_rounds*ratio_tr)
    id_tr = target_size * n_rounds_tr
    X_scaler = skpre.StandardScaler().fit(X[:id_tr])
    X = X_scaler.transform(X)
    y_scaler = skpre.StandardScaler().fit(y[:id_tr].reshape(-1, 1))
    y = y_scaler.transform(y.reshape(-1, 1)).reshape(y.shape)

    X, y = construct_data_event_process(X, y, target_size, window=window)
    print("dataset", X.shape, y.shape)

    n_rounds_tr_pred = n_rounds_tr-window//target_size + 1
    y_tr, X_tr = y[:n_rounds_tr], X[:n_rounds_tr]
    y_te, X_te = y[n_rounds_tr:], X[n_rounds_tr:]
    print("train set", X_tr.shape, y_tr.shape)
    print("test set", X_te.shape, y_te.shape)

    n_epochs = 1000
    lstm = LSTMBase(X_tr.shape[1:], target_size)
    hist = lstm.fit(X_tr, y_tr, n_epochs=n_epochs, validation_data=(X_te, y_te))

    plt.plot(hist['loss'].values, label='loss')
    plt.plot(hist['val_loss'].values, label='val_loss')
    plt.legend(loc='best')
    plt.savefig('{}/loss.png'.format(dir_out))
    plt.close()

    y_pred = lstm.model.predict(X_te).ravel()
    y_te = y_te.ravel()
    y_pred = y_scaler.inverse_transform(y_pred)
    y_te = y_scaler.inverse_transform(y_te)
    evaluate(y_te, y_pred, dir_out, target_size)







