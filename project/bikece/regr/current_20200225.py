import pandas as pd
import os

from project.bikece.utils import tag_path, makedirs
from project.bikece.configure import DIR_DATA, DIR_CACHE

dict_cols_chinese2english = {
    '给煤机A给煤率': 'coal_ratio',
    '#5机磨煤机A 6kV侧电流': 'current',
    '磨煤机A入口一次风温度': 'in_temperature',
    '磨煤机A入口一次风压力': 'in_pressure',
    '磨煤机A出口风粉压力': 'out_pressure',
    '磨煤机A通风阻力': 'out_resistance',
    '磨煤机A入口一次风量（计算后）': 'in_wind',
    '磨煤机A一次风与密封风差压': 'diff_pressure',
    '磨煤机A液压站作用力油压': 'oil_pressure',
    '磨煤机A分离器风粉混合物温度': 'mix_temperature',
    '热一次风母管压力': 'main_pressure',
}


def load():
    df = pd.read_csv(os.path.join(DIR_DATA, 'current-pred-20200225.csv'))
    df.drop(columns=['Unnamed: 12'], inplace=True)
    df = df.rename(columns=dict_cols_chinese2english)
    df.fillna(method='ffill', inplace=True)
    return df


def eda(dir_log):
    df = pd.read_csv(os.path.join(DIR_DATA, 'current-pred-20200225.csv'))
    df.drop(columns=['Unnamed: 12'], inplace=True)
    df = df.rename(columns=dict_cols_chinese2english)
    df.isna().sum().to_csv(os.path.join(dir_log, 'isna.csv'))
    df.describe().to_csv(os.path.join(dir_log, 'df_describe.csv'))
    df_corr = df.corr()
    df_corr.to_csv(os.path.join(dir_log, 'corr.csv'))
    df.fillna(method='bfill', inplace=True)
    return df


if __name__ == '__main__':
    tag = tag_path(os.path.abspath(__file__), 2)
    dir_log = makedirs(os.path.join(DIR_CACHE, tag))

    # prepare data
    df = eda(dir_log)
    print(df.head())






