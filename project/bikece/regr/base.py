from sklearn import metrics
import matplotlib.pyplot as plt
import os
import numpy as np


def evaluate(y_true, y_pred, dir_out, targetsize=10):
    print("R2", metrics.r2_score(y_true, y_pred))
    print("RMSE", np.sqrt(metrics.mean_squared_error(y_true, y_pred)))
    print("MAE", metrics.mean_absolute_error(y_true, y_pred))

    y_pred_std_p = np.zeros_like(y_pred)
    y_pred_std_n = np.zeros_like(y_pred)
    for i in range(0, len(y_pred), targetsize):
        std = np.std(y_true[i:(i+targetsize)])
        y_pred_std_p[i:(i+targetsize)] = y_pred[i:(i+targetsize)] + std
        y_pred_std_n[i:(i+targetsize)] = y_pred[i:(i+targetsize)] - std

    y_true_std_p = np.zeros_like(y_true)
    y_true_std_n = np.zeros_like(y_true)
    k = 3
    for i in range(0, len(y_true), targetsize):
        std = np.std(y_true[i:(i+targetsize)])
        y_true_std_p[i:(i+targetsize)] = y_true[i:(i+targetsize)] + k*std
        y_true_std_n[i:(i+targetsize)] = y_true[i:(i+targetsize)] - k*std
    hit = (y_true_std_n <= y_pred) & (y_pred <= y_true_std_p)
    hit_rate = np.sum(hit) / len(hit)

    # targetsize=10
    # 1std: 0.5707865168539326
    # 2std: 0.8764044943820225
    # 3std: 0.9752808988764045
    # targetsize=5
    # 1std: 0.4943820224719101
    # 2std: 0.8157303370786517
    # 3std: 0.9483146067415731

    std = np.std(y_true)
    k = 2
    y_true_std_p = y_true + k*std
    y_true_std_n = y_true - k*std
    hit = (y_true_std_n <= y_pred) & (y_pred <= y_true_std_p)
    hit_rate = np.sum(hit) / len(hit)
    # 1std: 0.7123595505617978
    # 2std: 0.9573033707865168
    # 3std: 1.0

    plt.plot(y_true, label='y')
    plt.plot(y_pred, label='y_pred')
    plt.legend(loc='best')
    plt.savefig(os.path.join(dir_out, 'pre.png'))
    plt.clf()

    plt.plot(y_true, label='y')
    plt.plot(y_pred, label='y_pred')
    plt.plot(y_pred_std_p, label='y_pred+std', color='gray')
    plt.plot(y_pred_std_n, label='y_pred-std', color='gray')
    plt.legend(loc='best')
    plt.savefig(os.path.join(dir_out, 'pred_std.png'))