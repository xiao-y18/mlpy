from project.bikece.utils import tag_path, makedirs
from project.bikece.configure import DIR_DATA, DIR_CACHE

import numpy as np
import pandas as pd
import os
import matplotlib.pyplot as plt

from sklearn.linear_model import LinearRegression
from sklearn import metrics


dict_cols_chinese2english = {
    '给煤机A给煤率':'coal_ratio',
    '#5机磨煤机A 6kV侧电流':'current',
    '磨煤机A入口一次风温度':'in_temperature',
    '磨煤机A入口一次风压力': 'in_pressure',
    '磨煤机A出口风粉压力': 'out_pressure',
    '磨煤机A通风阻力': 'out_resistance',
    '磨煤机A入口一次风量（计算后）': 'in_wind',
    '磨煤机A一次风与密封风差压': 'diff_pressure',
    '磨煤机A液压站作用力油压': 'oil_pressure',
    '磨煤机A分离器风粉混合物温度': 'mix_temperature',
    '热一次风母管压力': 'main_pressure', '#5机磨煤机A电能脉冲量': 'momentum'
}

def load():
    dir_data = DIR_DATA
    tag = tag_path(os.path.abspath(__file__), 2)
    dir_out = os.path.join(DIR_CACHE, tag)

    ## load data
    file_data = 'new_A磨16.csv'
    df = pd.read_csv(
        os.path.join(dir_data, file_data),
        header=0, index_col=0, encoding="gbk")
    df = df.rename(columns=dict_cols_chinese2english)
    dir_out = makedirs(os.path.join(dir_out, file_data.split('.')[0]))

    df.isna().sum().to_csv(os.path.join(dir_out, 'isna.csv'))
    df.describe().to_csv(os.path.join(dir_out, 'df_describe.csv'))

    df.fillna(method='bfill', inplace=True)

    for c in df.columns:
        plt.plot(df[c].values[:1000])
        plt.savefig(os.path.join(dir_out, 'plot_{}.png'.format(c)))
        plt.close()

    df_corr = df.corr()
    df_corr.to_csv(os.path.join(dir_out, 'corr.csv'))
    for c in df.columns:
        if abs(df_corr['current'][c]) < 0.5:
            df.drop(columns=[c], inplace=True)

    return df

def evaluate(y_true, y_pred, dir_out):
    print("R2", metrics.r2_score(y_true, y_pred))
    print("RMSE", np.sqrt(metrics.mean_squared_error(y_true, y_pred)))
    print("MAE", metrics.mean_absolute_error(y_true, y_pred))

    plt.plot(y_true, label='y')
    plt.plot(y_pred, label='y_pred')
    plt.legend(loc='best')
    plt.savefig(os.path.join(dir_out, 'pre.png'))


if __name__ == '__main__':
    dir_data = DIR_DATA
    tag = tag_path(os.path.abspath(__file__), 2)
    dir_out = os.path.join(DIR_CACHE, tag)

    ## load data
    file_data = 'new_A磨16.csv'
    df = pd.read_csv(
        os.path.join(dir_data, file_data),
        header=0, index_col=0, encoding = "gbk")
    df = df.rename(columns=dict_cols_chinese2english)
    dir_out = makedirs(os.path.join(dir_out, file_data.split('.')[0]))

    df.isna().sum().to_csv(os.path.join(dir_out, 'isna.csv'))
    df.describe().to_csv(os.path.join(dir_out, 'df_describe.csv'))

    df.fillna(method = 'bfill', inplace=True)

    for c in df.columns:
        plt.plot(df[c].values[:1000])
        plt.savefig(os.path.join(dir_out, 'plot_{}.png'.format(c)))
        plt.close()

    df_corr = df.corr()
    df_corr.to_csv(os.path.join(dir_out, 'corr.csv'))
    for c in df.columns:
        if abs(df_corr['current'][c]) < 0.5:
            df.drop(columns=[c], inplace=True)

    ### model

    col_y = 'current'
    cols_X = list(df.columns)
    cols_X.remove(col_y)
    y = df[col_y]
    X = df[cols_X]

    ratio_tr = 0.9
    len_tr = int(y.shape[0]*ratio_tr)
    y_tr, X_tr = y[:len_tr], X[:len_tr]
    y_te, X_te = y[len_tr:], X[len_tr:]

    model = LinearRegression()
    model.fit(X_tr, y_tr)
    y_pred = model.predict(X_te)

    print("R2", metrics.r2_score(y_te, y_pred))
    print("RMSE", np.sqrt(metrics.mean_squared_error(y_te, y_pred)))
    print("MAE", metrics.mean_absolute_error(y_te, y_pred))

    plt.plot(y_te, label='y')
    plt.plot(y_pred, label='y_pred')
    plt.legend(loc='best')
    plt.savefig(os.path.join(dir_out, 'pre.png'))









