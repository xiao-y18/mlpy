from project.bikece.utils import tag_path, makedirs

from project.bikece.regr.base import evaluate
from project.bikece.regr.current_20200225 import load
from project.bikece.configure import DIR_CACHE

import numpy as np
import tensorflow as tf
import pandas as pd
import os
import matplotlib.pyplot as plt


class LSTMBase(object):
    def __init__(self, input_shape, verbose=1):
        self.input_shape = input_shape
        self.verbose = verbose
        self.model = self.build_model(self.input_shape)
        if (self.verbose > 0):
            self.model.summary()

    def build_model(self, input_shape):
        model = tf.keras.models.Sequential()
        model.add(tf.keras.layers.LSTM(12, input_shape=input_shape))
        # model.add(tf.keras.layers.Dense(256))
        model.add(tf.keras.layers.Dense(1))
        return model

    def fit(self, x,
            y,
            batch_size=256,
            n_epochs=20,
            validation_data=None,
            shuffle=True,
            **kwargs):
        self.model.compile(loss='mae', optimizer=tf.keras.optimizers.RMSprop())
        # self.model.compile(loss='mae', optimizer=tf.keras.optimizers.Adam())
        # self.model.compile(loss='mae', optimizer='adam')
        if validation_data is None:
            hist = self.model.fit(
                x, y, batch_size=batch_size, epochs=n_epochs, verbose=self.verbose)
        else:
            hist = self.model.fit(
                x, y, batch_size=batch_size, epochs=n_epochs, verbose=self.verbose,
                validation_data=validation_data)

        return pd.DataFrame(hist.history)


def construct_data(X, y, window=1):
    n_total = X.shape[0]
    n_samples = n_total-window+1
    X_ret = np.zeros([n_samples, window, X.shape[1]])
    y_ret = np.zeros(n_samples)
    for i, t in zip(range(n_samples), range(window-1, n_total)):
        X_ret[i] = X[(t-window+1):(t+1)]
        y_ret[i] = y[t]

    return X_ret, y_ret


if __name__ == '__main__':
    tag = tag_path(os.path.abspath(__file__), 2)
    dir_out = makedirs(DIR_CACHE, tag)

    df = load()
    df = df[2000:-3000]
    df = df[:5000]

    col_y = 'current'
    cols_X = ['coal_ratio', 'oil_pressure']
    y = df[col_y].values
    X = df[cols_X].values
    X, y = construct_data(X, y, window=10)

    ratio_tr = 0.90
    len_tr = int(y.shape[0]*ratio_tr)
    y_tr, X_tr = y[:len_tr], X[:len_tr]
    y_te, X_te = y[len_tr:], X[len_tr:]

    x_mean = np.mean(X_tr, axis=0)
    x_std = np.std(X_tr, axis=0)
    X_tr = (X_tr-x_mean) / x_std
    X_te = (X_te-x_mean) / x_std

    n_epochs = 1000
    lstm = LSTMBase(X_tr.shape[1:])
    hist = lstm.fit(X_tr, y_tr, n_epochs=n_epochs, validation_data=(X_te, y_te))

    plt.plot(hist['loss'].values, label='loss')
    plt.plot(hist['val_loss'].values, label='val_loss')
    plt.legend(loc='best')
    plt.savefig('{}/loss.png'.format(dir_out))
    plt.close()

    y_pred = lstm.model.predict(X_te)
    evaluate(y_te, y_pred, dir_out)







