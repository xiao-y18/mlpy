from project.bikece.configure import DIR_DATA, DIR_CACHE
from project.bikece.utils import tag_path, makedirs

import numpy as np
import pandas as pd
import os


if __name__ == '__main__':
    dir_data = DIR_DATA
    tag = tag_path(os.path.abspath(__file__), 2)
    dir_out = os.path.join(DIR_CACHE, tag)

    ## load data
    # file_data = '2012一次风机_CPLFL.csv'
    file_data = '2012一次风机_CNF1D_1.csv'
    df = pd.read_csv(
        os.path.join(dir_data, file_data),
        header=0, index_col=0, encoding = "gbk")
    dir_out = makedirs(os.path.join(dir_out, file_data.split('.')[0]))

    ## delete useless columns
    cols_useless = ['Unnamed: 0.1', 'Unnamed: 0_x', 'Unnamed: 0_y',
                    'Alarm_information', 'proposal', 'machinetestId']
    df.drop(columns=cols_useless, inplace=True)

    ## processing nan
    df.isna().sum().to_csv(os.path.join(dir_out, 'isna.csv'))
    for c in df.columns:
        if df[c].dtype == np.object:
            df[c].fillna("", inplace=True)
        else:
            df[c].fillna(-1, inplace=True)

    cols_location = ['Axis', 'Range', 'Type', 'LocationName', 'lN']
    cols_signal = [str(i) for i in np.arange(5, 1604 + 1)]

    ## concat location information
    def concat_location_info(row, cols_location=cols_location):
        strsum = ""
        for c in cols_location:
            strsum += str(row.loc[c])
        return strsum
    df['Location'] = df.apply(concat_location_info, axis=1)
    df.drop(columns=cols_location, inplace=True)

    cols_global = ['MachineTestID', 'MIDNominalSpeed', 'MIDID']
    cols_label = ['Fault_label', 'degree', 'diagnostic_analysis']

    ## aggregate signals
    locations = np.unique(df['Location'])
    n_locations = len(locations)
    dic_vec_id = {}
    for l, i in zip(locations, np.arange(n_locations)):
        dic_vec_id[l] = i
    def agg_signals(df):
        vec = np.zeros(n_locations)
        df_s = df[cols_signal+['Location']]
        df_s = df_s.groupby('Location').mean()
        mu = df_s.mean(axis=1)
        for k, v in zip(mu.index, mu.values):
            vec[dic_vec_id[k]] = v
        return vec

    df_group_by_time = df.groupby('time')
    count_unique_location_in_segment = df_group_by_time.apply(lambda df: len(np.unique(df['Location'])))
    print("unique location in a segment: ", np.unique(count_unique_location_in_segment))
    df_agg_signals = df_group_by_time.apply(agg_signals)
    df_agg_signals.name = 'Signals'
    count_signals = [len(v) for v in df_agg_signals.values]

    df_unique = df.copy()
    df_unique.drop(columns=(cols_signal+['Location']), inplace=True)
    df_time = df_unique['time'].drop_duplicates()
    df_unique = df_unique.loc[df_time.index, :]
    df_unique.index = df_unique['time']
    df_unique.drop(columns=['time'], inplace=True)
    df_unique.sort_index(inplace=True)

    df_model = df_unique.join(df_agg_signals)

    df_info = df_unique
    cols_info_serial = ['MIDNominalSpeed']
    with open(os.path.join(dir_out, 'df_info'), 'w') as f:
        f.write("******** Category variables: \n")
        for c in df_info.columns:
            if c not in cols_info_serial+['time']:
                values, counts = np.unique(df_info[c], return_counts=True)
                f.write("Variable: {} \n".format(c))
                f.write("Numbers: {} \n".format(len(values)))
                f.write("Values: {} \n".format(','.join([str(v) for v in values])))
                f.write("Counts: {} \n".format(','.join([str(c) for c in counts])))
                f.write("\n\n")
        f.write("\n\n\n")
        f.write("******** Serial variablles")
        for c in cols_info_serial:
            f.write("Variable: {} \n".format(c))
            f.write(df[c].describe().to_string())
            f.write("\n\n")
        f.write("\n\n\n")


