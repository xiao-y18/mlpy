from project.bikece.configure import DIR_DATA, DIR_CACHE
from project.bikece.utils import tag_path, split_stratified, makedirs

import numpy as np
import pandas as pd
import os
from sklearn import preprocessing

from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import LinearSVC, SVC
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
StandardClassifierDic = {
    'C45': DecisionTreeClassifier(),
    'NaiveBayes': GaussianNB(),
    'LR': LogisticRegression(),
    'LSVC': LinearSVC(),
    # 'QSVC': SVC(kernel='poly', degree=2), too slow
    'MLP': MLPClassifier(),
    'RF10': RandomForestClassifier(),
    'RF500': RandomForestClassifier(n_estimators=500),
    'RF1000': RandomForestClassifier(n_estimators=1000), # add by myself to test whether the increase of n_estimators will lead to the increase of acc.
    '5NN': KNeighborsClassifier(),
    '1NN': KNeighborsClassifier(n_neighbors=1)
}

from time import time
from sklearn import metrics
def classify(model, x_tr, y_tr, x_te, y_te):
    ## train
    t_start = time()
    model.fit(x_tr, y_tr)
    time_tr = time() - t_start
    acc_tr = metrics.accuracy_score(y_tr, model.predict(x_tr))
    ## test
    t_start = time()
    y_pred = model.predict(x_te)
    acc_te = metrics.accuracy_score(y_te, y_pred)
    time_te = time() - t_start

    print(metrics.classification_report(y_te, y_pred))

    return [acc_tr, acc_te], [time_tr, time_te]


if __name__ == '__main__':
    dir_data = DIR_DATA
    dir_out = os.path.join(DIR_CACHE, tag_path(os.path.abspath(__file__), 2))

    ## load data
    # file_data = '2012一次风机_CPLFL.csv'
    file_data = '2012一次风机_CNF1D_1.csv'
    df = pd.read_csv(
        os.path.join(dir_data, file_data),
        header=0, index_col=0, encoding="gbk")
    dir_out = makedirs(os.path.join(dir_out, file_data.split('.')[0]))

    ## delete useless columns
    cols_useless = ['Unnamed: 0.1', 'Unnamed: 0_x', 'Unnamed: 0_y',
                    'Alarm_information', 'proposal', 'machinetestId']
    df.drop(columns=cols_useless, inplace=True)

    ## processing nan
    df.isna().sum().to_csv(os.path.join(dir_out, 'isna.csv'))
    for c in df.columns:
        if df[c].dtype == np.object:
            df[c].fillna("", inplace=True)
        else:
            df[c].fillna(-1, inplace=True)

    cols_location = ['Axis', 'Range', 'Type', 'LocationName', 'lN']
    cols_signal = [str(i) for i in np.arange(5, 1604 + 1)]


    ## concat location information
    def concat_location_info(row, cols_location=cols_location):
        strsum = ""
        for c in cols_location:
            strsum += str(row.loc[c])
        return strsum


    df['Location'] = df.apply(concat_location_info, axis=1)
    df.drop(columns=cols_location, inplace=True)

    cols_global = ['MachineTestID', 'MIDNominalSpeed', 'MIDID']
    cols_label = ['Fault_label', 'degree', 'diagnostic_analysis']

    ## aggregate signals
    locations = np.unique(df['Location'])
    n_locations = len(locations)
    dic_vec_id = {}
    for l, i in zip(locations, np.arange(n_locations)):
        dic_vec_id[l] = i


    def agg_signals(df):
        vec = np.zeros(n_locations)
        df_s = df[cols_signal + ['Location']]
        df_s = df_s.groupby('Location').mean()
        mu = df_s.mean(axis=1)
        for k, v in zip(mu.index, mu.values):
            vec[dic_vec_id[k]] = v
        return vec


    df_group_by_time = df.groupby('time')
    count_unique_location_in_segment = df_group_by_time.apply(
        lambda df: len(np.unique(df['Location'])))
    print("unique location in a segment: ", np.unique(count_unique_location_in_segment))
    df_agg_signals = df_group_by_time.apply(agg_signals)
    df_agg_signals.name = 'Signals'
    count_signals = [len(v) for v in df_agg_signals.values]

    df_unique = df.copy()
    df_unique.drop(columns=(cols_signal + ['Location']), inplace=True)
    df_time = df_unique['time'].drop_duplicates()
    df_unique = df_unique.loc[df_time.index, :]
    df_unique.index = df_unique['time']
    df_unique.drop(columns=['time'], inplace=True)
    df_unique.sort_index(inplace=True)

    df_model = df_unique.join(df_agg_signals)

    #### naive model
    # X = df_model[['MachineTestID', 'MIDID', 'MIDNominalSpeed']].values
    X = np.vstack(df_model['Signals'].values)
    # X = np.hstack([df_model[['MachineTestID', 'MIDID', 'MIDNominalSpeed']].values, np.vstack(df_model['Signals'].values)])
    y = df_model['Fault_label'].values


    # ## binary classification, normal vs. all fault
    # filter = (y >= 0)
    # y[filter] = 1

    # ## binary class, one fault vs. other
    # labels = list(np.unique(y))
    # labels.remove(-1)
    # i_label = 0 # 0 to 4
    # print("********** Identify label = {}".format(labels[i_label]))
    # filter = ((y == labels[i_label]) | (y == -1))
    # X = X[filter]
    # y = y[filter]

    ## fault classification
    filter_fault = (y>=0)
    X = X[filter_fault]
    y = y[filter_fault]
    # identify specific fault
    labels = list(np.unique(y))
    i_label = 4 # 0 to 4
    print("********** Identify label = {}".format(labels[i_label]))
    filter = (y == labels[i_label])
    y[filter] = 1
    y[~filter] = 0

    label_encoder = preprocessing.LabelEncoder().fit(np.unique(y))
    y = label_encoder.transform(y)

    inds = np.arange(len(y))
    np.random.shuffle(inds)
    X = X[inds]
    y = y[inds]
    ratio_tr = 0.7
    X_tr, y_tr, X_te, y_te = split_stratified(X, y, ratio_tr)

    print("Train set")
    values, counts = np.unique(y_tr, return_counts=True)
    print(values)
    print(counts)
    print("Test set")
    values, counts = np.unique(y_te, return_counts=True)
    print(values)
    print(counts)

    for name, model in StandardClassifierDic.items():
        print("******** Model {}".format(name))
        acc, t = classify(model, X_tr, y_tr, X_te, y_te)
        print(acc, t)



