import os
import sys
import shutil
from datetime import datetime
import logging
import numpy as np
from sklearn.utils.validation import column_or_1d


class AvgrageMeter(object):
    """copy from https://github.com/quark0/darts/blob/master/cnn/utils.py"""
    def __init__(self):
        self.reset()

    def reset(self):
        self.avg = 0
        self.sum = 0
        self.cnt = 0

    def update(self, val, n=1):
        self.sum += val * n
        self.cnt += n
        self.avg = self.sum / self.cnt


def get_strftime_now(fmt='%Y%m%d_%H%M'):
    return datetime.now().strftime(fmt)


""" path operations """


def makedirs(path, clean=False):
    if clean:
        if os.path.exists(path) is True:
            shutil.rmtree(path)

    if os.path.exists(path) is False:
        os.makedirs(path)

    return path


def makedirs_clean(path):
    if os.path.exists(path) is True:
        shutil.rmtree(path)
    os.makedirs(path)
    return path


def path_split(path):
    folders = []
    while 1:
        path, folder = os.path.split(path)

        if folder != "":
            folders.append(folder)
        else:
            if path != "":
                folders.append(path)
            break
    folders.reverse()
    return folders


def tag_path(path, nback=1):
    """
    example:
        tag_path(os.path.abspath(__file__), 1) # return file name
    :param path:
    :param nback:
    :return:
    """
    folders = path_split(path)
    nf = len(folders)

    assert nback >= 1, "nback={} should be larger than 0.".format(nback)
    assert nback <= nf, "nback={} should be less than the number of folder {}!".format(nback, nf)

    tag = folders[-1].split('.')[0]
    if nback > 0:
        for i in range(2, nback+1):
            tag = folders[-i] + '_' + tag
    return tag


""" log settings """

LOG_FORMAT = '%(asctime)s, %(levelname)s, %(name)s: %(message)s'
LOG_DATE_FORMAT = '%m/%d %I:%M:%S %p'


def set_logging(tag, dir_log=None):
    logging.basicConfig(stream=sys.stdout, filemode='w', level=logging.INFO, format=LOG_FORMAT, datefmt=LOG_DATE_FORMAT)
    log = logging.getLogger(tag)
    if dir_log is not None:
        fh = logging.FileHandler(os.path.join(dir_log, 'log.txt'))
        fh.setFormatter(logging.Formatter(fmt=LOG_FORMAT, datefmt=LOG_DATE_FORMAT))
        log.addHandler(fh)
    return log


"""data operations"""


def distribute_dataset(X, y):
    y = column_or_1d(y, warn=True)
    res = {}
    for id in np.unique(y):
        res[id] = X[y==id]
    return res


def shuffle_dataset(X, y=None):
    n = X.shape[0]
    inds = np.arange(n)
    np.random.shuffle(inds)
    if y is None:
        return y[inds]
    else:
        return X[inds], y[inds]


def split_stratified(X, y, ratio):
    distr = distribute_dataset(X, y)
    X_left, y_left = [], []
    X_right, y_right = [], []

    for key, X_batch in distr.items():
        n = X_batch.shape[0]
        i = int(np.ceil(n * ratio))
        if i == X_batch.shape[0]:  # at least split one sample to X_right/y_right that usually is a test set.
            i = i-1
        X_left.append(X_batch[:i])
        y_left.extend([key]*i)
        X_right.append(X_batch[i:])
        y_right.extend([key]*(n-i))

    X_left, y_left = np.vstack(X_left), np.array(y_left)
    X_right, y_right = np.vstack(X_right), np.array(y_right)

    X_left, y_left = shuffle_dataset(X_left, y_left)
    X_right, y_right = shuffle_dataset(X_right, y_right)

    return X_left, y_left, X_right, y_right