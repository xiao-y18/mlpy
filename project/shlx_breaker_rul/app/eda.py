"""
    EDA
"""

import os


from project.shlx_breaker_rul.lib.utils import tag_path, makedirs
from project.shlx_breaker_rul.configure import DIR_CACHE, DIR_DATA, set_logging
from project.shlx_breaker_rul.lib.data import plot_raw_data_machine
from project.shlx_breaker_rul.lib.data import plot_switch_signals_machine


if __name__ == '__main__':
    tag = tag_path(os.path.abspath(__file__), 2)
    log_dir = makedirs(os.path.join(DIR_CACHE, tag))
    log = set_logging(tag, log_dir)

    machine = '3#ToExcel'

    log_dir_m = makedirs(os.path.join(log_dir, machine))
    log.info(f"=========================== plot raw data for machine {machine} ===========================")
    data_dir_m = os.path.join(DIR_DATA, machine)
    plot_raw_data_machine(data_dir_m, log, makedirs(os.path.join(log_dir_m, 'raw_data')))

    log.info(f"=========================== plot switch signals for machine {machine} ===========================")
    data_dir_m = os.path.join(DIR_DATA, machine)
    plot_switch_signals_machine(data_dir_m, log, makedirs(os.path.join(log_dir_m, 'switch_signals')))




