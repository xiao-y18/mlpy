import os


def makedirs(path):
    if os.path.exists(path) is False:
        os.makedirs(path)
    return path


def path_split(path):
    folders = []
    while 1:
        path, folder = os.path.split(path)

        if folder != "":
            folders.append(folder)
        else:
            if path != "":
                folders.append(path)
            break
    folders.reverse()
    return folders


def tag_path(path, nback=1):
    """
    example:
        tag_path(os.path.abspath(__file__), 1) # return file name
    :param path:
    :param nback:
    :return:
    """
    folders = path_split(path)
    nf = len(folders)

    assert nback >= 1, "nback={} should be larger than 0.".format(nback)
    assert nback <= nf, "nback={} should be less than the number of folder {}!".format(nback, nf)

    tag = folders[-1].split('.')[0]
    if nback > 0:
        for i in range(2, nback+1):
            tag = folders[-i] + '_' + tag
    return tag


