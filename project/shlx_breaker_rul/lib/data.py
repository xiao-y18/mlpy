import os

import pandas as pd
import matplotlib.pyplot as plt


DATA_FIELDS = dict(
    tpc=['IA', 'IB', 'IC'],  # Three-Phase Current
    tpv=['UA', 'UB', 'UC'],  # Three-phase Voltage
    vbp=['UA', 'UB', 'UC']   # Voltage Between Phases
)


def load_cycle_df(file):
    """加载一个 cycle 文件，比如'2021年04月15日08时11分07.55秒 第1次.xlsx'"""
    return pd.read_excel(file)


def load_cycle_files(root_dir):
    """加载一次全生命周期的数据，即一个文件夹下的所有 cycles，按时间升序"""
    return sorted(
        [f for f in os.listdir(root_dir) if not f.startswith('~') and f.endswith('.xlsx')])
    # return sorted(
    #     [os.path.join(root_dir, f) for f in os.listdir(root_dir) if not f.startswith('~') and f.endswith('.xlsx')])


def find_switch_signals(df, threshold_I=2, threshold_U=5):
    # 输入dataframe，输出元组（合闸开始点，合闸结束点，分闸开始点，分闸结束点）
    # 若输入数据存在问题导致找不到合闸或分闸过程，则返回False
    switch_on_start, switch_on_end, switch_off_start, switch_off_end = [False] * 4

    # 粗略区分合闸与分闸
    # 找到连续10个电流值大于200的点，向后500个点
    for i in range(0, df.shape[0] - 10):
        if all(df['IA'][i:i + 10].abs() > 200):
            running = i + 500
            break

    # 合闸
    for i in reversed(range(10, running)):
        if all(df['IA'][i - 9:i + 1].abs() < threshold_I):
            switch_on_start = i
            break

    if not switch_on_start: return False
    for i in range(switch_on_start, running):
        if all(df['UA'][i:i + 5].abs() < threshold_U):
            switch_on_end = i
            break

    # 分闸
    for i in range(running, df.shape[0] - 10):
        if all(df['IA'][i:i + 10].abs() < threshold_I):
            switch_off_end = i
            break

    if not switch_off_end: return False
    for i in reversed(range(running, switch_off_end)):
        if all(df['UA'][i - 4:i + 1].abs() < threshold_U):
            switch_off_start = i
            break

    if switch_on_start and switch_on_end and switch_off_start and switch_off_end:
        return switch_on_start, switch_on_end, switch_off_start, switch_off_end
    else:
        return False


def plot_switch_signals(file, save_dir=None, is_show=False, margin_rate=10):
    df = load_cycle_df(file)
    switch_signals = find_switch_signals(df)

    if switch_signals:
        switch_on_start, switch_on_end, switch_off_start, switch_off_end = switch_signals
    else:
        print(f'file error : file={file}, switch_signals={switch_signals}')
        return

    plt.figure(figsize=(12, 8))

    plt.subplot(2, 1, 1)
    margin = round((switch_on_end - switch_on_start) * margin_rate) + 1
    plt.plot(df['IA'][switch_on_start - margin: switch_on_end + margin], label='IA')
    plt.plot(df['UA'][switch_on_start - margin: switch_on_end + margin], label='UA')
    plt.axvline(x=switch_on_start, ls="--", c="red")
    plt.axvline(x=switch_on_end, ls="--", c="red")
    plt.legend()

    plt.subplot(2, 1, 2)
    margin = round((switch_off_end - switch_off_start) * margin_rate) + 1
    plt.plot(df['IA'][switch_off_start - margin: switch_off_end + margin], label='IA')
    plt.plot(df['UA'][switch_off_start - margin: switch_off_end + margin], label='UA')
    plt.axvline(x=switch_off_start, ls="--", c="red")
    plt.axvline(x=switch_off_end, ls="--", c="red")
    plt.legend()

    if is_show:
        plt.show()
    if save_dir is not None:
        os.makedirs(save_dir, exist_ok=True)
        save_path = os.path.join(save_dir, f'{os.path.splitext(os.path.basename(file))[0]}.png')
        plt.savefig(save_path, bbox_inches='tight')
    plt.close()


def plot_switch_signals_machine(data_path, log, log_dir):
    cycle_files = load_cycle_files(data_path)
    for f in cycle_files:
        log.info(f"processing {f}.")
        plot_switch_signals(os.path.join(data_path, f), save_dir=log_dir, margin_rate=5)


def plot_raw_data_machine(data_path, log, log_dir):
    cycle_files = load_cycle_files(data_path)
    for f in cycle_files:
        log.info(f"processing {f}.")
        df = load_cycle_df(os.path.join(data_path, f))
        for key, columns in DATA_FIELDS.items():
            fig = plt.figure(figsize=(20, 5))
            for c in columns:
                plt.plot(df[c], label=c)
            plt.legend(loc='best')
            plt.tight_layout()
            plt.savefig(os.path.join(log_dir, f'{f}_{key}.png'))
            plt.close()
