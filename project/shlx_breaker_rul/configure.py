import os
import logging
import sys

from project import shlx_breaker_rul

PROJECT_NAME = os.path.basename(os.path.dirname(shlx_breaker_rul.__file__))
DIR_ROOT = os.path.dirname(shlx_breaker_rul.__file__)
DIR_CACHE = os.path.join(DIR_ROOT, 'cache')
DIR_DATA = os.path.join(os.path.dirname(DIR_ROOT), 'dataset', PROJECT_NAME)

LOG_FORMAT = '%(asctime)s, %(levelname)s, %(name)s: %(message)s'
LOG_DATE_FORMAT = '%m/%d %I:%M:%S %p'

N_PROCESSES = max(os.cpu_count() - 2, 1)  # the maximum number of processes used for a multiprocessing pool.


def set_logging(tag, dir_log=None):
    logging.basicConfig(stream=sys.stdout, filemode='w', level=logging.INFO, format=LOG_FORMAT, datefmt=LOG_DATE_FORMAT)
    log = logging.getLogger(tag)
    if dir_log is not None:
        fh = logging.FileHandler(os.path.join(dir_log, 'log.txt'))
        fh.setFormatter(logging.Formatter(fmt=LOG_FORMAT, datefmt=LOG_DATE_FORMAT))
        log.addHandler(fh)
    return log

