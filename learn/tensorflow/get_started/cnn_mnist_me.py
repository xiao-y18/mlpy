# reference mnist_deep

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import argparse
import sys
import tempfile

from tensorflow.examples.tutorials.mnist import input_data

import tensorflow as tf

FLAGS = None


def main(_):
    # load data
    mnist = input_data.read_data_sets(FLAGS.data_dir, one_hot=True)

    # model
    # placeholder
    x = tf.placeholder(tf.float32, [None, 784])
    y_true = tf.placeholder(tf.float32, [None, 10])

    # cnn
    with tf.name_scope('reshape'):
        x_image = tf.reshape(x, [-1, 28, 28, 1])
    with tf.name_scope('conv1'):
        conv1 = tf.layers.conv2d(
            inputs=x_image,
            filters=32,
            kernel_size=[5, 5],
            strides=[1, 1],
            padding='same',
            activation=tf.nn.relu,
            bias_initializer=tf.constant_initializer(0.1))
    with tf.name_scope('pool1'):
        pool1 = tf.layers.max_pooling2d(
            inputs=conv1, pool_size=[2, 2], strides=2)
    with tf.name_scope('conv2'):
        conv2 = tf.layers.conv2d(
            inputs=pool1,
            filters=64,
            kernel_size=[5, 5],
            strides=[1, 1],
            padding='same',
            activation=tf.nn.relu,
            bias_initializer=tf.constant_initializer(0.1))

    with tf.name_scope('pool2'):
        pool2 = tf.layers.max_pooling2d(
            inputs=conv2, pool_size=[2, 2], strides=2)
    # flatten
    pool2_flat = tf.reshape(pool2, [-1, 7 * 7 * 64])
    with tf.name_scope('fc1'):
        fc1 = tf.layers.dense(
            inputs=pool2_flat, units=1024, activation=tf.nn.relu)
    with tf.name_scope('dropout'):
        keep_prob = tf.placeholder(tf.float32)
        dropout = tf.layers.dropout(fc1, rate=keep_prob)
    with tf.name_scope('fc2'):
        y_pre = tf.layers.dense(dropout, units=10)

    # loss
    with tf.name_scope('loss'):
        cross_entropy = tf.nn.softmax_cross_entropy_with_logits(
            labels=y_true, logits=y_pre)
        cross_entropy = tf.reduce_mean(cross_entropy)

    # optimizer
    with tf.name_scope('adam_optimizer'):
        train_op = tf.train.AdamOptimizer(learning_rate=1e-4).minimize(
            cross_entropy)

    # accuracy
    with tf.name_scope('accuracy'):
        correct_prediction = tf.equal(
            tf.argmax(y_true, 1), tf.argmax(y_pre, 1))
        correct_prediction = tf.cast(correct_prediction, tf.float32)
        accuracy = tf.reduce_mean(correct_prediction)

    # make saving directory
    graph_location = tempfile.mkdtemp()
    print('Saving graph to: %s' % graph_location)
    train_writer = tf.summary.FileWriter(graph_location)
    train_writer.add_graph(tf.get_default_graph())

    # train
    batch_size = 50
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    with tf.Session(config=config) as sess:
        tf.global_variables_initializer().run()
        # train
        for i in range(20000):
            batch = mnist.train.next_batch(batch_size)
            if i % 100 == 0:
                train_accuracy = sess.run(accuracy,
                                          feed_dict={
                                              x: batch[0],
                                              y_true: batch[1],
                                              keep_prob: 1.0
                                          })
                print('step %d, training accuracy %g' % (i, train_accuracy))
            _ = sess.run(train_op,
                         feed_dict={
                             x: batch[0],
                             y_true: batch[1],
                             keep_prob: 0.4
                         })
        # test
        test_accuracy = sess.run(accuracy,
                                 feed_dict={
                                     x: mnist.test.images,
                                     y_true: mnist.test.labels,
                                     keep_prob: 1.0
                                 })
        print('test accuracy %g' % test_accuracy)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', type=str,
                        default='/tmp/dataset_fl/images/mnist',
                        help='Directory for storing input data.')
    FLAGS, unparsed = parser.parse_known_args()
    tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)
