"""
    reference: https://zhuanlan.zhihu.com/p/32887066
"""

import tensorflow as tf
import os
import shutil
from tensorflow.python.framework import graph_util
from tensorflow.python.platform import gfile

pb_file_path = os.getcwd()

def operate_typically(modelname='pd_typical'):
    def save(modelname):
        with tf.Session(graph=tf.Graph()) as sess:
            # set up variables
            x = tf.placeholder(tf.int32, name='x')
            y = tf.placeholder(tf.int32, name='y')
            b = tf.Variable(1, name='b')
            xy = tf.multiply(x, y)
            op = tf.add(xy, b, name='op_to_store')
            # initialize
            sess.run(tf.global_variables_initializer())
            # test op
            print(sess.run(op, feed_dict={x: 10, y: 3}))
            # convert variables to constants, note: name can't include device number.
            constant_graph = graph_util.convert_variables_to_constants(sess, sess.graph_def,
                                                                       [op.name.split(':')[0]])
            # write to pb file
            with tf.gfile.FastGFile(os.path.join(pb_file_path, '{}.pb'.format(modelname)), mode='wb') as f:
                f.write(constant_graph.SerializeToString())
    def load(modelname):
        sess = tf.Session()
        # load from pb file
        with gfile.FastGFile(os.path.join(pb_file_path, '{}.pb'.format(modelname)), mode='rb') as f:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())
            sess.graph.as_default()
            tf.import_graph_def(graph_def, name='') # import to current default `Graph`.
        # test constant variable
        print(sess.run('b:0'))
        # get variables
        input_x = sess.graph.get_tensor_by_name('x:0')
        input_y = sess.graph.get_tensor_by_name('y:0')
        op = sess.graph.get_tensor_by_name('op_to_store:0')
        # test op
        ret = sess.run(op, feed_dict={input_x:5, input_y:5})
        print(ret)
        sess.close()

    print()
    save(modelname)
    load(modelname)

def operate_savemodel(modelname='pd_savedmodel'):
    def save(modelname):
        with tf.Session(graph=tf.Graph()) as sess:
            # set up variables
            x = tf.placeholder(tf.int32, name='x')
            y = tf.placeholder(tf.int32, name='y')
            b = tf.Variable(1, name='b')
            xy = tf.multiply(x, y)
            op = tf.add(xy, b, name='op_to_store')
            # initialize
            sess.run(tf.global_variables_initializer())
            # test op
            print(sess.run(op, feed_dict={x: 10, y: 3}))
            # check directory
            if os.path.exists(os.path.join(pb_file_path, modelname)):
                shutil.rmtree(os.path.join(pb_file_path, modelname))
            # save model by SavedModel
            builder = tf.saved_model.builder.SavedModelBuilder(os.path.join(pb_file_path, modelname))
            builder.add_meta_graph_and_variables(sess, ['cpu_1']) # additional info
            builder.save()
    def load(modelname):
        with tf.Session(graph=tf.Graph()) as sess:
            # load model
            tf.saved_model.loader.load(sess, ['cpu_1'], os.path.join(pb_file_path, modelname))
            # get variables
            input_x = sess.graph.get_tensor_by_name('x:0')
            input_y = sess.graph.get_tensor_by_name('y:0')
            op = sess.graph.get_tensor_by_name('op_to_store:0')
            # test op
            ret = sess.run(op, feed_dict={input_x:5, input_y:5})
            print(ret)
    save(modelname)
    load(modelname) # note, the variables directory below model path can't be removed.


if __name__ == '__main__':
    operate_typically()
    # operate_savemodel()
    # it is note that, the checkpoint file also can be loaded without recovering architecture.




