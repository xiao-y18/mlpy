"""
    Source: https://github.com/RobRomijnders/AE_ts
    
    There are two reasons author mentioned for choosing LSTM:
    1. The length of time series may vary from sample to sample. Conventional techniques only work 
        on inputs of fixed size.
    2. The patterns in time series can have arbitrary time span and be non stationary. 
        The recurrent neural network can learn patterns in arbitrary time scaling. 
        The convolutional net, however, assumes only stationary patterns
        
"""
from mlpy.data import ucr
from mlpy.data.iterator import DataIterator
from mlpy.lib.tfops.base import *

import tensorflow as tf
import os
import argparse
import matplotlib.pyplot as plt
from sklearn.decomposition import TruncatedSVD
from sklearn.manifold import TSNE
import numpy as np

tf_conf = tf.ConfigProto()
tf_conf.gpu_options.allow_growth = True


class TSAutoEncoder(object):
    def __init__(self, config):
        self.n_layers = config['n_layers']
        self.n_units = config['n_units']
        self.n_latent = config['n_latent']
        self.lr = config['lr']
        self.max_grad_norm = config['max_grad_norm']
        self.n_coordinates = config['n_coordinates']
        self.length = config['length']

        self.x = tf.placeholder(tf.float32, [None, self.length], 'x')
        self.batch_size = tf.shape(self.x)[0]
        self.x_expand = tf.expand_dims(self.x, axis=1)
        self.keep_prob = tf.placeholder(tf.float32, name='keep_prob')

        with tf.variable_scope('Encoder'):
            cells_enc = tf.nn.rnn_cell.MultiRNNCell(
                [tf.nn.rnn_cell.LSTMCell(self.n_units) for _ in range(self.n_layers)])
            cells_enc = tf.nn.rnn_cell.DropoutWrapper(cells_enc, output_keep_prob=self.keep_prob)
            initial_state_enc = cells_enc.zero_state(self.batch_size, tf.float32)
            outputs_enc, _ = tf.nn.static_rnn(
                cells_enc, tf.unstack(self.x_expand, axis=2), initial_state=initial_state_enc)
            ## Embedding to get latent representation
            W_mu = tf.get_variable('W_mu', [self.n_units, self.n_latent])
            b_mu = tf.get_variable('b_mu', [self.n_latent])
            out_enc = outputs_enc[-1] # [batch_size, n_units]
            self.z_mu = tf.nn.xw_plus_b(out_enc, W_mu, b_mu, name='z_mu') # [batch_size, n_latent]
            # Train the point in latent space to have zero-mean and unit-variance on batch basis
            mean_latent, var_latent = tf.nn.moments(self.z_mu, axes=[1])
            # (variance - 1) insure variance converge to 1 from right side (positive side).
            # - tf.log(variance) insure variance converge to 1 from left side (negative, side).
            self.loss_latent = tf.reduce_mean(
                tf.square(mean_latent) + (var_latent - 1) - tf.log(var_latent))

        with tf.variable_scope('Latent2Decoder'):
            W_state = tf.get_variable('W_state', [self.n_latent, self.n_units])
            b_state = tf.get_variable('b_state', [self.n_units])
            z_state = tf.nn.xw_plus_b(self.z_mu, W_state, b_state, name='z_state') # [batch_size, n_units]

        with tf.variable_scope('Decoder'):
            cells_dec = tf.nn.rnn_cell.MultiRNNCell(
                [tf.nn.rnn_cell.LSTMCell(self.n_units) for _ in range(self.n_layers)]
            )
            inputs_dec = [tf.zeros([self.batch_size, 1])] * self.length
            initial_state_dec = tuple([(z_state, z_state)] * self.n_layers)
            outputs_dec, _ = tf.nn.static_rnn(
                cells_dec, inputs_dec, initial_state=initial_state_dec) # [batch_size, n_units] * length

        with tf.variable_scope('Output'):
            outputs = tf.concat(outputs_dec, axis=0) # [length*batch_size, n_units]
            n_params_out = 2 * self.n_coordinates # Number of coordinates + variances
            W_out = tf.get_variable('W_out', [self.n_units, n_params_out])
            b_out = tf.get_variable('b_out', [n_params_out])
            out = tf.nn.xw_plus_b(outputs, W_out, b_out) # [length*batch_size, n_params_out]
            mu_out, sigma_log_out = tf.unstack(
                tf.reshape(out, [self.length, self.batch_size, n_params_out]), axis=2)
            sigma_out = tf.exp(sigma_log_out)
            dist = tf.distributions.Normal(mu_out, sigma_out)
            px = dist.log_prob(tf.transpose(self.x)) # [length, batch_size]
            loss_seq = -px
            self.loss_seq = tf.reduce_mean(loss_seq)

        with tf.variable_scope('Train'):
            # using learning rate decay
            global_step = tf.Variable(0, trainable=False)
            lr_decay = tf.train.exponential_decay(self.lr, global_step, 1000, 0.1, staircase=False)

            self.loss = self.loss_seq + self.loss_latent

            trvars = tf.trainable_variables()
            grads = tf.gradients(self.loss, trvars)
            grads, _ = tf.clip_by_global_norm(grads, self.max_grad_norm)

            optimizer = tf.train.AdadeltaOptimizer(lr_decay)
            self.opt = optimizer.apply_gradients(zip(grads, trvars), global_step=global_step)

        # make summaries
        with tf.name_scope('Gradients'):
            for grad, var in zip(grads, trvars):
                tf.summary.histogram(var.name + '/gradients', grad)
                tf.summary.histogram(var.name, var)
        tf.summary.tensor_summary('latent_state', self.z_mu)
        tf.summary.scalar('lr', lr_decay)
        tf.summary.scalar('loss', self.loss)
        tf.summary.scalar('global_step', global_step)

        self.merged = tf.summary.merge_all()
        self.saver = tf.train.Saver()


def tran(config, x_tr, x_val, dir_log):
    n_epochs = config['n_epochs']
    batch_size = config['batch_size']
    plot_freq = config['plot_freq']
    keep_prob = config['keep_prob']

    model = TSAutoEncoder(config)
    data_tr = DataIterator(x_tr)
    data_val = DataIterator(x_val)
    with tf.Session(config=tf_conf) as sess:
        tf.global_variables_initializer().run()
        tb_writer = tf.summary.FileWriter(dir_log, sess.graph)
        n_steps_per_epoch = data_tr.n_samples // batch_size
        n_steps = n_epochs * n_steps_per_epoch
        g_step = 0
        for epoch in range(n_epochs):
            for _ in range(n_steps_per_epoch):
                x_batch = data_tr.next_batch(batch_size)
                result_tr = sess.run(
                    [model.loss, model.loss_seq, model.loss_latent, model.opt],
                    feed_dict={model.x: x_batch, model.keep_prob: keep_prob}
                )
                g_step += 1
                if (g_step % plot_freq == 0) or (g_step == n_steps):
                    result_val = sess.run(
                        [model.loss, model.loss_seq, model.loss_latent, model.merged],
                        feed_dict={model.x: data_val.next_batch(batch_size), model.keep_prob: 1.0}
                    )
                    tb_writer.add_summary(result_val[3], g_step)
                    # tb_writer.flush()
                    print("{}/{}, train ({:.4}, {:.4}, {:.4}), val ({:.4}, {:.4}, {:.4})".format(
                        g_step, n_steps,
                        result_tr[0], result_tr[1], result_tr[2],
                        result_val[0], result_val[1], result_val[2]
                    ))
        tb_writer.close()
        save_checkpoints(model.saver, sess, dir_log, g_step)


def vis_latent(config, x_val, y_val, dir_log):
    batch_size = config['batch_size']
    model = TSAutoEncoder(config)
    with tf.Session(config=tf_conf) as sess:
        loaded, counter = load_checkpoints(model.saver, sess, dir_log)
        if loaded is False:
            raise Exception("Model can't be found, please train model first!")
        n_val = x_val.shape[0]
        z_mu = []
        i_start = 0
        while i_start + batch_size <= n_val:
            inds = range(i_start, i_start + batch_size)
            z_mu_batch = sess.run(model.z_mu, feed_dict={model.x:x_val[inds], model.keep_prob:1.0})
            z_mu.append(z_mu_batch)
            i_start += batch_size
        if i_start < n_val:
            z_mu_batch = sess.run(model.z_mu, feed_dict={model.x:x_val[i_start::], model.keep_prob:1.0})
            z_mu.append(z_mu_batch)
        z_mu = np.concatenate(z_mu, axis=0)

    f1, axes = plt.subplots(2, 1)
    pca_model = TruncatedSVD()
    z_mu_reduced = pca_model.fit_transform(z_mu)
    axes[0].scatter(z_mu_reduced[:, 0], z_mu_reduced[:, 1], c=y_val, marker='*', linewidths=0)
    axes[0].set_title('PCA')
    tsne_model = TSNE(verbose=2, perplexity=80, min_grad_norm=1E-12, n_iter=3000)
    z_mu_tsne = tsne_model.fit_transform(z_mu)
    axes[1].scatter(z_mu_tsne[:,0], z_mu_tsne[:,1], c=y_val, marker='*', linewidths=0)
    axes[1].set_title('tSNE')
    plt.subplots_adjust(hspace=0.5)
    plt.savefig(os.path.join(dir_log, 'z_mu.png'))


if __name__ == '__main__':
    dir_data_root = '../../mlpy/dataset/UCR_TS_Archive_2015'
    data_name = 'ECG5000'
    dir_log = 'cache/'
    if os.path.exists(dir_log) is False:
        os.makedirs(dir_log)

    x_tr, y_tr, x_val, y_val, n_classes = ucr.load_ucr_flat(
        data_name, dir_data_root, encode_label=True)
    n_tr = x_tr.shape[0]
    length = x_tr.shape[1]

    config = dict()
    config['n_layers'] = 2 # number of layers of stacked RNN's
    config['n_units'] = 90 # memory cells in a layer
    config['n_latent'] = 20 # number of units in the latent space
    config['batch_size'] = 64
    config['lr'] = .005
    config['max_grad_norm'] = 5 # maximum gradient norm during training
    config['n_coordinates'] = 1 # hyper-parameter for future generalization
    config['length'] = length # length of sequence
    config['n_epochs'] = 16
    config['keep_prob'] = 0.8
    config['plot_freq'] = 50

    parser = argparse.ArgumentParser(description=__file__)
    parser.add_argument('mode', type=str,
                        help="'train' or 'vis_latent'")
    FLAGS, unparsed = parser.parse_known_args()
    mode = FLAGS.mode
    if mode == 'train':
        tran(config, x_tr, x_val, dir_log)
    elif mode == 'vis_latent':
        vis_latent(config, x_val, y_val, dir_log)
    else:
        raise ValueError("Can't find mode={}".format(mode))
























