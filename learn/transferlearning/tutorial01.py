"""
Tensorflow official tutorial (based keras):
    https://www.tensorflow.org/tutorials/images/transfer_learning#download_data_-_cats_and_dogs_filteredzip
"""

from __future__ import absolute_import, division, print_function, unicode_literals
import os
import tensorflow as tf
from tensorflow import keras
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

from mlpy.lib.utils import tag_path

DIR_DATA = '/Users/huangfanling/workspace/bitbucket/mlpy/mlpy/dataset/image'

def download_file():
    zip_file = tf.keras.utils.get_file(
        origin="https://storage.googleapis.com/mledu-datasets/cats_and_dogs_filtered.zip",
        fname="{}/cats_and_dogs_filtered.zip".format(DIR_DATA), extract=True)
    print("Finish and save to {}".format(zip_file))


if __name__ == '__main__':
    dir_out = 'cache/{}'.format(tag_path(os.path.abspath(__file__), 1))
    dir_out = os.path.join(os.path.split(os.path.abspath(__file__))[0], dir_out)
    if os.path.exists(dir_out) is False:
        os.makedirs(dir_out)

    ## train data
    base_dir = os.path.join(DIR_DATA, 'cats_and_dogs_filtered')

    ## prepare training and validation cats and dogs datasets
    train_dir = os.path.join(base_dir, 'train')
    validation_dir = os.path.join(base_dir, 'validation')
    train_cats_dir = os.path.join(train_dir, 'cats')
    print('Total training cat images:', len(os.listdir(train_cats_dir)))
    train_dogs_dir = os.path.join(train_dir, 'dogs')
    print('Total training dog images:', len(os.listdir(train_dogs_dir)))
    validation_cats_dir = os.path.join(validation_dir, 'cats')
    print('Total validation cat images:', len(os.listdir(validation_cats_dir)))
    validation_dogs_dir = os.path.join(validation_dir, 'dogs')
    print('Total validation dog images:', len(os.listdir(validation_dogs_dir)))

    ## create image data generator with image augmentation
    image_size = 160  # All images will be resized to 160x160
    batch_size = 32
    # Rescale all images by 1./255 and apply image augmentation
    train_datagen = keras.preprocessing.image.ImageDataGenerator(rescale=1. / 255)
    validation_datagen = keras.preprocessing.image.ImageDataGenerator(rescale=1. / 255)
    train_generator = train_datagen.flow_from_directory(
        train_dir, target_size=(image_size, image_size), batch_size=batch_size, class_mode='binary'
    )
    validation_generator = validation_datagen.flow_from_directory(
        validation_dir, target_size=(image_size, image_size), batch_size=batch_size, class_mode='binary'
    )

    ## Create the base model from the pre-trained convnets
    IMG_SHAPE = (image_size, image_size, 3)
    base_model = tf.keras.applications.MobileNet(input_shape=IMG_SHAPE,
                                                 include_top=False,
                                                 weights='imagenet')
    base_model.summary()
    # add a classification head
    model = tf.keras.models.Sequential([
        base_model,
        keras.layers.GlobalAveragePooling2D(),
        keras.layers.Dense(1, activation='sigmoid')
    ])
    # training parameters
    epochs = 10
    steps_per_epoch = train_generator.n // batch_size
    validation_steps = validation_generator.n // batch_size

    ## Feature extraction
    # freeze the convolutional base
    base_model.trainable = False
    # compile the model
    model.compile(optimizer=tf.keras.optimizers.RMSprop(lr=0.0001),
                  loss='binary_crossentropy',
                  metrics=['accuracy'])
    model.summary()
    print(len(model.trainable_variables))
    # train the model
    history = model.fit_generator(train_generator,
                                  steps_per_epoch=steps_per_epoch,
                                  epochs=epochs,
                                  workers=4,
                                  validation_data=validation_generator,
                                  validation_steps=validation_steps)
    # log result
    acc = history.history['acc']
    val_acc = history.history['val_acc']
    loss = history.history['loss']
    val_loss = history.history['val_loss']
    plt.figure(figsize=(8,8))
    plt.subplot(2, 1, 1)
    plt.plot(acc, label='Training Accuracy')
    plt.plot(val_acc, label='Validation Accuracy')
    plt.legend(loc='lower right')
    plt.ylabel('Accuracy')
    plt.ylim([min(plt.ylim()), 1])
    plt.title('Training and Validation Accuracy')
    plt.subplot(2, 1, 2)
    plt.plot(loss, label='Training Loss')
    plt.plot(val_loss, label='Validation Loss')
    plt.legend(loc='lower right')
    plt.ylabel('Loss')
    plt.ylim([0, max(plt.ylim())])
    plt.title('Training and Validation Loss')
    plt.savefig(os.path.join(dir_out, 'metric.png'))

    ## Fine tuning
    base_model.trainable = True
    print("Number of layers in the base model: ", len(base_model.layers))
    fine_tune_at = 100 # fine tune from this layer onwards
    # freeze all the layers before the `fine_tune_at` layer
    for layer in base_model.layers[:fine_tune_at]:
        layer.trainable = False
    # compile the model using a much-lower training rate
    model.compile(optimizer=tf.keras.optimizers.RMSprop(lr=2e-5),
                  loss='binary_crossentropy',
                  metrics=['accuracy'])
    model.summary()
    print(len(model.trainable_variables))
    # continue train the model: if you trained to convergence earlier, this will get you few percent more accuracy.
    history_fine = model.fit_generator(train_generator,
                                       steps_per_epoch=steps_per_epoch,
                                       epochs=epochs,
                                       workers=4,
                                       validation_data=validation_generator,
                                       validation_steps=validation_steps)
    acc += history_fine.history['acc']
    val_acc += history_fine.history['val_acc']
    loss += history_fine.history['loss']
    val_loss += history_fine.history['val_loss']
    plt.figure(figsize=(8, 8))
    plt.subplot(2, 1, 1)
    plt.plot(acc, label='Training Accuracy')
    plt.plot(val_acc, label='Validation Accuracy')
    plt.ylim([0.9, 1])
    plt.plot([epochs - 1, epochs - 1], plt.ylim(), label='Start Fine Tuning')
    plt.legend(loc='lower right')
    plt.title('Training and Validation Accuracy')
    plt.subplot(2, 1, 2)
    plt.plot(loss, label='Training Loss')
    plt.plot(val_loss, label='Validation Loss')
    plt.ylim([0, 0.2])
    plt.plot([epochs - 1, epochs - 1], plt.ylim(), label='Start Fine Tuning')
    plt.legend(loc='upper right')
    plt.title('Training and Validation Loss')
    plt.savefig(os.path.join(dir_out, 'finetune_metric.png'))


