- [ ] The primitive work to propose GCN (tensorflow): https://github.com/tkipf/gcn
- [ ] A keras realization: https://github.com/tkipf/gcn
- [ ] [Spektral](https://github.com/danielegrattarola/spektral), a cool library for graph deep learning build on Tensorflow
To read:
- [ ] https://tkipf.github.io/graph-convolutional-networks/