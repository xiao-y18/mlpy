from learn.oneshot.model import Siamese
import os


if __name__ == '__main__':
    dataset_path = '../../dataset/image/omniglot'
    use_augmentation = True
    lr = 10e-4
    batch_size = 32
    # Learning rate multipliers for each layer
    lr_multipliers = {}
    lr_multipliers['conv1'] = 1
    lr_multipliers['conv2'] = 1
    lr_multipliers['conv3'] = 1
    lr_multipliers['conv4'] = 1
    lr_multipliers['dense1'] = 1
    # L2 regularization penalization for each layer
    l2_regular = {}
    l2_regular['conv1'] = 1e-2
    l2_regular['conv2'] = 1e-2
    l2_regular['conv3'] = 1e-2
    l2_regular['conv4'] = 1e-2
    l2_regular['dense1'] = 1e-4
    # Path where the logs will be saved
    log_path = 'cache/'
    if os.path.exists(log_path) is False:
        os.makedirs(log_path)

    # set up model
    input_shape = [105, 105, 1]
    siames = Siamese(input_shape, lr, lr_multipliers, l2_regular, log_path)

    # train model
    final_momentum = 0.9 # Final layer-wise momentum (mu_j in paper)
    momentum_slop = 0.01 # linear epoch slope evolution
    support_set_size = 20
    evaluate_each = 1000
    n_iterations = 1000000
    acc_valid = siames.train(
        dataset_path=dataset_path, use_augmentation=use_augmentation,
        batch_size=batch_size, n_iterations=n_iterations,
        support_set_size=support_set_size, final_momentum=final_momentum,
        momentum_slop=momentum_slop, evaluate_each=evaluate_each,
        model_name='siamese_net_lr10e-4')

    if acc_valid == 0:
        acc_eval = 0
    else:
        siames.model.load_weights(os.path.join(log_path, 'siamese_net_lr10e-4.h5'))
        acc_eval = siames.loader.one_shot_test(
            siames.model, 20, 40, False)
    print("Final Evaluation Accuracy: {}".format(acc_eval))
