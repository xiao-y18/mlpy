import keras
import keras.backend as K
import os
from .modified_sgd import Modified_SGD
from .load import LoaderOmniglot

class Siamese:
    def __init__(self, input_shape, lr, lr_multipliers, l2_regular, log_path):
        self.input_shape = input_shape
        self.lr = lr
        self.lr_multipliers = lr_multipliers
        self.momentum = 0.5
        self.l2_regular = l2_regular
        self.log_path = log_path
        # input
        self.img1 = keras.layers.Input(self.input_shape)
        self.img2 = keras.layers.Input(self.input_shape)
        # build model
        self.model = self.build_model()


    def build_model(self):
        conv_net = keras.models.Sequential()
        conv_net.add(keras.layers.Conv2D(
            filters=64, kernel_size=(10, 10),
            input_shape=self.input_shape,
            activation='relu',
            kernel_regularizer=keras.regularizers.l2(self.l2_regular['conv1']),
            name='conv1'))
        conv_net.add(keras.layers.MaxPool2D())
        conv_net.add(keras.layers.Conv2D(
            filters=128, kernel_size=(7, 7),
            activation='relu',
            kernel_regularizer=keras.regularizers.l2(self.l2_regular['conv2']),
            name='conv2'))
        conv_net.add(keras.layers.MaxPool2D())
        conv_net.add(keras.layers.Conv2D(
            filters=128, kernel_size=(4, 4),
            activation='relu',
            kernel_regularizer=keras.regularizers.l2(self.l2_regular['conv3']),
            name='conv3'))
        conv_net.add(keras.layers.MaxPool2D())
        conv_net.add(keras.layers.Conv2D(
            filters=256, kernel_size=(4, 4),
            activation='relu',
            kernel_regularizer=keras.regularizers.l2(self.l2_regular['conv4']),
            name='conv4'))
        conv_net.add(keras.layers.Flatten())
        conv_net.add(keras.layers.Dense(
            units=4096, activation='sigmoid',
            kernel_regularizer=keras.regularizers.l2(self.l2_regular['dense1']),
            name='dense1'
        ))

        # encode pair input images
        img1_encoded = conv_net(self.img1)
        img2_encoded = conv_net(self.img2)

        # customize a layer to calculate the L1 siamese dist,
        # in which Lambda can wrap arbitrary expression as a Layer Object.
        l1_distance_layer = keras.layers.Lambda(lambda tensors: K.abs(tensors[0] - tensors[1]))
        l1_distance = l1_distance_layer([img1_encoded, img2_encoded])

        # last output layer
        pred = keras.layers.Dense(units=1, activation='sigmoid')(l1_distance)

        # compile model
        model = keras.models.Model(inputs=[self.img1, self.img2], outputs=pred)
        optimizer = Modified_SGD(
            lr=self.lr, lr_multipliers=self.lr_multipliers, momentum=self.momentum)
        model.compile(
            loss='binary_crossentropy', metrics=['binary_accuracy'], optimizer=optimizer)

        return model


    def train(self,
              dataset_path, use_augmentation, batch_size,
              n_iterations, support_set_size,
              final_momentum, momentum_slop,
              evaluate_each, model_name):
        """
        
        :param n_iterations: 
        :param support_set_size: 
        :param final_momentum: 
        :param momentum_slop: 
        :param evaluate_each: 
        :param model_name: 
        :return: 
        """
        log = {'iter':[], 'loss':[], 'acc':[], 'acc_valid':[]}
        self.loader = LoaderOmniglot(dataset_path, use_augmentation, batch_size)
        self.loader.split_train_datasets()

        best_acc_valid = 0
        best_iter = 0
        for i in range(n_iterations):
            images, labels = self.loader.get_train_batch()
            loss_tr, acc_tr = self.model.train_on_batch(images, labels)
            # Decay learning rate 1% per 500 iterations (in the paper the decay is
            # 1% per epoch). Also update linearly the momentum (starting from 0.5 to 1)
            if (i + 1) % 500 == 0:
                K.set_value(self.model.optimizer.lr, K.get_value(self.model.optimizer.lr * 0.99))
            if K.get_value(self.model.optimizer.momentum) < final_momentum:
                K.set_value(self.model.optimizer.momentum,
                            K.get_value(self.model.optimizer.momentum) + momentum_slop)
            print("[{}/{}] loss:{}, acc:{}, lr:{}".format(
                i, n_iterations, loss_tr, acc_tr, K.get_value(self.model.optimizer.lr)))

            # perform one-shot task and log performance metrics
            if (i + 1) % evaluate_each == 0:
                n_runs_per_alphabet = 40
                acc_valid = self.loader.one_shot_test(
                    self.model, support_set_size, n_runs_per_alphabet, is_validation=True)
                log['iter'].append(i)
                log['loss'].append(loss_tr)
                log['acc'].append(acc_tr)
                log['acc_valid'].append(acc_valid)
                print("acc_valid:{}".format(acc_valid))

                # Some hyperparameters lead to 100%, although the output is almost the same in all images.
                if(acc_valid == 1.0 and acc_tr == 0.5):
                    print('Early Stopping: Gradient Explosion')
                    print('Validation Accuracy = ' + str(best_acc_valid))
                    return 0
                elif acc_tr == 0.0:
                    return 0
                else: # save model
                    if acc_valid > best_acc_valid:
                        best_acc_valid = acc_valid
                        best_iter = i
                        model_json = self.model.to_json()
                        with open(os.path.join(self.log_path, model_name+'.json'), 'w') as f:
                            f.write(model_json)
                        self.model.save_weights(os.path.join(self.log_path, model_name+'.h5'))
            # early stop
            if (i - best_iter) > 10000:
                print('Early Stopping: validation accuracy did not increase for 10000 iterations')
                print('Best Validation Accuracy = {}'.format(best_acc_valid))
                break

        print("Trained Ended!")
        return best_acc_valid




















