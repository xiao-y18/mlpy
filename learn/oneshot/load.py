from .image_augmentor import ImageAugmentor

import os
import random
import numpy as np
import math
from PIL import Image


class LoaderOmniglot:
    def __init__(self, dataset_path, use_augmentation, batch_size):
        self.dataset_path = dataset_path
        self.train_folder = 'images_background'
        self.evaluation_folder = 'images_evaluation'
        self.train_dictionary = self.load(os.path.join(self.dataset_path, self.train_folder))
        self.evaluation_dictionary = self.load(os.path.join(self.dataset_path, self.evaluation_folder))
        self.image_height = 105
        self.image_width = 105
        self.number_of_images_for_each_character = 20
        self.batch_size = batch_size
        self.use_augmentation = use_augmentation
        if self.use_augmentation:
            self.image_augmentor = self.create_augment()
        else:
            self.use_augmentation = None
        self._train_alphabets = []
        self._validation_alphabets =[]
        self._evaluation_alphabets = []
        self._index_in_train_alphabets = 0
        self._index_in_validation_alphabets = 0
        self._index_in_evaluation_alphabets = 0
        self._maximum_index_in_train_alphabet = 0


    def load(self, path):
        """
        
        :param path: path to save alphabets 
        :return: a nested dictionary to save the folder names of characters,  which can be indexed 
            by [alphabet][character] then get a list of the names of image examples.
        """
        res = {}
        for alphabet in os.listdir(path):
            alphabet_path = os.path.join(path, alphabet)
            alphabet_dictionary = {}
            for character in os.listdir(alphabet_path):
                character_path = os.path.join(alphabet_path, character)
                alphabet_dictionary[character] = os.listdir(character_path)
            res[alphabet] = alphabet_dictionary
        return res

    def create_augment(self):
        rotation_range = [-15, 15]
        shear_range = [-0.3 * 180 / math.pi, 0.3 * 180 / math.pi]
        zoom_range = [0.8, 2]
        shift_range = [5, 5]

        return ImageAugmentor(0.5, shear_range, rotation_range, shift_range, zoom_range)

    def split_train_datasets(self):
        """ 80% - 20% split
        
        :return: 
        """
        alphabets = np.array(list(self.train_dictionary.keys()))
        n_alphabets = len(alphabets)
        indexes = np.arange(n_alphabets)
        random.shuffle(indexes)
        n_train = int(0.8 * n_alphabets)
        indexes_train = indexes[:n_train]
        indexes_valid = indexes[n_train:]
        self._train_alphabets = alphabets[indexes_train]
        self._validation_alphabets = alphabets[indexes_valid]
        self._evaluation_alphabets = np.array(list(self.evaluation_dictionary.keys()))
        self._maximum_index_in_train_alphabet = n_train - 1

    def convert_pair_path_list_to_images(self, pair_path_list):
        """
        
        :param pair_path_list: 
        :return: list images, images[0] and images[1] correspond to pairwise input channels.
        """
        n_pairs = len(pair_path_list)
        images = [np.zeros((n_pairs, self.image_height, self.image_width, 1)) for _ in range(2)]
        for i_pair in range(n_pairs):
            pair_path = pair_path_list[i_pair]
            for j_input in range(2):
                image = Image.open(pair_path[j_input])
                image = np.array(image).astype(np.float64)
                image = image / image.std() - image.mean()
                images[j_input][i_pair, :, :, 0] = image
        return images

    def get_train_batch(self):
        """sample a batch images on the same alphabet that contain a set of characters.
        
        :return: 
        """
        alphabet = self._train_alphabets[self._index_in_train_alphabets]
        characters = np.array(list(self.train_dictionary[alphabet].keys()))
        n_characters = len(characters)

        batch_images_path = []
        labels = []
        selected_characters_indexes = [random.randint(0, n_characters-1)
                                    for _ in range(self.batch_size)]
        for index in selected_characters_indexes:
            # 1. random sample 3 images on the selected character
            # 2. two of them form a positive sample
            # 3. the one another connect with an image from a different character to form a negative sample
            character = characters[index]
            images = self.train_dictionary[alphabet][character]
            image_path = os.path.join(self.dataset_path, self.train_folder, alphabet, character)
            # sample 3 images
            image_indexes = random.sample(range(0, self.number_of_images_for_each_character), 3)
            # add a positive sample, pair images from the same character, label with 1
            batch_images_path.append((
                os.path.join(image_path, images[image_indexes[0]]),
                os.path.join(image_path, images[image_indexes[1]])
            ))
            labels.append(1)
            # random sample an image that differ from the selected character
            diff_index = random.sample(range(0, n_characters), 1)
            while(diff_index == index):
                diff_index = random.sample(range(0, n_characters), 1)
            diff_character = characters[diff_index[0]]
            diff_images = self.train_dictionary[alphabet][diff_character]
            diff_image_path = os.path.join(self.dataset_path, self.train_folder, alphabet, diff_character)
            diff_image_index = random.sample(range(0, self.number_of_images_for_each_character), 1)
            # add a negative sample, pair images from different character, label with 0
            batch_images_path.append((
                os.path.join(image_path, images[image_indexes[2]]),
                os.path.join(diff_image_path, diff_images[diff_image_index[0]])
            ))
            labels.append(0)
        self._index_in_train_alphabets += 1
        if self._index_in_train_alphabets > self._maximum_index_in_train_alphabet:
            self._index_in_train_alphabets = 0

        # load images
        batch_images = self.convert_pair_path_list_to_images(batch_images_path)
        if self.use_augmentation:
            batch_images = self.image_augmentor.get_random_transform(batch_images)
        labels = np.array(labels)
        return batch_images, labels

    def get_one_shot_batch(self, support_set_size, is_validation):
        if is_validation:
            alphabets = self._validation_alphabets
            alphabet_index = self._index_in_validation_alphabets
            dictionary = self.train_dictionary
            image_folder = self.train_folder
        else:
            alphabets = self._evaluation_alphabets
            alphabet_index = self._index_in_evaluation_alphabets
            dictionary = self.evaluation_dictionary
            image_folder = self.evaluation_folder

        alphabet = alphabets[alphabet_index]
        characters = list(dictionary[alphabet].keys())
        n_characters = len(characters)

        if support_set_size == -1 or support_set_size > n_characters:
            support_set_size = n_characters

        batch_images_path = []
        labels = []
        # sample the test character and its image
        # and add a positive sample
        test_character_index = random.sample(range(0, n_characters), 1)
        test_character = characters[test_character_index[0]]
        test_image_path = os.path.join(self.dataset_path, image_folder, alphabet, test_character)
        test_images = dictionary[alphabet][test_character]
        test_image_indexes = random.sample(range(0, self.number_of_images_for_each_character), 2)
        test_image = os.path.join(test_image_path, test_images[test_image_indexes[0]])
        # add a positive sample
        support_image = os.path.join(test_image_path, test_images[test_image_indexes[1]])
        batch_images_path.append((test_image, support_image))
        labels.append(1)

        # construct support_set_size-1 negative samples
        diff_characters_indexes = list(np.arange(n_characters))
        diff_characters_indexes.remove(test_character_index)
        support_characters_indexes = random.sample(diff_characters_indexes, support_set_size-1)
        for character_index in support_characters_indexes:
            character = characters[character_index]
            images = dictionary[alphabet][character]
            image_path = os.path.join(self.dataset_path, image_folder, alphabet, character)
            image_indexes = random.sample(range(0, self.number_of_images_for_each_character), 1)
            # add a negative sample
            image = os.path.join(image_path, images[image_indexes[0]])
            batch_images_path.append((test_image, image))
            labels.append(0)

        # load images
        batch_images = self.convert_pair_path_list_to_images(batch_images_path)
        labels = np.array(labels)
        return batch_images, labels

    def one_shot_test(self, model, support_set_size, number_of_tasks_per_alphabet, is_validation):
        """ Prepare one-shot task and evaluate its performance.
        
        Make one shot task in validation and evaluation sets
        if support_set_size = -1 we perform a N-Way one-shot task with
        N being the total of characters in the alphabet

        :param model: 
        :param support_set_size: 
        :param number_of_tasks_per_alphabet: 
        :param is_validation: 
        :return: mean accuracy for the one-shot task
        """
        if is_validation:
            alphabets = self._validation_alphabets
            print("\nMaking One Shot Task validation alphabets: ")
        else:
            alphabets = self._evaluation_alphabets
            print("\nMaking One Shot Task evaluation alphabets: ")

        mean_global_accuracy = 0
        for alphabet in alphabets:
            mean_alphabet_accuracy = 0
            for _ in range(number_of_tasks_per_alphabet):
                # a batch images
                # first pair is positive while the rest is negative.
                images, _ = self.get_one_shot_batch(support_set_size, is_validation)
                probs = model.predict_on_batch(images)
                if np.argmax(probs) == 0 and probs.std() > 0.01:
                    # the additional condition based on the notice that sometimes the outputs
                    # of the classifier was almost the same in all images.
                    accuracy = 1.0
                else:
                    accuracy = 0.0
                mean_alphabet_accuracy += accuracy
                mean_global_accuracy += accuracy
            if is_validation:
                self._index_in_validation_alphabets += 1
            else:
                self._index_in_evaluation_alphabets += 1

            mean_alphabet_accuracy /= number_of_tasks_per_alphabet
            print("{} alphabet, accuracy: {}".format(alphabet, mean_alphabet_accuracy))

        mean_global_accuracy /= (len(alphabets) * number_of_tasks_per_alphabet)
        print("\nMean global accuracy: {}".format(mean_global_accuracy))

        if is_validation:
            self._index_in_validation_alphabets = 0
        else:
            self._index_in_evaluation_alphabets = 0

        return mean_global_accuracy


