"""
    refer to https://autokeras.com/tutorial/image_classification/
"""

import autokeras as ak
import os
import pandas as pd

from mlpy.lib.utils import tag_path, makedirs
from mlpy.data import ucr
from mlpy.data.utils import split_stratified
from research.autotsc.base import DIR_DATASET_UCR15, DIR_LOG, BIG_DATASETS, arry_extend_dim


def get_model(type_block, project_name):
    max_trials = 4
    input_node = ak.Input()
    # input_node = ak.ImageInput()
    if type_block == 'dense':
        # 2(batchnorm)*3(dropout)*3(layers)*7(units) = 126 possibilities
        output_node = ak.DenseBlock()(input_node)
        max_trials = 30  # ~1/4 * 126
        # max_trials = 2 # TODO: test
    elif type_block == 'rnn':
        # 2(bidirectional)*2(layer_type)*3(num_layers) = 12 possibilities
        # Note: #units = feature_size = shape[-1] = vec_len = #dimensionality of TS (=1 for univariate TS)
        output_node = ak.RNNBlock()(input_node)
        max_trials = 3
    elif type_block == 'conv':
        # 3(kernel_size)*3(num_blocks)*2(num_layers)*2(separable)*2(max_pooling)*3(dropout)*6(filters)
        # = 1296 possibilities
        output_node = ak.ConvBlock()(input_node)
        max_trials = 300
    elif type_block == 'xception':
        # 2(pretrained)*1(version)*2(imagenet_size) = 4 possibilities
        output_node = ak.XceptionBlock()(input_node)
        max_trials = 2
    elif type_block == 'efficientnet':
        # 8(version)*2(pretrained)*1(version)*2(imagenet_size) = 32 possibilities
        output_node = ak.EfficientNetBlock()(input_node)
        max_trials = 8
    elif type_block == 'resnet_v1':
        # 3(version)*2(pretrained)*1(version)*2(imagenet_size) = 16 possibilities
        output_node = ak.ResNetBlock(version='v1')(input_node)
        max_trials = 4
    elif type_block == 'resnet_v2':
        # 3(version)*2(pretrained)*1(version)*2(imagenet_size) = 16 possibilities
        output_node = ak.ResNetBlock(version="v2")(input_node)
        max_trials = 4
    else:
        raise ValueError("type_block={} can not be found !".format(type_block))
    output_node = ak.ClassificationHead()(output_node)
    clf = ak.AutoModel(
        inputs=input_node,
        outputs=output_node,
        overwrite=True,
        max_trials=max_trials,
        project_name=project_name)

    return clf


def run(model_name, data_name, dir_log, extend_dim=1):
    x_train, y_train, x_test, y_test, n_classes = ucr.load_ucr_flat(data_name, DIR_DATASET_UCR15)

    x_train = arry_extend_dim(x_train, extend_dim)
    x_test = arry_extend_dim(x_test, extend_dim)

    clf = get_model(model_name, dir_log)

    ratio = 0.8  # corresponding to the default value in clf.fit
    x_train, y_train, x_valid, y_valid = split_stratified(x_train, y_train, ratio)
    batch_size = min(16, x_train.shape[0], x_valid.shape[0])
    epochs = 1000
    clf.fit(x_train, y_train, batch_size=batch_size, validation_data=(x_valid, y_valid), epochs=epochs)

    loss, acc = clf.evaluate(x_test, y_test)

    return loss, acc


if __name__ == '__main__':
    tag = tag_path(os.path.abspath(__file__), 1)
    model_name = 'resnet_v1'
    extend_dim = 2  # set according to the model_name, e.g. 1 for dense, 2 for xception.
    dir_log = os.path.join(DIR_LOG, tag, model_name)
    makedirs(dir_log)

    res = {'dataset': [], 'loss': [], 'acc': []}
    for data_name in BIG_DATASETS:
        dir_log_data = os.path.join(dir_log, data_name)
        makedirs(dir_log_data)

        loss, acc = run(model_name, data_name, dir_log_data, extend_dim)
        res['dataset'].append(data_name)
        res['loss'].append(loss)
        res['acc'].append(acc)

    res = pd.DataFrame(res)
    res.to_csv(os.path.join(dir_log, 'res.csv'), index=False)











