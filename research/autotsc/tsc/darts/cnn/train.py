import os
import sys
import time
import glob
import numpy as np
import torch
import utils
import logging
import argparse
import torch.nn as nn
import genotypes
import torch.utils
import torchvision.datasets as dset
import torch.backends.cudnn as cudnn

from torch.autograd import Variable
from model import NetworkCIFAR as Network

from mlpy.lib.utils import tag_path, makedirs
from mlpy.data import ucr
from mlpy.data.iterator import DataIterator
from mlpy.data.utils import split_stratified
from research.autotsc.base import DIR_DATASET_UCR15, DIR_LOG, BIG_DATASETS, arry_extend_dim

parser = argparse.ArgumentParser("cifar")
parser.add_argument('--data', type=str, default='../data', help='location of the data corpus')
# parser.add_argument('--batch_size', type=int, default=96, help='batch size')
parser.add_argument('--batch_size', type=int, default=16, help='batch size')
parser.add_argument('--learning_rate', type=float, default=0.025, help='init learning rate')
parser.add_argument('--momentum', type=float, default=0.9, help='momentum')
parser.add_argument('--weight_decay', type=float, default=3e-4, help='weight decay')
parser.add_argument('--report_freq', type=float, default=50, help='report frequency')
parser.add_argument('--gpu', type=int, default=0, help='gpu device id')
parser.add_argument('--epochs', type=int, default=600, help='num of training epochs')
# parser.add_argument('--init_channels', type=int, default=36, help='num of init channels')
parser.add_argument('--init_channels', type=int, default=6, help='num of init channels')
parser.add_argument('--layers', type=int, default=20, help='total number of layers')
parser.add_argument('--model_path', type=str, default='saved_models', help='path to save the model')
parser.add_argument('--auxiliary', action='store_true', default=False, help='use auxiliary tower')
parser.add_argument('--auxiliary_weight', type=float, default=0.4, help='weight for auxiliary loss')
parser.add_argument('--cutout', action='store_true', default=False, help='use cutout')
parser.add_argument('--cutout_length', type=int, default=16, help='cutout length')
parser.add_argument('--drop_path_prob', type=float, default=0.2, help='drop path probability')
parser.add_argument('--save', type=str, default='EXP', help='experiment name')
parser.add_argument('--seed', type=int, default=0, help='random seed')
parser.add_argument('--arch', type=str, default='DARTS', help='which architecture to use')
parser.add_argument('--grad_clip', type=float, default=5, help='gradient clipping')
args = parser.parse_args()

args.save = 'eval-{}-{}'.format(args.save, time.strftime("%Y%m%d-%H%M%S"))
utils.create_exp_dir(args.save, scripts_to_save=glob.glob('*.py'))

log_format = '%(asctime)s %(message)s'
logging.basicConfig(stream=sys.stdout, level=logging.INFO,
                    format=log_format, datefmt='%m/%d %I:%M:%S %p')
fh = logging.FileHandler(os.path.join(args.save, 'log.txt'))
fh.setFormatter(logging.Formatter(log_format))
logging.getLogger().addHandler(fh)


def main(data_name):
    if not torch.cuda.is_available():
        logging.info('no gpu device available')
        sys.exit(1)

    # data preparation

    extend_dim = 2
    x_train, y_train, x_test, y_test, n_classes = ucr.load_ucr_flat(data_name, DIR_DATASET_UCR15)
    x_train = arry_extend_dim(x_train, extend_dim)
    x_test = arry_extend_dim(x_test, extend_dim)
    x_train = np.transpose(x_train, [0, 3, 2, 1])
    x_test = np.transpose(x_test, [0, 3, 2, 1])
    batch_size = min(args.batch_size, x_train.shape[0], x_test.shape[0])
    iter_train = DataIterator(x_train, y_train, batch_size)
    iter_test = DataIterator(x_test, y_test, batch_size)

    np.random.seed(args.seed)
    torch.cuda.set_device(args.gpu)
    cudnn.benchmark = True
    torch.manual_seed(args.seed)
    cudnn.enabled = True
    torch.cuda.manual_seed(args.seed)
    logging.info('gpu device = %d' % args.gpu)
    logging.info("args = %s", args)

    genotype = eval("genotypes.%s" % args.arch)
    model = Network(args.init_channels, n_classes, args.layers, args.auxiliary, genotype)
    model = model.cuda()

    logging.info("param size = %fMB", utils.count_parameters_in_MB(model))

    criterion = nn.CrossEntropyLoss()
    criterion = criterion.cuda()
    optimizer = torch.optim.SGD(
        model.parameters(),
        args.learning_rate,
        momentum=args.momentum,
        weight_decay=args.weight_decay
    )

    scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(optimizer, float(args.epochs))

    for epoch in range(args.epochs):
        scheduler.step()
        logging.info('epoch %d lr %e', epoch, scheduler.get_lr()[0])
        model.drop_path_prob = args.drop_path_prob * epoch / args.epochs

        train_acc, train_obj = train(iter_train, model, criterion, optimizer)
        logging.info('train_acc %f', train_acc)

        valid_acc, valid_obj = infer(iter_test, model, criterion)
        logging.info('valid_acc %f', valid_acc)

        utils.save(model, os.path.join(args.save, 'weights.pt'))


def train(iter_train, model, criterion, optimizer):
    objs = utils.AvgrageMeter()
    top1 = utils.AvgrageMeter()
    model.train()

    step = 0
    epoch_pred = iter_train.n_epochs_completed
    while epoch_pred == iter_train.n_epochs_completed:
        input, target = iter_train.next_batch()
        input = torch.FloatTensor(input)
        target = torch.LongTensor(target)

        input = Variable(input).cuda()
        target = Variable(target).cuda(async=True)

        optimizer.zero_grad()
        logits, logits_aux = model(input)
        loss = criterion(logits, target)
        if args.auxiliary:
            loss_aux = criterion(logits_aux, target)
            loss += args.auxiliary_weight * loss_aux
        loss.backward()
        nn.utils.clip_grad_norm(model.parameters(), args.grad_clip)
        optimizer.step()

        prec1 = utils.accuracy(logits, target)
        n = input.size(0)
        objs.update(loss.data[0], n)
        top1.update(prec1.data[0], n)

        if step % args.report_freq == 0:
            logging.info('train %03d %e %f', step, objs.avg, top1.avg)
        step += 1

    return top1.avg, objs.avg


def infer(iter_test, model, criterion):
    objs = utils.AvgrageMeter()
    top1 = utils.AvgrageMeter()
    model.eval()

    step = 0
    epoch_pred = iter_test.n_epochs_completed
    while epoch_pred == iter_test.n_epochs_completed:
        input, target = iter_test.next_batch()
        input = torch.FloatTensor(input)
        target = torch.LongTensor(target)

        input = Variable(input, volatile=True).cuda()
        target = Variable(target, volatile=True).cuda(async=True)

        logits, _ = model(input)
        loss = criterion(logits, target)

        prec1 = utils.accuracy(logits, target)
        n = input.size(0)
        objs.update(loss.data[0], n)
        top1.update(prec1.data[0], n)

        if step % args.report_freq == 0:
            logging.info('valid %03d %e %f', step, objs.avg, top1.avg)
        step += 1

    return top1.avg, objs.avg


if __name__ == '__main__':
    data_name = 'Computers'
    main(data_name)
