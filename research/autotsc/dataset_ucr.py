import os
import logging

from mlpy.lib.utils import tag_path, makedirs
from mlpy.data import ucr
from mlpy.data.utils import distribute_y
from research.autotsc.base import DIR_DATASET_UCR15, DIR_LOG


if __name__ == '__main__':
    tag = tag_path(os.path.abspath(__file__), 1)
    dir_log = os.path.join(DIR_LOG, tag)
    makedirs(dir_log)
    logging.basicConfig(filename=os.path.join(dir_log, 'log.log'), filemode='w', level=logging.INFO)
    log = logging.getLogger(tag)

    data_name_list = ucr.get_data_name_list(DIR_DATASET_UCR15)
    threshold = 100  # #samples of each class >= threshold
    log.info("datasets that #samples of each class >= {}".format(threshold))
    log.info("data_name\t#classes\tavg(#samples of each class)")
    for data_name in data_name_list:
        x_train, y_train, x_test, y_test, n_classes = ucr.load_ucr_flat(data_name, DIR_DATASET_UCR15)
        y_train_distr = distribute_y(y_train)
        choose = True
        count_sum = 0
        for id, count in y_train_distr.items():
            if count < threshold:
                choose = False
                break
            count_sum += count
        if choose:
            log.info("{}\t{}\t{}".format(data_name, n_classes, count_sum / n_classes))












