import os
from mlpy.configure import DIR_DATASET, UCR15, TAG_CACHE
import research.autotsc as autotsc

DIR_ROOT = os.path.dirname(autotsc.__file__)
DIR_DATASET_UCR15 = os.path.join(DIR_DATASET, UCR15)
DIR_LOG = os.path.join(DIR_ROOT, TAG_CACHE)

# #samples of each class in training set >= 100
BIG_DATASETS = ['Computers', 'DistalPhalanxOutlineCorrect', 'ElectricDevices', 'FordA', 'FordB', 'HandOutlines',
                'LargeKitchenAppliances', 'MiddlePhalanxOutlineCorrect', 'PhalangesOutlinesCorrect',
                'ProximalPhalanxOutlineCorrect', 'RefrigerationDevices', 'ScreenType', 'SmallKitchenAppliances',
                'StarLightCurves', 'Strawberry', 'Two_Patterns', 'UWaveGestureLibraryAll', 'uWaveGestureLibrary_X',
                'uWaveGestureLibrary_Y', 'uWaveGestureLibrary_Z', 'yoga']


def arry_extend_dim(x, extend_dim):
    if extend_dim > 0:
        extend_shape = ()
        for _ in range(extend_dim):
            extend_shape += (1,)
        x = x.reshape(x.shape + extend_shape)
    return x