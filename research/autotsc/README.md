## [DARTS: Differentiable Architecture Search](https://github.com/quark0/darts)

### Configuring enviroment

1. create a new virtual enviroment with Python=3.5.5
2. install PyTorch==0.3.1 following [this instruction](https://www.ptorch.com/news/145.html) 
	- select the setting with `pip, linux, cuda9.1, python3.5`.
	- note: the command `pip3 install xx` should be replaced with `pip install xx`.
3. evaluate our pretrained DARTS models: CIFAR-10. 
4. follow the instructions to conduct experiments.   

### Some notifications
- [how to derive the best genotype](https://github.com/quark0/darts/issues/107)
- `test.py` just for test, and thus it is always no use.

### Current state
- [x] run the experiments on CIFAR-10: pre-trained model, train-search (using small proxy models), evaluation. 
- [ ] inspect the realization detail and adapt the cnn for time series classification task.

### Training schema
1. randomly run `train_search.py` multiple times (4 times in the paper) and pick up the best genotype for evaluation. The corresponding command is: `genotype = model.genotype()`.
2. run `train.py` to evaluate the best architecture (the paper used an average of 10 random runs.)
    - name and put the searched genotype in `genotypes.py` (e.g. DARTS_V1)
    - set the searched genotype in `genotypes.py`. The corresponding command is: `parser.add_argument('--arch', type=str, default='DARTS', help='which architecture to use')`

### TODO
- [ ] convert Conv2d to Conv1d.
- [ ] replace `NetworkCIFAR` with a DNN model for Time series classification.
- [ ] tune hyper-parameters.

## [AGD](https://github.com/VITA-Group/AGD)  
AutoGAN-Distiller: Searching to Compress Generative Adversarial Networks, ICML, 2020.  

- [ ] configure enviroment. 

## [AutoKeras](https://autokeras.com/)