"""
    Adapted from https://www.tensorflow.org/tutorials/generative/autoencoder
"""

from abc import ABC
import os
import json
from time import time

import tensorflow as tf

from tensorflow.keras import layers, losses
from tensorflow.keras.models import Model

from research.tsgan.lib.utils import makedirs
from research.tsgan.lib.metrics.clf import clf
from research.tsgan.lib.utils import save_series, save_loss


class AEConfig(object):
    def __init__(self,
                 log_dir,
                 logger,
                 input_shape,
                 latent_dim=100,
                 batch_size=16,
                 epochs=300,
                 ckpt_max_to_keep=2,  # spends memory
                 seed=42,
                 verbose=0,
                 **kwargs
                 ):
        self.logger = logger
        self.log_dir = log_dir
        self.logger.info("****** configure init ******")

        # # set dirs
        self.train_dir = makedirs(os.path.join(self.log_dir, 'training'))
        self.eval_dir = makedirs(os.path.join(self.log_dir, 'evaluation'))
        self.ckpt_dir = makedirs(os.path.join(self.log_dir, 'checkpoint'))
        self.ckpt_prefix = os.path.join(self.ckpt_dir, "ckpt")
        self.ckpt_max_to_keep = ckpt_max_to_keep

        # # hyper-parameters
        self.input_shape = input_shape
        self.latent_dim = latent_dim
        # for training
        self.batch_size = batch_size
        self.epochs = epochs
        # random setting
        self.seed = seed

        # other
        self.verbose = verbose

        log_str = "The parameter settings are as follows: \n"
        for key, value in self.__dict__.items():
            log_str += f'{key}:{value}\n'
        self.logger.info(log_str)

    def clean_paths(self):
        self.train_dir = makedirs(self.train_dir, clean=True)
        self.eval_dir = makedirs(self.eval_dir, clean=True)
        self.ckpt_dir = makedirs(self.ckpt_dir, clean=True)


class AutoEncoder(object):
    def __init__(self, cfg: AEConfig):
        self.cfg = cfg
        self._init_model()

    def _init_model(self):
        self.model = AutoEncoderModel(self.cfg.input_shape, self.cfg.latent_dim)
        self.model.compile(optimizer='adam', loss=losses.MeanSquaredError())

    def fit(self, x_tr, x_te=None, restore=False):
        self.model.fit(x_tr, x_tr,
                       epochs=self.cfg.epochs,
                       shuffle=True,
                       batch_size=self.cfg.batch_size,
                       validation_data=(x_te, x_te))

        encoded_samples = self.model.encoder(x_te).numpy()
        decoded_samples = self.model.decoder(encoded_samples).numpy()
        save_series(x_te, os.path.join(self.cfg.eval_dir, 'real_sample.png'))
        save_series(decoded_samples, os.path.join(self.cfg.eval_dir, 'decoded_sample.png'))

    def eval_clf(self, x_tr, y_tr, x_te, y_te, n_classes, idx_layer=-1, epoch=None):
        self.cfg.logger.info("****** eval_clf start ******")
        t0 = time()
        acc_tr, acc_te = clf(self.model.encoder, x_tr, y_tr, x_te, y_te, n_classes,
                             self.cfg.epochs, self.cfg.batch_size, idx_layer=idx_layer, verbose=self.cfg.verbose)
        res = {'epoch': epoch, 'acc_tr': acc_tr, 'acc_te': acc_te}
        with open(os.path.join(self.cfg.eval_dir, 'clf_hidden.json'), 'a') as f:
            f.write(json.dumps(res) + "\n")

        self.cfg.logger.info("time={}".format(time()-t0))
        self.cfg.logger.info("****** eval_clf end ******")

        return res


class AutoEncoderModel(Model, ABC):
    def __init__(self, input_shape, latent_dim):
        super(AutoEncoderModel, self).__init__()
        self.inp_shape = input_shape
        self.latent_dim = latent_dim

        self.encoder = None
        self.decoder = None
        self.build_model(self.inp_shape)

    def build_model(self, input_shape):
        self.encoder = tf.keras.Sequential([
            layers.Flatten(),
            layers.Dense(self.latent_dim, activation='relu'),
        ])

        dense_dim = 1
        for s in input_shape:
            dense_dim *= s
        self.decoder = tf.keras.Sequential([
            layers.Dense(dense_dim),
            layers.Reshape(input_shape)
        ])

    def call(self, inputs, training=None, mask=None):
        """When subclassing the `Model` class, you should implement a `call` method."""
        encoded = self.encoder(inputs)
        decoded = self.decoder(encoded)
        return decoded
