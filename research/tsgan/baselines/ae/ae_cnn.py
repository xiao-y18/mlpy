"""
    Adapted from https://www.tensorflow.org/tutorials/generative/autoencoder
"""

from abc import ABC
import os
import json
from time import time

import tensorflow as tf

from tensorflow.keras import layers, losses
from tensorflow.keras.models import Model

from research.tsgan.lib.metrics.clf import clf
from research.tsgan.lib.utils import save_series, save_loss
from research.tsgan.model.ops import conv_out_size_same, gan_accuracy
from research.tsgan.lib.utils import makedirs

from .ae import AutoEncoder, AutoEncoderModel


class AEConfig(object):
    def __init__(self,
                 log_dir,
                 logger,
                 input_shape,
                 latent_dim=100,
                 batch_size=16,
                 epochs=300,
                 ckpt_max_to_keep=2,  # spends memory
                 seed=42,
                 verbose=0,
                 **kwargs
                 ):
        self.logger = logger
        self.log_dir = log_dir
        self.logger.info("****** configure init ******")

        # # set dirs
        self.train_dir = makedirs(os.path.join(self.log_dir, 'training'))
        self.eval_dir = makedirs(os.path.join(self.log_dir, 'evaluation'))
        self.ckpt_dir = makedirs(os.path.join(self.log_dir, 'checkpoint'))
        self.ckpt_prefix = os.path.join(self.ckpt_dir, "ckpt")
        self.ckpt_max_to_keep = ckpt_max_to_keep

        # # hyper-parameters
        self.input_shape = input_shape
        self.latent_dim = latent_dim
        # for training
        self.batch_size = batch_size
        self.epochs = epochs
        # random setting
        self.seed = seed

        ######## TODO: test
        self.d_units_base = 64
        self.g_units_base = 64
        self.n_layers = 2
        # self.n_layers = 4
        ########

        # other
        self.verbose = verbose

        log_str = "The parameter settings are as follows: \n"
        for key, value in self.__dict__.items():
            log_str += f'{key}:{value}\n'
        self.logger.info(log_str)

    def clean_paths(self):
        self.train_dir = makedirs(self.train_dir, clean=True)
        self.eval_dir = makedirs(self.eval_dir, clean=True)
        self.ckpt_dir = makedirs(self.ckpt_dir, clean=True)


class AutoEncoderCNN(AutoEncoder):
    def __init__(self, cfg: AEConfig):
        super(AutoEncoderCNN, self).__init__(cfg)

    def _init_model(self):
        self.model = AutoEncoderModelCNN(self.cfg.input_shape, self.cfg.latent_dim, self.cfg)
        self.model.compile(optimizer='adam', loss=losses.MeanSquaredError())


class AutoEncoderModelCNN(AutoEncoderModel):
    def __init__(self, input_shape, latent_dim, cfg):
        super(AutoEncoderModel, self).__init__()
        self.inp_shape = input_shape
        self.latent_dim = latent_dim
        self.cfg = cfg

        self.strides = 2
        self.padding = 'same'
        self.initializer = tf.keras.initializers.truncated_normal(stddev=0.02)
        self.leak_slope = 0.2

        self.encoder = None
        self.decoder = None
        self.build_model(self.inp_shape)

    def call(self, inputs, training=None, mask=None):
        """When subclassing the `Model` class, you should implement a `call` method."""
        encoded = self.encoder(inputs)
        decoded = self.decoder(encoded)
        return decoded

    def build_model(self, input_shape):
        input_layer = layers.Input(input_shape)
        output_layer = self.build_discriminator_network(input_layer)
        self.encoder = tf.keras.Model(input_layer, output_layer)

        z_shape = self.encoder.layers[-1].output.shape[1:]
        z = layers.Input(z_shape)
        output_layer = self.build_generator_network(self.cfg.input_shape, z)
        self.decoder = tf.keras.Model(z, output_layer)

    def build_discriminator_network(self, input_layer, with_flat=False):
        units = [self.cfg.d_units_base]
        for _ in range(self.cfg.n_layers - 1):  # exclude the first layer.
            units.append(units[-1] * 2)
        # add a bottleneck layer
        units.append(1)

        # conv layers
        h = input_layer
        for i in range(self.cfg.n_layers):
            name = f'conv_{i}'
            h = layers.Conv1D(units[i], 5, self.strides, self.padding, kernel_initializer=self.initializer,
                              name=f'{name}_conv')(h)
            # if i > 1:  # the first layer without batch-norm
            #     if self.cfg.d_norm is not None:
            #         h = self.cfg.d_norm(name=f'{name}_norm')(h)
            # h = layers.LeakyReLU(self.leak_slope, name=f'{name}_relu')(h)
            # h = layers.Dropout(self.cfg.d_dropout_rate, name=f'{name}_dropout')(h)

        # fc layer
        flat = layers.Flatten()(h)
        return flat

    def build_generator_network(self, generated_sample_shape, input_layer):
        def _get_output_padding(kernel_size, strides):  # padding = same
            """
            References:
                - https://tensorflow.google.cn/api_docs/python/tf/keras/layers/Conv1DTranspose?hl=zh_cn
                    - new_timesteps = ((timesteps - 1) * strides + kernel_size - 2 * padding + output_padding)
            """
            return (kernel_size - 1) % strides

        steps = generated_sample_shape[0]
        layer_steps = [steps]
        for i in range(self.cfg.n_layers):
            layer_steps.append(conv_out_size_same(layer_steps[-1], self.strides))
        layer_steps.reverse()

        conv_units = []
        if self.cfg.n_layers > 1:
            conv_units.append(self.cfg.g_units_base)
            for _ in range(self.cfg.n_layers - 2):  # minus the first and the last layers
                conv_units.append(conv_units[-1] * 2)
        conv_units.reverse()
        # the last layer must be aligned to the number of dimensions of input.
        conv_units.append(generated_sample_shape[-1])

        name = 'dense_0'
        h = layers.Dense(layer_steps[0] * conv_units[0] * 2, kernel_initializer=self.initializer,
                         name=f'{name}_dense')(input_layer)
        # if self.cfg.g_norm is not None:
        #     h = self.cfg.g_norm(name=f'{name}_norm')(h)
        h = layers.ReLU(name=f'{name}_relu')(h)
        h = layers.Reshape((layer_steps[0], conv_units[0] * 2))(h)
        assert h.shape[1] == layer_steps[0]

        # fractional conv layers
        for i in range(self.cfg.n_layers):
            name = f'conv_{i}'
            if layer_steps[i] * self.strides == layer_steps[i + 1]:
                conv = layers.Conv1DTranspose(conv_units[i], 5, self.strides, self.padding,
                                              kernel_initializer=self.initializer, name=f'{name}_conv')
            else:
                conv = layers.Conv1DTranspose(conv_units[i], 5, self.strides, self.padding,
                                              output_padding=_get_output_padding(5, self.strides),
                                              kernel_initializer=self.initializer, name=f'{name}_conv')
            h = conv(h)
            # if i < self.cfg.n_layers - 1:
            #     # the last layer
            #     # - does not apply ReLU
            #     # - does not apply BatchNorm
            #     if self.cfg.g_norm is not None:
            #         h = self.cfg.g_norm(name=f'{name}_norm')(h)
            #     h = layers.ReLU(name=f'{name}_relu')(h)
            assert h.shape[1] == layer_steps[i + 1]
        assert h.shape[-1] == generated_sample_shape[-1]

        return h


