import numpy as np


def sim(func, x1, x2):
    score = func(x1, x2)
    return score


def sim_in_class(func, x1_distr, x2_distr, y_distr):
    score_distr = {}
    score = 0
    count = 0
    for y in y_distr.keys():
        x1 = x1_distr[y]
        x2 = x2_distr[y]
        # most of the time, input of similarity function (func) is 2D array.
        score_ = func(np.reshape(x2, (x2.shape[0], -1)), np.reshape(x1, (x1.shape[0], -1)))
        score_distr[y] = score_
        score += score_
        count += 1
    score = score / count  # average on all classes
    return score, score_distr
