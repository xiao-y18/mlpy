"""
    dscore means the discriminative score proposed in https://papers.nips.cc/paper/8789-time-series-generative-adversarial-networks
"""

import numpy as np
import tensorflow as tf
from tensorflow.keras import layers
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score


def dscore(x, y, discriminator, epochs, batch_size, seed, test_size=0.3, verbose=0):
    # Construct a network with discriminator as the backbone.
    # Note: do not consider the last layer.
    clf = tf.keras.Sequential(discriminator.layers[:-1])
    clf.add(layers.Dense(1, activation='sigmoid'))
    clf.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

    x_tr, x_te, y_tr, y_te = train_test_split(x, y, test_size=test_size, random_state=seed)
    history = clf.fit(x, y, epochs=epochs, batch_size=batch_size, verbose=verbose, validation_data=(x_te, y_te))
    y_pred_prob = clf.predict(x_te)

    prob_mean = np.mean(y_pred_prob)
    prob_std = np.std(y_pred_prob)
    acc = accuracy_score(y_te, (y_pred_prob > 0.5))
    dscore = np.abs(0.5 - acc)

    res = {'dscore': np.round(np.float(dscore), 4),
           'prob_mean': np.round(np.float(prob_mean), 4),
           'proba_std': np.round(np.float(prob_std), 4)}

    return res

