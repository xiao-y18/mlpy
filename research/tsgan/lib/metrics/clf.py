import tensorflow as tf
from tensorflow.keras import layers


def clf(discriminator, x_tr, y_tr, x_te, y_te, n_classes, epochs, batch_size, idx_layer, verbose=0):
    """ Reuse the generator up to the layer indexed by idx_layer and frozen it to support a classification task.

    -----------
    reference
        - (kratzert's answer): https://github.com/keras-team/keras/issues/3465
        - official tutorial: https://www.tensorflow.org/tutorials/images/transfer_learning
    """

    base_model = discriminator.layers[idx_layer].output
    # add new layers
    flat = layers.Flatten()(base_model)
    predictions = layers.Dense(n_classes, activation='softmax')(flat)
    # ---- above
    model = tf.keras.models.Model(inputs=discriminator.input, outputs=predictions)
    # frozen base_model
    for l in model.layers[:-2]:  # exclude the 3 new layers
        l.trainable = False
    print(f"#trainable_variables={len(model.trainable_variables)}")

    model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
    history = model.fit(x_tr, y_tr, batch_size=batch_size, epochs=epochs, verbose=verbose,
                        validation_data=(x_te, y_te))
    history = history.history

    acc_tr, acc_te = history['accuracy'][-1], history['val_accuracy'][-1]

    return acc_tr, acc_te