import matplotlib.pyplot as plt
import os

from mlpy.lib import utils
import mlpy.lib.tfops.base as tfops

makedirs = utils.makedirs
tag_path = utils.tag_path
set_logging = utils.set_logging

tf_keras_set_gpu_allow_growth = tfops.tf_keras_set_gpu_allow_growth


def save_series(samples, path):
    # just show 4 * 4 samples
    fig = plt.figure(figsize=(16, 8))  # each sample takes an area of 4*2.
    n_samples = min(samples.shape[0], 16)
    for i in range(n_samples):
        plt.subplot(4, 4, i + 1)
        plt.plot(samples[i])
        plt.axis('off')
    plt.tight_layout()
    plt.savefig(path)
    plt.close()


def save_loss(df, path):

    def _plot(loss, name):
        plt.plot(loss['d_loss'], label='d_loss')
        plt.plot(loss['g_loss'], label='g_loss')
        plt.legend(loc='best')
        plt.savefig(os.path.join(path, f'loss_{name}.png'))
        plt.close()

    df.to_csv(os.path.join(path, 'loss.csv'), index=False)
    _plot(df, 'batch')
    dfg_epoch = df.groupby('epoch')
    _plot(dfg_epoch.first(), 'epoch_first')
    _plot(dfg_epoch.last(), 'epoch_last')
    _plot(dfg_epoch.mean(), 'epoch_mean')

