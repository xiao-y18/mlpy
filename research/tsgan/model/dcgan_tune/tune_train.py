import tensorflow as tf

from research.tsgan.model.ops import gan_accuracy

from research.tsgan.model.dcgan.dcgan import DCGAN


class DCGANTuneTrain(DCGAN):
    def __init__(self, cfg):
        super(DCGANTuneTrain, self).__init__(cfg)

    # Notice the use of `tf.function`. It causes the function to be "compiled".
    @tf.function
    def train_step(self, samples, acc_pre=tf.constant(0.0, dtype=tf.float32)):
        n_samples = samples.shape[0]  # it may be less than batch_size
        noise = self.cfg.noise_sampler([n_samples, self.cfg.noise_dim])

        # Note:
        # GradientTape.gradient can only be called once on non-persistent tapes, therefore there are two tapes according
        # to generator and discriminator, respectively.
        # For more details about tf.GradientTape, please refer to the official tutorial:
        # https://www.tensorflow.org/api_docs/python/tf/GradientTape.
        with tf.GradientTape() as g_tape, tf.GradientTape() as d_tape:
            generated_samples = self.g(noise, training=True)

            real_logits = self.d(samples, training=True)
            fake_logits = self.d(generated_samples, training=True)

            d_loss, g_loss, real_loss, fake_loss = self.build_loss(real_logits, fake_logits)

        def _opt_g():
            g_grad = g_tape.gradient(g_loss, self.g.trainable_variables)
            self.g_opt.apply_gradients(zip(g_grad, self.g.trainable_variables))

        def _opt_d():
            d_grad = d_tape.gradient(d_loss, self.d.trainable_variables)
            self.d_opt.apply_gradients(zip(d_grad, self.d.trainable_variables))

        # TODO: differ from the base
        tf.cond(acc_pre <= self.cfg.acc_threshold_to_train_d, true_fn=_opt_d, false_fn=_opt_g)
        # ---- the above

        acc = gan_accuracy(real_logits, fake_logits)

        return d_loss, g_loss, real_loss, fake_loss, acc
