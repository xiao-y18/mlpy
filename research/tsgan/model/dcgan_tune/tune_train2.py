import tensorflow as tf

from research.tsgan.model.ops import gan_accuracy

from research.tsgan.model.dcgan.dcgan import DCGAN

# TODO: trying, Use SGD for discriminator and ADAM for generator


class DCGANTuneTrain(DCGAN):
    def __init__(self, cfg):
        super(DCGANTuneTrain, self).__init__(cfg)

        # # build optimizers
        self.g_opt = tf.keras.optimizers.Adam(self.cfg.g_lr, beta_1=self.cfg.g_beta1, name='g_opt')
        self.d_opt = tf.keras.optimizers.SGD(self.cfg.d_lr, momentum=self.cfg.d_beta1, name='d_opt')

        # # set checkpoint
        self.ckpt = tf.train.Checkpoint(g_opt=self.g_opt, d_opt=self.d_opt, g=self.g, d=self.d)
        self.ckpt_manager = tf.train.CheckpointManager(self.ckpt, self.cfg.ckpt_dir, self.cfg.ckpt_max_to_keep)
