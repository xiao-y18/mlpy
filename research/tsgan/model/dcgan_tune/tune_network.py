import tensorflow as tf

from research.tsgan.model.ops import gan_accuracy

from research.tsgan.model.dcgan.dcgan import DCGAN

from tensorflow.keras import layers
from research.tsgan.model.ops import conv_out_size_same

# TODO: trying, LeakyReLU = good (in both G and D)


class DCGANTuneNetwork(DCGAN):
    def __init__(self, cfg):
        super(DCGANTuneNetwork, self).__init__(cfg)

    def build_generator(self):
        steps = self.cfg.input_shape[0]
        layer_steps = [steps]
        for i in range(self.cfg.n_layers):
            layer_steps.append(conv_out_size_same(layer_steps[-1], self.strides))
        layer_steps.reverse()

        def _get_output_padding(kernel_size, strides):  # padding = same
            """
            References:
                - https://tensorflow.google.cn/api_docs/python/tf/keras/layers/Conv1DTranspose?hl=zh_cn
                    - new_timesteps = ((timesteps - 1) * strides + kernel_size - 2 * padding + output_padding)
            """
            return (kernel_size - 1) % strides

        conv_units = []
        if self.cfg.n_layers > 1:
            conv_units.append(self.cfg.g_units_base)
            for _ in range(self.cfg.n_layers-2):  # minus the first and the last layers
                conv_units.append(conv_units[-1]*2)
        conv_units.reverse()
        # the last layer must be aligned to the number of dimensions of input.
        conv_units.append(self.cfg.input_shape[-1])

        # dense layer
        # conv_units[0] * 2: 2 times of the first conv layer.
        model = tf.keras.Sequential()
        model.add(layers.Dense(layer_steps[0] * conv_units[0] * 2,
                               input_shape=(self.cfg.noise_dim,),
                               kernel_initializer=self.initializer))
        if self.cfg.g_norm is not None:
            model.add(self.cfg.g_norm())
        model.add(layers.LeakyReLU(self.leak_slope))
        model.add(layers.Reshape((layer_steps[0], conv_units[0] * 2)))
        assert model.output_shape[1] == layer_steps[0]

        # fractional conv layers
        for i in range(self.cfg.n_layers):
            if layer_steps[i] * self.strides == layer_steps[i+1]:
                model.add(layers.Conv1DTranspose(conv_units[i], 5, self.strides, self.padding,
                                                 kernel_initializer=self.initializer))
            else:
                model.add(layers.Conv1DTranspose(conv_units[i], 5, self.strides, self.padding,
                                                 output_padding=_get_output_padding(5, self.strides),
                                                 kernel_initializer=self.initializer))
            if i < self.cfg.n_layers - 1:
                # the last layer
                # - does not apply ReLU
                # - does not apply BatchNorm
                if self.cfg.g_norm is not None:
                    model.add(self.cfg.g_norm())
                model.add(layers.LeakyReLU(self.leak_slope))
            assert model.output_shape[1] == layer_steps[i+1]
        assert model.output_shape[-1] == self.cfg.input_shape[-1]

        return model

    def build_discriminator(self):
        model = tf.keras.Sequential()

        units = [self.cfg.d_units_base]
        for _ in range(self.cfg.n_layers-1):  # exclude the first layer.
            units.append(units[-1] * 2)

        # conv layers
        for i in range(self.cfg.n_layers):
            model.add(layers.Conv1D(units[i], 5, self.strides, self.padding,
                                    input_shape=self.cfg.input_shape,
                                    kernel_initializer=self.initializer))
            if i > 1:  # the first layer without batch-norm
                if self.cfg.d_norm is not None:
                    model.add(self.cfg.d_norm())
            model.add(layers.LeakyReLU(self.leak_slope))
            model.add(layers.Dropout(self.cfg.d_dropout_rate))

        # fc layer
        model.add(layers.Flatten())
        model.add(layers.Dense(1))

        return model
