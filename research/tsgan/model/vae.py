"""
Reference:
    - Convolutional variational autoencoder: https://www.tensorflow.org/tutorials/generative/cvae.

Note: The model implemented in the above tutorial dose not converge on UCR time-series datasets. To make it work, I
make the following changes:
    - Do not use sigmoid on the output of decoder.
    - Use 'mean_squared_error' to replace the primitive ' tf.nn.sigmoid_cross_entropy_with_logits' in loss function.

In addition, I polish the program in following aspects:
    -

"""

import os
import numpy as np
import tensorflow as tf
from tensorflow.keras import layers
import time

from research.tsgan.lib.utils import makedirs

from .ops import conv_out_size_same


class CVAEModel(tf.keras.Model):
    def __init__(self, input_shape, latent_dim):
        super(CVAEModel, self).__init__()

        self.inp_shape = input_shape
        self.latent_dim = latent_dim
        self.strides = 2
        self.padding = 'same'

        self.encoder = tf.keras.Sequential(
            [
                layers.InputLayer(input_shape=self.inp_shape),
                layers.Conv1D(32, 5, self.strides, self.padding, activation='relu'),
                layers.Conv1D(64, 5, self.strides, self.padding, activation='relu'),
                layers.Flatten(),
                # No activation
                layers.Dense(latent_dim + latent_dim),  # mean, var
            ]
        )

        def _get_output_padding(kernel_size, strides):  # padding = same
            """
            References:
                - https://tensorflow.google.cn/api_docs/python/tf/keras/layers/Conv1DTranspose?hl=zh_cn
                    - new_timesteps = ((timesteps - 1) * strides + kernel_size - 2 * padding + output_padding)
            """
            return (kernel_size - 1) % strides

        self.d_layers = 3
        conv_units = [64, 32, 1]
        # self.d_layers = 2
        # conv_units = [32, 1]
        steps = self.inp_shape[0]
        layer_steps = [steps]
        for i in range(self.d_layers):
            layer_steps.append(conv_out_size_same(layer_steps[-1], self.strides))
        layer_steps.reverse()

        self.decoder = tf.keras.Sequential()
        self.decoder.add(layers.InputLayer(input_shape=(latent_dim,)))
        self.decoder.add(layers.Dense(layer_steps[0]*32, activation=tf.nn.relu))
        self.decoder.add(layers.Reshape((layer_steps[0], 32)))
        assert self.decoder.output_shape[1] == layer_steps[0]

        for i in range(self.d_layers):
            if layer_steps[i] * self.strides == layer_steps[i+1]:
                self.decoder.add(layers.Conv1DTranspose(conv_units[i], 5, self.strides, self.padding))
            else:
                self.decoder.add(layers.Conv1DTranspose(conv_units[i], 5, self.strides, self.padding,
                                                        output_padding=_get_output_padding(5, self.strides)))
            # self.decoder.add(layers.BatchNormalization())
            # self.decoder.add(layers.LeakyReLU())
            assert self.decoder.output_shape[1] == layer_steps[i+1]
        assert self.decoder.output_shape[-1] == self.inp_shape[-1]

    @tf.function
    def sample(self, eps=None):
        if eps is None:
            eps = tf.random.normal(shape=(100, self.latent_dim))
        return self.decode(eps, apply_sigmoid=False)

    def encode(self, x):
        mean, logvar = tf.split(self.encoder(x), num_or_size_splits=2, axis=1)
        return mean, logvar

    def reparameterize(self, mean, logvar):
        eps = tf.random.normal(shape=mean.shape)
        return eps * tf.exp(logvar * .5) + mean

    def decode(self, z, apply_sigmoid=False):
        logits = self.decoder(z)
        if apply_sigmoid:
            probs = tf.sigmoid(logits)
            return probs
        return logits


class CVAE(object):
    def __init__(self, input_shape, log_dir):
        self.input_shape = input_shape
        self.log_dir = log_dir
        self.train_dir = makedirs(os.path.join(self.log_dir, 'training'))
        # set the dimensionality of the latent space to a plane for visualization later
        self.latent_dim = 100
        self.num_examples_to_generate = 16

        # keeping the random vector constant for generation (prediction) so it will be easier to see the improvement.
        self.random_vector_for_generation = tf.random.normal(shape=[self.num_examples_to_generate, self.latent_dim])

        self.model = CVAEModel(self.input_shape, self.latent_dim)
        self.optimizer = tf.keras.optimizers.Adam(0.0001, beta_1=0.5, beta_2=0.5)

    def fit(self, data, test_data, batch_size, epochs=10):
        # Pick a sample of the test set for generating output images
        # assert batch_size >= self.num_examples_to_generate
        # for test_batch in test_data.take(1):
        #     test_sample = test_batch[0:self.num_examples_to_generate, :, :, :]
        # self.generate_and_save_images(self.model, 0, test_sample, self.train_dir)

        dataset_tr = tf.data.Dataset.from_tensor_slices(data).shuffle(batch_size).batch(batch_size)
        dataset_te = tf.data.Dataset.from_tensor_slices(test_data).shuffle(batch_size).batch(batch_size)

        for epoch in range(1, epochs + 1):
            start_time = time.time()
            for train_x in dataset_tr:
                self.train_step(self.model, train_x, self.optimizer)
            end_time = time.time()

            loss = tf.keras.metrics.Mean()
            for test_x in dataset_te:
                loss(self.compute_loss(self.model, test_x))
            elbo = -loss.result()
            print('Epoch: {}, Test set ELBO: {}, time elapse for current epoch: {}'
                  .format(epoch, elbo, end_time - start_time))
        #     self.generate_and_save_images(self.model, epoch, test_sample,self.train_dir)
        #
        # self.generate_and_save_gif(self.train_dir, self.log_dir)
        # self.plot_latent_images(self.model, 20, self.log_dir)

    @staticmethod
    def log_normal_pdf(sample, mean, logvar, raxis=1):
        log2pi = tf.math.log(2. * np.pi)
        return tf.reduce_sum(-.5 * ((sample - mean) ** 2. * tf.exp(-logvar) + logvar + log2pi), axis=raxis)

    def compute_loss(self, model, x):
        mean, logvar = model.encode(x)
        z = model.reparameterize(mean, logvar)
        x_logit = model.decode(z)
        mse = tf.metrics.mean_squared_error(x, x_logit)
        logpx_z = -tf.reduce_sum(mse, axis=[1])
        logpz = self.log_normal_pdf(z, 0., 0.)
        logqz_x = self.log_normal_pdf(z, mean, logvar)
        return -tf.reduce_mean(logpx_z + logpz - logqz_x)

    @tf.function
    def train_step(self, model, x, optimizer):
        """Executes one training step and returns the loss.

        This function computes the loss and gradients, and uses the latter to
        update the model's parameters.
        """
        with tf.GradientTape() as tape:
            loss = self.compute_loss(model, x)
        gradients = tape.gradient(loss, model.trainable_variables)
        optimizer.apply_gradients(zip(gradients, model.trainable_variables))