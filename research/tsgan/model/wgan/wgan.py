"""
TODO: key points
- [x] loss
- [x] opt algorithm
- [x] clip
- [x] a customized ConfigureWGAN, inherit from Configure.
"""
import tensorflow as tf
from tensorflow.keras import layers
import os
from time import time
import pandas as pd
import numpy as np

from ..dcgan.config import Configure
from ..dcgan.dcgan import DCGAN


class WGANConf(Configure):
    def __init__(self,
                 log_dir,
                 logger,
                 input_shape,
                 # WGAN recommends small learning rate.
                 g_lr=0.00005,
                 d_lr=0.00005,
                 # customized parameters for WGAN
                 clip_value=0.01,
                 n_critic=5,
                 **kwargs
                 ):
        super(WGANConf, self).__init__(log_dir, logger, input_shape, g_lr=g_lr, d_lr=d_lr, **kwargs)

        self.clip_value = clip_value
        self.n_critic = n_critic


class WGAN(DCGAN):
    def __init__(self, cfg: WGANConf):
        # self.clip_value = 0.01
        # self.n_critic = 5
        super(WGAN, self).__init__(cfg)

        # self.cfg.g_lr = 0.00005
        # self.cfg.d_lr = 0.00005

    def _init_optimizer(self):
        self.g_opt = tf.keras.optimizers.RMSprop(self.cfg.g_lr)
        self.d_opt = tf.keras.optimizers.RMSprop(self.cfg.d_lr)

    def build_discriminator_network(self, input_layer, with_flat=False):
        units = [self.cfg.d_units_base]
        for _ in range(self.cfg.n_layers - 1):  # exclude the first layer.
            units.append(units[-1] * 2)

        # conv layers
        h = input_layer
        for i in range(self.cfg.n_layers):
            name = f'conv_{i}'
            h = layers.Conv1D(units[i], 5, self.strides, self.padding,
                              kernel_initializer=self.initializer,
                              # weight clip, just clip conv weights here.
                              kernel_constraint=tf.keras.constraints.min_max_norm(
                                  -self.cfg.clip_value, self.cfg.clip_value),
                              # ----------
                              name=f'{name}_conv')(h)
            if i > 1:  # the first layer without batch-norm
                if self.cfg.d_norm is not None:
                    h = self.cfg.d_norm(name=f'{name}_norm')(h)
            h = layers.LeakyReLU(self.leak_slope, name=f'{name}_relu')(h)
            h = layers.Dropout(self.cfg.d_dropout_rate, name=f'{name}_dropout')(h)

        # fc layer
        flat = layers.Flatten()(h)
        out = layers.Dense(1)(flat)

        if with_flat:
            return out, flat
        else:
            return out

    @tf.function
    def train_step(self, samples, acc_pre=tf.constant(0.0, dtype=tf.float32)):
        # opt d
        d_loss = tf.constant(np.inf, tf.float32)  # it must be defined before loop
        for _ in tf.range(self.cfg.n_critic):
            # tf.print("optimize d")
            # tf.print(i)
            noise = self.cfg.noise_sampler([self.cfg.batch_size, self.cfg.noise_dim])
            with tf.GradientTape() as d_tape:
                generated_samples = self.g(noise, training=True)

                real_logits = self.d(samples, training=True)
                fake_logits = self.d(generated_samples, training=True)

                d_loss = tf.reduce_mean(fake_logits) - tf.reduce_mean(real_logits)

            d_grad = d_tape.gradient(d_loss, self.d.trainable_variables)
            self.d_opt.apply_gradients(zip(d_grad, self.d.trainable_variables))

        # opt g
        noise = self.cfg.noise_sampler([self.cfg.batch_size, self.cfg.noise_dim])
        with tf.GradientTape() as g_tape:
            generated_samples = self.g(noise, training=True)
            fake_logits = self.d(generated_samples, training=True)
            g_loss = -tf.reduce_mean(fake_logits)
        g_grad = g_tape.gradient(g_loss, self.g.trainable_variables)
        self.g_opt.apply_gradients(zip(g_grad, self.g.trainable_variables))

        return d_loss, g_loss, np.nan, np.nan, np.nan

    # def fit(self, data, test_data=None, restore=False):
    #     self.cfg.logger.info("****** fit start ******")
    #
    #     if test_data is None:
    #         test_data = data
    #
    #     if restore:  # continue the last ckpt to train
    #         init_epoch = self.load_ckpt()
    #         self.cfg.logger.info(f"restore model from epoch-{init_epoch} and continue to train.")
    #         if init_epoch >= self.cfg.epochs:
    #             new_epochs = self.cfg.epochs + init_epoch
    #             self.cfg.logger.info(f"init_epoch={init_epoch} is grater than epochs={self.cfg.epochs}. "
    #                                  f"increase the upper bound to {new_epochs}")
    #             self.cfg.epochs = new_epochs
    #     else:
    #         self.cfg.logger.info("train from scratch.")
    #         self.cfg.clean_paths()
    #         init_epoch = 0
    #
    #     # Batch and shuffle the data
    #     dataset = tf.data.Dataset.from_tensor_slices(data).shuffle(self.cfg.batch_size).batch(self.cfg.batch_size)
    #
    #     def _get_file_name(ep):
    #         return os.path.join(self.cfg.train_dir, 'epoch_{:04d}.png'.format(ep))
    #
    #     # take 1 batch of real samples to show
    #     real_samples = [e for e in dataset.take(1)][0]
    #     self.save_series(real_samples, os.path.join(self.cfg.train_dir, '000_real.png'))
    #
    #     loss = {'epoch': [], 'batch': [], 'd_loss': [], 'g_loss': []}
    #     for epoch in range(init_epoch, self.cfg.epochs):
    #         t_start = time()
    #         for i, batch in dataset.enumerate():
    #             d_loss, g_loss = self.train_step(batch)
    #             # sanity check: weights should be clipped in the assigned range
    #             # just consider conv layers.
    #             conv_layers = [l for l in self.d.layers if 'conv' in l.name]
    #             for l in conv_layers:
    #                 weights = l.get_weights()[0]  # do not include bias
    #                 for w in weights:
    #                     if ((w >= -self.clip_value) & (w <= self.clip_value)).all():
    #                         pass
    #                     else:
    #                         from IPython import embed; embed()
    #                         print("i:", i)
    #                         print("weights:", w)
    #                         raise ValueError(f"weights do not be clipped to the assigned range.\n "
    #                                          f"layer={l}")
    #
    #             loss['epoch'].append(epoch)
    #             loss['batch'].append(i.numpy())
    #             loss['d_loss'].append(d_loss.numpy())
    #             loss['g_loss'].append(g_loss.numpy())
    #         self.cfg.logger.info(f"epoch[{epoch}/{self.cfg.epochs}], "
    #                              f"d_loss={d_loss:.4}, g_loss={g_loss:.4}, time={(time() - t_start):.4}")
    #         # save the model every 15 epochs
    #         if (epoch + 1) % self.cfg.n_epochs_to_save_ckpt == 0:
    #             self.ckpt_manager.save(checkpoint_number=epoch)
    #             # self.ckpt.save(file_prefix=self.cfg.ckpt_prefix)
    #             self.generate(self.cfg.noise_seed, _get_file_name(epoch))  # produce samples
    #         if (epoch + 1) % self.cfg.n_epochs_to_evaluate == 0:
    #             self.eval(test_data, epoch)
    #
    #     # final epoch
    #     self.ckpt_manager.save(checkpoint_number=self.cfg.epochs)
    #     # self.ckpt.save(file_prefix=self.cfg.ckpt_prefix)
    #     self.generate(self.cfg.noise_seed, _get_file_name(self.cfg.epochs))
    #     pd.DataFrame(loss).to_csv(os.path.join(self.cfg.train_dir, '000_loss.csv'), index=False)
    #     self.eval(test_data, self.cfg.epochs)
    #
    #     self.cfg.logger.info("****** fit end ******")