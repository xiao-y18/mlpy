import math
import tensorflow as tf


def conv_out_size_same(size, stride):  # padding = same
    return int(math.ceil(float(size) / float(stride)))


def gan_accuracy(real_logits, fake_logits):
    real_prob = tf.keras.activations.sigmoid(real_logits)
    fake_prob = tf.keras.activations.sigmoid(fake_logits)
    y_real = tf.ones_like(real_prob)
    y_fake = tf.zeros_like(fake_prob)
    y = tf.concat([y_real, y_fake], axis=0)
    y_pred = tf.concat([real_prob, fake_prob], axis=0)
    acc = tf.reduce_mean(tf.keras.metrics.binary_accuracy(y, y_pred))
    return acc


class InstanceNormalization(tf.keras.layers.Layer):
    """ Instance Normalization Layer (https://arxiv.org/abs/1607.08022). """

    def __init__(self, epsilon=1e-5, name=None):
        super(InstanceNormalization, self).__init__(name=name)
        self.epsilon = epsilon

    def build(self, input_shape):
        self.scale = self.add_weight(
            name='scale',
            shape=input_shape[-1:],
            initializer=tf.random_normal_initializer(1., 0.02),
            trainable=True)

        self.offset = self.add_weight(
            name='offset',
            shape=input_shape[-1:],
            initializer='zeros',
            trainable=True)
