"""
    Basic Conditional GAN, just condition y on the inputs of D and G.
"""
import tensorflow as tf
from tensorflow.keras import layers
import os
from time import time
import pandas as pd
import numpy as np
import json
from sklearn.neighbors import KNeighborsClassifier

from research.tsgan.data import utils
from research.tsgan.lib.utils import save_loss, save_series
from research.tsgan.lib.metrics.mmd import mmd_score
from research.tsgan.lib.metrics.nnd import nnd_score
from research.tsgan.lib.metrics.similarity import sim_in_class
from research.tsgan.model.ops import gan_accuracy

from research.tsgan.model.dcgan.dcgan import DCGAN


class DCGANCond(DCGAN):
    def __init__(self, cfg):
        self.z_shape = (cfg.noise_dim,)
        self.x_shape = cfg.input_shape[0]
        self.y_shape = cfg.input_shape[1]
        self.n_classes = self.y_shape[0]

        super(DCGANCond, self).__init__(cfg)

    def _init_seed(self):
        # We will reuse the following seed samples overtime (so it's easier) to visualize progress.
        self.noise_seed = self.cfg.noise_sampler([self.cfg.n_examples_to_generate, self.cfg.noise_dim],
                                                 seed=self.cfg.seed)
        self.y_seed = self.y_sampler(self.cfg.n_examples_to_generate)
        y = np.argmax(self.y_seed, axis=1)
        np.savetxt(os.path.join(self.cfg.train_dir, 'y_seed.txt'), y, fmt='%d')

    def build_generator(self):
        """G([z,y])"""
        z = layers.Input(self.z_shape)
        y = layers.Input(self.y_shape)
        input_layer = layers.Concatenate()([z, y])
        output_layer = self.build_generator_network(self.x_shape, input_layer)
        model = tf.keras.Model([z, y], output_layer)
        return model

    def build_discriminator(self):
        """D([x,y])"""
        # x_shape = self.cfg.input_shape[0]
        # y_shape = self.cfg.input_shape[1]
        x = layers.Input(self.x_shape)
        x_flat = layers.Flatten()(x)
        y = layers.Input(self.y_shape)
        input_layer = layers.Concatenate()([x_flat, y])
        input_layer = layers.Reshape((-1, 1))(input_layer)
        output_layer = self.build_discriminator_network(input_layer)
        model = tf.keras.Model([x, y], output_layer)
        return model

    @tf.function
    def train_step(self, x_samples, y_samples, acc_pre=tf.constant(0.0, dtype=tf.float32)):
        n_samples = x_samples.shape[0]  # it may be less than batch_size
        noise = self.cfg.noise_sampler([n_samples, self.cfg.noise_dim])

        with tf.GradientTape() as g_tape, tf.GradientTape() as d_tape:
            generated_samples = self.g([noise, y_samples], training=True)

            real_logits = self.d([x_samples, y_samples], training=True)
            fake_logits = self.d([generated_samples, y_samples], training=True)

            d_loss, g_loss, real_loss, fake_loss = self.build_loss(real_logits, fake_logits)

        def _opt_g():
            g_grad = g_tape.gradient(g_loss, self.g.trainable_variables)
            self.g_opt.apply_gradients(zip(g_grad, self.g.trainable_variables))

        def _opt_d():
            d_grad = d_tape.gradient(d_loss, self.d.trainable_variables)
            self.d_opt.apply_gradients(zip(d_grad, self.d.trainable_variables))

        def _null():
            pass

        _opt_g()
        tf.cond(acc_pre <= self.cfg.acc_threshold_to_train_d, true_fn=_opt_d, false_fn=_null)

        acc = gan_accuracy(real_logits, fake_logits)

        return d_loss, g_loss, real_loss, fake_loss, acc

    def fit(self, data, test_data=None, restore=False):
        self.cfg.logger.info("****** fit start ******")

        if test_data is None:
            test_data = data

        if restore:  # continue the last ckpt to train
            init_epoch = self.load_ckpt()
            self.cfg.logger.info(f"restore model from epoch-{init_epoch} and continue to train.")
            if init_epoch >= self.cfg.epochs:
                new_epochs = self.cfg.epochs + init_epoch
                self.cfg.logger.info(f"init_epoch={init_epoch} is grater than epochs={self.cfg.epochs}. "
                                     f"increase the upper bound to {new_epochs}")
                self.cfg.epochs = new_epochs
        else:
            self.cfg.logger.info("train from scratch.")
            self.cfg.clean_paths()
            init_epoch = 0

        # Batch and shuffle the data
        x_data, y_data = data
        # TODO: try to shuffle data, but note that separately shuffling x and y is forbidden.
        x_dataset = tf.data.Dataset.from_tensor_slices(x_data).batch(self.cfg.batch_size)
        y_dataset = tf.data.Dataset.from_tensor_slices(y_data).batch(self.cfg.batch_size)

        def _get_file_name(ep):
            return os.path.join(self.cfg.train_dir, 'epoch_{:04d}.png'.format(ep))

        # take 1 batch of real samples to show
        real_samples = [e for e in x_dataset.take(1)][0]
        save_series(real_samples, os.path.join(self.cfg.train_dir, '000_real.png'))
        self.generate(self.noise_seed, y=self.y_seed, file=_get_file_name(0))
        self.eval(test_data, 0)

        loss = {'epoch': [], 'batch': [], 'd_loss': [], 'g_loss': [], 'real_loss': [], 'fake_loss': []}
        acc_pre = tf.constant(0.0, dtype=tf.float32)
        for epoch in range(init_epoch, self.cfg.epochs):
            t_start = time()
            for i, (x_batch, y_batch) in enumerate(zip(x_dataset, y_dataset)):
                d_loss, g_loss, real_loss, fake_loss, acc = self.train_step(x_batch, y_batch, acc_pre)
                acc_pre = acc

                loss['epoch'].append(epoch+1)
                loss['batch'].append(i)
                loss['d_loss'].append(d_loss.numpy())
                loss['g_loss'].append(g_loss.numpy())
                loss['real_loss'].append(real_loss.numpy())
                loss['fake_loss'].append(fake_loss.numpy())

            self.cfg.logger.info(f"epoch[{epoch+1}/{self.cfg.epochs}], "
                                 f"d_loss={d_loss:.4}, g_loss={g_loss:.4}, real_loss={real_loss:.4}, "
                                 f"fake_loss={fake_loss:.4}, acc={acc:.4}, time={(time() - t_start):.4}")

            if (epoch + 1) % self.cfg.n_epochs_to_save_ckpt == 0:
                self.ckpt_manager.save(checkpoint_number=epoch+1)
                self.generate(self.noise_seed, y=self.y_seed, file=_get_file_name(epoch+1))
            if (epoch + 1) % self.cfg.n_epochs_to_evaluate == 0:
                self.eval(test_data, epoch+1)

        # final epoch
        self.ckpt_manager.save(checkpoint_number=self.cfg.epochs)
        self.generate(self.noise_seed, y=self.y_seed, file=_get_file_name(self.cfg.epochs))
        save_loss(pd.DataFrame(loss), self.cfg.train_dir)
        self.eval(test_data, self.cfg.epochs)

        self.cfg.logger.info("****** fit end ******")

    def generate(self, noise, y=None, file=None, with_y=False):
        # Notice `training` is set to False.
        # This is so all layers run in inference mode (batchnorm).
        if y is None:
            y = self.y_sampler(noise.shape[0])

        generated_samples = self.g([noise, y], training=False)

        if file is not None:
            save_series(generated_samples, file)
        if with_y is False:
            return generated_samples
        else:
            return generated_samples, y

    def generate_batches(self, n_samples, y=None):
        n_batches = n_samples // self.cfg.batch_size
        fake_data = []
        for ib in range(n_batches):
            noise = self.cfg.noise_sampler([self.cfg.batch_size, self.cfg.noise_dim])
            if y is not None:
                fake_samples = self.generate(noise, y=y[ib*self.cfg.batch_size:(ib+1)*self.cfg.batch_size])
            else:
                fake_samples = self.generate(noise)
            fake_data.append(fake_samples.numpy())
        # remainder
        re = n_samples % self.cfg.batch_size
        if re > 0:
            noise = self.cfg.noise_sampler([re, self.cfg.noise_dim])
            if y is not None:
                fake_samples = self.generate(noise, y=y[-re:])
            else:
                fake_samples = self.generate(noise)
            fake_data.append(fake_samples.numpy())

        fake_data = np.vstack(fake_data)
        return fake_data

    def generate_by_y(self, y_distr, with_array_data=False):
        x_gen_distr = {}
        n_classes = len(y_distr.keys())
        for label, count in y_distr.items():
            y_ = np.array([label]*count)
            y_ = utils.dense_to_one_hot(y_, n_classes)
            x_ = self.generate_batches(count, y_)
            x_gen_distr[label] = x_

        if with_array_data:
            y_gen = []
            x_gen = []
            for y_, x_ in x_gen_distr.items():
                y_gen.append([y_]*x_.shape[0])
                x_gen.append(x_)
            x_gen = np.vstack(x_gen)
            y_gen = np.hstack(y_gen)

            return x_gen_distr, x_gen, y_gen
        else:
            return x_gen_distr

    def y_sampler(self, n_samples):
        n_classes = self.cfg.input_shape[1][0]
        y = tf.random.uniform((n_samples,), minval=0, maxval=n_classes-1, dtype=tf.int32, seed=self.cfg.seed)
        y_onehot = tf.one_hot(y, n_classes)
        return y_onehot

    def eval(self, real_data, epoch=None, methods=['mmd', 'nnd']):
        self.cfg.logger.info("****** eval start ******")
        # consider labels in conditional GAN
        x, y = real_data
        y = utils.one_hot_to_dense(y)

        t0 = time()
        x_distr = utils.distribute_dataset(x, y)
        y_distr = utils.distribute_y(y)
        x_fake_distr = self.generate_by_y(y_distr)
        self.cfg.logger.info("time to generate data {}".format(time() - t0))

        # =============================
        # scores to measure similarity
        # =============================
        metrics = {'epoch': epoch}

        def _save_score_distr(score_distr, path, epoch):
            score_json_dict = {}
            for key, score in score_distr.items():
                score_json_dict[str(key)] = np.round(np.float(score), 4)
            res = {'epoch': epoch, 'score': score_json_dict}
            with open(path, 'a') as f:
                f.write(json.dumps(res) + "\n")

        if 'nnd' in methods:
            t0 = time()
            nnd, nnd_distr = sim_in_class(nnd_score, x_distr, x_fake_distr, y_distr)
            self.cfg.logger.info(f"nnd={nnd}, time={time()-t0}")
            metrics.update({'nnd': np.round(np.float(nnd), 4)})
            _save_score_distr(nnd_distr, os.path.join(self.cfg.eval_dir, 'similarity_nnd.json'), epoch)

        if 'mmd' in methods:
            t0 = time()
            mmd, mmd_distr = sim_in_class(mmd_score, x_distr, x_fake_distr, y_distr)
            self.cfg.logger.info(f"mmd={mmd}, time={time()-t0}")
            metrics.update({'mmd': np.round(np.float(mmd), 4)})
            _save_score_distr(mmd_distr, os.path.join(self.cfg.eval_dir, 'similarity_mmd.json'), epoch)

        with open(os.path.join(self.cfg.eval_dir, 'similarity.json'), 'a') as f:
            f.write(json.dumps(metrics) + "\n")

        self.cfg.logger.info("****** eval end ******")

        return metrics

    def eval_clf_data(self, x_tr, y_tr, x_te, y_te, n_classes, epoch=None):
        y_tr = utils.one_hot_to_dense(y_tr)
        y_te = utils.one_hot_to_dense(y_te)

        _, x_gen_tr, y_gen_tr = self.generate_by_y(utils.distribute_y(y_tr), with_array_data=True)
        _, x_gen_te, y_gen_te = self.generate_by_y(utils.distribute_y(y_te), with_array_data=True)
        assert x_tr.shape == x_gen_tr.shape and x_te.shape == x_gen_te.shape

        # convert x to a 2-D array for KNN model
        x_tr = np.reshape(x_tr, (x_tr.shape[0], -1))
        x_te = np.reshape(x_te, (x_te.shape[0], -1))
        x_gen_tr = np.reshape(x_gen_tr, (x_gen_tr.shape[0], -1))
        x_gen_te = np.reshape(x_gen_te, (x_gen_te.shape[0], -1))

        res = {'epoch': epoch}

        # train on real, test on real
        clf = KNeighborsClassifier(n_neighbors=1)
        clf.fit(x_tr, y_tr)
        acc_trtr = clf.score(x_te, y_te)
        res.update({'TRTR': np.round(acc_trtr, 4)})

        # train on fake, test on real
        clf.fit(x_gen_tr, y_gen_tr)
        acc_tftr = clf.score(x_te, y_te)
        res.update({'TFTR': np.round(acc_tftr, 4)})

        # train on real, test on fake
        clf.fit(x_tr, y_tr)
        acc_trtf = clf.score(x_gen_te, y_gen_te)
        res.update({'TRTF': np.round(acc_trtf, 4)})

        # train on real+fake, test on real, that mimic data augmentation.
        clf.fit(np.vstack([x_tr, x_gen_tr]), np.hstack([y_tr, y_gen_tr]))
        acc_trftf = clf.score(x_te, y_te)
        res.update({'TRFTF': np.round(acc_trftf, 4)})

        with open(os.path.join(self.cfg.eval_dir, 'clf_dff_data.json'), 'a') as f:
            f.write(json.dumps(res) + "\n")

        return res

