"""
    The conditional version of the primitive DCGAN: condition y on all layers.
"""
import tensorflow as tf
from tensorflow.keras import layers

from research.tsgan.model.ops import conv_out_size_same
from .dcgan import DCGANCond


def conv_concat(h, y):
    y_dup = layers.RepeatVector(h.shape[1])(y)
    return layers.Concatenate(axis=-1)([h, y_dup])


class DCGANCond2(DCGANCond):
    def __init__(self, cfg):
        super(DCGANCond2, self).__init__(cfg)

    def build_generator_network(self, generated_sample_shape, inputs):
        input_layer = layers.Concatenate()(inputs)
        y = inputs[-1]

        def _get_output_padding(kernel_size, strides):  # padding = same
            """
            References:
                - https://tensorflow.google.cn/api_docs/python/tf/keras/layers/Conv1DTranspose?hl=zh_cn
                    - new_timesteps = ((timesteps - 1) * strides + kernel_size - 2 * padding + output_padding)
            """
            return (kernel_size - 1) % strides

        steps = generated_sample_shape[0]
        layer_steps = [steps]
        for i in range(self.cfg.n_layers):
            layer_steps.append(conv_out_size_same(layer_steps[-1], self.strides))
        layer_steps.reverse()

        conv_units = []
        if self.cfg.n_layers > 1:
            conv_units.append(self.cfg.g_units_base)
            for _ in range(self.cfg.n_layers - 2):  # minus the first and the last layers
                conv_units.append(conv_units[-1] * 2)
        conv_units.reverse()
        # the last layer must be aligned to the number of dimensions of input.
        conv_units.append(generated_sample_shape[-1])

        name = 'dense_0'
        h = layers.Dense(layer_steps[0] * conv_units[0] * 2, kernel_initializer=self.initializer,
                         name=f'{name}_dense')(input_layer)
        if self.cfg.g_norm is not None:
            h = self.cfg.g_norm(name=f'{name}_norm')(h)
        h = layers.ReLU(name=f'{name}_relu')(h)
        h = layers.Reshape((layer_steps[0], conv_units[0] * 2))(h)
        assert h.shape[1] == layer_steps[0]

        # fractional conv layers
        for i in range(self.cfg.n_layers):
            name = f'conv_{i}'
            if layer_steps[i] * self.strides == layer_steps[i + 1]:
                conv = layers.Conv1DTranspose(conv_units[i], 5, self.strides, self.padding,
                                              kernel_initializer=self.initializer, name=f'{name}_conv')
            else:
                conv = layers.Conv1DTranspose(conv_units[i], 5, self.strides, self.padding,
                                              output_padding=_get_output_padding(5, self.strides),
                                              kernel_initializer=self.initializer, name=f'{name}_conv')
            conv_inp = conv_concat(h, y)
            h = conv(conv_inp)
            if i < self.cfg.n_layers - 1:
                # the last layer
                # - does not apply ReLU
                # - does not apply BatchNorm
                if self.cfg.g_norm is not None:
                    h = self.cfg.g_norm(name=f'{name}_norm')(h)
                h = layers.ReLU(name=f'{name}_relu')(h)
            assert h.shape[1] == layer_steps[i + 1]
        assert h.shape[-1] == generated_sample_shape[-1]

        return h

    def build_generator(self):
        """G([z,y])"""

        z = layers.Input(self.z_shape)
        y = layers.Input(self.y_shape)
        input_layer = [z, y]
        output_layer = self.build_generator_network(self.x_shape, input_layer)
        model = tf.keras.Model([z, y], output_layer)
        return model


