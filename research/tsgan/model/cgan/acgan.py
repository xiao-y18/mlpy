"""
New features in ACGAN
- a multi-classifier is added to the discriminator.
- a new optimization objective that leverage classification loss.
"""

import tensorflow as tf
from tensorflow.keras import layers

from research.tsgan.model.ops import gan_accuracy
from research.tsgan.model.cgan.dcgan import DCGANCond


class ACGAN(DCGANCond):
    def __init__(self, cfg):
        super(ACGAN, self).__init__(cfg)

    def build_discriminator(self):
        """[real, class]=D(x)"""
        x = layers.Input(self.x_shape)
        output, flat = self.build_discriminator_network(x, with_flat=True)
        output_clf = layers.Dense(self.n_classes)(flat)
        model = tf.keras.Model(x, [output, output_clf])
        return model

    @tf.function
    def train_step(self, x_samples, y_samples, acc_pre=tf.constant(0.0, dtype=tf.float32)):
        n_samples = x_samples.shape[0]  # it may be less than batch_size
        noise = self.cfg.noise_sampler([n_samples, self.cfg.noise_dim])

        with tf.GradientTape() as g_tape, tf.GradientTape() as d_tape:
            generated_samples = self.g([noise, y_samples], training=True)

            real_logits, real_labels = self.d(x_samples, training=True)
            fake_logits, fake_labels = self.d(generated_samples, training=True)

            d_loss, g_loss, real_loss, fake_loss = self.build_loss(real_logits, fake_logits)

            # Note: use logits
            clf_loss_real = tf.keras.losses.CategoricalCrossentropy(from_logits=True)(y_samples, real_labels)
            clf_loss_fake = tf.keras.losses.CategoricalCrossentropy(from_logits=True)(y_samples, fake_labels)
            clf_loss = clf_loss_real + clf_loss_fake

            g_loss = g_loss + clf_loss
            d_loss = d_loss + clf_loss

        def _opt_g():
            g_grad = g_tape.gradient(g_loss, self.g.trainable_variables)
            self.g_opt.apply_gradients(zip(g_grad, self.g.trainable_variables))

        def _opt_d():
            d_grad = d_tape.gradient(d_loss, self.d.trainable_variables)
            self.d_opt.apply_gradients(zip(d_grad, self.d.trainable_variables))

        def _null():
            pass

        _opt_g()
        tf.cond(acc_pre <= self.cfg.acc_threshold_to_train_d, true_fn=_opt_d, false_fn=_null)

        acc = gan_accuracy(real_logits, fake_logits)

        return d_loss, g_loss, real_loss, fake_loss, acc




