import tensorflow as tf
import numpy as np

from research.tsgan.model.dcgan.config import Configure
from .dcgan import DCGAN
"""
TODO:
- [x] loss
- [x] opt algorithm
- [x] delete batch norm
- [x] a customized ConfigureWGAN, inherit from Configure.
"""


class WGANGPConfigure(Configure):
    def __init__(self,
                 log_dir,
                 logger,
                 input_shape,
                 **kwargs
                 ):
        self.n_critic = 5
        self.lambda_ = 10
        self.g_beta2 = 0.9
        self.d_beta2 = 0.9
        super(WGANGPConfigure, self).__init__(log_dir, logger, input_shape,
                                              g_lr=1e-4, d_lr=1e-4,
                                              g_norm_method='null', d_norm_method='null',
                                              **kwargs)


class WGANGP(DCGAN):
    def __init__(self, cfg: WGANGPConfigure):
        super(WGANGP, self).__init__(cfg)
        self.g_opt = tf.keras.optimizers.Adam(self.cfg.g_lr, beta_1=self.cfg.g_beta1, beta_2=self.cfg.g_beta2,
                                              name='g_opt')
        self.d_opt = tf.keras.optimizers.Adam(self.cfg.d_lr, beta_1=self.cfg.d_beta1, beta_2=self.cfg.d_beta2,
                                              name='d_opt')

    @tf.function
    def train_step(self, samples, acc_pre=tf.constant(0.0, dtype=tf.float32)):
        # TODO: acc_pre is useless.
        n_samples = samples.shape[0]  # it may be less than batch_size
        # opt g
        noise = self.cfg.noise_sampler([n_samples, self.cfg.noise_dim])
        with tf.GradientTape() as g_tape:
            generated_samples = self.g(noise, training=True)
            fake_logits = self.d(generated_samples, training=True)
            g_loss = -tf.reduce_mean(fake_logits)
        g_grad = g_tape.gradient(g_loss, self.g.trainable_variables)
        self.g_opt.apply_gradients(zip(g_grad, self.g.trainable_variables))

        # opt d
        d_loss = tf.constant(np.inf, tf.float32)  # it must be defined before loop
        for i in tf.range(self.cfg.n_critic):
            alpha = tf.random.uniform(shape=[n_samples, 1], minval=0., maxval=1., seed=self.cfg.seed)
            noise = self.cfg.noise_sampler([n_samples, self.cfg.noise_dim])
            with tf.GradientTape() as d_tape:
                with tf.GradientTape() as dd_tape:
                    generated_samples = self.g(noise, training=True)

                    real_logits = self.d(samples, training=True)
                    fake_logits = self.d(generated_samples, training=True)

                    d_loss = tf.reduce_mean(fake_logits) - tf.reduce_mean(real_logits)

                    generated_samples_flat = tf.reshape(generated_samples, [generated_samples.shape[0], -1])
                    samples_flat = tf.reshape(samples, [samples.shape[0], -1])
                    differences = generated_samples_flat - samples_flat
                    interpolates = samples_flat + (alpha * differences)

                    inter_logits = self.d(tf.reshape(interpolates, (interpolates.shape[0],)+self.d.input_shape[1:]),
                                          training=True)

                gradients = dd_tape.gradient(inter_logits, [interpolates])[0]
                slopes = tf.sqrt(tf.reduce_sum(tf.square(gradients), axis=1))
                gradient_penalty = tf.reduce_mean((slopes - 1.) ** 2)
                d_loss += self.cfg.lambda_ * gradient_penalty

            d_grad = d_tape.gradient(d_loss, self.d.trainable_variables)
            self.d_opt.apply_gradients(zip(d_grad, self.d.trainable_variables))

        return d_loss, g_loss, tf.constant(0.0, tf.float32), tf.constant(0.0, tf.float32), tf.constant(0.0, tf.float32)
