"""

"""
import tensorflow as tf
from tensorflow.keras import layers
import os
import matplotlib.pyplot as plt
from time import time
import pandas as pd
import numpy as np
import json

from .config import Configure
from .dcgan import DCGAN, conv_out_size_same
"""
TODO:
- [x] loss
- [x] opt algorithm
- [x] delete batch norm
- [ ] a customized ConfigureWGAN, inherit from Configure.
"""


class WGANGPConfigure(Configure):
    def __init__(self,
                 log_dir,
                 logger,
                 input_shape,
                 **kwargs
                 ):
        super(WGANGPConfigure, self).__init__(log_dir, logger, input_shape,
                                              g_lr=1e-4, d_lr=1e-4,
                                              g_norm_method='null', d_norm_method='null',
                                              **kwargs)
        self.n_critic = 5
        self.lambda_ = 10


class WGANGP(DCGAN):
    def __init__(self, cfg: Configure):
        self.n_critic = 5
        super(WGANGP, self).__init__(cfg)

        self.cfg.g_lr = 1e-4
        self.cfg.d_lr = 1e-4
        self.g_opt = tf.keras.optimizers.Adam(self.cfg.g_lr, beta_1=0.5, beta_2=0.9, name='g_opt')
        self.d_opt = tf.keras.optimizers.Adam(self.cfg.d_lr, beta_1=0.5, beta_2=0.9, name='d_opt')
        self.lambda_ = 10

    def build_generator(self):
        steps = self.cfg.input_shape[0]
        layer_steps = [steps]
        for i in range(self.cfg.n_layers):
            layer_steps.append(conv_out_size_same(layer_steps[-1], self.strides))
        layer_steps.reverse()

        def _get_output_padding(kernel_size, strides):  # padding = same
            """
            References:
                - https://tensorflow.google.cn/api_docs/python/tf/keras/layers/Conv1DTranspose?hl=zh_cn
                    - new_timesteps = ((timesteps - 1) * strides + kernel_size - 2 * padding + output_padding)
            """
            return (kernel_size - 1) % strides

        conv_units = []
        if self.cfg.n_layers > 1:
            conv_units.append(self.cfg.g_units_base)
            for _ in range(self.cfg.n_layers-2):  # minus the first and the last layers
                conv_units.append(conv_units[-1]*2)
        conv_units.reverse()
        # the last layer must be aligned to the number of dimensions of input.
        conv_units.append(self.cfg.input_shape[-1])

        # dense layer
        # conv_units[0] * 2: 2 times of the first conv layer.
        model = tf.keras.Sequential()
        model.add(layers.Dense(layer_steps[0] * conv_units[0] * 2, input_shape=(self.cfg.noise_dim,),
                               kernel_initializer=self.initializer))
        # model.add(self.cfg.g_norm())
        model.add(layers.ReLU())
        model.add(layers.Reshape((layer_steps[0], conv_units[0] * 2)))
        assert model.output_shape[1] == layer_steps[0]

        # fractional conv layers
        for i in range(self.cfg.n_layers):
            if layer_steps[i] * self.strides == layer_steps[i+1]:
                model.add(layers.Conv1DTranspose(conv_units[i], 5, self.strides, self.padding,
                                                 kernel_initializer=self.initializer))
            else:
                model.add(layers.Conv1DTranspose(conv_units[i], 5, self.strides, self.padding,
                                                 output_padding=_get_output_padding(5, self.strides),
                                                 kernel_initializer=self.initializer))
            if i < self.cfg.n_layers - 1:  # except the last layer
                # model.add(self.cfg.g_norm())
                model.add(layers.ReLU())
            assert model.output_shape[1] == layer_steps[i+1]
        assert model.output_shape[-1] == self.cfg.input_shape[-1]

        return model

    def build_discriminator(self):
        model = tf.keras.Sequential()

        units = [self.cfg.d_units_base]
        for _ in range(self.cfg.n_layers-1):  # exclude the first layer.
            units.append(units[-1] * 2)

        # conv layers
        for i in range(self.cfg.n_layers):
            model.add(layers.Conv1D(units[i], 5, self.strides, self.padding, input_shape=self.cfg.input_shape,
                                    kernel_initializer=self.initializer))
            # if i > 1:  # the first layer without batch-norm
            #     model.add(self.cfg.d_norm())
            model.add(layers.LeakyReLU(self.leak_slope))
            model.add(layers.Dropout(self.cfg.d_dropout_rate))

        # fc layer
        model.add(layers.Flatten())
        model.add(layers.Dense(1))

        return model

    # Notice the use of `tf.function`
    # This annotation causes the function to be "compiled".
    @tf.function
    def train_step(self, samples, acc_pre=tf.constant(0.0, dtype=tf.float32)):
        # TODO: acc_pre is useless.
        n_samples = samples.shape[0]  # it may be less than batch_size
        # opt g
        noise = self.cfg.noise_sampler([n_samples, self.cfg.noise_dim])
        with tf.GradientTape() as g_tape:
            generated_samples = self.g(noise, training=True)
            fake_logits = self.d(generated_samples, training=True)
            g_loss = -tf.reduce_mean(fake_logits)
        g_grad = g_tape.gradient(g_loss, self.g.trainable_variables)
        self.g_opt.apply_gradients(zip(g_grad, self.g.trainable_variables))

        # opt d
        d_loss = tf.constant(np.inf, tf.float32)  # it must be defined before loop
        for i in tf.range(self.n_critic):
            alpha = tf.random.uniform(shape=[n_samples, 1], minval=0., maxval=1., seed=self.cfg.seed)
            noise = self.cfg.noise_sampler([n_samples, self.cfg.noise_dim])
            with tf.GradientTape() as d_tape:
                with tf.GradientTape() as dd_tape:
                    generated_samples = self.g(noise, training=True)

                    real_logits = self.d(samples, training=True)
                    fake_logits = self.d(generated_samples, training=True)

                    d_loss = tf.reduce_mean(fake_logits) - tf.reduce_mean(real_logits)

                    generated_samples_flat = tf.reshape(generated_samples, [generated_samples.shape[0], -1])
                    samples_flat = tf.reshape(samples, [samples.shape[0], -1])
                    differences = generated_samples_flat - samples_flat
                    interpolates = samples_flat + (alpha * differences)

                    inter_logits = self.d(tf.reshape(interpolates, (interpolates.shape[0],)+self.d.input_shape[1:]),
                                          training=True)

                gradients = dd_tape.gradient(inter_logits, [interpolates])[0]
                slopes = tf.sqrt(tf.reduce_sum(tf.square(gradients), axis=1))
                gradient_penalty = tf.reduce_mean((slopes - 1.) ** 2)
                d_loss += self.lambda_ * gradient_penalty

            d_grad = d_tape.gradient(d_loss, self.d.trainable_variables)
            self.d_opt.apply_gradients(zip(d_grad, self.d.trainable_variables))
            del d_tape

        return d_loss, g_loss

    def fit(self, data, test_data=None, restore=False):
        self.cfg.logger.info("****** fit start ******")

        if test_data is None:
            test_data = data

        if restore:  # continue the last ckpt to train
            init_epoch = self.load_ckpt()
            self.cfg.logger.info(f"restore model from epoch-{init_epoch} and continue to train.")
            if init_epoch >= self.cfg.epochs:
                new_epochs = self.cfg.epochs + init_epoch
                self.cfg.logger.info(f"init_epoch={init_epoch} is grater than epochs={self.cfg.epochs}. "
                                     f"increase the upper bound to {new_epochs}")
                self.cfg.epochs = new_epochs
        else:
            self.cfg.logger.info("train from scratch.")
            self.cfg.clean_paths()
            init_epoch = 0

        # Batch and shuffle the data
        dataset = tf.data.Dataset.from_tensor_slices(data).shuffle(self.cfg.batch_size).batch(self.cfg.batch_size)

        def _get_file_name(ep):
            return os.path.join(self.cfg.train_dir, 'epoch_{:04d}.png'.format(ep))

        # take 1 batch of real samples to show
        real_samples = [e for e in dataset.take(1)][0]
        self.save_series(real_samples, os.path.join(self.cfg.train_dir, '000_real.png'))

        loss = {'epoch': [], 'batch': [], 'd_loss': [], 'g_loss': []}
        for epoch in range(init_epoch, self.cfg.epochs):
            t_start = time()
            for i, batch in dataset.enumerate():
                d_loss, g_loss = self.train_step(batch)

                loss['epoch'].append(epoch)
                loss['batch'].append(i.numpy())
                loss['d_loss'].append(d_loss.numpy())
                loss['g_loss'].append(g_loss.numpy())
            self.cfg.logger.info(f"epoch[{epoch}/{self.cfg.epochs}], "
                                 f"d_loss={d_loss:.4}, g_loss={g_loss:.4}, time={(time() - t_start):.4}")
            # save the model every 15 epochs
            if (epoch + 1) % self.cfg.n_epochs_to_save_ckpt == 0:
                self.ckpt_manager.save(checkpoint_number=epoch)
                # self.ckpt.save(file_prefix=self.cfg.ckpt_prefix)
                self.generate(self.cfg.noise_seed, _get_file_name(epoch))  # produce samples
            if (epoch + 1) % self.cfg.n_epochs_to_evaluate == 0:
                self.eval(test_data, epoch)

        # final epoch
        self.ckpt_manager.save(checkpoint_number=self.cfg.epochs)
        # self.ckpt.save(file_prefix=self.cfg.ckpt_prefix)
        self.generate(self.cfg.noise_seed, _get_file_name(self.cfg.epochs))
        pd.DataFrame(loss).to_csv(os.path.join(self.cfg.train_dir, '000_loss.csv'), index=False)
        self.eval(test_data, self.cfg.epochs)

        self.cfg.logger.info("****** fit end ******")