# DCGAN

Please refer to my doc for more details.

## Implementation

In general, my implementation experience the following 3 stages.

1. Adapt the DCGAN implemented in [a tensorflow tutorial](https://github.com/tensorflow/docs/blob/master/site/en/tutorials/generative/dcgan.ipynb) to model time series. However, I found this implementation has many settings that are different from the primitive DCGAN.
2. Refer to the [primitive paper](https://arxiv.org/abs/1511.06434) to configure whole framework.
3. Make the codebase more general to use. For instance, implement a `confg.py` for efficient experiments.
4. Try some latest tricks to improve the network.


## Settings used in the primitive work 

I found almost all settings mentioned in the primitive paper are important to train a stable DCGAN. 

- "All weights were initialized from a zero-centered Normal distribution with standard deviation 0.02"
- "Use ReLU activation in generator for all layers except for the output, which uses Tanh."
- "In the LeakyReLU, the slope of the leak was set to 0.2 in all models."
- "we used the Adam optimizer (Kingma & Ba, 2014) with tuned hyperparameters. We found the suggested learning
rate of 0.001, to be too high, using 0.0002 instead. Additionally, we found leaving the momentum term β1 at the
suggested value of 0.9 resulted in training oscillation and instability while reducing it to 0.5 helped
stabilize training."
- "The first layer of the GAN, which takes a uniform noise distribution Z as input"
- Without dropout layer.
- Structures for different datasets are different slightly.



