import tensorflow as tf
from tensorflow.keras import layers
import os
from time import time
import pandas as pd
import numpy as np
import json

from research.tsgan.model.ops import conv_out_size_same, gan_accuracy
from research.tsgan.lib.metrics.vis import tsne
from research.tsgan.lib.metrics.dscore import dscore
from research.tsgan.lib.metrics.mmd import mmd_score
from research.tsgan.lib.metrics.nnd import nnd_score
from research.tsgan.lib.metrics.clf import clf
from research.tsgan.lib.utils import save_series, save_loss


class DCGAN(object):
    def __init__(self, cfg):
        self.cfg = cfg

        self._init_parameters()
        self._init_model()
        self._init_optimizer()
        self._init_seed()

        # set checkpoint
        self.ckpt = tf.train.Checkpoint(g_opt=self.g_opt, d_opt=self.d_opt, g=self.g, d=self.d)
        self.ckpt_manager = tf.train.CheckpointManager(self.ckpt, self.cfg.ckpt_dir, self.cfg.ckpt_max_to_keep)

    def _init_parameters(self):
        self.strides = 2
        self.padding = 'same'
        self.initializer = tf.keras.initializers.truncated_normal(stddev=0.02)
        self.leak_slope = 0.2

    def _init_model(self):
        # build generator
        self.g = self.build_generator()
        if self.cfg.verbose:
            self.cfg.logger.info("Generator's summary: ")
            self.g.summary(print_fn=self.cfg.logger.info)
        # build discriminator
        self.d = self.build_discriminator()
        if self.cfg.verbose:
            self.cfg.logger.info("Discriminator's summary: ")
            self.d.summary(print_fn=self.cfg.logger.info)

    def _init_seed(self):
        # We will reuse the following seed samples overtime (so it's easier) to visualize progress.
        self.noise_seed = self.cfg.noise_sampler([self.cfg.n_examples_to_generate, self.cfg.noise_dim],
                                                 seed=self.cfg.seed)

    def _init_optimizer(self):
        self.g_opt = tf.keras.optimizers.Adam(self.cfg.g_lr, beta_1=self.cfg.g_beta1, name='g_opt')
        self.d_opt = tf.keras.optimizers.Adam(self.cfg.d_lr, beta_1=self.cfg.d_beta1, name='d_opt')

    def build_generator_network(self, generated_sample_shape, input_layer):
        def _get_output_padding(kernel_size, strides):  # padding = same
            """
            References:
                - https://tensorflow.google.cn/api_docs/python/tf/keras/layers/Conv1DTranspose?hl=zh_cn
                    - new_timesteps = ((timesteps - 1) * strides + kernel_size - 2 * padding + output_padding)
            """
            return (kernel_size - 1) % strides

        steps = generated_sample_shape[0]
        layer_steps = [steps]
        for i in range(self.cfg.n_layers):
            layer_steps.append(conv_out_size_same(layer_steps[-1], self.strides))
        layer_steps.reverse()

        conv_units = []
        if self.cfg.n_layers > 1:
            conv_units.append(self.cfg.g_units_base)
            for _ in range(self.cfg.n_layers - 2):  # minus the first and the last layers
                conv_units.append(conv_units[-1] * 2)
        conv_units.reverse()
        # the last layer must be aligned to the number of dimensions of input.
        conv_units.append(generated_sample_shape[-1])

        name = 'dense_0'
        h = layers.Dense(layer_steps[0] * conv_units[0] * 2, kernel_initializer=self.initializer,
                         name=f'{name}_dense')(input_layer)
        if self.cfg.g_norm is not None:
            h = self.cfg.g_norm(name=f'{name}_norm')(h)
        h = layers.ReLU(name=f'{name}_relu')(h)
        h = layers.Reshape((layer_steps[0], conv_units[0] * 2))(h)
        assert h.shape[1] == layer_steps[0]

        # fractional conv layers
        for i in range(self.cfg.n_layers):
            name = f'conv_{i}'
            if layer_steps[i] * self.strides == layer_steps[i + 1]:
                conv = layers.Conv1DTranspose(conv_units[i], 5, self.strides, self.padding,
                                              kernel_initializer=self.initializer, name=f'{name}_conv')
            else:
                conv = layers.Conv1DTranspose(conv_units[i], 5, self.strides, self.padding,
                                              output_padding=_get_output_padding(5, self.strides),
                                              kernel_initializer=self.initializer, name=f'{name}_conv')
            h = conv(h)
            if i < self.cfg.n_layers - 1:
                # the last layer
                # - does not apply ReLU
                # - does not apply BatchNorm
                if self.cfg.g_norm is not None:
                    h = self.cfg.g_norm(name=f'{name}_norm')(h)
                h = layers.ReLU(name=f'{name}_relu')(h)
            assert h.shape[1] == layer_steps[i + 1]
        assert h.shape[-1] == generated_sample_shape[-1]

        return h

    def build_generator(self):
        z_shape = (self.cfg.noise_dim,)
        z = layers.Input(z_shape)
        output_layer = self.build_generator_network(self.cfg.input_shape, z)
        model = tf.keras.Model(z, output_layer)
        return model

    def build_discriminator_network(self, input_layer, with_flat=False):
        units = [self.cfg.d_units_base]
        for _ in range(self.cfg.n_layers - 1):  # exclude the first layer.
            units.append(units[-1] * 2)

        # conv layers
        h = input_layer
        for i in range(self.cfg.n_layers):
            name = f'conv_{i}'
            h = layers.Conv1D(units[i], 5, self.strides, self.padding, kernel_initializer=self.initializer,
                              name=f'{name}_conv')(h)
            if i > 1:  # the first layer without batch-norm
                if self.cfg.d_norm is not None:
                    h = self.cfg.d_norm(name=f'{name}_norm')(h)
            h = layers.LeakyReLU(self.leak_slope, name=f'{name}_relu')(h)
            h = layers.Dropout(self.cfg.d_dropout_rate, name=f'{name}_dropout')(h)

        # fc layer
        flat = layers.Flatten()(h)
        out = layers.Dense(1)(flat)

        if with_flat:
            return out, flat
        else:
            return out

    def build_discriminator(self):
        input_layer = layers.Input(self.cfg.input_shape)
        output_layer = self.build_discriminator_network(input_layer)
        model = tf.keras.Model(input_layer, output_layer)
        return model

    @staticmethod
    def build_loss(real_logits, fake_logits):
        cross_entropy = tf.keras.losses.BinaryCrossentropy(from_logits=True)

        # The discriminator's loss quantifies how well it is able to distinguish real images from fakes.
        # It compares the discriminator's predictions on real samples to an array of 1s, and the discriminator's
        # predictions on fake (generated) samples to an array of 0s.
        real_loss = cross_entropy(tf.ones_like(real_logits), real_logits)
        fake_loss = cross_entropy(tf.zeros_like(fake_logits), fake_logits)
        d_loss = real_loss + fake_loss

        # The generator's loss quantifies how well it was able to trick the discriminator. Intuitively, if the generator
        # is performing well, the discriminator will classify the fake samples as real (or 1). Here, we will compare the
        # discriminators decisions on the generated samples to an array of 1s.
        g_loss = cross_entropy(tf.ones_like(fake_logits), fake_logits)

        return d_loss, g_loss, real_loss, fake_loss

    # Notice the use of `tf.function`. It causes the function to be "compiled".
    @tf.function
    def train_step(self, samples, acc_pre=tf.constant(0.0, dtype=tf.float32)):
        n_samples = samples.shape[0]  # it may be less than batch_size
        noise = self.cfg.noise_sampler([n_samples, self.cfg.noise_dim])

        # Note:
        # GradientTape.gradient can only be called once on non-persistent tapes, therefore there are two tapes according
        # to generator and discriminator, respectively.
        # For more details about tf.GradientTape, please refer to the official tutorial:
        # https://www.tensorflow.org/api_docs/python/tf/GradientTape.
        with tf.GradientTape() as g_tape, tf.GradientTape() as d_tape:
            generated_samples = self.g(noise, training=True)

            real_logits = self.d(samples, training=True)
            fake_logits = self.d(generated_samples, training=True)

            d_loss, g_loss, real_loss, fake_loss = self.build_loss(real_logits, fake_logits)

        def _opt_g():
            g_grad = g_tape.gradient(g_loss, self.g.trainable_variables)
            self.g_opt.apply_gradients(zip(g_grad, self.g.trainable_variables))

        def _opt_d():
            d_grad = d_tape.gradient(d_loss, self.d.trainable_variables)
            self.d_opt.apply_gradients(zip(d_grad, self.d.trainable_variables))

        def _null():
            pass

        _opt_g()
        tf.cond(acc_pre <= self.cfg.acc_threshold_to_train_d, true_fn=_opt_d, false_fn=_null)

        acc = gan_accuracy(real_logits, fake_logits)

        return d_loss, g_loss, real_loss, fake_loss, acc

    def fit(self, data, test_data=None, restore=False):
        self.cfg.logger.info("****** fit start ******")

        if test_data is None:
            test_data = data

        if restore:  # continue the last ckpt to train
            init_epoch = self.load_ckpt()
            self.cfg.logger.info(f"restore model from epoch-{init_epoch} and continue to train.")
            if init_epoch >= self.cfg.epochs:
                new_epochs = self.cfg.epochs + init_epoch
                self.cfg.logger.info(f"init_epoch={init_epoch} is grater than epochs={self.cfg.epochs}. "
                                     f"increase the upper bound to {new_epochs}")
                self.cfg.epochs = new_epochs
        else:
            self.cfg.logger.info("train from scratch.")
            self.cfg.clean_paths()
            init_epoch = 0

        # Batch and shuffle the data
        dataset = tf.data.Dataset.from_tensor_slices(data).shuffle(self.cfg.batch_size).batch(self.cfg.batch_size)

        def _get_file_name(ep):
            return os.path.join(self.cfg.train_dir, 'epoch_{:04d}.png'.format(ep))

        # take 1 batch of real samples to show
        real_samples = [e for e in dataset.take(1)][0]
        save_series(real_samples, os.path.join(self.cfg.train_dir, '000_real.png'))
        self.generate(self.noise_seed, _get_file_name(0))
        self.eval(test_data, 0)

        loss = {'epoch': [], 'batch': [], 'd_loss': [], 'g_loss': [], 'real_loss': [], 'fake_loss': []}
        acc_pre = tf.constant(0.0, dtype=tf.float32)
        for epoch in range(init_epoch, self.cfg.epochs):
            t_start = time()
            for i, batch in dataset.enumerate():
                d_loss, g_loss, real_loss, fake_loss, acc = self.train_step(batch, acc_pre)
                acc_pre = acc

                loss['epoch'].append(epoch+1)
                loss['batch'].append(i.numpy())
                loss['d_loss'].append(d_loss.numpy())
                loss['g_loss'].append(g_loss.numpy())
                loss['real_loss'].append(real_loss.numpy())
                loss['fake_loss'].append(fake_loss.numpy())

            self.cfg.logger.info(f"epoch[{epoch+1}/{self.cfg.epochs}], "
                                 f"d_loss={d_loss:.4}, g_loss={g_loss:.4}, real_loss={real_loss:.4}, "
                                 f"fake_loss={fake_loss:.4}, acc={acc:.4}, time={(time() - t_start):.4}")

            if (epoch + 1) % self.cfg.n_epochs_to_save_ckpt == 0:
                self.ckpt_manager.save(checkpoint_number=epoch+1)
                self.generate(self.noise_seed, _get_file_name(epoch+1))
            if (epoch + 1) % self.cfg.n_epochs_to_evaluate == 0:
                self.eval(test_data, epoch+1)

        # final epoch
        self.ckpt_manager.save(checkpoint_number=self.cfg.epochs)
        self.generate(self.noise_seed, _get_file_name(self.cfg.epochs))
        save_loss(pd.DataFrame(loss), self.cfg.train_dir)
        self.eval(test_data, self.cfg.epochs)

        self.cfg.logger.info("****** fit end ******")

    def generate(self, noise, file=None):
        # Notice `training` is set to False.
        # This is so all layers run in inference mode (batchnorm).
        generated_samples = self.g(noise, training=False)

        if file is not None:
            save_series(generated_samples, file)

        return generated_samples

    def discriminate(self, samples):
        # Notice `training` is set to False.
        # This is so all layers run in inference mode (batchnorm).
        predicted_labels = self.d(samples, training=False)
        return predicted_labels

    def load_ckpt(self):
        # self.ckpt.restore(tf.train.latest_checkpoint(self.cfg.ckpt_dir))
        if self.ckpt_manager.latest_checkpoint:
            # I did not apply assert_consumed() for the following reason, which is copied from the official
            # tutorial ( https://www.tensorflow.org/guide/checkpoint ):
            # "There are many objects in the checkpoint which haven't matched, including the layer's kernel and the
            # optimizer's variables. status.assert_consumed only passes if the checkpoint and the program match exactly,
            # and would throw an exception here."
            self.ckpt.restore(self.ckpt_manager.latest_checkpoint)
            print("Restored checkpoint from {}".format(self.ckpt_manager.latest_checkpoint))
            epoch = int(self.ckpt_manager.latest_checkpoint.split('-')[-1])
        else:
            print("Initializing from scratch.")
            epoch = 0
        return epoch

    def generate_batches(self, n_samples):
        n_batches = int(np.ceil(n_samples / self.cfg.batch_size))
        fake_data = []
        for _ in range(n_batches):
            noise = self.cfg.noise_sampler([self.cfg.batch_size, self.cfg.noise_dim])
            fake_samples = self.generate(noise)
            fake_data.append(fake_samples.numpy())
        fake_data = np.vstack(fake_data)
        return fake_data[:n_samples]

    def generate_real_fake_data(self, real_data, shuffle=True):
        n_samples = real_data.shape[0]
        fake_data = self.generate_batches(n_samples)
        assert fake_data.shape[0] == n_samples

        real_y = np.ones(n_samples)
        fake_y = np.zeros(n_samples)

        if shuffle:
            idx = self.cfg.np_rs.permutation(np.arange(n_samples))
            real_data = real_data[idx]
            fake_data = fake_data[idx]
            real_y = real_y[idx]
            fake_y = fake_y[idx]

        return real_data, fake_data, real_y, fake_y

    def eval(self, real_data, epoch=None, methods=['mmd', 'nnd']):
        self.cfg.logger.info("****** eval start ******")
        t0 = time()
        real_data, fake_data, real_y, fake_y = self.generate_real_fake_data(real_data)
        self.cfg.logger.info("time of generate_real_fake_data is {}".format(time()-t0))
        t0 = time()

        real_data_2d = np.reshape(real_data, (real_data.shape[0], -1))
        fake_data_2d = np.reshape(fake_data, (fake_data.shape[0], -1))

        if 'vis' in methods:  # visually compare distributions.
            # Note: It is time-consuming. Visual analysis is cumbersome.
            tsne(real_data_2d, fake_data_2d,
                 os.path.join(self.cfg.eval_dir, 'eval_tsne{}.png'.format('' if epoch is None else epoch)))
            self.cfg.logger.info("tsne, time={}".format(time()-t0))
            t0 = time()

        metrics = {'epoch': epoch}

        if 'dscore' in methods:
            # Note: It seems saturated and thus is hardly to reflect differences of methods.
            res = dscore(np.vstack([real_data, fake_data]), np.hstack([real_y, fake_y]), self.build_discriminator(),
                         self.cfg.epochs, self.cfg.batch_size, self.cfg.seed, verbose=self.cfg.verbose)
            metrics.update(res)
            self.cfg.logger.info("dscore, time={}".format(time()-t0))
            t0 = time()

        # scores to measure similarity
        if 'nnd' in methods:
            nnd = nnd_score(real_data_2d, fake_data_2d, metric='euclidean')
            self.cfg.logger.info("nnd, time={}".format(time()-t0))
            t0 = time()
            metrics.update({'nnd': np.round(np.float(nnd), 4)})
        if 'mmd' in methods:
            mmd = mmd_score(fake_data_2d, real_data_2d)
            self.cfg.logger.info("mmd, time={}".format(time()-t0))
            metrics.update({'mmd': np.round(np.float(mmd), 4)})

        with open(os.path.join(self.cfg.eval_dir, 'similarity.json'), 'a') as f:
            f.write(json.dumps(metrics) + "\n")

        self.cfg.logger.info("****** eval end ******")

        return metrics

    def eval_clf(self, x_tr, y_tr, x_te, y_te, n_classes, idx_layer=-3, epoch=None):
        self.cfg.logger.info("****** eval_clf start ******")
        t0 = time()
        acc_tr, acc_te = clf(self.d, x_tr, y_tr, x_te, y_te, n_classes, self.cfg.epochs, self.cfg.batch_size, idx_layer,
                             verbose=self.cfg.verbose)
        res = {'epoch': epoch, 'acc_tr': acc_tr, 'acc_te': acc_te}
        with open(os.path.join(self.cfg.eval_dir, 'clf_hidden.json'), 'a') as f:
            f.write(json.dumps(res) + "\n")

        self.cfg.logger.info("time={}".format(time()-t0))
        self.cfg.logger.info("****** eval_clf end ******")

        return res
