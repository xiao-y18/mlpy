import os
import shutil
import numpy as np
import tensorflow as tf

from research.tsgan.lib.utils import makedirs
from research.tsgan.model.ops import InstanceNormalization


def get_norm_class(method):
    if method == 'batch':
        return tf.keras.layers.BatchNormalization
    elif method == 'layer':
        return tf.keras.layers.LayerNormalization
    elif method == 'instance':
        return InstanceNormalization
    elif method == 'null':
        return None
    else:
        raise ValueError(f"normalization method={method} can not be found!")


class Configure(object):
    def __init__(self,
                 log_dir,
                 logger,
                 input_shape,
                 noise_dim=100,  # follows the tutorial
                 noise_method='normal',  # the method to generate noise
                 batch_size=16,  # ts dataset is usually small
                 epochs=300,
                 g_lr=0.0002,
                 d_lr=0.0002,
                 g_beta1=0.5,
                 d_beta1=0.5,
                 g_units_base=32,
                 d_units_base=32,
                 n_layers=4,  # >= 1
                 d_dropout_rate=0.0,  # default, without dropout
                 g_norm_method='batch',
                 d_norm_method='batch',
                 acc_threshold_to_train_d=1.0,  # default, no balance, D:G=1:1
                 n_epochs_to_save_ckpt=15,
                 n_examples_to_generate=16,
                 n_to_evaluate=3,  # trade-off: evaluation is time wasting.
                 ckpt_max_to_keep=2,  # spends memory
                 seed=42,
                 verbose=0,
                 **kwargs
                 ):
        self.logger = logger
        self.log_dir = log_dir
        self.logger.info("****** configure init ******")

        # # set dirs
        self.train_dir = makedirs(os.path.join(self.log_dir, 'training'))
        self.eval_dir = makedirs(os.path.join(self.log_dir, 'evaluation'))
        self.ckpt_dir = makedirs(os.path.join(self.log_dir, 'checkpoint'))
        self.ckpt_prefix = os.path.join(self.ckpt_dir, "ckpt")
        self.ckpt_max_to_keep = ckpt_max_to_keep

        # # hyper-parameters
        self.input_shape = input_shape
        # for noise
        if isinstance(noise_dim, np.float):
            self.noise_dim = int(np.ceil(noise_dim * self.input_shape[1]))
        elif isinstance(noise_dim, np.int):
            self.noise_dim = noise_dim
        else:
            raise ValueError(f"can not find a setting for noise_dim={noise_dim}")
        self.noise_method = noise_method
        if self.noise_method == 'normal':
            self.noise_sampler = tf.random.normal
        elif self.noise_method == 'uniform':
            # self.noise_sampler = tf_random_uniform()
            self.noise_sampler = lambda shape, seed=None: tf.random.uniform(shape, -1, 1, seed=seed)
        else:
            raise ValueError(f"noise_method={self.noise_method} can not be found!")
        # for training
        self.batch_size = batch_size
        self.epochs = epochs
        self.g_lr = g_lr
        self.d_lr = d_lr
        self.g_beta1 = g_beta1
        self.d_beta1 = d_beta1
        self.g_units_base = g_units_base
        self.d_units_base = d_units_base
        self.n_layers = n_layers
        self.d_dropout_rate = d_dropout_rate
        self.g_norm = get_norm_class(g_norm_method)
        self.d_norm = get_norm_class(d_norm_method)
        self.acc_threshold_to_train_d = tf.constant(acc_threshold_to_train_d, dtype=tf.float32)
        self.n_epochs_to_save_ckpt = n_epochs_to_save_ckpt
        self.n_to_evaluate = n_to_evaluate
        self.n_epochs_to_evaluate = self.epochs // self.n_to_evaluate
        self.n_examples_to_generate = n_examples_to_generate
        # random setting
        self.seed = seed
        self.np_rs = np.random.RandomState(self.seed)

        # other
        self.verbose = verbose

        log_str = "The parameter settings are as follows: \n"
        for key, value in self.__dict__.items():
            log_str += f'{key}:{value}\n'
        self.logger.info(log_str)

    def clean_paths(self):
        self.train_dir = makedirs(self.train_dir, clean=True)
        self.eval_dir = makedirs(self.eval_dir, clean=True)
        self.ckpt_dir = makedirs(self.ckpt_dir, clean=True)
