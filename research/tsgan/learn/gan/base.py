import tensorflow as tf
from tensorflow.keras import layers
import glob
import imageio
import os
import matplotlib.pyplot as plt
from time import time
import json

from research.tsgan.lib.utils import makedirs, tag_path, set_logging
from research.tsgan import configure

TAG = tag_path(os.path.abspath(__file__), 2)
DIR_LOG = makedirs(os.path.join(configure.DIR_LOG, TAG))
log = set_logging(TAG, DIR_LOG)


class DCGAN(object):
    def __init__(self, dir_log):
        log.info("****** __init__ start ******")

        # hyper-parameters
        self.dir_log = dir_log
        self.train_dir = makedirs(os.path.join(self.dir_log, 'training_outputs'))
        self.ckpt_dir = makedirs(os.path.join(self.dir_log, 'training_checkpoints'))
        self.ckpt_prefix = os.path.join(self.ckpt_dir, "ckpt")
        self.buffer_size = 6000  # Note: I use a smaller value than the tutorial's
        self.batch_size = 256
        self.noise_dim = 100
        self.epochs = 50
        self.num_examples_to_generate = 16
        # We will reuse this seed overtime (so it's easier)
        # to visualize progress in the animated GIF)
        self.seed = tf.random.normal([self.num_examples_to_generate, self.noise_dim])
        # training parameters
        self.g_lr = 1e-4
        self.d_lr = 1e-4

        # build generator
        self.g = self.build_generator()
        log.info("Generator's summary: ")
        self.g.summary(print_fn=log.info)
        # build discriminator
        self.d = self.build_discriminator()
        log.info("Discriminator's summary: ")
        self.d.summary(print_fn=log.info)

        # build optimizers
        self.g_opt = tf.keras.optimizers.Adam(self.g_lr)
        self.d_opt = tf.keras.optimizers.Adam(self.d_lr)

        log.info("****** __init__ end ******")

    @staticmethod
    def build_generator(**kwargs):
        raise NotImplementedError('This is an abstract method.')

    @staticmethod
    def build_discriminator():
        raise NotImplementedError('This is an abstract method.')

    @staticmethod
    def build_loss(real_output, fake_output):
        cross_entropy = tf.keras.losses.BinaryCrossentropy(from_logits=True)

        # The discriminator's loss quantifies how well it is able to distinguish real images from fakes.
        # It compares the discriminator's predictions on real images to an array of 1s, and the discriminator's
        # predictions on fake (generated) images to an array of 0s.
        real_loss = cross_entropy(tf.ones_like(real_output), real_output)
        fake_loss = cross_entropy(tf.zeros_like(fake_output), fake_output)
        d_loss = real_loss + fake_loss

        # The generator's loss quantifies how well it was able to trick the discriminator. Intuitively, if the generator
        # is performing well, the discriminator will classify the fake images as real (or 1). Here, we will compare the
        # discriminators decisions on the generated images to an array of 1s.
        g_loss = cross_entropy(tf.ones_like(fake_output), fake_output)

        return d_loss, g_loss, real_loss, fake_loss

    @staticmethod
    def save_image(images, path):
        fig = plt.figure(figsize=(4, 4))
        for i in range(images.shape[0]):
            plt.subplot(4, 4, i + 1)
            plt.imshow(images[i, :, :, 0] * 127.5 + 127.5, cmap='gray')
            plt.axis('off')
        plt.savefig(path)

    # Notice the use of `tf.function`
    # This annotation causes the function to be "compiled".
    @tf.function
    def train_step(self, images):
        noise = tf.random.normal([self.batch_size, self.noise_dim])

        # Note:
        # GradientTape.gradient can only be called once on non-persistent tapes, therefore there are two tapes according
        # to generator and discriminator, respectively.
        # For more details about tf.GradientTape, please refer to the official tutorial:
        # https://www.tensorflow.org/api_docs/python/tf/GradientTape.
        with tf.GradientTape() as g_tape, tf.GradientTape() as d_tape:
            generated_images = self.g(noise, training=True)

            real_output = self.d(images, training=True)
            fake_output = self.d(generated_images, training=True)

            d_loss, g_loss, real_loss, fake_loss = self.build_loss(real_output, fake_output)

        g_grad = g_tape.gradient(g_loss, self.g.trainable_variables)
        d_grad = d_tape.gradient(d_loss, self.d.trainable_variables)
        self.g_opt.apply_gradients(zip(g_grad, self.g.trainable_variables))
        self.d_opt.apply_gradients(zip(d_grad, self.d.trainable_variables))

    def fit(self, images):
        log.info("****** fit start ******")
        t_start_0 = time()

        # Batch and shuffle the data
        dataset = tf.data.Dataset.from_tensor_slices(images).shuffle(self.batch_size).batch(self.batch_size)

        checkpoint = tf.train.Checkpoint(generator_optimizer=self.g_opt,
                                         discriminator_optimizer=self.d_opt,
                                         generator=self.g,
                                         discriminator=self.d)

        def _path(ep):
            return os.path.join(self.train_dir, 'image_at_epoch_{:04d}.png'.format(ep))

        for epoch in range(self.epochs):
            t_start = time()
            for image_batch in dataset:
                self.train_step(image_batch)
            # Produce images for the GIF as we go
            self.generate(self.seed, _path(epoch))
            # Save the model every 15 epochs
            if (epoch + 1) % 15 == 0:
                checkpoint.save(file_prefix=self.ckpt_prefix)
            log.info('Time for epoch {} is {} sec'.format(epoch + 1, time() - t_start))
        # Generate after the final epoch
        self.generate(self.seed, _path(self.epochs))

        log.info('Time for training is {} sec'.format(time() - t_start_0))

        log.info("****** fit end ******")

    def generate(self, noise, path=None):
        # Notice `training` is set to False.
        # This is so all layers run in inference mode (batchnorm).
        generated_images = self.g(noise, training=False)

        if path is not None:
            self.save_image(generated_images, path)

        return generated_images

    def discriminate(self, images):
        # Notice `training` is set to False.
        # This is so all layers run in inference mode (batchnorm).
        predicted_labels = self.d(images, training=False)
        return predicted_labels
