"""
Reference: https://github.com/tensorflow/docs/blob/master/site/en/tutorials/generative/cyclegan.ipynb
    "This notebook assumes you are familiar with Pix2Pix, which you can learn about in the Pix2Pix tutorial. T
    he code for CycleGAN is similar, the main difference is an additional loss function, and the use of unpaired
    training data."

TODO:
 - this tutorial has lots of bugs. I fix some, but the program still fails to run. I spent almost a whole day, therefore I give up.

"""
import os
import time
import matplotlib.pyplot as plt
import tensorflow as tf
import datetime

from mlpy.configure import DIR_DATASET
from research.tsgan.lib.utils import makedirs, tag_path, set_logging
from research.tsgan import configure
from mlpy.data.image import hourse2zerbra
from research.tsgan.learn.gan import Pix2Pix, NormMethod


class CycleGAN(object):
    def __init__(self, log_dir, log):
        self.log_dir = log_dir
        self.log = log
        self.epochs = 40
        self.lambda_ = 10
        self.num_epochs_to_save_ckpt = 5
        self.output_channels = 3
        self.lambda_ = 100
        self.epochs = 150
        self.batch_size = 1
        self.num_epochs_to_save_ckpt = 20
        self.train_dir = makedirs(os.path.join(self.log_dir, 'training_outputs'))
        self.ckpt_dir = makedirs(os.path.join(self.log_dir, 'training_checkpoints'))
        self.ckpt_prefix = os.path.join(self.ckpt_dir, "ckpt")
        self.sum_dir = os.path.join(self.log_dir, 'summary', datetime.datetime.now().strftime("%Y%m%d-%H%M%S"))

        self.loss_object = tf.keras.losses.BinaryCrossentropy(from_logits=True)

        self.g_g = self.build_generator(self.output_channels)
        self.g_f = self.build_generator(self.output_channels)
        print(self.g_g.summary())
        print(self.g_f.summary())

        self.d_x = self.build_discriminator()
        self.d_y = self.build_discriminator()
        print(self.d_x.summary())
        print(self.d_y.summary())

        self.g_g_opt = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)
        self.g_f_opt = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)

        self.d_x_opt = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)
        self.d_y_opt = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)

        self.ckpt = tf.train.Checkpoint(g_g=self.g_g, g_f=self.g_f,
                                        d_x=self.d_x, d_y=self.d_y,
                                        g_g_opt=self.g_g_opt, g_f_opt=self.g_f_opt,
                                        d_x_opt=self.d_x_opt, d_y_opt=self.d_y_opt)

        self.sum_w = tf.summary.create_file_writer(self.sum_dir)

    @staticmethod
    def build_generator(output_channels, norm_type=NormMethod.INSTANCE_NORM):
        return Pix2Pix.build_generator(output_channels, norm_type)

    @staticmethod
    def build_discriminator(norm_type=NormMethod.INSTANCE_NORM, target=False):
        return Pix2Pix.build_discriminator(norm_type, target)

    def build_generator_loss(self, generated):
        return self.loss_object(tf.ones_like(generated), generated)

    def build_discriminator_loss(self, real, generated):
        real_loss = self.loss_object(tf.ones_like(real), real)

        generated_loss = self.loss_object(tf.zeros_like(generated), generated)

        total_disc_loss = real_loss + generated_loss

        return total_disc_loss * 0.5

    def calc_cycle_loss(self, real_image, cycled_image):
        loss1 = tf.reduce_mean(tf.abs(real_image - cycled_image))

        return self.lambda_ * loss1

    def identity_loss(self, real_image, same_image):
        loss = tf.reduce_mean(tf.abs(real_image - same_image))
        return self.lambda_ * 0.5 * loss

    @tf.function
    def train_step(self, real_x, real_y, epoch):
        # persistent is set to True because the tape is used more than
        # once to calculate the gradients.
        with tf.GradientTape(persistent=True) as tape:
            # Generator G translates X -> Y
            # Generator F translates Y -> X.
            fake_y = self.g_g(real_x, training=True)
            cycled_x = self.g_f(fake_y, training=True)

            fake_x = self.g_f(real_y, training=True)
            cycled_y = self.g_g(fake_x, training=True)

            # same_x and same_y are used for identity loss.
            same_x = self.g_f(real_x, training=True)
            same_y = self.g_g(real_y, training=True)

            disc_real_x = self.d_x(real_x, training=True)
            disc_real_y = self.d_y(real_y, training=True)

            disc_fake_x = self.d_x(fake_x, training=True)
            disc_fake_y = self.d_y(fake_y, training=True)

            # calculate the loss
            gen_g_loss = self.build_generator_loss(disc_fake_y)
            gen_f_loss = self.build_generator_loss(disc_fake_x)

            total_cycle_loss = self.calc_cycle_loss(real_x, cycled_x) + self.calc_cycle_loss(real_y, cycled_y)

            # Total generator loss = adversarial loss + cycle loss
            total_gen_g_loss = gen_g_loss + total_cycle_loss + self.identity_loss(real_y, same_y)
            total_gen_f_loss = gen_f_loss + total_cycle_loss + self.identity_loss(real_x, same_x)

            disc_x_loss = self.build_discriminator_loss(disc_real_x, disc_fake_x)
            disc_y_loss = self.build_discriminator_loss(disc_real_y, disc_fake_y)

        # Calculate the gradients for generator and discriminator
        generator_g_gradients = tape.gradient(total_gen_g_loss, self.g_g.trainable_variables)
        generator_f_gradients = tape.gradient(total_gen_f_loss, self.g_f.trainable_variables)
        discriminator_x_gradients = tape.gradient(disc_x_loss, self.d_x.trainable_variables)
        discriminator_y_gradients = tape.gradient(disc_y_loss, self.d_y.trainable_variables)

        # Apply the gradients to the optimizer
        self.g_g_opt.apply_gradients(zip(generator_g_gradients, self.g_g.trainable_variables))
        self.g_f_opt.apply_gradients(zip(generator_f_gradients, self.g_f.trainable_variables))
        self.d_x_opt.apply_gradients(zip(discriminator_x_gradients, self.d_x.trainable_variables))
        self.d_y_opt.apply_gradients(zip(discriminator_y_gradients, self.d_y.trainable_variables))

        with self.sum_w.as_default():
            tf.summary.scalar('g_g_total_loss', total_gen_g_loss, step=epoch)
            tf.summary.scalar('g_f_total_loss', total_gen_f_loss, step=epoch)
            tf.summary.scalar('d_x_loss', disc_x_loss, step=epoch)
            tf.summary.scalar('d_y_loss', disc_y_loss, step=epoch)

    def fit(self, train_horses, train_zebras):
        sample_horse = train_horses.take(1)
        for epoch in range(self.epochs):
            start = time.time()

            n = 0
            for image_x, image_y in tf.data.Dataset.zip((train_horses, train_zebras)):
                self.train_step(image_x, image_y, epoch)
                if n % 10 == 0:
                    print('.', end='')
                n += 1

            # Using a consistent image (sample_horse) so that the progress of the model
            # is clearly visible.
            self.generate(sample_horse)

            if (epoch + 1) % self.num_epochs_to_save_ckpt == 0:
                self.ckpt.save(file_prefix=self.ckpt_prefix)

            self.log.info('Time taken for epoch {} is {} sec\n'.format(epoch + 1, time.time() - start))
        self.ckpt.save(file_prefix=self.ckpt_prefix)

    def generate(self, test_input, file=None):
        prediction = self.g_g(test_input)
        plt.figure(figsize=(12, 12))

        display_list = [test_input[0], prediction[0]]
        title = ['Input Image', 'Predicted Image']

        for i in range(2):
            plt.subplot(1, 2, i + 1)
            plt.title(title[i])
            # getting the pixel values between [0, 1] to plot it.
            plt.imshow(display_list[i] * 0.5 + 0.5)
            plt.axis('off')

        if file is not None:
            plt.savefig(file)


if __name__ == '__main__':
    tag = tag_path(os.path.abspath(__file__), 2)
    log_dir = makedirs(os.path.join(configure.DIR_LOG, tag))
    log = set_logging(tag, log_dir)

    cycle_gan = CycleGAN(log_dir, log)

    train_horses, train_zebras, test_horses, test_zebras, data_path = hourse2zerbra.load_data(cycle_gan.batch_size,
                                                                                              DIR_DATASET)
    cycle_gan.fit(train_horses, train_zebras)






