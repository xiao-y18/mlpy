"""
On the base of :
- tutorial: https://www.tensorflow.org/tutorials/generative/pix2pix
- refactor for cyclegan : https://github.com/tensorflow/examples/blob/master/tensorflow_examples/models/pix2pix/pix2pix.py

Requirements:
- support tf.keras.utils.plot_model
    - https://graphviz.gitlab.io/download/
    - https://pypi.org/project/pydot/

TODO:
- restore latest checkpoint and test.

"""

import tensorflow as tf

import os
import time
import datetime

from matplotlib import pyplot as plt

from mlpy.configure import DIR_DATASET
from research.tsgan.lib.utils import makedirs, tag_path, set_logging
from research.tsgan import configure
from mlpy.data.image import facedes

TAG = tag_path(os.path.abspath(__file__), 2)
DIR_LOG = makedirs(os.path.join(configure.DIR_LOG, TAG))
log = set_logging(TAG, DIR_LOG)


class NormMethod(object):
    BATCH_NORM = 'batchnorm'
    INSTANCE_NORM = 'instancenorm'


class InstanceNormalization(tf.keras.layers.Layer):
  """ Instance Normalization Layer (https://arxiv.org/abs/1607.08022). """

  def __init__(self, epsilon=1e-5):
    super(InstanceNormalization, self).__init__()
    self.epsilon = epsilon

  def build(self, input_shape):
      self.scale = self.add_weight(
        name='scale',
        shape=input_shape[-1:],
        initializer=tf.random_normal_initializer(1., 0.02),
        trainable=True)

      self.offset = self.add_weight(
        name='offset',
        shape=input_shape[-1:],
        initializer='zeros',
        trainable=True)

  def call(self, x, **kwargs):
    mean, variance = tf.nn.moments(x, axes=[1, 2], keepdims=True)
    inv = tf.math.rsqrt(variance + self.epsilon)
    normalized = (x - mean) * inv
    return self.scale * normalized + self.offset


def downsample(filters, size, norm_type=NormMethod.BATCH_NORM, apply_norm=True):
    """ Conv -> Batchnorm -> Leaky ReLU """
    initializer = tf.random_normal_initializer(0., 0.02)

    result = tf.keras.Sequential()
    result.add(
        tf.keras.layers.Conv2D(filters, size,
                               strides=2, padding='same', kernel_initializer=initializer, use_bias=False))

    if apply_norm:
        if norm_type.lower() == NormMethod.BATCH_NORM:
            result.add(tf.keras.layers.BatchNormalization())
        elif norm_type.lower() == NormMethod.INSTANCE_NORM:
            result.add(InstanceNormalization())

    result.add(tf.keras.layers.LeakyReLU())

    return result


def upsample(filters, size, norm_type=NormMethod.BATCH_NORM, apply_dropout=False):
    """
    Each block in the decoder is
    (Transposed Conv -> Batchnorm -> Dropout(applied to the first 3 blocks) -> ReLU)
    """
    initializer = tf.random_normal_initializer(0., 0.02)

    result = tf.keras.Sequential()
    result.add(
        tf.keras.layers.Conv2DTranspose(filters, size,
                                        strides=2, padding='same', kernel_initializer=initializer, use_bias=False))

    if norm_type.lower() == NormMethod.BATCH_NORM:
        result.add(tf.keras.layers.BatchNormalization())
    elif norm_type.lower() == NormMethod.BATCH_NORM:
        result.add(InstanceNormalization())

    if apply_dropout:
        result.add(tf.keras.layers.Dropout(0.5))

    result.add(tf.keras.layers.ReLU())

    return result


class Pix2Pix(object):
    def __init__(self, dir_log):
        self.dir_log = dir_log
        self.output_channels = 3
        self.lambda_ = 100
        self.epochs = 150
        self.batch_size = 1
        self.num_epochs_to_save_ckpt = 20
        self.train_dir = makedirs(os.path.join(self.dir_log, 'training_outputs'))
        self.ckpt_dir = makedirs(os.path.join(self.dir_log, 'training_checkpoints'))
        self.ckpt_prefix = os.path.join(self.ckpt_dir, "ckpt")
        self.sum_dir = os.path.join(self.dir_log, 'summary', datetime.datetime.now().strftime("%Y%m%d-%H%M%S"))

        self.loss_object = tf.keras.losses.BinaryCrossentropy(from_logits=True)

        self.g = self.build_generator(self.output_channels)
        tf.keras.utils.plot_model(self.g, to_file=os.path.join(self.dir_log, 'model_g.png'), show_shapes=True, dpi=64)
        self.d = self.build_discriminator()
        tf.keras.utils.plot_model(self.d, to_file=os.path.join(self.dir_log, 'model_d.png'), show_shapes=True, dpi=64)

        self.g_opt = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)
        self.d_opt = tf.keras.optimizers.Adam(2e-4, beta_1=0.5)

        self.ckpt = tf.train.Checkpoint(g_opt=self.g_opt, d_opt=self.d_opt, g=self.g, d=self.d)

        self.sum_w = tf.summary.create_file_writer(self.sum_dir)

    @staticmethod
    def build_generator(output_channels, norm_type=NormMethod.BATCH_NORM):
        """ Modified u-net generator model (https://arxiv.org/abs/1611.07004). """
        down_stack = [
            downsample(64, 4, norm_type, apply_norm=False),  # (bs, 128, 128, 64)
            downsample(128, 4, norm_type),  # (bs, 64, 64, 128)
            downsample(256, 4, norm_type),  # (bs, 32, 32, 256)
            downsample(512, 4, norm_type),  # (bs, 16, 16, 512)
            downsample(512, 4, norm_type),  # (bs, 8, 8, 512)
            downsample(512, 4, norm_type),  # (bs, 4, 4, 512)
            downsample(512, 4, norm_type),  # (bs, 2, 2, 512)
            downsample(512, 4, norm_type),  # (bs, 1, 1, 512)
        ]

        up_stack = [
            upsample(512, 4, norm_type, apply_dropout=True),  # (bs, 2, 2, 1024)
            upsample(512, 4, norm_type, apply_dropout=True),  # (bs, 4, 4, 1024)
            upsample(512, 4, norm_type, apply_dropout=True),  # (bs, 8, 8, 1024)
            upsample(512, 4, norm_type),  # (bs, 16, 16, 1024)
            upsample(256, 4, norm_type),  # (bs, 32, 32, 512)
            upsample(128, 4, norm_type),  # (bs, 64, 64, 256)
            upsample(64, 4, norm_type),  # (bs, 128, 128, 128)
        ]

        initializer = tf.random_normal_initializer(0., 0.02)
        last = tf.keras.layers.Conv2DTranspose(output_channels, 4,
                                               strides=2,
                                               padding='same',
                                               kernel_initializer=initializer,
                                               activation='tanh')  # (bs, 256, 256, 3)

        concat = tf.keras.layers.Concatenate()

        # inputs = tf.keras.layers.Input(shape=[256, 256, 3])
        inputs = tf.keras.layers.Input(shape=[None, None, 3])
        x = inputs

        # Downsampling through the model
        skips = []
        for down in down_stack:
            x = down(x)
            skips.append(x)

        skips = reversed(skips[:-1])

        # Upsampling and establishing the skip connections
        for up, skip in zip(up_stack, skips):
            x = up(x)
            x = concat([x, skip])

        x = last(x)

        return tf.keras.Model(inputs=inputs, outputs=x)

    @staticmethod
    def build_discriminator(norm_type=NormMethod.BATCH_NORM, target=True):
        """ PatchGan discriminator model (https://arxiv.org/abs/1611.07004). """
        initializer = tf.random_normal_initializer(0., 0.02)

        inp = tf.keras.layers.Input(shape=[None, None, 3], name='input_image')
        x = inp

        if target:
            tar = tf.keras.layers.Input(shape=[None, None, 3], name='target_image')
            x = tf.keras.layers.concatenate([inp, tar])  # (bs, 256, 256, channels*2)

        down1 = downsample(64, 4, norm_type, False)(x)  # (bs, 128, 128, 64)
        down2 = downsample(128, 4, norm_type)(down1)  # (bs, 64, 64, 128)
        down3 = downsample(256, 4, norm_type)(down2)  # (bs, 32, 32, 256)

        zero_pad1 = tf.keras.layers.ZeroPadding2D()(down3)  # (bs, 34, 34, 256)
        conv = tf.keras.layers.Conv2D(512, 4, strides=1,
                                      kernel_initializer=initializer,
                                      use_bias=False)(zero_pad1)  # (bs, 31, 31, 512)

        if norm_type.lower() == NormMethod.BATCH_NORM:
            norm1 = tf.keras.layers.BatchNormalization()(conv)
        elif norm_type.lower() == NormMethod.INSTANCE_NORM:
            norm1 = InstanceNormalization()(conv)
        else:
            raise ValueError("norm_type = {} can not  be found!".format(norm_type))

        leaky_relu = tf.keras.layers.LeakyReLU()(norm1)

        zero_pad2 = tf.keras.layers.ZeroPadding2D()(leaky_relu)  # (bs, 33, 33, 512)

        last = tf.keras.layers.Conv2D(1, 4, strides=1,
                                      kernel_initializer=initializer)(zero_pad2)  # (bs, 30, 30, 1)

        if target:
            return tf.keras.Model(inputs=[inp, tar], outputs=last)
        else:
            return tf.keras.Model(inputs=inp, outputs=last)

    def build_generator_loss(self, disc_generated_output, gen_output, target):
        gan_loss = self.loss_object(tf.ones_like(disc_generated_output), disc_generated_output)

        # mean absolute error
        l1_loss = tf.reduce_mean(tf.abs(target - gen_output))

        total_gen_loss = gan_loss + (self.lambda_ * l1_loss)

        return total_gen_loss, gan_loss, l1_loss

    def build_discriminator_loss(self, disc_real_output, disc_generated_output):
        real_loss = self.loss_object(tf.ones_like(disc_real_output), disc_real_output)

        generated_loss = self.loss_object(tf.zeros_like(disc_generated_output), disc_generated_output)

        total_disc_loss = real_loss + generated_loss

        return total_disc_loss

    @tf.function
    def train_step(self, input_image, target_image, epoch):
        with tf.GradientTape() as g_tape, tf.GradientTape() as d_tape:
            g_output = self.g(input_image, training=True)

            d_real_output = self.d([input_image, target_image], training=True)
            d_generated_output = self.d([input_image, g_output], training=True)

            g_total_loss, g_gan_loss, ge_l1_loss = self.build_generator_loss(d_generated_output, g_output, target_image)
            d_loss = self.build_discriminator_loss(d_real_output, d_generated_output)

        g_grad = g_tape.gradient(g_total_loss, self.g.trainable_variables)
        d_grad = d_tape.gradient(d_loss, self.d.trainable_variables)

        self.g_opt.apply_gradients(zip(g_grad, self.g.trainable_variables))
        self.d_opt.apply_gradients(zip(d_grad, self.d.trainable_variables))

        with self.sum_w.as_default():
            tf.summary.scalar('g_total_loss', g_total_loss, step=epoch)
            tf.summary.scalar('g_gan_loss', g_gan_loss, step=epoch)
            tf.summary.scalar('g_l1_loss', ge_l1_loss, step=epoch)
            tf.summary.scalar('d_loss', d_loss, step=epoch)

    def fit(self, train_ds, test_ds):

        def _img_file(ep):
            return os.path.join(self.train_dir, 'test_image_at_epoch_{:04d}.png'.format(ep))

        for epoch in range(self.epochs):
            start = time.time()

            for example_input, example_target in test_ds.take(1):
                self.generate(example_input, example_target, _img_file(epoch))

            # Train
            for n, (input_image, target) in train_ds.enumerate():
                print('.', end='')
                if (n + 1) % 100 == 0:
                    print()
                self.train_step(input_image, target, epoch)

            if (epoch + 1) % self.num_epochs_to_save_ckpt == 0:
                self.ckpt.save(file_prefix=self.ckpt_prefix)

            log.info('Time taken for epoch {} is {} sec\n'.format(epoch + 1, time.time() - start))
        self.ckpt.save(file_prefix=self.ckpt_prefix)

    def generate(self, test_input, target, file=None):
        """
        Note: The training=True is intentional here since we want the batch statistics while running the model on the
        test dataset. If we use training=False, we will get the accumulated statistics learned from the training dataset
        (which we don't want)
        """
        prediction = self.g(test_input, training=True)
        plt.figure(figsize=(15, 15))

        display_list = [test_input[0], target[0], prediction[0]]
        title = ['Input Image', 'Ground Truth', 'Predicted Image']

        for i in range(3):
            plt.subplot(1, 3, i + 1)
            plt.title(title[i])
            # getting the pixel values between [0, 1] to plot it.
            plt.imshow(display_list[i] * 0.5 + 0.5)
            plt.axis('off')
        if file is not None:
            plt.savefig(file)

    def load(self):
        self.ckpt.restore(tf.train.latest_checkpoint(self.ckpt_dir))


def program_test(data_path, pix2pix, dir_log):
    log.info("******  program_test start ****** ")

    dir_log = makedirs(os.path.join(dir_log, 'program_test'))

    input_image, real_image = facedes.load_image(data_path + 'train/100.jpg')
    plt.imshow(input_image/225.0)
    plt.savefig(os.path.join(dir_log, 'input_image.png'))
    plt.close()
    plt.imshow(real_image/225.0)
    plt.savefig(os.path.join(dir_log, 'real_image.png'))
    plt.close()

    log.info("*** test random_jitter ***")
    plt.figure(figsize=(6, 6))
    for i in range(4):
        rj_inp, rj_re = facedes.random_jitter_image(input_image, real_image)
        plt.subplot(2, 2, i + 1)
        plt.imshow(rj_inp / 255.0)
        plt.axis('off')
    plt.savefig(os.path.join(dir_log, 'random_jitter_image.png'))
    plt.close()

    log.info("*** test downsample and upsample programs ***")
    log.info("input_image = {}".format(input_image.shape))

    down_model = downsample(3, 4)
    down_result = down_model(tf.expand_dims(input_image, 0))
    log.info("downsample(3, 4), down_result={}".format(down_result.shape))

    up_model = upsample(3, 4)
    up_result = up_model(down_result)
    log.info("upsample(3, 4), up_result={}".format(up_result.shape))

    log.info("*** test Pix2Pix object ***")
    g_output = pix2pix.g(input_image[tf.newaxis, ...], training=False)
    plt.imshow(g_output[0, ...])
    plt.savefig(os.path.join(dir_log, 'g.png'))
    plt.close()

    d_output = pix2pix.d([input_image[tf.newaxis, ...], g_output], training=False)
    plt.imshow(d_output[0, ..., -1], vmin=-20, vmax=20, cmap='RdBu_r')
    plt.colorbar()
    plt.savefig(os.path.join(dir_log, 'd_from_g.png'))
    plt.close()

    log.info("******  program_test end ****** ")


if __name__ == '__main__':
    pix2pix = Pix2Pix(DIR_LOG)
    train_dataset, test_dataset, data_path = facedes.load_data(pix2pix.batch_size, cache_dir=DIR_DATASET)

    program_test(data_path, pix2pix, DIR_LOG)

    pix2pix.fit(train_dataset, test_dataset)

