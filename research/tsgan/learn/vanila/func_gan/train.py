"""
    reference: https://github.com/MorvanZhou/Tensorflow-Tutorial/blob/master/tutorial-contents/406_GAN.py
"""
import argparse
import os

import numpy as np
import tensorflow as tf
from six.moves import xrange

import gan
from mlpy.lib import utils
from mlpy.lib.utils import data

seed = 1
tf.set_random_seed(seed)
np.random.seed(seed)


def train(params):
    if not os.path.exists(params.out_root):
        os.makedirs(params.out_root)
    # prepare variable
    model = gan.GAN(params.batch_size,
                    params.z_dim, params.x_dim,
                    params.g_hidden_units, params.d_hidden_units,
                    params.g_learning_rate, params.d_learning_rate)
    distr_x = data.DataDistribution(
        params.x_dim,
        lambda X: data.func_quadratic(X, coef_min=1, coef_max=2),
        range=1
    )
    distr_z = data.GeneratorDistribution(params.z_dim)

    # plot the real sample
    utils.plot_samples(distr_x.sample(params.batch_size),
                       '00_real_sample',
                       params.out_root)

    # train
    with tf.Session() as session:
        # initialization
        tf.local_variables_initializer().run()
        tf.global_variables_initializer().run()
        d_losses = []
        g_losses = []
        for step in xrange(params.train_steps):
            # update discriminator
            d_loss = np.inf
            for i in xrange(params.d_steps):
                x = distr_x.sample(params.batch_size)
                z = distr_z.sample(params.batch_size)
                d_loss, _ = session.run(
                    [model.d_loss, model.d_opt],
                    feed_dict={
                        model.x: x,
                        model.z: z
                    })
            # update generator
            g_loss = np.inf
            for i in xrange(params.g_steps):
                z = distr_z.sample(params.batch_size)
                g_loss, _ = session.run(
                    [model.g_loss, model.g_opt],
                    feed_dict={
                        model.z: z
                    }
                )
            # log training information
            d_losses.append(d_loss)
            g_losses.append(g_loss)
            if step % params.print_freq == 0:
                print('step:{}, d_loss:{:.4f}, g_loss:{:.4f}'
                      .format(step, d_loss, g_loss))
            if step % params.plot_freq == 0 or step == params.train_steps-1:
                z = distr_z.sample(params.batch_size)
                g_samples = session.run(
                    model.G,
                    feed_dict={
                        model.z: z
                    }
                )
                utils.plot_samples(g_samples,
                                   '{}_generated_sample'.format(step),
                                   os.path.join(params.out_root))


        # plot loss
        d_losses = np.array(d_losses)
        g_losses = np.array(g_losses)
        utils.plot_loss(g_losses, d_losses, os.path.join(params.out_root,
                                                         'loss.png'))


def parse_args():
    parser = argparse.ArgumentParser()
    # training parameters
    parser.add_argument('--train_steps', type=int, default=5000,
                        help='the whole training steps.')
    parser.add_argument('--g_steps', type=int, default=1,
                        help='the updating steps of generator in each '
                             'training epoch.')
    parser.add_argument('--d_steps', type=int, default=1,
                        help='the updating steps of discriminator in each '
                             'training epoch.')
    parser.add_argument('--batch_size', type=int, default=64,
                        help='the batch size')
    # model parameters
    parser.add_argument('--g_hidden_units', type=int, default=128,
                        help='the number of hidden unit of generator.')
    parser.add_argument('--d_hidden_units', type=int, default=128,
                        help='the number of hidden unit of discriminator.')
    parser.add_argument('--g_learning_rate', type=float, default=0.0001,
                        help='the learning rate of generator.')
    parser.add_argument('--d_learning_rate', type=float, default=0.0001,
                        help='teh learning rate of discriminator.')

    # data distribution parameters
    parser.add_argument('--z_dim', type=int, default=5,
                        help='the dimension of noise sample z.')
    parser.add_argument('--x_dim', type=int, default=15,
                        help='the dimension of real data sample x.')
    # log parameters
    parser.add_argument('--print_freq', type=int, default=10,
                        help='the print frequency.')
    parser.add_argument('--plot_freq', type=int, default=500,
                        help='the plot frequency.')
    parser.add_argument('--out_root', type=str, default='./density_gan',
                        help='the output root directory.')

    return parser.parse_args()


if __name__ == '__main__':
    train(parse_args())
