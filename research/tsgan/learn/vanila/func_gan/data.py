import numpy as np

from six.moves import xrange


class DataDistribution(object):
    def __init__(self, seq_length, func, range=1):
        """
        
        :param seq_length: 
        :param func: 
        :param range: 
        """
        self.seq_length = seq_length
        self.func = func
        self.range = range

    def sample(self, batch_size):
        """
        
        :param batch_size: 
        :return: 
        """
        X = np.vstack([np.linspace(-self.range, self.range, self.seq_length)
                       for _ in xrange(batch_size)])
        return self.func(X)


class GeneratorDistribution(object):
    def __init__(self, seq_length):
        """
        
        :param seq_length: 
        """
        self.seq_length = seq_length

    def sample(self, batch_size):
        """
        
        :param batch_size: 
        :return: 
        """
        Z = np.random.randn(batch_size, self.seq_length)
        return Z


def func_quadratic(X, coef_min=1, coef_max=2):
    """
    
    :param X: 
    :param coef_min: 
    :param coef_max: 
    :return: 
    """
    a = np.random.uniform(coef_min, coef_max, X.shape[0])[:, np.newaxis]
    y = a * np.power(X, 2) + (a-1)
    return y








