import matplotlib.pyplot as plt
import os


def plot_loss(g_losses, d_losses, out_path):
    """
    
    :param g_losses: 
    :param d_losses: 
    :param out_path: 
    :return: 
    """
    plt.figure()
    plt.plot(g_losses, label='g_loss')
    plt.plot(d_losses, label='d_loss')
    plt.title('loss')
    plt.xlabel('step')
    plt.ylabel('loss')
    plt.legend(loc='best')
    plt.savefig(out_path)


def plot_samples(samples, title, out_root):
    """
    
    :param samples: 
    :param title: 
    :param out_root: 
    :return: 
    """
    nrows = min(2, samples.shape[0] // 2)
    fig, axes = plt.subplots(nrows=nrows, ncols=2)
    plt.title(title)
    axes = axes.flat[:]
    for i, ax in enumerate(axes):
        ax.plot(samples[i])
    plt.savefig(os.path.join(out_root, '{}.png'.format(title)))


if __name__ == '__main__':
    print("haha")
