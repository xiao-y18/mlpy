import tensorflow as tf


def log(x):
    """safe log"""
    return tf.log(tf.maximum(x, 1e-5))


def linear_op(in_x, out_dim, scope=None, stddev=1.0):
    """
    
    :param in_x: 
    :param out_dim: 
    :param scope: 
    :param stddev: 
    :return: 
    """
    with tf.variable_scope(scope or 'linear'):
        weights = tf.get_variable(
            name='weights',
            shape=[in_x.shape[1], out_dim],
            initializer=tf.random_normal_initializer(stddev=stddev)
        )
        bias = tf.get_variable(
            name='bias',
            shape=[out_dim],
            initializer=tf.constant_initializer()
        )
        return tf.matmul(in_x, weights) + bias


def optimizer(loss, vars_list, learning_rate=0.001):
    """
    
    :param loss: 
    :param vars_list: 
    :param learning_rate: 
    :return: 
    """
    step = tf.Variable(0, trainable=False)
    with tf.name_scope('optimizer'):
        opt = tf.train.AdamOptimizer(learning_rate=learning_rate)\
            .minimize(loss=loss, var_list=vars_list, global_step=step)
        return opt


class GAN(object):
    def __init__(self, batch_size,
                 z_dim, x_dim,
                 g_hidden_units, d_hidden_units,
                 g_learning_rate, d_learning_rate):
        """
        
        :param batch_size: 
        :param z_dim: 
        :param x_dim: 
        :param g_hidden_units:                       
        :param d_hidden_units: 
        :param g_learning_rate: 
        :param d_learning_rate: 
        """
        # set up generator
        with tf.variable_scope('G'):
            self.z = tf.placeholder(tf.float32, shape=[batch_size, z_dim])
            self.G = self.generator(self.z, x_dim, g_hidden_units)

        # set up discriminator
        self.x = tf.placeholder(tf.float32, shape=[batch_size, x_dim])
        with tf.variable_scope('D'):
            self.DX = self.discriminator(self.x, d_hidden_units)
        with tf.variable_scope('D', reuse=True):
            self.DZ = self.discriminator(self.G, d_hidden_units)

        # calculate loss
        self.d_loss = tf.reduce_mean(-log(self.DX) - log(1 - self.DZ))
        self.g_loss = tf.reduce_mean(-log(self.DZ))

        # get trainable variables
        vars = tf.trainable_variables()
        self.g_vars = [v for v in vars if v.name.startswith('G/')]
        self.d_vars = [v for v in vars if v.name.startswith('D/')]

        # optimize
        self.g_opt = optimizer(self.g_loss, self.g_vars, g_learning_rate)
        self.d_opt = optimizer(self.d_loss, self.d_vars, d_learning_rate)

    @staticmethod
    def generator(in_z, out_dim, hidden_units):
        """
        
        :param in_z: 
        :param out_dim: 
        :param hidden_units: 
        :return: 
        """

        # h0 = tf.nn.relu(linear_op(in_z, hidden_units, scope='generator_0'))
        # h1 = linear_op(h0, out_dim, scope='generator_1')
        h0 = tf.layers.dense(in_z, hidden_units, tf.nn.relu)
        h1 = tf.layers.dense(h0, out_dim)
        return h1

    @staticmethod
    def discriminator(in_x, hidden_units):
        """
        
        :param in_x: 
        :param hidden_units: 
        :return: 
        """

        # h0 = tf.nn.relu(linear_op(in_x, hidden_units, scope='discriminator_0'))
        # #h1 = tf.nn.relu(linear_op(h0, hidden_units, scope='discriminator_1'))
        # #h2 = tf.nn.relu(linear_op(h1, hidden_units, scope='discriminator_2'))
        # prob = tf.nn.sigmoid(linear_op(h0, 1, scope='discriminator_3'))
        # return prob
        h0 = tf.layers.dense(in_x, hidden_units, tf.nn.relu)
        h1 = tf.layers.dense(h0, 1, tf.nn.sigmoid)
        return h1


