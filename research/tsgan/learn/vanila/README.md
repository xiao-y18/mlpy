# This is a demo to use GAN

All of realization are based on the first version GAN proposed by GoodFellow. 

- [ x ] density_gan
    - data set: simulating Gaussian distribution.
    - key points:
       - the noise sampled from generator should go with **stratified sampling** to insure. [more detail](http://blog.evjang.com/2016/06/generative-adversarial-nets-in.html) 
       - discriminator should be more powerful than the generator.
       - tanh activator maybe better.
    - reference: 
       - An introduction to Generative Adversarial Networks. [blog](http://blog.aylien.com/introduction-generative-adversarial-networks-code-tensorflow/). It is a good start to learn GAN. 
       - Improving sample diversity. [minibatch](https://arxiv.org/abs/1606.03498) can alliviate the problem of **generator collapsing**.
- [ x ] func_gan
   - This program try to generate equal-distance time series which produced by spercific function (e.g quadratic function). 
   - reference:
      - [source code](https://github.com/MorvanZhou/Tensorflow-Tutorial/blob/master/tutorial-contents/406_GAN.py)  
      - [blog](https://morvanzhou.github.io/tutorials/machine-learning/torch/4-06-GAN/)
- [ x ] image_gan
   - This is an example to generate image by GAN.
   - reference:
      - [source code](https://github.com/wiseodd/generative-models/tree/master/GAN/vanilla_gan)
 - result of each program below directory `result/`
