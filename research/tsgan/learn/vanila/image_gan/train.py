import argparse
import os

import numpy as np
import tensorflow as tf
from six.moves import xrange
from tensorflow.examples.tutorials.mnist import input_data

import gan
from mlpy.lib import utils

os.environ['CUDA_VISIBLE_DEVICES'] = "1"


def sample_z(size):
    return np.random.uniform(-1, 1, size)


def train(params):
    if not os.path.exists(params.out_root):
        os.makedirs(params.out_root)
    # set up variable
    model = gan.GAN(params.z_dim, params.x_dim,
                    params.g_hidden_units, params.d_hidden_units,
                    params.g_learning_rate, params.d_learning_rate)
    mnist = input_data.read_data_sets(params.data_dir, one_hot=True).train

    with tf.Session() as session:
        # initialize
        tf.local_variables_initializer().run()
        tf.global_variables_initializer().run()

        g_losses = []
        d_losses = []
        for step in xrange(params.train_steps):
            # update discriminator
            d_loss = np.inf
            for i in xrange(params.d_steps):
                x, _ = mnist.next_batch(params.batch_size)
                z = sample_z([params.batch_size, params.z_dim])
                d_loss, _ = session.run([model.d_loss, model.d_opt],
                                        feed_dict={
                                            model.x: x,
                                            model.z: z
                                        })
            d_losses.append(d_loss)

            # update generator
            g_loss = np.inf
            for i in xrange(params.g_steps):
                z = sample_z([params.batch_size, params.z_dim])
                g_loss, _ = session.run([model.g_loss, model.g_opt],
                                        feed_dict={
                                            model.z: z
                                        })
            g_losses.append(g_loss)

            # log
            if step % params.print_freq == 0:
                print('step:{}, d_loss:{:.4f}, g_loss:{:.4f}'
                      .format(step, d_loss, g_loss))
            if step % params.plot_freq == 0 or step == params.train_steps-1:
                z = sample_z([16, params.z_dim])
                samples = session.run(model.G,
                                      feed_dict={
                                          model.z: z
                                      })
                utils.plot_images(samples,
                                  os.path.join(params.out_root,
                                               '{}_generated.png'.format(step)),
                                  params.image_height, params.image_width)

        # plot loss
        g_losses = np.array(g_losses)
        d_losses = np.array(d_losses)
        utils.plot_loss(g_losses, d_losses, params.out_root)


def parse_args():
    parser = argparse.ArgumentParser('GAN for images')
    # feed parameter
    parser.add_argument('--data_dir', type=str,
                        default='../../../data/mnist',
                        help='the data directory.')
    parser.add_argument('--x_dim', type=int, default=784,
                        help='the dimension of real data.')
    parser.add_argument('--image_height', type=int, default=28,
                        help='the height of image.')
    parser.add_argument('--image_width', type=int, default=28,
                        help='the width of image.')
    parser.add_argument('--z_dim', type=int, default=100,
                        help='the dimension of noise data.')
    # model parameter
    parser.add_argument('--g_hidden_units', type=int, default=128,
                        help='the hidden units of generator.')
    parser.add_argument('--d_hidden_units', type=int, default=128,
                        help='the hidden units of discriminator.')
    # train parameter
    parser.add_argument('--batch_size', type=int, default=128,
                        help='the batch size.')
    parser.add_argument('--g_learning_rate', type=int, default=0.001,
                        help='the learning rate of generator.')
    parser.add_argument('--d_learning_rate', type=int, default=0.001,
                        help='the learning rate of discriminator.')
    parser.add_argument('--train_steps', type=int, default=100000,
                        help='the train step of whole model.')
    parser.add_argument('--g_steps', type=int, default=1,
                        help='the train step of generator in each train epoch.')
    parser.add_argument('--d_steps', type=int, default=1,
                        help='the train step of discriminator in each train '
                             'epoch.')
    # log parameter
    parser.add_argument('--print_freq', type=int, default=100,
                        help='the print frequency.')
    parser.add_argument('--plot_freq', type=int, default=10000,
                        help='the plot frequency.')
    parser.add_argument('--out_root', type=str, default='./density_gan',
                        help='the output root directory.')

    return parser.parse_args()


if __name__ == '__main__':
    train(parse_args())
