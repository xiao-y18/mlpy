import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import os


def plot_loss(g_losses, d_losses, out_root):
    fig = plt.figure()
    plt.plot(g_losses)
    plt.plot(d_losses)
    plt.xlabel('step')
    plt.ylabel('loss')
    plt.savefig(os.path.join(out_root, 'loss.png'))
    plt.close(fig)


def plot_images(samples, out_path=None, row=28, col=28):
    f_row = min(4, samples.shape[0] // 4)
    fig = plt.figure(figsize=(f_row, 4))
    gs = gridspec.GridSpec(f_row, 4)
    gs.update(wspace=0.05, hspace=0.05)

    for i, sample in enumerate(samples):
        ax = plt.subplot(gs[i])
        plt.axis('off')
        ax.set_xticklabels([])
        ax.set_yticklabels([])
        ax.set_aspect('equal')
        plt.imshow(sample.reshape(row, col), cmap='Greys_r')

    if out_path:
        plt.savefig(out_path)
    plt.close(fig)
