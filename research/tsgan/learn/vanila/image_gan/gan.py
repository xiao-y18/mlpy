import tensorflow as tf


def log(x):
    """safe log"""
    return tf.log(tf.maximum(x, 1e-5))


def optimizer(loss, var_list, learning_rate=0.001):
    """
    
    :param loss: 
    :param var_list: 
    :param learning_rate: 
    :return: 
    """
    step = tf.Variable(0, trainable=False)
    with tf.variable_scope('optimizer'):
        opt = tf.train.AdamOptimizer(learning_rate=learning_rate) \
            .minimize(
            loss=loss, var_list=var_list, global_step=step
        )
        return opt


class GAN(object):
    def __init__(self,
                 z_dim, x_dim,
                 g_hidden_units, d_hidden_units,
                 g_learning_rate, d_learning_rate):
        """
        
        :param z_dim: 
        :param x_dim: 
        :param g_hidden_units: 
        :param d_hidden_units: 
        :param g_learning_rate: 
        :param d_learning_rate: 
        """
        # set up generator
        with tf.variable_scope('G'):
            self.z = tf.placeholder(tf.float32, shape=[None, z_dim],
                                    name='noise')
            self.G = self.generator(self.z, g_hidden_units, x_dim)
        # set up discriminator
        self.x = tf.placeholder(tf.float32, shape=[None, x_dim],
                                name='real_data')
        with tf.variable_scope('D'):
            self.DX = self.discriminator(self.x, d_hidden_units)
        with tf.variable_scope('D', reuse=True):
            self.DZ = self.discriminator(self.G, d_hidden_units)

        # calculate loss
        self.d_loss = tf.reduce_mean(-log(self.DX) - log(1 - self.DZ))
        self.g_loss = tf.reduce_mean(-log(self.DZ))

        # get trainable variables
        var_list = tf.trainable_variables()
        self.d_vars = [v for v in var_list if v.name.startswith('D/')]
        self.g_vars = [v for v in var_list if v.name.startswith('G/')]

        # optimize
        self.d_opt = optimizer(self.d_loss, self.d_vars, d_learning_rate)
        self.g_opt = optimizer(self.g_loss, self.g_vars, g_learning_rate)

    @staticmethod
    def discriminator(in_x, hidden_units):
        """
        
        :param in_x: 
        :param hidden_units: 
        :return: 
        """
        h0 = tf.layers.dense(in_x, hidden_units, tf.nn.relu,
                             name='discriminator_0')
        h1 = tf.layers.dense(h0, 1, tf.nn.sigmoid,
                             name='discriminator_1')
        return h1

    @staticmethod
    def generator(in_z, hidden_units, out_dim):
        """
        
        :param in_z: 
        :param hidden_units: 
        :param out_dim: 
        :return: 
        """
        h0 = tf.layers.dense(in_z, hidden_units, tf.nn.relu,
                             name='generator_0')
        h1 = tf.layers.dense(h0, out_dim, tf.nn.sigmoid,
                             name='generator_1')
        return h1
