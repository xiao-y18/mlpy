"""
    An example of distribution approximation using Generative Adversarial 
    Networks in TensorFlow.
   
reference: 
  - paper: https://arxiv.org/abs/1406.2661
  - blog-1: http://blog.aylien.com/introduction-generative-adversarial-networks
 -code-tensorflow/
  - blog-2: http://blog.evjang.com/2016/06/generative-adversarial-nets-in.html

"""

import argparse
import os

import numpy as np
import tensorflow as tf
from six.moves import xrange

import gan
from mlpy.lib import utils
from mlpy.lib.utils import data

seed = 42
np.random.seed(seed)
tf.set_random_seed(seed)


def train(params):
    if not os.path.exists(params.out_root):
        os.makedirs(params.out_root)
    model = gan.GAN(params.g_hidden_units, params.d_hidden_units,
                    params.g_learning_rate, params.d_learning_rate,
                    params.optimizer_type)
    distr_x = data.DataDistribution()
    distr_z = data.GeneratorDistribution(range=8)

    with tf.Session() as session:
        # initialization
        tf.local_variables_initializer().run()
        tf.global_variables_initializer().run()

        d_losses = []
        g_losses = []
        for step in xrange(params.train_steps):
            # update discriminator
            d_loss = np.inf
            for id in xrange(params.d_steps):
                x = distr_x.sample(params.batch_size)
                z = distr_z.sample(params.batch_size)
                d_loss, _ = session.run(
                    [model.d_loss, model.d_opt],
                    feed_dict={
                        model.x: np.reshape(x, [params.batch_size, 1]),
                        model.z: np.reshape(z, [params.batch_size, 1])})
            # update generator
            g_loss = np.inf
            for ig in xrange(params.g_steps):
                z = distr_z.sample(params.batch_size)
                g_loss, _ = session.run(
                    [model.g_loss, model.g_opt],
                    feed_dict={
                        model.z: np.reshape(z, [params.batch_size, 1])})
            # log
            if step % params.print_freq == 0:
                print("step:{}, d_loss={:.4f}, g_loss={:.4f}".format(
                    step, d_loss, g_loss
                ))
                d_losses.append(d_loss)
                g_losses.append(g_loss)

            if step % params.plot_freq == 0 or step == params.train_steps - 1:
                db, hist_x, hist_g = utils.sample(model, session, distr_x,
                                                  distr_z,
                                                  params.batch_size)
                utils.plot_distribution(db, hist_x, hist_g, distr_z.range,
                                        os.path.join(params.out_root,
                                                     '{}'.format(
                                                         str(step).zfill(4))))
        # plot loss
        d_losses = np.array(d_losses)
        g_losses = np.array(g_losses)
        utils.plot_loss(d_losses, g_losses, params.out_root)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--train_steps', type=int, default=5000,
                        help='the training step of whole process.')
    parser.add_argument('--g_steps', type=int, default=1,
                        help='the training step of generator in each epoch.')
    parser.add_argument('--d_steps', type=int, default=1,
                        help='the training step of discriminator in each epoch.')
    parser.add_argument('--g_learning_rate', type=float, default=0.001,
                        help='the learning rate of generator.')
    parser.add_argument('--d_learning_rate', type=float, default=0.001,
                        help='the learning rate of discriminator.')
    parser.add_argument('--g_hidden_units', type=int, default=4,
                        help='the hidden units of generator.')
    parser.add_argument('--d_hidden_units', type=int, default=8,
                        help='the hidden units of discriminator.')
    parser.add_argument('--optimizer_type', type=str, default='Adam',
                        help='choose the optimizer. '
                             'Adam or Decay. Default is Adam')
    parser.add_argument('--batch_size', type=int, default=8,
                        help='the batch size')
    parser.add_argument('--print_freq', type=int, default=10,
                        help='the print frequency.')
    parser.add_argument('--plot_freq', type=int, default=500,
                        help='the plot frequency.')
    parser.add_argument('--out_root', type=str,
                        default='./density_gan',
                        help='the root directory of output.')

    return parser.parse_args()


if __name__ == '__main__':
    params = parse_args()
    train(params)
