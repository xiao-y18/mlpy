import numpy as np

# if you want to know why sample data by the following way refer blog-2.


class DataDistribution(object):
    def __init__(self, mu=4.0, sigma=0.5):
        self.mu = mu
        self.sigma = sigma

    def sample(self, batch_size):
        data = np.random.normal(self.mu, self.sigma, batch_size)
        data.sort()
        return data


class GeneratorDistribution(object):
    def __init__(self, range):
        self.range = range

    def sample(self, batch_size):
        return np.linspace(-self.range, self.range, batch_size) + \
               np.random.random(batch_size) * 0.01