import tensorflow as tf


def log(x):
    """safe log to avoid underflow"""
    return tf.log(tf.maximum(x, 1e-5))


def optimizer(loss, var_list, learning_rate=0.001):
    step = tf.Variable(0, trainable=False)
    with tf.name_scope('optimizer'):
        opt = tf.train.AdamOptimizer(learning_rate=learning_rate) \
            .minimize(loss=loss, global_step=step, var_list=var_list)
    return opt


def optimizer_decay(loss, var_list, init_learning_rate=0.005, decay=0.95,
                    num_decay_steps=150):
    batch = tf.Variable(0)
    learning_rate = tf.train.exponential_decay(
        init_learning_rate,
        batch,
        num_decay_steps,
        decay,
        staircase=True
    )
    opt = tf.train.GradientDescentOptimizer(learning_rate).minimize(
        loss,
        global_step=batch,
        var_list=var_list
    )
    return opt


class GAN(object):
    def __init__(self, g_hidden_units, d_hidden_units,
                 g_learning_rate, d_learning_rate,
                 optimizer_type):
        # set up generator
        with tf.variable_scope('G'):
            self.z = tf.placeholder(tf.float32, [None, 1])
            self.G = self.generator(self.z, 1, g_hidden_units)

        # set up discriminator
        self.x = tf.placeholder(tf.float32, [None, 1])
        with tf.variable_scope('D'):
            self.DX = self.discriminator(self.x, d_hidden_units)
        with tf.variable_scope('D', reuse=True):
            self.DZ = self.discriminator(self.G, d_hidden_units)

        # calculate loss
        self.d_loss = tf.reduce_mean(-log(self.DX) - log(1 - self.DZ))
        self.g_loss = tf.reduce_mean(-log(self.DZ))

        # get trainable variables
        vars = tf.trainable_variables()
        self.d_vars = [v for v in vars if v.name.startswith('D/')]
        self.g_vars = [v for v in vars if v.name.startswith('G/')]

        # optimize
        if optimizer_type == 'Decay':
            self.d_opt = optimizer_decay(self.d_loss, self.d_vars)
            self.g_opt = optimizer_decay(self.g_loss, self.g_vars)
        else:  # optimizer_type == 'Adam' or others
            self.d_opt = optimizer(self.d_loss, self.d_vars, d_learning_rate)
            self.g_opt = optimizer(self.g_loss, self.g_vars, g_learning_rate)

    @staticmethod
    def generator(in_z, out_dim, hidden_units):
        h0 = tf.layers.dense(in_z, hidden_units, tf.nn.relu, name='generator_0')
        h1 = tf.layers.dense(h0, out_dim, name='generator_1')
        return h1

    @staticmethod
    def discriminator(in_x, hidden_units):
        h0 = tf.layers.dense(in_x, hidden_units, tf.nn.tanh,
                             name='discriminator_0')
        h1 = tf.layers.dense(h0, hidden_units, tf.nn.tanh,
                             name='discriminator_1')
        h2 = tf.layers.dense(h1, hidden_units, tf.nn.tanh,
                             name='discriminator_2')
        prob = tf.layers.dense(h2, 1, tf.nn.sigmoid,
                               name='discriminator_3')
        return prob
