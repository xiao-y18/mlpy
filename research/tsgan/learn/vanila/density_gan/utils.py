import numpy as np
import matplotlib.pyplot as plt
from matplotlib import animation
from six.moves import xrange
import os


def sample(model, session, distr_x, distr_z, batch_size, num_points=10000,
           num_bins=100):
    """
    
    :param model: 
    :param session: 
    :param distr_x: 
    :param distr_z: 
    :param batch_size: 
    :param num_points: 
    :param num_bins: 
    :return: tuple (db, hist_x, hist_g)
        db: is the current decision boundary 
        hist_x: is the histogram of data distribution
        hist_g: is the histogram of generator distribution
    """
    sample_range = distr_z.range
    xs = np.linspace(-sample_range, sample_range, num_points)
    bins = np.linspace(-sample_range, sample_range, num_bins)

    # db
    db = np.zeros([num_points, 1])
    for i in xrange(num_points // batch_size):
        db[batch_size * i: batch_size * (i + 1)] = session.run(
            model.DX,
            feed_dict={
                model.x: np.reshape(xs[batch_size * i: batch_size * (i + 1)],
                                    [batch_size, 1])
            }
        )

    # hist_x
    data = distr_x.sample(num_points)
    hist_x, _ = np.histogram(data, bins=bins, density=True)

    # hist_g
    zs = np.linspace(-sample_range, sample_range, num_points)
    g = np.zeros([num_points, 1])
    for i in xrange(num_points // batch_size):
        g[batch_size * i: batch_size * (i + 1)] = session.run(
            model.G,
            feed_dict={
                model.z: np.reshape(zs[batch_size * i: batch_size * (i + 1)],
                                    [batch_size, 1])
            }
        )
    hist_g, _ = np.histogram(g, bins=bins, density=True)

    return db, hist_x, hist_g


def plot_distribution(db, hist_x, hist_g, sample_range, out_path):
    """
    
    :param db: 
    :param hist_x: 
    :param hist_g: 
    :param sample_range: 
    :param out_path: 
    :return: 
    """
    dx = np.linspace(-sample_range, sample_range, len(db))
    x = np.linspace(-sample_range, sample_range, len(hist_x))
    plt.figure()
    plt.plot(dx, db, label='decision boundary')
    plt.ylim(0, 1)
    plt.plot(x, hist_x, label='real data')
    plt.plot(x, hist_g, label='generated data')
    plt.title('1-D GAN')
    plt.legend(loc='best')
    plt.xlabel('Data values')
    plt.ylabel('Probability density')
    plt.savefig(out_path)
    plt.close()


def plot_loss(g_losses, d_losses, out_root):
    """
    
    :param g_losses: 
    :param d_losses: 
    :param out_root: 
    :return: 
    """
    plt.figure()
    plt.plot(g_losses, label='g_loss')
    plt.plot(d_losses, label='d_loss')
    plt.xlabel('step')
    plt.ylabel('loss')
    plt.legend(loc='best')
    plt.savefig(os.path.join(out_root, 'loss.png'))
    plt.close()


def save_animation(anim_frames, out_root, sample_range):
    """
    
    :param anim_frames: 
    :param out_root: 
    :param sample_range: 
    :return: 
    """
    f = plt.figure()
    plt.title('1D GAN')
    plt.xlabel('Data values')
    plt.ylabel('Probability density')
    plt.xlim(-6, 6)
    plt.ylim(0, 1.4)

    line_db, = plt.plot([], [], label='descision boundary')
    line_hist_x, = plt.plot([], [], label='real data')
    line_hist_g, = plt.plot([], [], label='generated data')

    frame_number = plt.text(
        0.02,
        0.95,
        ''
    )

    plt.legend(loc='best')

    db, hist_x, _ = anim_frames[0]
    db_x = np.linspace(-sample_range, sample_range, len(db))
    x = np.linspace(-sample_range, sample_range, len(hist_x))

    def init():
        line_db.set_data([], [])
        line_hist_x.set_data([], [])
        line_hist_g.set_data([], [])
        frame_number.set_text('')
        return line_db, line_hist_x, line_hist_g, frame_number

    def animate(i):
        frame_number.set_text(
            'Frame: {}/{}'.format(i, len(anim_frames))
        )
        db, hist_x, hist_g = anim_frames[i]
        line_db.set_data(db_x, db)
        line_hist_x.set_data(x, hist_x)
        line_hist_g.set_data(x, hist_g)
        return line_db, line_hist_x, line_hist_g, frame_number

    anim = animation.FuncAnimation(
        f,
        animate,
        init_func=init,
        frames=len(anim_frames),
        blit=True
    )
    anim.save(os.path.join(out_root, 'anim.mp4'), fps=30)
