"""
Learn from the official tutorial: https://www.tensorflow.org/tutorials/generative/autoencoder.
This tutorial introduces autoencoders with three examples: the basics, image denoising, and anomaly detection.

TODO:
- [ ] follow the recommended materials in the end of this tutorial to learn more about anomaly detection with AE.

"""

import os
from abc import ABC

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

from sklearn.metrics import accuracy_score, precision_score, recall_score
from sklearn.model_selection import train_test_split
from tensorflow.keras import layers, losses
from tensorflow.keras.models import Model

from mlpy.configure import DIR_DATASET
from mlpy.data import ucr
from research.tsgan.lib.utils import makedirs, tag_path, set_logging
from research.tsgan import configure
from mlpy.data.image import fashion_mnist

""" the basic autoencoder """


class AutoEncoder(Model, ABC):
    def __init__(self, input_shape, latent_dim):
        super(AutoEncoder, self).__init__()
        self.inp_shape = input_shape
        self.latent_dim = latent_dim

        self.encoder = None
        self.decoder = None
        self.build_model(self.inp_shape)

    def build_model(self, input_shape):
        self.encoder = tf.keras.Sequential([
            layers.Flatten(),
            layers.Dense(self.latent_dim, activation='relu'),
        ])

        dense_dim = 1
        for s in input_shape:
            dense_dim *= s
        self.decoder = tf.keras.Sequential([
            layers.Dense(dense_dim, activation='sigmoid'),  # 784 = 28*28
            layers.Reshape(input_shape)
        ])

    def call(self, inputs, training=None, mask=None):
        encoded = self.encoder(inputs)
        decoded = self.decoder(encoded)
        return decoded


def run_auto_encoder(log_dir):
    # To start, you will train the basic autoencoder using the Fashon MNIST dataset.
    # Each image in this dataset is 28x28 pixels.
    (x_train, _), (x_test, _) = fashion_mnist.load_data(DIR_DATASET)

    x_train = x_train.astype('float32') / 255.
    x_test = x_test.astype('float32') / 255.

    # Define an autoencoder with two Dense layers: an encoder, which compresses the images into a 64 dimensional latent
    # vector, and a decoder, that reconstructs the original image from the latent space.
    latent_dim = 64
    input_shape = x_train.shape[1:]
    ae = AutoEncoder(input_shape, latent_dim)
    ae.compile(optimizer='adam', loss=losses.MeanSquaredError())

    # Train the model using x_train as both the input and the target. The encoder will learn to compress the dataset
    # from784 dimensions to the latent space, and the decoder will learn to reconstruct the original images.
    ae.fit(x_train, x_train,
           epochs=10,
           shuffle=True,
           validation_data=(x_test, x_test))

    # Now that the model is trained, let's test it by encoding and decoding images from the test set.
    encoded_imgs = ae.encoder(x_test).numpy()
    decoded_imgs = ae.decoder(encoded_imgs).numpy()

    n = 10
    plt.figure(figsize=(20, 4))
    for i in range(n):
        # display original
        ax = plt.subplot(2, n, i + 1)
        plt.imshow(x_test[i])
        plt.title("original")
        plt.gray()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)

        # display reconstruction
        ax = plt.subplot(2, n, i + 1 + n)
        plt.imshow(decoded_imgs[i])
        plt.title("reconstructed")
        plt.gray()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
    plt.savefig(os.path.join(log_dir, 'ae_results.png'))
    plt.close()


""" autoencoder for image denoising """


class AEDenoise(Model):
    def __init__(self, inp_shape):
        assert len(inp_shape) == 3, "input_shape should be a 3D tuple."
        super(AEDenoise, self).__init__()

        self.inp_shape = inp_shape
        self.encoder, self.decoder = self.build_model(self.inp_shape)

    @staticmethod
    def build_model(input_shape):
        encoder = tf.keras.Sequential([
            layers.Input(shape=input_shape),
            layers.Conv2D(16, (3, 3), activation='relu', padding='same', strides=2),
            layers.Conv2D(8, (3, 3), activation='relu', padding='same', strides=2)])
        # input: (None, 28, 28, 1)
        # (None, 14, 14, 16)
        # (None, 7, 7, 8)

        decoder = tf.keras.Sequential([
            layers.Input(shape=encoder.layers[-1].output.shape[1:]),
            layers.Conv2DTranspose(8, kernel_size=3, strides=2, activation='relu', padding='same'),
            layers.Conv2DTranspose(16, kernel_size=3, strides=2, activation='relu', padding='same'),
            layers.Conv2D(1, kernel_size=(3, 3), activation='sigmoid', padding='same')])
        # input: (None, 7, 7, 8)
        # (None, 14, 14, 8)
        # (None, 28, 28, 16)
        # (None, 28, 28, 1)

        return encoder, decoder

    def call(self, inputs, training=None, mask=None):
        encoded = self.encoder(inputs)
        decoded = self.decoder(encoded)
        return decoded


def run_ae_for_denoising(log_dir):
    # To start, you will train the basic autoencoder using the Fashon MNIST dataset.
    # Each image in this dataset is 28x28 pixels.
    (x_train, _), (x_test, _) = fashion_mnist.load_data(DIR_DATASET)

    x_train = x_train.astype('float32') / 255.
    x_test = x_test.astype('float32') / 255.

    # An autoencoder can also be trained to remove noise from images. In the following section, you will create a noisy
    # version of the Fashion MNIST dataset by applying random noise to each image. You will then train an autoencoder
    # using the noisy image as input, and the original image as the target.
    # Let's reimport the dataset to omit the modifications made earlier.
    x_train = x_train[..., tf.newaxis]
    x_test = x_test[..., tf.newaxis]
    print(x_train.shape)

    # Adding random noise to the images
    noise_factor = 0.2
    x_train_noisy = x_train + noise_factor * tf.random.normal(shape=x_train.shape)
    x_test_noisy = x_test + noise_factor * tf.random.normal(shape=x_test.shape)

    x_train_noisy = tf.clip_by_value(x_train_noisy, clip_value_min=0., clip_value_max=1.)
    x_test_noisy = tf.clip_by_value(x_test_noisy, clip_value_min=0., clip_value_max=1.)

    # Plot the noisy images.
    n = 10
    plt.figure(figsize=(20, 2))
    for i in range(n):
        ax = plt.subplot(1, n, i + 1)
        plt.title("original + noise")
        plt.imshow(tf.squeeze(x_test_noisy[i]))
        plt.gray()
    plt.savefig(os.path.join(log_dir, 'ae_denoise_noisy_images.png'))
    plt.close()

    aed = AEDenoise(x_train.shape[1:])
    aed.compile(optimizer='adam', loss=losses.MeanSquaredError())
    from IPython import embed; embed()
    aed.fit(x_train_noisy, x_train,
            epochs=10,
            shuffle=True,
            validation_data=(x_test_noisy, x_test))

    # Let's take a look at a summary of the encoder. Notice how the images are downsampled from 28x28 to 7x7.
    aed.encoder.summary()
    # The decoder upsamples the images back from 7x7 to 28x28.
    aed.decoder.summary()

    # Plotting both the noisy images and the denoised images produced by the autoencoder.
    encoded_imgs = aed.encoder(x_test).numpy()
    decoded_imgs = aed.decoder(encoded_imgs).numpy()

    n = 10
    plt.figure(figsize=(20, 4))
    for i in range(n):
        # display original + noise
        ax = plt.subplot(2, n, i + 1)
        plt.title("original + noise")
        plt.imshow(tf.squeeze(x_test_noisy[i]))
        plt.gray()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)

        # display reconstruction
        bx = plt.subplot(2, n, i + n + 1)
        plt.title("reconstructed")
        plt.imshow(tf.squeeze(decoded_imgs[i]))
        plt.gray()
        bx.get_xaxis().set_visible(False)
        bx.get_yaxis().set_visible(False)
    plt.savefig(os.path.join(log_dir, 'ae_denoise_denoised_results.png'))
    plt.close()


""" autoencoder for anomaly detection """
"""
    In this example, you will train an autoencoder to detect anomalies on the ECG5000 dataset. This dataset contains 
    5,000 Electrocardiograms, each with 140 data points. You will use a simplified version of the dataset, where each 
    example has been labeled either 0 (corresponding to an abnormal rhythm), or 1 (corresponding to a normal rhythm). 
    You are interested in identifying the abnormal rhythms.

    Note: This is a labeled dataset, so you could phrase this as a supervised learning problem. The goal of this example 
    is to illustrate anomaly detection concepts you can apply to larger datasets, where you do not have labels available 
    (for example, if you had many thousands of normal rhythms, and only a small number of abnormal rhythms).

    How will you detect anomalies using an autoencoder? Recall that an autoencoder is trained to minimize reconstruction 
    error. You will train an autoencoder on the normal rhythms only, then use it to reconstruct all the data. 
    Our hypothesis is that the abnormal rhythms will have higher reconstruction error. You will then classify a rhythm 
    as an anomaly if the reconstruction error surpasses a fixed threshold.
"""


class AEAnomalyDetector(Model):
    def __init__(self):
        super(AEAnomalyDetector, self).__init__()
        self.encoder = tf.keras.Sequential([
            layers.Dense(32, activation="relu"),
            layers.Dense(16, activation="relu"),
            layers.Dense(8, activation="relu")])

        self.decoder = tf.keras.Sequential([
            layers.Dense(16, activation="relu"),
            layers.Dense(32, activation="relu"),
            layers.Dense(140, activation="sigmoid")])

    def call(self, x):
        encoded = self.encoder(x)
        decoded = self.decoder(encoded)
        return decoded


def run_ae_for_ad(log_dir):
    data_name = 'ECG5000'
    data_dir = os.path.join(DIR_DATASET, 'UCR_TS_Archive_2015')
    x_train, y_train, x_test, y_test, n_classes = ucr.load_ucr_flat(
        data_name, data_dir)

    data = np.vstack([x_train, x_test])
    labels = np.hstack([y_train, y_test])
    train_data, test_data, train_labels, test_labels = train_test_split(
        data, labels, test_size=0.2, random_state=21
    )

    # Normalize the data to [0,1].
    min_val = tf.reduce_min(train_data)
    max_val = tf.reduce_max(train_data)
    train_data = (train_data - min_val) / (max_val - min_val)
    test_data = (test_data - min_val) / (max_val - min_val)
    train_data = tf.cast(train_data, tf.float32)
    test_data = tf.cast(test_data, tf.float32)

    # You will train the autoencoder using only the normal rhythms, which are labeled in this dataset as 1.
    # Separate the normal rhythms from the abnormal rhythms.
    train_labels = train_labels.astype(bool)
    test_labels = test_labels.astype(bool)

    normal_train_data = train_data[train_labels]
    normal_test_data = test_data[test_labels]
    print("normal_train_data: ", normal_train_data.shape)
    print("normal_test_data: ", normal_test_data.shape)

    anomalous_train_data = train_data[~train_labels]
    anomalous_test_data = test_data[~test_labels]
    print("anomalous_train_data", anomalous_train_data.shape)
    print("anomalous_test_data", anomalous_test_data.shape)

    # Plot a normal ECG.
    plt.grid()
    plt.plot(np.arange(140), normal_train_data[0])
    plt.title("A Normal ECG")
    plt.savefig(os.path.join(log_dir, 'ad_normal_data.png'))
    plt.close()

    # Plot an anomalous ECG.
    plt.grid()
    plt.plot(np.arange(140), anomalous_train_data[0])
    plt.title("An Anomalous ECG")
    plt.savefig(os.path.join(log_dir, 'ad_anomalous_data.png'))
    plt.close()

    # build model and train
    ae = AEAnomalyDetector()
    ae.compile(optimizer='adam', loss='mae')
    # Notice that the autoencoder is trained using only the normal ECGs, but is evaluated using the full test set.
    history = ae.fit(normal_train_data, normal_train_data,
                     epochs=20,
                     batch_size=512,
                     validation_data=(test_data, test_data),
                     shuffle=True)
    plt.plot(history.history["loss"], label="Training Loss")
    plt.plot(history.history["val_loss"], label="Validation Loss")
    plt.legend()
    plt.savefig(os.path.join(log_dir, 'ad_losses.png'))
    plt.close()

    # You will soon classify an ECG as anomalous if the reconstruction error is greater than one standard deviation
    # from the normal training examples. First, let's plot a normal ECG from the training set, the reconstruction after
    # it's encoded and decoded by the autoencoder, and the reconstruction error.
    encoded_imgs = ae.encoder(normal_test_data).numpy()
    decoded_imgs = ae.decoder(encoded_imgs).numpy()
    plt.plot(normal_test_data[0], 'b')
    plt.plot(decoded_imgs[0], 'r')
    plt.fill_between(np.arange(140), decoded_imgs[0], normal_test_data[0], color='lightcoral')
    plt.legend(labels=["Input", "Reconstruction", "Error"])
    plt.savefig(os.path.join(log_dir, 'ad_ae_normal_test.png'))
    plt.close()

    # Create a similar plot, this time for an anomalous test example.
    encoded_imgs = ae.encoder(anomalous_test_data).numpy()
    decoded_imgs = ae.decoder(encoded_imgs).numpy()
    plt.plot(anomalous_test_data[0], 'b')
    plt.plot(decoded_imgs[0], 'r')
    plt.fill_between(np.arange(140), decoded_imgs[0], anomalous_test_data[0], color='lightcoral')
    plt.legend(labels=["Input", "Reconstruction", "Error"])
    plt.savefig(os.path.join(log_dir, 'ad_ae_anomalous_test.png'))
    plt.close()

    # Detect anomalies
    # Detect anomalies by calculating whether the reconstruction loss is greater than a fixed threshold. In this
    # tutorial, you will calculate the mean average error for normal examples from the training set, then classify
    # future examples as anomalous if the reconstruction error is higher than one standard deviation from the training
    # set.

    # Plot the reconstruction error on normal ECGs from the training set.
    reconstructions = ae.predict(normal_train_data)
    train_loss = tf.keras.losses.mae(reconstructions, normal_train_data)
    plt.hist(train_loss, bins=50)
    plt.xlabel("Train loss")
    plt.ylabel("No of examples")
    plt.savefig(os.path.join(log_dir, 'ad_scores_of_normal_train_data.png'))
    plt.close()

    # Choose a threshold value that is one standard deviations above the mean.
    threshold = np.mean(train_loss) + np.std(train_loss)
    print("Threshold: ", threshold)
    # Note: There are other strategies you could use to select a threshold value above which test examples should be
    # classified as anomalous, the correct approach will depend on your dataset. You can learn more with the links at
    # the end of this tutorial.
    #
    # If you examine the reconstruction error for the anomalous examples in the test set, you'll notice most have
    # greater reconstruction error than the threshold. By varing the threshold, you can adjust the precision and recall
    # of your classifier.
    reconstructions = ae.predict(anomalous_test_data)
    test_loss = tf.keras.losses.mae(reconstructions, anomalous_test_data)
    plt.hist(test_loss, bins=50)
    plt.xlabel("Test loss")
    plt.ylabel("No of examples")
    plt.savefig(os.path.join(log_dir, 'ad_scores_of_anomalous_test_data.png'))
    plt.close()

    # Classify an ECG as an anomaly if the reconstruction error is greater than the threshold.
    def predict(model, data, threshold):
        reconstructions = model(data)
        loss = tf.keras.losses.mae(reconstructions, data)
        return tf.math.less(loss, threshold)

    def print_stats(preds, labels):
        print("Accuracy = {}".format(accuracy_score(labels, preds)))
        print("Precision = {}".format(precision_score(labels, preds)))
        print("Recall = {}".format(recall_score(labels, preds)))

    preds = predict(ae, test_data, threshold)
    print_stats(preds, test_labels)
    """ outputs
    Threshold:  0.043003365
    Accuracy = 0.919
    Precision = 0.9574468085106383
    Recall = 0.8470588235294118
    """


if __name__ == '__main__':
    tag = tag_path(os.path.abspath(__file__), 2)
    log_dir = makedirs(os.path.join(configure.DIR_LOG, tag))
    logger = set_logging(tag, log_dir)

    # run_auto_encoder(log_dir)
    run_ae_for_denoising(log_dir)
    # run_ae_for_ad(log_dir)
