"""
Learn from the official tutorial: https://www.tensorflow.org/tutorials/generative/cvae.
    This notebook demonstrates how train a Variational Autoencoder (VAE) (1, 2). on the MNIST dataset. A VAE is a
    probabilistic take on the autoencoder, a model which takes high dimensional input data and compresses it into a
    smaller representation. Unlike a traditional autoencoder, which maps the input onto a latent vector, a VAE maps the
    input data into the parameters of a probability distribution, such as the mean and variance of a Gaussian. This
    approach produces a continuous, structured latent space, which is useful for image generation.

"""

import os
import glob
import imageio
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
import tensorflow_probability as tfp
import time

from mlpy.configure import DIR_DATASET
from research.tsgan.lib.utils import makedirs, tag_path, set_logging
from research.tsgan import configure


def preprocess_images(images):
    images = images.reshape((images.shape[0], 28, 28, 1)) / 255.
    return np.where(images > .5, 1.0, 0.0).astype('float32')


class CVAEModel(tf.keras.Model):
    """Convolutional variational autoencoder."""

    def __init__(self, latent_dim):
        super(CVAEModel, self).__init__()
        self.latent_dim = latent_dim
        self.encoder = tf.keras.Sequential(
            [
                tf.keras.layers.InputLayer(input_shape=(28, 28, 1)),  # a fixed input
                tf.keras.layers.Conv2D(
                    filters=32, kernel_size=3, strides=(2, 2), activation='relu'),
                tf.keras.layers.Conv2D(
                    filters=64, kernel_size=3, strides=(2, 2), activation='relu'),
                tf.keras.layers.Flatten(),
                # No activation
                tf.keras.layers.Dense(latent_dim + latent_dim),
            ]
        )

        self.decoder = tf.keras.Sequential(
            [
                tf.keras.layers.InputLayer(input_shape=(latent_dim,)),
                tf.keras.layers.Dense(units=7 * 7 * 32, activation=tf.nn.relu),
                tf.keras.layers.Reshape(target_shape=(7, 7, 32)),
                tf.keras.layers.Conv2DTranspose(
                    filters=64, kernel_size=3, strides=2, padding='same',
                    activation='relu'),
                tf.keras.layers.Conv2DTranspose(
                    filters=32, kernel_size=3, strides=2, padding='same',
                    activation='relu'),
                # No activation
                tf.keras.layers.Conv2DTranspose(
                    filters=1, kernel_size=3, strides=1, padding='same'),
            ]
        )

    @tf.function
    def sample(self, eps=None):
        if eps is None:
            eps = tf.random.normal(shape=(100, self.latent_dim))
        return self.decode(eps, apply_sigmoid=True)

    def encode(self, x):
        mean, logvar = tf.split(self.encoder(x), num_or_size_splits=2, axis=1)
        return mean, logvar

    def reparameterize(self, mean, logvar):
        eps = tf.random.normal(shape=mean.shape)
        return eps * tf.exp(logvar * .5) + mean

    def decode(self, z, apply_sigmoid=False):
        logits = self.decoder(z)
        if apply_sigmoid:
            probs = tf.sigmoid(logits)
            return probs
        return logits


class CVAE(object):
    def __init__(self, log_dir):
        self.log_dir = log_dir
        self.train_dir = makedirs(os.path.join(self.log_dir, 'training'))
        # set the dimensionality of the latent space to a plane for visualization later
        self.latent_dim = 2
        self.num_examples_to_generate = 16

        # keeping the random vector constant for generation (prediction) so it will be easier to see the improvement.
        self.random_vector_for_generation = tf.random.normal(shape=[self.num_examples_to_generate, self.latent_dim])

        self.model = CVAEModel(self.latent_dim)
        self.optimizer = tf.keras.optimizers.Adam(1e-4)

    def fit(self, train_dataset, test_dataset, batch_size, epochs=10):
        # Pick a sample of the test set for generating output images
        assert batch_size >= self.num_examples_to_generate
        for test_batch in test_dataset.take(1):
            test_sample = test_batch[0:self.num_examples_to_generate, :, :, :]
        self.generate_and_save_images(self.model, 0, test_sample, self.train_dir)

        for epoch in range(1, epochs + 1):
            start_time = time.time()
            for train_x in train_dataset:
                self.train_step(self.model, train_x, self.optimizer)
            end_time = time.time()

            loss = tf.keras.metrics.Mean()
            for test_x in test_dataset:
                loss(self.compute_loss(self.model, test_x))
            elbo = -loss.result()
            print('Epoch: {}, Test set ELBO: {}, time elapse for current epoch: {}'
                  .format(epoch, elbo, end_time - start_time))
            self.generate_and_save_images(self.model, epoch, test_sample,self.train_dir)

        self.generate_and_save_gif(self.train_dir, self.log_dir)
        self.plot_latent_images(self.model, 20, self.log_dir)

    @staticmethod
    def log_normal_pdf(sample, mean, logvar, raxis=1):
        log2pi = tf.math.log(2. * np.pi)
        return tf.reduce_sum(-.5 * ((sample - mean) ** 2. * tf.exp(-logvar) + logvar + log2pi), axis=raxis)

    def compute_loss(self, model, x):
        mean, logvar = model.encode(x)
        z = model.reparameterize(mean, logvar)
        x_logit = model.decode(z)
        cross_ent = tf.nn.sigmoid_cross_entropy_with_logits(logits=x_logit, labels=x)
        logpx_z = -tf.reduce_sum(cross_ent, axis=[1, 2, 3])
        logpz = self.log_normal_pdf(z, 0., 0.)
        logqz_x = self.log_normal_pdf(z, mean, logvar)
        return -tf.reduce_mean(logpx_z + logpz - logqz_x)

    @tf.function
    def train_step(self, model, x, optimizer):
        """Executes one training step and returns the loss.

        This function computes the loss and gradients, and uses the latter to
        update the model's parameters.
        """
        with tf.GradientTape() as tape:
            loss = self.compute_loss(model, x)
        gradients = tape.gradient(loss, model.trainable_variables)
        optimizer.apply_gradients(zip(gradients, model.trainable_variables))

    @staticmethod
    def generate_and_save_images(model, epoch, test_sample, log_dir):
        mean, logvar = model.encode(test_sample)
        z = model.reparameterize(mean, logvar)
        predictions = model.sample(z)
        fig = plt.figure(figsize=(4, 4))

        for i in range(predictions.shape[0]):
            plt.subplot(4, 4, i + 1)
            plt.imshow(predictions[i, :, :, 0], cmap='gray')
            plt.axis('off')

        # tight_layout minimizes the overlap between 2 sub-plots
        plt.savefig(os.path.join(log_dir, 'image_at_epoch_{:04d}.png'.format(epoch)))
        plt.close()

    @staticmethod
    def generate_and_save_gif(in_dir, out_dir):
        anim_file = 'cvae.gif'
        with imageio.get_writer(os.path.join(out_dir, anim_file), mode='I') as writer:
            filenames = glob.glob(os.path.join(in_dir, 'image*.png'))
            filenames = sorted(filenames)
            for filename in filenames:
                image = imageio.imread(filename)
                writer.append_data(image)
            image = imageio.imread(filename)
            writer.append_data(image)

    @staticmethod
    def plot_latent_images(model, n, log_dir, digit_size=28):
        """Plots n x n digit images decoded from the latent space."""

        norm = tfp.distributions.Normal(0, 1)
        grid_x = norm.quantile(np.linspace(0.05, 0.95, n))
        grid_y = norm.quantile(np.linspace(0.05, 0.95, n))
        image_width = digit_size * n
        image_height = image_width
        image = np.zeros((image_height, image_width))

        for i, yi in enumerate(grid_x):
            for j, xi in enumerate(grid_y):
                z = np.array([[xi, yi]])
                x_decoded = model.sample(z)
                digit = tf.reshape(x_decoded[0], (digit_size, digit_size))
                image[i * digit_size: (i + 1) * digit_size,
                j * digit_size: (j + 1) * digit_size] = digit.numpy()

        plt.figure(figsize=(10, 10))
        plt.imshow(image, cmap='Greys_r')
        plt.axis('Off')
        plt.savefig(os.path.join(log_dir, 'latent_space.png'))
        plt.close()


if __name__ == '__main__':
    tag = tag_path(os.path.abspath(__file__), 2)
    log_dir = makedirs(os.path.join(configure.DIR_LOG, tag))
    logger = set_logging(tag, log_dir)

    # Load the MNIST dataset
    # Each MNIST image is originally a vector of 784 integers, each of which is between 0-255 and represents the
    # intensity of a pixel. We model each pixel with a Bernoulli distribution in our model, and we statically binarize
    # the dataset.
    (train_images, _), (test_images, _) = tf.keras.datasets.mnist.load_data(os.path.join(DIR_DATASET,
                                                                                         'image', 'mnist.npz'))
    train_images = preprocess_images(train_images)
    test_images = preprocess_images(test_images)
    print(train_images.shape)
    print(test_images.shape)

    train_size = train_images.shape[0]
    test_size = test_images.shape[0]
    batch_size = 32

    # Use tf.data to batch and shuffle the data
    train_dataset = (tf.data.Dataset.from_tensor_slices(train_images)
                     .shuffle(train_size).batch(batch_size))
    test_dataset = (tf.data.Dataset.from_tensor_slices(test_images)
                    .shuffle(test_size).batch(batch_size))

    cvae = CVAE(log_dir)
    cvae.fit(train_dataset, test_dataset, batch_size)
