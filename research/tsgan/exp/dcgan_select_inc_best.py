import os

from research.tsgan.lib.utils import makedirs, tag_path, set_logging, tf_keras_set_gpu_allow_growth
tf, K = tf_keras_set_gpu_allow_growth()

import fire
import numpy as np
import json
from sklearn.model_selection import train_test_split

from research.tsgan.configure import DIR_LOG, DIR_DATA_UCR15
from research.tsgan.data import ucr
from research.tsgan.model.dcgan.config import Configure
from research.tsgan.model.dcgan import DCGAN
from research.tsgan.exp.base import get_data_name_list, reduce_results

DIR_BEST_SETTING = os.path.join(DIR_LOG, 'exp_dcgan_select_inc')


def run_a_dataset(data_name, log_dir, tag, data_dir):
    tf.keras.backend.clear_session()
    logger = set_logging("{}_{}".format(tag, data_name), log_dir)

    x_train, y_train, x_test, y_test, n_classes = ucr.load_ucr_flat(data_name, data_dir, one_hot=True)
    x_train = x_train[..., np.newaxis]
    x_test = x_test[..., np.newaxis]

    # TODO: here, load best setting to train
    with open(os.path.join(DIR_BEST_SETTING, data_name, 'select_best_cfg.json')) as f:
        cfg_args = json.load(f)

    cfg = Configure(log_dir, logger, x_train.shape[1:], **cfg_args)
    dcgan = DCGAN(cfg)
    # all data can be used in unsupervised learning
    x_all = np.vstack([x_train, x_test])
    y_all = np.vstack([y_train, y_test])
    # Note: do not fix random_state for multiple runs. check the model's stability
    x_tr_gan, x_te_gan = train_test_split(x_all, test_size=0.1, stratify=y_all.argmax(axis=1))
    dcgan.fit(x_tr_gan, x_te_gan)

    # evaluate
    epoch = dcgan.load_ckpt()
    res = {'data_name': data_name}
    metrics_unsupervised = dcgan.eval(x_test, epoch='latest_ckpt')
    res.update(metrics_unsupervised)
    metrics_supervised = dcgan.eval_clf(x_train, y_train, x_test, y_test, n_classes, epoch='latest_ckpt')
    res.update(metrics_supervised)


def run_batch_datasets(log_dir, tag, data_dir, data_name_list):
    logger = set_logging(tag, log_dir)
    for data_name in data_name_list:
        logger.info(f"****** process dataset {data_name}")
        _log_dir = makedirs(os.path.join(log_dir, data_name))
        run_a_dataset(data_name, _log_dir, tag, data_dir)
    reduce_results(log_dir, data_name_list)


def run(i_run=0):
    log_dir_run = makedirs(os.path.join(log_dir, str(i_run)))
    run_batch_datasets(log_dir_run, tag, DIR_DATA_UCR15, data_name_list)


if __name__ == '__main__':
    tag = tag_path(os.path.abspath(__file__), 2)
    log_dir = makedirs(os.path.join(DIR_LOG, tag))
    logger = set_logging(tag, log_dir)
    data_name_list = get_data_name_list()
    data_name_list.remove('FordA')

    fire.Fire(run)






