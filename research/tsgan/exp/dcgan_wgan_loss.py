import os

from research.tsgan.lib.utils import makedirs, tag_path, set_logging, tf_keras_set_gpu_allow_growth
tf, K = tf_keras_set_gpu_allow_growth()

import fire
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split

from research.tsgan.configure import DIR_LOG, DIR_DATA_UCR15
from research.tsgan.data import ucr
from research.tsgan.model.dcgan.config import Configure
from research.tsgan.model.dcgan import DCGAN, gan_accuracy
from research.tsgan.exp.base import get_data_name_list, reduce_results


from time import time
class DCGANNew(DCGAN):
    def __init__(self, cfg: Configure):
        super(DCGANNew, self).__init__(cfg)

    # Notice the use of `tf.function`
    # This annotation causes the function to be "compiled".
    @tf.function
    def train_step(self, samples, acc_pre=tf.constant(0.0, dtype=tf.float32)):
        noise = self.cfg.noise_sampler([self.cfg.batch_size, self.cfg.noise_dim])

        # Note:
        # GradientTape.gradient can only be called once on non-persistent tapes, therefore there are two tapes according
        # to generator and discriminator, respectively.
        # For more details about tf.GradientTape, please refer to the official tutorial:
        # https://www.tensorflow.org/api_docs/python/tf/GradientTape.
        with tf.GradientTape() as g_tape, tf.GradientTape() as d_tape:
            generated_samples = self.g(noise, training=True)

            real_logits = self.d(samples, training=True)
            fake_logits = self.d(generated_samples, training=True)

            # TODO: here, wgan loss
            d_loss = tf.reduce_mean(fake_logits) - tf.reduce_mean(real_logits)
            g_loss = -tf.reduce_mean(fake_logits)

        def _opt_g():
            g_grad = g_tape.gradient(g_loss, self.g.trainable_variables)
            self.g_opt.apply_gradients(zip(g_grad, self.g.trainable_variables))

        def _opt_d():
            d_grad = d_tape.gradient(d_loss, self.d.trainable_variables)
            self.d_opt.apply_gradients(zip(d_grad, self.d.trainable_variables))

        def _null():
            pass

        _opt_g()
        tf.cond(acc_pre <= self.cfg.acc_threshold_to_train_d, true_fn=_opt_d, false_fn=_null)

        acc = gan_accuracy(real_logits, fake_logits)

        return d_loss, g_loss, acc

    def fit(self, data, test_data=None, restore=False):
        self.cfg.logger.info("****** fit start ******")

        if test_data is None:
            test_data = data

        if restore:  # continue the last ckpt to train
            init_epoch = self.load_ckpt()
            self.cfg.logger.info(f"restore model from epoch-{init_epoch} and continue to train.")
            if init_epoch >= self.cfg.epochs:
                new_epochs = self.cfg.epochs + init_epoch
                self.cfg.logger.info(f"init_epoch={init_epoch} is grater than epochs={self.cfg.epochs}. "
                                     f"increase the upper bound to {new_epochs}")
                self.cfg.epochs = new_epochs
        else:
            self.cfg.logger.info("train from scratch.")
            self.cfg.clean_paths()
            init_epoch = 0

        # Batch and shuffle the data
        dataset = tf.data.Dataset.from_tensor_slices(data).shuffle(self.cfg.batch_size).batch(self.cfg.batch_size)

        def _get_file_name(ep):
            return os.path.join(self.cfg.train_dir, 'epoch_{:04d}.png'.format(ep))

        # take 1 batch of real samples to show
        real_samples = [e for e in dataset.take(1)][0]
        self.save_series(real_samples, os.path.join(self.cfg.train_dir, '000_real.png'))

        loss = {'epoch': [], 'batch': [], 'd_loss': [], 'g_loss': []}
        acc_pre = tf.constant(0.0, dtype=tf.float32)
        for epoch in range(init_epoch, self.cfg.epochs):
            t_start = time()
            for i, batch in dataset.enumerate():
                d_loss, g_loss, real_loss, fake_loss, acc = self.train_step(batch, acc_pre)
                acc_pre = acc
                loss['epoch'].append(epoch)
                loss['batch'].append(i.numpy())
                loss['d_loss'].append(d_loss.numpy())
                loss['g_loss'].append(g_loss.numpy())
            self.cfg.logger.info(f"epoch[{epoch}/{self.cfg.epochs}], "
                                 f"d_loss={d_loss:.4}, g_loss={g_loss:.4}, acc={acc:.4}, time={(time() - t_start):.4}")
            # save the model every 15 epochs
            if (epoch + 1) % self.cfg.n_epochs_to_save_ckpt == 0:
                self.ckpt_manager.save(checkpoint_number=epoch)
                # self.ckpt.save(file_prefix=self.cfg.ckpt_prefix)
                self.generate(self.cfg.noise_seed, _get_file_name(epoch))  # produce samples
            if (epoch + 1) % self.cfg.n_epochs_to_evaluate == 0:
                self.eval(test_data, epoch)

        # final epoch
        self.ckpt_manager.save(checkpoint_number=self.cfg.epochs)
        # self.ckpt.save(file_prefix=self.cfg.ckpt_prefix)
        self.generate(self.cfg.noise_seed, _get_file_name(self.cfg.epochs))
        pd.DataFrame(loss).to_csv(os.path.join(self.cfg.train_dir, '000_loss.csv'), index=False)
        self.eval(test_data, self.cfg.epochs)

        self.cfg.logger.info("****** fit end ******")


def run_a_dataset(data_name, log_dir, tag, data_dir):
    tf.keras.backend.clear_session()
    logger = set_logging("{}_{}".format(tag, data_name), log_dir)

    x_train, y_train, x_test, y_test, n_classes = ucr.load_ucr_flat(data_name, data_dir, one_hot=True)
    x_train = x_train[..., np.newaxis]
    x_test = x_test[..., np.newaxis]

    cfg = Configure(log_dir, logger, x_train.shape[1:])
    dcgan = DCGAN(cfg)
    # all data can be used in unsupervised learning
    x_all = np.vstack([x_train, x_test])
    y_all = np.vstack([y_train, y_test])
    # x_tr_gan, x_te_gan = train_test_split(x_all, test_size=0.1, random_state=dcgan.cfg.seed,
    #                                       stratify=y_all.argmax(axis=1))
    # Note: do not fix random_state for multiple runs. check the model's stability
    x_tr_gan, x_te_gan = train_test_split(x_all, test_size=0.1, stratify=y_all.argmax(axis=1))
    dcgan.fit(x_tr_gan, x_te_gan)

    # evaluate
    epoch = dcgan.load_ckpt()
    res = {'data_name': data_name}
    metrics_unsupervised = dcgan.eval(x_test, epoch='latest_ckpt')
    res.update(metrics_unsupervised)
    metrics_supervised = dcgan.eval_clf(x_train, y_train, x_test, y_test, n_classes, epoch='latest_ckpt')
    res.update(metrics_supervised)


def run_batch_datasets(log_dir, tag, data_dir, data_name_list):
    logger = set_logging(tag, log_dir)
    for data_name in data_name_list:
        logger.info(f"****** process dataset {data_name}")
        _log_dir = makedirs(os.path.join(log_dir, data_name))
        run_a_dataset(data_name, _log_dir, tag, data_dir)
    reduce_results(log_dir, data_name_list)


def run(i_run=0):
    log_dir_run = makedirs(os.path.join(log_dir, str(i_run)))
    run_batch_datasets(log_dir_run, tag, DIR_DATA_UCR15, data_name_list)


if __name__ == '__main__':
    tag = tag_path(os.path.abspath(__file__), 2)
    log_dir = makedirs(os.path.join(DIR_LOG, tag))
    logger = set_logging(tag, log_dir)
    data_name_list = get_data_name_list()

    fire.Fire(run)





