import os

from research.tsgan.lib.utils import makedirs, tag_path, set_logging, tf_keras_set_gpu_allow_growth
tf, K = tf_keras_set_gpu_allow_growth()

import fire
import numpy as np
import json
from sklearn.model_selection import train_test_split

from research.tsgan.configure import DIR_LOG, DIR_DATA_UCR15
from research.tsgan.data import ucr
from research.tsgan.model.dcgan.config import Configure
from research.tsgan.model.dcgan import DCGAN
from research.tsgan.exp.base import get_data_name_list

from time import time
from research.tsgan.lib.metrics.mmd import mmd_score
class DCGANNew(DCGAN):
    def __init__(self, cfg: Configure):
        super(DCGANNew, self).__init__(cfg)

    def fit_select(self, data, test_data=None, restore=False):
        self.cfg.logger.info("****** fit_select start ******")

        if test_data is None:
            test_data = data

        if restore:  # continue the last ckpt to train
            init_epoch = self.load_ckpt()
            self.cfg.logger.info(f"restore model from epoch-{init_epoch} and continue to train.")
            if init_epoch >= self.cfg.epochs:
                new_epochs = self.cfg.epochs + init_epoch
                self.cfg.logger.info(f"init_epoch={init_epoch} is grater than epochs={self.cfg.epochs}. "
                                     f"increase the upper bound to {new_epochs}")
                self.cfg.epochs = new_epochs
        else:
            self.cfg.logger.info("train from scratch.")
            self.cfg.clean_paths()
            init_epoch = 0

        # Batch and shuffle the data
        dataset = tf.data.Dataset.from_tensor_slices(data).shuffle(self.cfg.batch_size).batch(self.cfg.batch_size)

        acc_pre = tf.constant(0.0, dtype=tf.float32)
        for epoch in range(init_epoch, self.cfg.epochs):
            t_start = time()
            for i, batch in dataset.enumerate():
                d_loss, g_loss, real_loss, fake_loss, acc = self.train_step(batch, acc_pre)
                acc_pre = acc
            self.cfg.logger.info(f"epoch[{epoch}/{self.cfg.epochs}], "
                                 f"d_loss={d_loss:.4}, g_loss={g_loss:.4}, real_loss={real_loss:.4}, "
                                 f"fake_loss={fake_loss:.4}, acc={acc:.4}, time={(time() - t_start):.4}")

        # scores to measure similarity
        t0 = time()
        real_data, fake_data, real_y, fake_y = self.generate_real_fake_data(test_data)
        mmd = mmd_score(fake_data.squeeze(), real_data.squeeze())
        self.cfg.logger.info("mmd, time={}".format(time()-t0))
        self.cfg.logger.info("****** fit_select end ******")
        return mmd

    def fit(self, data, test_data=None, restore=False):
        raise NotImplementedError("This function is not available.")


def run_a_dataset(data_name, log_dir, tag, data_dir):
    tf.keras.backend.clear_session()
    logger = set_logging("{}_{}".format(tag, data_name), log_dir)

    x_train, y_train, x_test, y_test, n_classes = ucr.load_ucr_flat(data_name, data_dir, one_hot=True)
    x_train = x_train[..., np.newaxis]
    x_test = x_test[..., np.newaxis]

    # all data can be used in unsupervised learning
    cfg = Configure(log_dir, logger, x_train.shape[1:])
    x_all = np.vstack([x_train, x_test])
    y_all = np.vstack([y_train, y_test])
    x_tr_gan, x_te_gan = train_test_split(x_all, test_size=0.1, random_state=cfg.seed, stratify=y_all.argmax(axis=1))

    # TODO: each param is selected independently on the base of the default settings.
    params = dict(
        n_layers=[1, 2, 3, 4, 5],  # DCGAN for image just use 4 layers at most, I needn't to use such deep network.
        g_norm_method=['batch', 'layer', 'instance'],
        d_norm_method=['batch', 'layer', 'instance'],
        d_dropout_rate=[0.0, 0.2, 0.3, 0.5, 0.7, 0.8],  # 0.0 means without Dropout.
        noise_method=['normal', 'uniform'],
        noise_dim=[100, 0.2, 0.5, 1.0, 1.5, 1.8, 2.0],  # default setting, 100, should be included.
        acc_threshold_to_train_d=[1.0, 0.8, 0.7, 0.6, 0.5]  # 1.0 means D will always be trained.
    )
    n_runs = 5

    def _select(param_name, param_values, cfg_param):
        best_mmd = np.inf
        best_val = None
        detail = {'val': [], 'mmd': []}
        for val in param_values:
            args = {**cfg_param, **{f'{param_name}': val}}
            cfg = Configure(log_dir, logger, x_train.shape[1:], **args)
            dcgan = DCGANNew(cfg)
            mmds = []
            for i in range(n_runs):
                mmd = np.float(dcgan.fit_select(x_tr_gan, x_te_gan))
                mmds.append(mmd)
            mmd = np.mean(mmds)
            detail['val'].append(val)
            detail['mmd'].append(mmd)
            logger.info(f"n_layers={val}, mmd={mmd:.4}")
            if mmd < best_mmd:
                best_mmd = mmd
                best_val = val
        return {f'{param_name}': best_val}, best_mmd, detail

    logger.info(f"****** start to select parameters: ")
    # Notes:
    # - verbose=0, discard log details.
    cfg_param = {'verbose': 0}
    cfg_param_best = {}
    # cfg_param = {'epochs': 1, 'verbose': 0}  # TODO: for test
    for name, values in params.items():
        best_param, best_mmd, detail = _select(name, values, cfg_param)
        cfg_param_best.update(best_param)  # use best settings for next train.
        logger.info(f'best_param:{json.dumps(best_param)}, best_mmd:{best_mmd:.4}')
        with open(os.path.join(log_dir, 'select_details.txt'), 'a') as f:
            f.write(f"best_param:{json.dumps(best_param)}, best_mmd:{best_mmd:.4}, details:{json.dumps(detail)}\n")
    with open(os.path.join(log_dir, 'select_best_cfg.json'), 'a') as f:
        f.write(json.dumps(cfg_param_best) + "\n")


def run_batch_datasets(log_dir, tag, data_dir, data_name_list):
    logger = set_logging(tag, log_dir)
    for data_name in data_name_list:
        logger.info(f"****** process dataset {data_name}")
        _log_dir = makedirs(os.path.join(log_dir, data_name))
        run_a_dataset(data_name, _log_dir, tag, data_dir)
    # reduce_results(log_dir, data_name_list)


def run_unit_test(i_run=0):
    log_dir_run = makedirs(os.path.join(log_dir, str(i_run)))
    data_name_list = get_data_name_list()
    data_name_list = data_name_list[:1]
    run_batch_datasets(log_dir_run, tag, DIR_DATA_UCR15, data_name_list)


def run(i_run=0):
    # this number should be adjusted to meet practical needs.
    # Here, I split datasets to 5 gpus each of which according to one run enumerated from 0 to 4.
    bath_size = 4
    data_name_list = get_data_name_list()
    data_name_list = data_name_list[i_run*bath_size:(i_run+1)*bath_size]
    run_batch_datasets(log_dir, tag, DIR_DATA_UCR15, data_name_list)


if __name__ == '__main__':
    tag = tag_path(os.path.abspath(__file__), 2)
    log_dir = makedirs(os.path.join(DIR_LOG, tag))
    logger = set_logging(tag, log_dir)

    fire.Fire(run)





