import os

from research.tsgan.lib.utils import makedirs, tag_path, set_logging, tf_keras_set_gpu_allow_growth
tf, K = tf_keras_set_gpu_allow_growth()

import fire

from research.tsgan.configure import DIR_LOG, DIR_DATA_UCR15
from research.tsgan.exp.base import get_data_name_list, Experiment

from research.tsgan.model.dcgan_tune.tune_train22 import DCGANTuneTrain
from research.tsgan.model.dcgan_tune.config import Configure


def run():
    tag = tag_path(os.path.abspath(__file__), 2)
    log_dir = makedirs(os.path.join(DIR_LOG, tag))
    data_name_list = get_data_name_list()

    cfg_kwargs = dict(acc_threshold_to_train_d=1.0)  # train both D and G once in each training step.
    exp = Experiment(tag, DCGANTuneTrain, Configure, DIR_DATA_UCR15, data_name_list, log_dir, **cfg_kwargs)

    fire.Fire(exp.run)


if __name__ == '__main__':
    run()






