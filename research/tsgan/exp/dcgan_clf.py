import os

from research.tsgan.lib.utils import makedirs, tag_path, set_logging, tf_keras_set_gpu_allow_growth

tf, K = tf_keras_set_gpu_allow_growth()

import fire
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.svm import LinearSVC
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from time import time
import json
import pandas as pd

from mlpy.data import ucr
from mlpy.data import ts

from research.tsgan.configure import DIR_LOG, DIR_DATA_UCR15
from research.tsgan.model.dcgan.dcgan import DCGAN
from research.tsgan.model.dcgan.config import Configure
from research.tsgan.exp.base import get_data_name_list, Experiment

""" TODO: xx
- different level in a conv layer: conv, norm, relu, dropout
    - Note: just reuse conv because not all layers have norm, relu and dropout
    - fixed: softmax, no pool
- different types of pools: '', max, avg
    - the best level of a conv layer, softmax, 
+ norm/not, relu/leakyrelu/not, 
- for the best pooling method: different pooling windows, stridess
    - the best level of a conv layer, the best pooling layer
- different types of classifiers
    - the best settings of the above.
- train the GAN.d from scratch
    - conduct clf directly. + sigmoid layer -> softmax layer.
"""


class Classifier(object):
    def __init__(self, gan, cfg, data_name, x_tr, y_tr, x_te, y_te, n_classes):
        self.gan = gan
        self.cfg = cfg
        self.logger = self.cfg.logger
        self.log_dir = makedirs(os.path.join(self.cfg.log_dir, 'clf'))
        # self.log_dir = self.cfg.eval_dir

        self.data_name = data_name
        self.x_tr = x_tr
        self.y_tr_onehot = y_tr
        self.y_tr = np.argmax(self.y_tr_onehot, axis=1)
        self.x_te = x_te
        self.y_te_onehot = y_te
        self.y_te = np.argmax(self.y_te_onehot, axis=1)
        self.n_classes = n_classes

    def run(self):
        switch = 'diff_layers'

        clf_model_names = ['softmax', 'lr', 'lsvc', 'knn', 'mlp']

        # reuse conv layers because not all layers have norm, relu and dropout
        layers = self.get_conv_layers('conv', 'conv')

        # ===================================================
        # try different types of pooling
        # ===================================================
        if switch == 'diff_pools':
            pools = ['', 'max-2-2', 'avg-2-2']
            # init result
            res = {'type': []}
            for n in clf_model_names:
                res[n] = []
            for pool in pools:
                name_prefix = f'pool={pool}'
                # concat all layers
                extractor = self.get_extractor_concat_layers(layers, pool=pool)
                res_ = self.clf(extractor)
                res['type'].append(f'{name_prefix}')
                for key in clf_model_names:
                    res[key].append(res_[key])
            res = pd.DataFrame(res)
            res.to_csv(os.path.join(self.log_dir, 'res_diff_pools.csv'), index=False)

        # ===================================================
        # try different layers
        # ===================================================
        elif switch == 'diff_layers':
            res = {'type': []}
            for n in clf_model_names:
                res[n] = []
            # for each layers
            pool = 'max-2-2'
            for i, l in enumerate(layers):
                extractor = self.get_extractor_a_layer(l, pool=pool)
                res_ = self.clf(extractor)
                res['type'].append(f'layer-{i}')
                for key in clf_model_names:
                    res[key].append(res_[key])
            # concat all layers
            extractor = self.get_extractor_concat_layers(layers, pool=pool)
            res_ = self.clf(extractor)
            res['type'].append('concat_all')
            for key in clf_model_names:
                res[key].append(res_[key])
            res = pd.DataFrame(res)
            res.to_csv(os.path.join(self.log_dir, 'clf_diff_layers.csv'), index=False)

        # ===================================================
        # apply norm or not, TODO
        # ===================================================
        elif switch == 'diff_norm':
            norms = ['None'] + ts.TS_NORMALIZE_TYPES
            res = {'type': []}
            for n in clf_model_names:
                res[n] = []
            pool = 'max-2-2'
            for norm in norms:
                # concat all layers
                extractor = self.get_extractor_concat_layers(layers, pool=pool)
                res_ = self.clf(extractor, norm=norm)
                res['type'].append(f'norm-{norm}')
                for key in clf_model_names:
                    res[key].append(res_[key])
            res = pd.DataFrame(res)
            res.to_csv(os.path.join(self.log_dir, 'res_diff_norms.csv'), index=False)

        # ===================================================
        # apply activation function, TODO
        # ===================================================
        elif switch == 'diff_act':
            acts = ['elu', 'prelu', 'relu', 'lrelu', 'trelu']
            res = {'type': []}
            for n in clf_model_names:
                res[n] = []
            pool = 'max-2-2'
            for act in acts:
                # concat all layers
                extractor = self.get_extractor_concat_layers(layers, pool=pool)
                res_ = self.clf(extractor, act=act)
                res['type'].append(f'norm-{act}')
                for key in clf_model_names:
                    res[key].append(res_[key])
            res = pd.DataFrame(res)
            res.to_csv(os.path.join(self.log_dir, 'res_diff_acts.csv'), index=False)

    def get_conv_layers(self, start='conv', end='dropout'):
        layers = []
        for l in self.gan.d.layers:
            name = l.name
            if name.startswith(start) and name.endswith(end):
                layers.append(l)
        self.logger.info(f'get {len(layers)} layers')
        return layers

    def get_pool_layer(self, pool='max-2-2'):
        if pool.startswith('max'):
            _, pool_size, strides = pool.split('-')
            pool_size = int(pool_size)
            strides = int(strides)
            return tf.keras.layers.MaxPool1D(pool_size=pool_size, strides=strides)
        elif pool.startswith('avg'):
            _, pool_size, strides = pool.split('-')
            pool_size = int(pool_size)
            strides = int(strides)
            return tf.keras.layers.AvgPool1D(pool_size=pool_size, strides=strides)
        else:
            raise ValueError(f"pool={pool} can not be found!")

    def get_extractor_a_layer(self, layer, pool=''):
        h = layer.output
        if pool != '':
            pool_layer = self.get_pool_layer(pool)
            h = pool_layer(h)
        # add new layers
        flat = tf.keras.layers.Flatten()(h)
        extractor = tf.keras.models.Model(inputs=self.gan.d.input, outputs=flat)
        for l in extractor.layers:  # exclude the added layers
            l.trainable = False

        return extractor

    def get_extractor_concat_layers(self, layers, pool=''):
        flat_outputs = []
        for l in layers:
            h = l.output
            if pool != '':
                pool_layer = self.get_pool_layer(pool)
                h = pool_layer(h)
            flat = tf.keras.layers.Flatten()(h)
            flat_outputs.append(flat)
        concat = tf.keras.layers.Concatenate()(flat_outputs)
        extractor = tf.keras.models.Model(inputs=self.gan.d.input, outputs=concat)
        for l in extractor.layers:  # exclude the added layers
            l.trainable = False

        return extractor

    def extract(self, extractor, x):
        batch_size = self.cfg.batch_size
        features = []
        n_samples = x.shape[0]
        for i in range(n_samples // batch_size):
            feat = extractor(x[i * batch_size:(i + 1) * batch_size]).numpy()
            features.append(feat)
        residual = n_samples % batch_size
        if residual > 0:
            feat = extractor(x[-residual:])
            features.append(feat)
        features = np.vstack(features)

        # features = ts.normalize(features, 'znorm')

        return features

    def clf(self, extractor, act=None, norm='znorm'):
        feat_tr = self.extract(extractor, self.x_tr)
        feat_te = self.extract(extractor, self.x_te)

        # ===================================================
        # pre-processing
        # ===================================================
        # apply activation
        if act is not None:
            if act == 'elu':
                act_layer = tf.keras.layers.ELU()
            elif act == 'prelu':
                act_layer = tf.keras.layers.PReLU()
            elif act == 'relu':
                act_layer = tf.keras.layers.ReLU()
            elif act == 'lrelu':
                act_layer = tf.keras.layers.LeakyReLU()
            elif act == 'trelu':
                act_layer = tf.keras.layers.ThresholdedReLU()
            else:
                raise ValueError(f"act={act} can not be found!")
            feat_tr = act_layer(feat_tr)
            feat_te = act_layer(feat_te)
        # apply normalization
        if norm is not None:
            feat_tr = ts.normalize(feat_tr, norm)
            feat_te = ts.normalize(feat_te, norm)

        # ===================================================
        # conduct clf with different models
        # ===================================================
        softmax = tf.keras.Sequential(tf.keras.layers.Dense(self.n_classes,
                                                            input_shape=feat_tr.shape[1:],
                                                            activation='softmax'))
        softmax.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
        softmax.fit(feat_tr, self.y_tr_onehot, batch_size=self.cfg.batch_size, epochs=self.cfg.epochs, verbose=0)
        softmax_acc = softmax.evaluate(feat_te, self.y_te_onehot)[1]

        lr = LogisticRegression(multi_class='auto', max_iter=1000)
        lr.fit(feat_tr, self.y_tr)
        lr_acc = accuracy_score(self.y_te, lr.predict(feat_te))

        lsvc = LinearSVC(max_iter=1000)
        lsvc.fit(feat_tr, self.y_tr)
        lsvc_acc = accuracy_score(self.y_te, lsvc.predict(feat_te))

        knn = KNeighborsClassifier(n_neighbors=1)
        knn.fit(feat_tr, self.y_tr)
        knn_acc = accuracy_score(self.y_te, knn.predict(feat_te))

        mlp = MLPClassifier()
        mlp.fit(feat_tr, self.y_tr)
        mlp_acc = accuracy_score(self.y_te, mlp.predict(feat_te))

        res = {'softmax': softmax_acc, 'lr': lr_acc, 'lsvc': lsvc_acc, 'knn': knn_acc, 'mlp': mlp_acc}

        return res


class ExperimentClf(Experiment):
    def __init__(self, tag, model_obj, cfg_obj, data_dir, data_name_list, log_dir, use_testset=True, **cfg_kwargs):
        super(ExperimentClf, self).__init__(tag, model_obj, cfg_obj, data_dir, data_name_list, log_dir, use_testset,
                                            **cfg_kwargs)

    def run_a_dataset(self, data_name, log_dir):
        tf.keras.backend.clear_session()
        logger = set_logging("{}_{}".format(self.tag, data_name), log_dir)

        x_train, y_train, x_test, y_test, n_classes = ucr.load_ucr_flat(data_name, self.data_dir, one_hot=True)
        x_train = x_train[..., np.newaxis]
        x_test = x_test[..., np.newaxis]

        cfg = self.cfg_obj(log_dir, logger, x_train.shape[1:], **self.cfg_kwargs)
        model = self.model_obj(cfg)
        epoch = model.load_ckpt()

        clf = Classifier(model, cfg, data_name, x_train, y_train, x_test, y_test, n_classes)
        clf.run()

    def reduce(self, log_dir):
        pass


def reduce_clf_results(log_dir, n_runs=5):
    data_name_list = get_data_name_list()
    experiment = 'res_diff_pools'
    df_agg = None
    for i_run in range(n_runs):
        df_list = []
        for data_name in data_name_list:
            df = pd.read_csv(os.path.join(log_dir, str(i_run), data_name, 'clf', f'{experiment}.csv'))
            df['dataset'] = [data_name] * df.shape[0]
            df_list.append(df)
        df = pd.concat(df_list, ignore_index=True)
        df.set_index(['type', 'dataset'], inplace=True)
        df = df.unstack(['type'])
        df.to_csv(os.path.join(log_dir, str(i_run), f'{experiment}.csv'))
        if i_run == 0:
            df_agg = df
        else:
            df_agg = df_agg + df
    df_agg = df_agg / n_runs
    df_agg.to_csv(os.path.join(log_dir, f'{experiment}_mean.csv'))


if __name__ == '__main__':
    tag = tag_path(os.path.abspath(__file__), 2)
    log_dir = makedirs(os.path.join(DIR_LOG, tag))
    logger = set_logging(tag, log_dir)
    data_name_list = get_data_name_list()

    switch = 'clf'

    if switch == 'fit':
        exp = Experiment(tag, DCGAN, Configure, DIR_DATA_UCR15, data_name_list, log_dir)
        fire.Fire(exp.run)
    elif switch == 'clf':
        clf = ExperimentClf(tag, DCGAN, Configure, DIR_DATA_UCR15, data_name_list, log_dir)
        fire.Fire(clf.run)
    else:
        raise ValueError(f'switch={switch} can not be found!')

