import os

from research.tsgan.lib.utils import makedirs, tag_path, set_logging, tf_keras_set_gpu_allow_growth
tf, K = tf_keras_set_gpu_allow_growth()

import fire

from research.tsgan.configure import DIR_LOG, DIR_DATA_UCR15
from research.tsgan.model.dcgan.dcgan import DCGAN
from research.tsgan.model.dcgan.config import Configure
from research.tsgan.exp.base import get_data_name_list, Experiment


if __name__ == '__main__':
    tag = tag_path(os.path.abspath(__file__), 2)
    log_dir = makedirs(os.path.join(DIR_LOG, tag))
    logger = set_logging(tag, log_dir)
    data_name_list = get_data_name_list()

    exp = Experiment(tag, DCGAN, Configure, DIR_DATA_UCR15, data_name_list, log_dir)

    fire.Fire(exp.run)





