import os
import numpy as np
import json
import pandas as pd

import tensorflow as tf

from sklearn.model_selection import train_test_split

from research.tsgan.data import ucr
from research.tsgan.lib.utils import makedirs, set_logging
from research.tsgan.configure import DIR_DATA_UCR15


def random_a_subset_of_ucr15(n):
    """
    ['SonyAIBORobotSurface', 'yoga', 'InsectWingbeatSound', 'FISH', 'Lighting7', 'ProximalPhalanxOutlineAgeGroup',
    'MiddlePhalanxOutlineCorrect', 'BeetleFly', 'ECGFiveDays', 'WordsSynonyms']
    """
    np.random.seed(18)
    datasets = os.listdir(DIR_DATA_UCR15)
    n_datasets = len(datasets)
    inds = np.arange(n_datasets)
    np.random.shuffle(inds)
    subsets = [datasets[i] for i in inds[:n]]
    return subsets


def get_data_name_list():
    # my previous TSGAN archived good performances on these 10 datasets
    data_name_list = ['50words', 'ECG5000', 'ArrowHead', 'ChlorineConcentration', 'NonInvasiveFatalECG_Thorax2',
                      'SonyAIBORobotSurfaceII', 'FISH', 'DiatomSizeReduction', 'Strawberry', 'Two_Patterns']
    # random another 10 datasets for test.
    data_name_list_random = ['SonyAIBORobotSurface', 'yoga', 'InsectWingbeatSound', 'FordA', 'Lighting7',
                             'ProximalPhalanxOutlineAgeGroup', 'MiddlePhalanxOutlineCorrect', 'BeetleFly',
                             'ECGFiveDays', 'WordsSynonyms']  # change 'FISH' to 'FordA'
    data_name_list = data_name_list + data_name_list_random
    return data_name_list


"""
    experimental framework
"""


class Experiment(object):
    def __init__(self, tag, model_obj, cfg_obj, data_dir, data_name_list, log_dir, use_testset=True, **cfg_kwargs):
        self.tag = tag
        self.model_obj = model_obj
        self.cfg_obj = cfg_obj
        self.data_dir = data_dir
        self.data_name_list = data_name_list
        self.cfg_kwargs = cfg_kwargs
        self.log_dir = log_dir
        self.use_testset = use_testset

    def run_a_dataset(self, data_name, log_dir):
        tf.keras.backend.clear_session()
        logger = set_logging("{}_{}".format(self.tag, data_name), log_dir)

        x_train, y_train, x_test, y_test, n_classes = ucr.load_ucr_flat(data_name, self.data_dir, one_hot=True)
        x_train = x_train[..., np.newaxis]
        x_test = x_test[..., np.newaxis]

        cfg = self.cfg_obj(log_dir, logger, x_train.shape[1:], **self.cfg_kwargs)
        model = self.model_obj(cfg)
        if self.use_testset:
            # all data can be used in unsupervised learning
            x_all = np.vstack([x_train, x_test])
            y_all = np.vstack([y_train, y_test])
            # Note: I thought split should be fixed to insure fair comparisons among different methods.
            x_tr_gan, x_te_gan = train_test_split(x_all, test_size=0.1, random_state=model.cfg.seed,
                                                  stratify=y_all.argmax(axis=1))
            model.fit(x_tr_gan, x_te_gan)
        else:
            # Note: I did not make decision on test set and just use it for evaluation. Therefore, x_test can be fed
            # into the training process.
            model.fit(x_train, x_test)

        # evaluate
        epoch = model.load_ckpt()
        res = {'data_name': data_name}
        metrics_unsupervised = model.eval(x_test, epoch='latest_ckpt')
        res.update(metrics_unsupervised)
        metrics_supervised = model.eval_clf(x_train, y_train, x_test, y_test, n_classes, epoch='latest_ckpt')
        res.update(metrics_supervised)

    def run_batch_datasets(self, log_dir):
        logger = set_logging(self.tag, log_dir)
        for data_name in self.data_name_list:
            logger.info(f"****** process dataset {data_name}")
            _log_dir = makedirs(os.path.join(log_dir, data_name))
            self.run_a_dataset(data_name, _log_dir)
        # reduce_results(log_dir, self.data_name_list)
        self.reduce(log_dir)

    def run(self, i_run=0):
        log_dir_run = makedirs(os.path.join(self.log_dir, str(i_run)))
        self.run_batch_datasets(log_dir_run)

    def reduce(self, log_dir):
        json_files = ['similarity.json', 'clf_hidden.json']
        self.reduce_eval_results(log_dir, self.data_name_list, json_files)

    @staticmethod
    def reduce_eval_results(log_dir, data_name_list, json_files, out_fname='res'):
        res_list = []
        for data_name in data_name_list:
            res = {'data_name': data_name}
            for fname in json_files:
                with open(os.path.join(log_dir, data_name, 'evaluation', fname), 'r') as f:
                    last = f.readlines()[-1]
                    res.update(json.loads(last))
            res_list.append(res)

        df = {}
        if len(res_list) > 0:
            for key in res_list[0].keys():
                df[key] = []
            for r in res_list:
                for key in r.keys():
                    df[key].append(r[key])
        df = pd.DataFrame(df)
        df.to_csv(os.path.join(log_dir, f'{out_fname}.csv'), index=False)


class ExperimentCond(Experiment):
    def __init__(self, tag, model_obj, cfg_obj, data_dir, data_name_list, log_dir, **cfg_kwargs):
        # Note: CGAN uses a supervised learning, therefore, samples in test set can not be applied.
        super(ExperimentCond, self).__init__(tag, model_obj, cfg_obj, data_dir, data_name_list, log_dir,
                                             use_testset=False, **cfg_kwargs)

    def run_a_dataset(self, data_name, log_dir):
        tf.keras.backend.clear_session()
        logger = set_logging("{}_{}".format(self.tag, data_name), log_dir)

        x_train, y_train, x_test, y_test, n_classes = ucr.load_ucr_flat(data_name, self.data_dir, one_hot=True)
        x_train = x_train[..., np.newaxis]
        x_test = x_test[..., np.newaxis]

        input_shape = [x_train.shape[1:], y_train.shape[1:]]
        cfg = self.cfg_obj(log_dir, logger, input_shape, **self.cfg_kwargs)
        model = self.model_obj(cfg)
        model.fit((x_train, y_train), (x_test, y_test))

        # evaluate
        epoch = model.load_ckpt()
        res = {'data_name': data_name}
        metrics_unsupervised = model.eval((x_test, y_test), epoch=epoch)
        res.update(metrics_unsupervised)
        # Note: currently, clf isn't enabled.
        y_tr_fake = np.zeros_like(y_train)
        y_te_fake = np.zeros_like(y_test)
        metrics_clf_hidden = model.eval_clf([x_train, y_tr_fake], y_train, [x_test, y_te_fake], y_test, n_classes,
                                            epoch=epoch)
        res.update(metrics_clf_hidden)

        metrics_clf_data = model.eval_clf_data(x_train, y_train, x_test, y_test, n_classes, epoch=epoch)
        res.update(metrics_clf_data)

    def reduce(self, log_dir):
        json_files = ['similarity.json', 'clf_hidden.json', 'clf_dff_data.json']
        self.reduce_eval_results(log_dir, self.data_name_list, json_files, out_fname='res_clf')


class ExperimentAE(Experiment):
    def __init__(self, tag, model_obj, cfg_obj, data_dir, data_name_list, log_dir, **cfg_kwargs):
        # Note: CGAN uses a supervised learning, therefore, samples in test set can not be applied.
        super(ExperimentAE, self).__init__(tag, model_obj, cfg_obj, data_dir, data_name_list, log_dir, **cfg_kwargs)

    def run_a_dataset(self, data_name, log_dir):
        tf.keras.backend.clear_session()
        logger = set_logging("{}_{}".format(self.tag, data_name), log_dir)

        x_train, y_train, x_test, y_test, n_classes = ucr.load_ucr_flat(data_name, self.data_dir, one_hot=True)
        x_train = x_train[..., np.newaxis]
        x_test = x_test[..., np.newaxis]

        cfg = self.cfg_obj(log_dir, logger, x_train.shape[1:], **self.cfg_kwargs)
        model = self.model_obj(cfg)
        if self.use_testset:
            # all data can be used in unsupervised learning
            x_all = np.vstack([x_train, x_test])
            y_all = np.vstack([y_train, y_test])
            # Note: I thought split should be fixed to insure fair comparisons among different methods.
            x_tr_gan, x_te_gan = train_test_split(x_all, test_size=0.1, random_state=model.cfg.seed,
                                                  stratify=y_all.argmax(axis=1))
            model.fit(x_tr_gan, x_te_gan)
        else:
            # Note: I did not make decision on test set and just use it for evaluation. Therefore, x_test can be fed
            # into the training process.
            model.fit(x_train, x_test)

        # evaluate
        res = {'data_name': data_name}
        metrics_supervised = model.eval_clf(x_train, y_train, x_test, y_test, n_classes, epoch='latest_ckpt')
        res.update(metrics_supervised)

    def reduce(self, log_dir):
        json_files = ['clf_hidden.json']
        self.reduce_eval_results(log_dir, self.data_name_list, json_files, out_fname='res_clf')
