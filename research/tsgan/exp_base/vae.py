import os
import numpy as np

from sklearn.model_selection import train_test_split

from research.tsgan.lib.utils import makedirs, tag_path, set_logging, tf_keras_set_gpu_allow_growth
from research.tsgan.configure import DIR_LOG, DIR_DATA_UCR15
from research.tsgan.data import ucr

from research.tsgan.model.vae import CVAE
from research.tsgan.lib.utils import save_series
from research.tsgan.lib.metrics.clf import clf


tf_keras_set_gpu_allow_growth()


def run(data_name, log_dir, logger, tag, data_dir):
    logger.info(f"****** process dataset {data_name}")

    model_log_dir = makedirs(os.path.join(log_dir, data_name))
    model_logger = set_logging("{}_{}".format(tag, data_name), model_log_dir)

    x_train, y_train, x_test, y_test, n_classes = ucr.load_ucr_flat(data_name, data_dir, one_hot=True)
    x_train = x_train[..., np.newaxis]
    x_test = x_test[..., np.newaxis]  # test set can be used to train in unsupervised setting.

    # build model
    seed = 42
    batch_size = 16
    latent_dim = int(0.5*x_train.shape[1])
    epochs = 300
    input_shape = x_train.shape[1:]
    ae = CVAE(input_shape, log_dir)

    x_all = np.vstack([x_train, x_test])
    y_all = np.vstack([y_train, y_test])
    x_tr_gan, x_te_gan = train_test_split(x_all, test_size=0.1, random_state=seed, stratify=y_all.argmax(axis=1))
    ae.fit(x_tr_gan, x_te_gan, batch_size, epochs)

    # ae.fit(x_train, x_test, batch_size, epochs)

    # Now that the model is trained, let's test it by encoding and decoding images from the test set.
    mean, logvar = ae.model.encode(x_test)
    z = ae.model.reparameterize(mean, logvar)
    predictions = ae.model.sample(z)
    save_series(predictions, model_log_dir)

    acc_tr, acc_te = clf(ae.model.encoder, x_train, y_train, x_test, y_test, n_classes, epochs, batch_size,
                         idx_layer=-1, verbose=1)
    print(acc_tr, acc_te)


if __name__ == '__main__':
    tag = tag_path(os.path.abspath(__file__), 2)
    log_dir = makedirs(os.path.join(DIR_LOG, tag))
    logger = set_logging(tag, log_dir)

    data_name_list = ['50words', 'ECG5000', 'ArrowHead', 'ChlorineConcentration', 'NonInvasiveFatalECG_Thorax2',
                      'SonyAIBORobotSurfaceII', 'FISH', 'DiatomSizeReduction', 'Strawberry', 'Two_Patterns']
    data_name_list_random = ['SonyAIBORobotSurface', 'yoga', 'InsectWingbeatSound', 'FISH', 'Lighting7',
                             'ProximalPhalanxOutlineAgeGroup', 'MiddlePhalanxOutlineCorrect', 'BeetleFly',
                             'ECGFiveDays', 'WordsSynonyms']
    data_name_list = data_name_list + data_name_list_random
    data_name_list = data_name_list[:1]  # TODO: for test
    # data_name_list = data_name_list[:4]  # TODO: for test

    for data_name in data_name_list:
        run(data_name, log_dir, logger, tag, DIR_DATA_UCR15)